typedef struct
{
    int nbMaxLivraisons, multiplicateurTemps, port;
    char cheminIdentifiants[100];
} listeArgs;

int lireArgs(int argc, char* argv[], listeArgs* res)
{
    int i;
    char* temp;
    bool argsDejaPasses[4], verifTout;

    for (i = 0; i < 4; i++)
        argsDejaPasses[i] = false;

    if (argc == 2)
        if (strcmp(argv[1], "--help") == 0)
            return 2;
        
    for (i = 1; i < argc; i++)
    {
        if (argv[i][0] == '-')
        {
            switch (argv[i][1])
            {
                case 'm':
                    if (argsDejaPasses[0])
                    {
                        printf("-m est passé 2 fois\n");
                        return 1;
                    }
                    
                    if (i + 1 < argc)
                    {
                        res->nbMaxLivraisons = strtol(argv[i + 1], &temp, 0);
                        
                        if (argv[i + 1] == temp || *temp != '\0')
                        {
                            printf("-m doit être suivi d'un nombre\n");
                            return 1;
                        }

                        argsDejaPasses[0] = true;
                        i++;
                    }
                    else
                    {
                        printf("-m doit être suivi d'un nombre\n");
                        return 1;
                    }
                    break;
                
                case 't':
                    if (argsDejaPasses[1])
                    {
                        printf("-t est passé 2 fois\n");
                        return 1;
                    }
                    
                    if (i + 1 < argc)
                    {
                        res->multiplicateurTemps = strtol(argv[i + 1], &temp, 0);
                        
                        if (argv[i + 1] == temp || *temp != '\0')
                        {
                            printf("-t doit être suivi d'un nombre\n");
                            return 1;
                        }

                        argsDejaPasses[1] = true;
                        i++;
                    }
                    else
                    {
                        printf("-t doit être suivi d'un nombre\n");
                        return 1;
                    }
                    break;
                
                case 'p':
                    if (argsDejaPasses[2])
                    {
                        printf("-p est passé 2 fois\n");
                        return 1;
                    }
                    
                    if (i + 1 < argc)
                    {
                        res->port = strtol(argv[i + 1], &temp, 0);
                        
                        if (argv[i + 1] == temp || *temp != '\0')
                        {
                            printf("-p doit être suivi d'un nombre\n");
                            return 1;
                        }

                        argsDejaPasses[2] = true;
                        i++;
                    }
                    else
                    {
                        printf("-p doit être suivi d'un nombre\n");
                        return 1;
                    }
                    break;
                
                case 'i':
                    if (argsDejaPasses[3])
                    {
                        printf("-i est passé 2 fois\n");
                        return 1;
                    }
                    
                    if (i + 1 < argc)
                    {
                        if (strlen(argv[i + 1]) + 1 < 100)
                            strcpy(res->cheminIdentifiants, argv[i + 1]);
                        else
                        {
                            printf("le chemin vers les identifiants est trop long\n");
                            return 1;
                        }

                        argsDejaPasses[3] = true;
                        i++;
                    }
                    else
                    {
                        printf("-i doit être suivi d'un chemin\n");
                        return 1;
                    }
                    break;
                
                default:
                    printf("argument invalide \"%s\"\n", argv[i]);
                    return 1;
            }
        }
        else
        {
            printf("argument invalide \"%s\"\n", argv[i]);
            return 1;
        }
    }

    verifTout = true;
    for (i = 0; i < 4; i++)
        if (!argsDejaPasses[i])
        {
            if (verifTout)
                printf("Il manque le(s) champ(s): ");
            
            if (!verifTout)
                printf(", ");

            switch (i)
            {
                case 0:
                    printf("-m");
                    break;
                
                case 1:
                    printf("-t");
                    break;

                case 2:
                    printf("-p");
                    break;

                case 3:
                    printf("-i");
                    break;
            }

            verifTout = false;
        }
    
    if (verifTout == false)
    {
        printf("\n");
        return 1;
    }
    
    return 0;
}