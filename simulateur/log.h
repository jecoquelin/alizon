void logFdp(char* logChaine) {
    FILE* file;
    file = fopen("./log.txt", "a");
    fwrite(logChaine, 1, strlen(logChaine), file);
    fclose(file);
}

void logHeure()
{
    char tempsChaine[32];
    time_t tempsNb;
    time(&tempsNb);
    struct tm *temps = localtime(&tempsNb);
    sprintf(tempsChaine, "%02d:%02d:%02d %02d/%02d/%02d ", temps->tm_hour, temps->tm_min, temps->tm_sec, temps->tm_mday, temps->tm_mon + 1, temps->tm_year + 1900);
    logFdp(tempsChaine);
}

void logIP()
{
    logFdp(inet_ntoa(addrClient.sin_addr));
    logFdp(" ");
}