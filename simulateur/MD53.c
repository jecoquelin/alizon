#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

bool verifMdp(char*, char*);

int main(){
    printf("%d\n", verifMdp("prout", "8532054bba77e4d26caca1b1299cdb0c"));
    return 0;
}

bool verifMdp(char* mdp, char* hash){
    FILE *fp;
    char hashVrai[33];

    /* Open the command for reading. */
    char commande[80];
    sprintf(commande, "echo %s | md5sum | cut -d ' ' -f 1", mdp);
    
    fp = popen(commande, "r");
    if (fp == NULL) {
        printf("Failed to run command\n" );
        exit(1);
    }

    int i = 0;
    char res[33] = "";

    fgets(hashVrai, sizeof(hashVrai), fp);
    strcat(res, hashVrai);
    
    printf("%s\n", res);
    pclose(fp);

    if (strcmp(hashVrai, hash) == 0){
        return true;
    } else {
        return false;
    }
}