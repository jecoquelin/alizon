#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


// void md5(char message, char *hash){}
int main(int argc, char *argv[]){
    char message[1000] ;

    if (argc < 2){
        return 0 ;
    }
    else {
        strcpy(message, argv[1]) ;
    }

    float r[64], k[64] ;
    int i = 0 ;         // compteur
    float s = 0 ;   // calcul sinus
    int f, g ;

    // Définition de r
    // *r = {7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21} ;
    for (i = 0 ; i <= 15 ; i++){
        if (i%4 == 0){
            r[i] = 7 ;
        }
        else if (i%4 == 1) {
            r[i] = 12 ;
        }
        else if (i%4 == 2) {
            r[i] = 17 ;
        }
        else if (i%4 == 3) {
            r[i] = 22 ;
        }
    }
    
    for (i = 16 ; i <= 31 ; i++){
        if (i%4 == 0){
            r[i] = 5 ;
        }
        else if (i%4 == 1) {
            r[i] = 9 ;
        }
        else if (i%4 == 2) {
            r[i] = 14 ;
        }
        else if (i%4 == 3) {
            r[i] = 20 ;
        }
    }

    for (i = 32 ; i <= 47 ; i++){
        if (i%4 == 0){
            r[i] = 4 ;
        }
        else if (i%4 == 1) {
            r[i] = 11 ;
        }
        else if (i%4 == 2) {
            r[i] = 16 ;
        }
        else if (i%4 == 3) {
            r[i] = 23 ;
        }
    }

        for (i = 48 ; i <= 63 ; i++){
        if (i%4 == 0){
            r[i] = 6 ;
        }
        else if (i%4 == 1) {
            r[i] = 10 ;
        }
        else if (i%4 == 2) {
            r[i] = 15 ;
        }
        else if (i%4 == 3) {
            r[i] = 21 ;
        }
    }


    // MD5 utilise des sinus d'entiers pour ses constances
    for (i = 0 ; i<64 ; i++){
        
        s = sin(i + 1);
        // On récupère la valeur absolue
        if (s < 0) {
            s = s * (-1);
        }
        k[i] = floor(s * pow(2, 32)) ;
    }


    // Préparation des variable
    int h0 = 0x67452301;
    int h1 = 0xEFCDAB89;
    int h2 = 0x98BADCFE;
    int h3 = 0x10325476;


    // Préparation du message (padding)
    printf("%s\n", message);
    char *messageBinaire ;
    int tailleMessage = strlen(message);
    for(i=0;i<8*strlen(tailleMessage);i++)
        *messageBinaire << (0 != (message[i/8] & 1 << (~i&7)));
            

    // Il faut rajouter le bit 1 à droite du message
    strcpy(message, *message << 1) ;
    // Puis combler de 0 à droite également jusqu'à que la taille du message en bits soit égale à 448 (mod 512)
    

    while ( (strlen(message)*8)%512 != 448){
        strcpy(message, *message << 0);
    }
    // Enfin ajouter la taille du message initial codée (avant padding) au message
    // IMPORTANT
    
    // Découpage en blocs de 512 bits
    // IMPORTANT
    for (int j = 0 ; i < 512 ; j++){ // A revoir mais c'est juste pour pas qu'il y est d'erreur
        // subdiviser en 16 mots de 32 bits en little-endian w[i], 0 ≤ i ≤ 15
        // little-endian --> petit boutiste --> on lit les bits de droite à gauche

        // Initialiser les valeurs de hachage
        int a = h0 ;
        int b = h1 ;
        int c = h2 ;
        int d = h3 ;

        //Boucle principale
        int temp ;
        for (i = 0 ; i < 64 ; i++){
            if (0 <= i <= 15 ){
                f = (b & c) | ((~b) & d);
                g = i;
            }
            else if (16 <= i <= 31){
                f = (d & b) | ((~b) & c);
                g = (5*i +1)%16 ;
            }
            else if (32 <= i <= 47){
                f = b ^ c ^ d ;
                g = (3*i + 5)%16 ;
            }
            else if (48 <= i <= 63){
                f = c ^ (b | (~d));
                g = (7*i)%16 ;
            }

            temp = d ;
            d = c ;
            c = b ;
            // IMPORTANT
            b = 0 ; // leftrotate(a+f+k[i]+w[g], r[i])+b
            a = temp ;
        }

    // Ajouter le résultat au bloc précédent
    h0 = h0 + a ;
    h1 = h1 + b ;
    h2 = h2 + c ;
    h3 = h3 + d ;
    }

    // IMPORTANT
    int empreinte = h0 ; // h0 concaténer h1 concaténer h2 concaténer h3 en little-endian



/*  JEUX DE TEST
    for (i = 0 ; i<64 ; i++){
        printf("%d = %f\n", i, k[i]);
    }


    printf("h0 : %X", h0);
    printf("h1 : %X", h1);
    printf("h2 : %X", h2);
    printf("h3 : %X", h3);
    */
}


