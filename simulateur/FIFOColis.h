#define NB_JOURS_MAX 4

// TODO
// - afficher les erreurs dans ajouterColis, initEtatColis
// - s'occuper des types (parfois uint32_t et parfois int)
// - remplir contientColis et contient
//


typedef struct listeChainee
{
    struct listeChainee* suivant;
    uint32_t numeroColis;
} listeChainee;

typedef struct
{
    listeChainee* debut;
    listeChainee* fin;
} FIFOColis;

typedef struct
{
    FIFOColis* colisNBJoursRestants[NB_JOURS_MAX + 1];
    int taillesPiles[NB_JOURS_MAX + 1];
    int nbColisEnCours;
    int maxColisEnCours;
} etatColis;

void initFIFOColis(FIFOColis* f)
{
    f->debut = NULL;
    f->fin = NULL;
}

int ajouter(FIFOColis* f, uint32_t numeroColis)
{
    listeChainee* nouveau = malloc(sizeof(listeChainee));

    if (nouveau == NULL)
        return 1;

    nouveau->numeroColis = numeroColis;
    nouveau->suivant = NULL;

    if (f->fin != NULL)
    {
        f->fin->suivant = nouveau;
    }

    f->fin = nouveau;

    if (f->debut == NULL)
    {
        f->debut = nouveau;
    }

    return 0;
}

int enlever(FIFOColis* f)
{
    listeChainee* temp;
    int val;

    if (f->debut == NULL)
        return -1;

    val = f->debut->numeroColis;
    temp = f->debut->suivant;
    free(f->debut);
    f->debut = temp;

    if (f->debut == NULL)
    {
        f->fin = NULL;
    }

    return val;
}

void vider(FIFOColis* f)
{
    listeChainee* element = f->debut;
    listeChainee* temp;

    while (element != NULL)
    {
        temp = element;
        element = element->suivant;
        free(temp);
    }

    initFIFOColis(f);
}

void afficher(FIFOColis* f)
{
    listeChainee* element = f->debut;

    while (element != NULL)
    {
        printf("%d\n", element->numeroColis);
        element = element->suivant;
    }
}

bool contient(FIFOColis* f, uint32_t numeroColis)
{
    listeChainee* element = f->debut;

    while (element != NULL)
    {
        if (element->numeroColis == numeroColis)
            return true;

        element = element->suivant;
    }

    return false;
}

int initEtatColis(etatColis* etat, int maxColisEnCours)
{
    int i;

    for (i = 0; i < NB_JOURS_MAX + 1; i++)
    {
        etat->colisNBJoursRestants[i] = malloc(sizeof(FIFOColis));

        if (etat->colisNBJoursRestants[i] == NULL)
            return 1;

        initFIFOColis(etat->colisNBJoursRestants[i]);
        etat->taillesPiles[i] = 0;
    }

    etat->nbColisEnCours = 0;
    etat->maxColisEnCours = maxColisEnCours;

    return 0;
}

int contientColis(etatColis* etat, uint32_t numeroColis)
{
    int i;

    for (i = 0; i < NB_JOURS_MAX + 1; i++)
    {
        if (contient(etat->colisNBJoursRestants[i], numeroColis))
            return i;
    }

    return -1;
}

int ajouterColis(etatColis* etat, uint32_t numeroColis, int nbJours)
{
    if (nbJours > NB_JOURS_MAX)
        return 1;
    
    if (etat->nbColisEnCours >= etat->maxColisEnCours)
        return 1;

    if (contientColis(etat, numeroColis) != -1)
        return 1;

    if (ajouter(etat->colisNBJoursRestants[nbJours], numeroColis) == 1)
        return 1;
    
    etat->taillesPiles[nbJours]++;

    if (nbJours != 0)
        etat->nbColisEnCours++;

    return 0;
}

void afficherEtatColis(etatColis* etat)
{
    int i;

    printf("nb colis en cours: %d\ntaille max: %d\n", etat->nbColisEnCours, etat->maxColisEnCours);

    for (i = 0; i < NB_JOURS_MAX + 1; i++)
    {
        printf("%d jours restants:\ntaille: %d\n", i, etat->taillesPiles[i]);
        afficher(etat->colisNBJoursRestants[i]);
    }

    printf("-------------------------------------------------\n");
}

int avancer1Jour(etatColis* etat)
{
    FIFOColis* temp;
    int i, val;

    val = enlever(etat->colisNBJoursRestants[1]);
    while (val != -1)
    {
        ajouter(etat->colisNBJoursRestants[0], val);
        etat->nbColisEnCours--;

        val = enlever(etat->colisNBJoursRestants[1]);
    }

    etat->taillesPiles[0] += etat->taillesPiles[1];
    temp = etat->colisNBJoursRestants[1];

    for (i = 1; i < NB_JOURS_MAX; i++)
    {
        etat->colisNBJoursRestants[i] = etat->colisNBJoursRestants[i + 1];
        etat->taillesPiles[i] = etat->taillesPiles[i + 1];
    }

    etat->colisNBJoursRestants[NB_JOURS_MAX] = temp;
    etat->taillesPiles[NB_JOURS_MAX] = 0;

    return 0;
}