#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

int leftrotate(uint32_t F, int n)
{
    return (F << n) | (F >> (32 - n));
}

double monAbs(double nb)
{
    if (nb < 0)
        return -nb;
    else
        return nb;
}

int main()
{
    // trucs que Yo a dit pendant visio
        // surligner 
        // rendre scripte BDD


    int i;

    char message[64] = "The quick brown fox jumps over the lazy dog";

    int s[64] = { 7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
                  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
                  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
                  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21 };

    int k[64];/* = { 0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
                  0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
                  0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
                  0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
                  0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
                  0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
                  0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
                  0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
                  0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
                  0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
                  0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
                  0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
                  0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
                  0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
                  0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
                  0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391 };*/
    
    for (i = 0; i < 64; i++)
        k[i] = (int)((1<<31) * monAbs((int)sin(i + 1)));

    uint32_t a0 = 0x67452301, b0 = 0xefcdab89, c0 = 0x98badcfe, d0 = 0x10325476;
    // uint32_t a0 = 0x67452301, b0 = 0xfedcba98, c0 = 0xfedcba98, d0 = 0x76543210;
    int tailleMessage;

    tailleMessage = strlen(message);

    for (i = 0; i < tailleMessage; i++)
    {
        printf("%u", message[i]);
    }
    printf("\n");

    if (tailleMessage > 55)
    {
        printf("Le message est trop long\n");
        return 1;
    }

    message[tailleMessage] = 0x80;
    for (i = tailleMessage + 1; i < 56; i++)
    {
        message[i] = 0x00;
    }

    uint64_t temp;
    temp = tailleMessage * 8;
    memcpy(message + 56, &temp, 8);

    for (i = 0; i < 64; i++)
        printf("%d ", message[i]);
    printf("\n");

    uint32_t A = a0, B = b0, C = c0, D = d0;

    for (i = 0; i < 64; i++)
    {
        uint32_t F, g;
        if (0 <= i && i <= 15)
        {
            F = (B & C) | ((~B) & D); 
            g = i;
        }
        else if (16 <= i && i <= 31)
        {
            F = (D & B) | ((~D) & C);
            g = (5 * i + 1) % 16;
        }
        else if (32 <= i && i <= 47)
        {
            F = B ^ C ^ D;
            g = (3 * i + 5) % 16;
        }
        else
        {
            F = C ^ (B | (~D));
            g = (7 * i) % 16;
        }
        F = F + A + k[i] + ((uint16_t*)message)[g];
        A = D;
        D = C;
        C = B;
        B = B + leftrotate(F, s[i]);
    }

    a0 += A;
    b0 += B;
    c0 += C;
    d0 += D;

    char digest[16];
    memcpy(digest, &a0, 4);
    memcpy(digest + 4, &b0, 4);
    memcpy(digest + 8, &c0, 4);
    memcpy(digest + 12, &d0, 4);

    // for (i = 0; i < 16; i++)
    // {
    //     printf("%x   ", digest[i] + 128);
    // }

    printf("%x|", a0);
    printf("%x|", b0);
    printf("%x|", c0);
    printf("%x\n", d0);


    return 0;
}