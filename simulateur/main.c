#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "FIFOColis.h"
#include "obtenirArgs.h"
#include "lectureFichierIdentifiants.h"
// cette variable sert à passer l'adresse du client à la fonction de log
// sans avoir à passer cette variable en paramètre de chaque fonction qui utilise le log
struct sockaddr_in addrClient;
#include "log.h"
#define TAILLE_MAX_REQUETE 256
#include "codeSocket.h"


int getNbJoursPasses(time_t tempsDepart, int nbSecondesJour)
{
    return (int)difftime(time(NULL), tempsDepart) / nbSecondesJour;
}

int getNbJoursAPasser(time_t tempsDepart, int nbSecondesJour, int nbJoursPasses)
{
    return getNbJoursPasses(tempsDepart, nbSecondesJour) - nbJoursPasses;
}

int main(int argc, char* argv[])
{
    etatColis etat;
    initEtatColis(&etat, 10);
    listeArgs la;
    listeNomsMdps lsm;
    int nbJoursPasses = 0, valRequete, valCommande, i, temp;
    time_t tempsDepart = time(NULL);
    bool identifie, enAttenteAccuseSupprLivre;
    int nbChampsRecus;

    int sock, cnx;

    char lecture_commande[TAILLE_MAX_REQUETE], deuxiemeChampRequete[TAILLE_MAX_REQUETE], troisiemeChampRequete[TAILLE_MAX_REQUETE];

    srand(time(NULL));

    switch (lireArgs(argc, argv, &la))
    {
        case 0:
            printf("Le programme va démarer avec ces paramètres: %d, %d, %d, %s\n", la.nbMaxLivraisons, la.multiplicateurTemps, la.port, la.cheminIdentifiants);
            break;
        
        case 1:
            printf("Le programme va s'arrêter à cause de l'erreur\n");
            return 1;
        
        case 2:
            printf("Voici l'aide:\nUtilisation : simulateur [options]"
"Options requises:\n"
"  -m <nombre>       <nombre> maximum de livraisons simultanées\n"
"  -p <nombre>       <nombre> numéro de port utilisé\n"
"  -t <nombre>       <nombre> de secondes que prend un jour à être simulé\n"
"  -i <fichier>      <fichier> transmet le fichier contenant les identifiants et mot de passe\n");
            return 0;
        
        default:
            printf("aaaaaaa\n");
            return 1;
    }

    FILE* fichierIdentifiantsMdps = fopen(la.cheminIdentifiants, "r");

    if (fichierIdentifiantsMdps == NULL)
    {
        printf("Erreur lors de l'ouverture du fichier: \"%s\"\n", la.cheminIdentifiants);
        printf("Le programme va s'arrêter à cause de l'erreur\n");
        return 1;
    }
    else
        printf("Le fichier \"%s\" à été ouvert avec succès\n", la.cheminIdentifiants);

    switch (LectureIdentifiantsMdps(fichierIdentifiantsMdps, &lsm))
    {
        case 0:
            printf("Les identifiants et les mots de passes ont étés lues avec succès\n");
            break;
        
        case 1:
            printf("Le programme va s'arrêter à cause de l'erreur\n");
            return 1;
        
        default:
            printf("bbbbbbbbbbbbbbb\n");
            return 1;
    }

    fclose(fichierIdentifiantsMdps);

    logHeure();
    logFdp("--------------- Démarrage du serveur ---------------\n");
    daemon(1, 0);
    sock = creerSocketAccept(&cnx, &la);
    identifie = false;
    enAttenteAccuseSupprLivre = false;

    while (true)
    {   
        valRequete = lectureRequete(cnx, lecture_commande);

        temp = getNbJoursAPasser(tempsDepart, la.multiplicateurTemps, nbJoursPasses);
        nbJoursPasses += temp;

        for (i = 0; i < temp; i++)
        {
            avancer1Jour(&etat);
        }

        switch (valRequete)
        {
            case 0:
                printf("requete lue: %s\n", lecture_commande);
                valCommande = LireRequete(lecture_commande, deuxiemeChampRequete, troisiemeChampRequete, &nbChampsRecus);

                if (valCommande != 4)
                    enAttenteAccuseSupprLivre = false;

                switch (valCommande)
                {
                    case 0:
                        envoyer(cnx, "pas-ok fdp/1.0");
                        break;
                    
                    case 1: // reçu prend avec le bon nombre de champs
                        if (identifie)
                        {
                            if (prend(&etat, deuxiemeChampRequete, troisiemeChampRequete) == 0)
                                envoyer(cnx, "ok fdp/1.0");
                            else
                                envoyer(cnx, "pas-ok fdp/1.0");
                        }
                        else
                            envoyer(cnx, "qui-es-tu fdp/1.0");
                        break;
                    
                    case 2: // reçu donne avec le bon nombre de champs
                        if (identifie)
                        {
                            temp = donne(cnx, &etat, deuxiemeChampRequete, troisiemeChampRequete, nbChampsRecus);
                            if (temp == 1)
                                envoyer(cnx, "pas-ok fdp/1.0");
                            else if (temp == 2)
                                enAttenteAccuseSupprLivre = true;
                            else
                                enAttenteAccuseSupprLivre = false;
                        }
                        else
                            envoyer(cnx, "qui-es-tu fdp/1.0");
                        break;
                    
                    case 3: // reçu je-suis avec le bon nombre de champs
                        identifie = indentification(deuxiemeChampRequete, troisiemeChampRequete, &lsm);
                        if (identifie)
                            envoyer(cnx, "ok fdp/1.0");
                        else
                            envoyer(cnx, "pas-ok fdp/1.0");
                        break;
                    
                    case 4: // reçu ok avec le bon nombre de champs
                        if (identifie)
                        {
                            if (enAttenteAccuseSupprLivre)
                            {
                                vider(etat.colisNBJoursRestants[0]);
                                etat.taillesPiles[0] = 0;
                                envoyer(cnx, "ok fdp/1.0");
                            }
                            else
                                envoyer(cnx, "pas-ok fdp/1.0");
                            
                            enAttenteAccuseSupprLivre = false;
                        }
                        else
                            envoyer(cnx, "qui-es-tu fdp/1.0");
                        break;

                    case 5:
                        if (identifie) // reçu arrete avec le bon nombre de champs
                        {
                            logHeure();
                            logFdp("--------------- Fermeture du serveur ---------------\n");
                            return 0;
                        }
                        else
                            envoyer(cnx, "qui-es-tu fdp/1.0");
                        break;
                    
                    default:
                        printf("dddddddd\n");
                        return 1;
                }
                break;
            
            case 1:
                printf("argument trop long\n");
                envoyer(cnx, "pas-ok fdp/1.0");
                break;
            
            case 2:
                printf("Fermeture de la connection par l'hôte\n");
                close(cnx);
                close(sock);
                logHeure();
                logIP();
                logFdp("--------------- Déconnection du Client ---------------\n");
                sock = creerSocketAccept(&cnx, &la);
                if (cnx == -1)
                {
                    perror("erreur lors de la connection");
                    return 1;
                }
                printf("connection réussie\n");
                identifie = false;
                break;
            
            default:
                printf("ccccccccccccccccc\n");
                return 1;
        }
    }

    libererListeNomsMdps(&lsm);

    return 0;
}