#define TAILLE_IDENTIFIANT 20
#define TAILLE_MDP_MD5 32

typedef struct
{
    char** tabNoms;
    char** tabMdps;
    int tailleListe;
} listeNomsMdps;

int LectureIdentifiantsMdps(FILE* fichier, listeNomsMdps* liste)
{
    int nombreLignes = 0, i, j;
    char temp, charAvant = 'a';

    temp = fgetc(fichier);
    while (temp != EOF)
    {
        if (temp == '\n')
            nombreLignes++;
        charAvant = temp;
        temp = fgetc(fichier);
    }
    if (!(charAvant == '\n' || charAvant == '\r'))
        nombreLignes++;
    
    liste->tailleListe = nombreLignes;

    liste->tabNoms = malloc(sizeof(char*) * nombreLignes);
    liste->tabMdps = malloc(sizeof(char*) * nombreLignes);

    if (liste->tabNoms == NULL || liste->tabMdps == NULL)
    {
        printf("Il y a eu une erreur technique lors d'une allocation mémoire\n");
        return 1;
    }

    for (i = 0; i < nombreLignes; i++)
    {
        liste->tabNoms[i] = malloc(sizeof(char) * (TAILLE_IDENTIFIANT + 1));
        liste->tabMdps[i] = malloc(sizeof(char) * (TAILLE_MDP_MD5 + 1));

        if (liste->tabNoms[i] == NULL || liste->tabMdps[i] == NULL)
        {
            printf("Il y a eu une erreur technique lors d'une allocation mémoire\n");
            return 1;
        }
    }

    rewind(fichier);

    for (i = 0; i < nombreLignes; i++)
    {
        temp = fgetc(fichier);
        j = 0;
        while (temp != ',')
        {
            if (temp == '\n' || temp == '\r' || temp == EOF)
            {
                printf("La ligne %d du fichier ne contient pas de mot de passe\n", i + 1);
                return 1;
            }
            
            if (j >= TAILLE_IDENTIFIANT)
            {
                printf("L'identifiant ligne %d est trop long (%d caractères max)\n", i + 1, TAILLE_IDENTIFIANT);
                return 1;
            }
            
            liste->tabNoms[i][j] = temp;

            j++;
            temp = fgetc(fichier);
        }
        if (j == 0)
        {
            printf("Ligne %d l'identifiant doit faire au moins un charactère\n", i + 1);
            return 1;
        }

        liste->tabNoms[i][j] = '\0';

        temp = fgetc(fichier);
        j = 0;
        while (temp != '\n' && temp != EOF)
        {
            if (j >= TAILLE_MDP_MD5)
            {
                printf("Ligne %d le hash du mot de passe ne fait pas la bonne taille\n", i + 1);
                return 1;
            }
            
            if (temp != '\r')
                liste->tabMdps[i][j] = temp;

            j++;
            temp = fgetc(fichier);
        }
        if (j != TAILLE_MDP_MD5)
        {
            printf("Ligne %d le hash du mot de passe ne fait pas la bonne taille\n", i + 1);
            return 1;
        }

        liste->tabMdps[i][j] = '\0';
    }

    return 0;
}

void libererListeNomsMdps(listeNomsMdps* liste)
{
    int i;

    for (i = 0; i < liste->tailleListe; i++)
    {
        free(liste->tabNoms[i]);
        free(liste->tabMdps[i]);
    }

    free(liste->tabNoms);
    free(liste->tabMdps);
}

bool verifMdp(char* mdp, char* hash){
    FILE *fp;
    char hashVrai[33];

    /* Open the command for reading. */
    char commande[80];
    sprintf(commande, "echo %s | md5sum | cut -d ' ' -f 1", mdp);
    
    fp = popen(commande, "r");
    if (fp == NULL) {
        printf("Failed to run command\n" );
        exit(1);
    }

    fgets(hashVrai, sizeof(hashVrai), fp);
    
    pclose(fp);

    if (strcmp(hashVrai, hash) == 0){
        return true;
    } else {
        return false;
    }
}

bool indentification(char* identifiant, char* mdp, listeNomsMdps* lnm) {
    bool userValide = false;
    for (int i = 0; i < lnm->tailleListe; i++){
        if (strcmp(lnm->tabNoms[i], identifiant) == 0) {
            if (verifMdp(mdp, lnm->tabMdps[i])) {
                userValide = true;
            }
        }
    }
    
    return userValide;
}