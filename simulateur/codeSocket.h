int lectureRequete(int cnx, char commandeChaineLue[TAILLE_MAX_REQUETE])
{
    int tailleChaine = 0, ret;
    char temp;

    // vide les '/n' et les '/r' avant la lecture
    ret = read(cnx, &temp, 1);
    while (ret > 0)
    {
        if (temp != '\n' && temp != '\r')
        {
            commandeChaineLue[tailleChaine] = temp;
            tailleChaine++;
            break;
        }

        ret = read(cnx, &temp, 1);
    }

    if (ret == 0)
        return 2;

    ret = read(cnx, &temp, 1);
    while (ret > 0)
    {
        if (temp == '\n' || temp == '\r')
            break;
        
        if (tailleChaine >= TAILLE_MAX_REQUETE - 1)
            return 1;
        
        commandeChaineLue[tailleChaine] = temp;

        tailleChaine++;
        ret = read(cnx, &temp, 1);
    }

    commandeChaineLue[tailleChaine] = '\0';

    if (ret == 0)
        return 2;

    logHeure();
    logIP();
    logFdp("<< ");
    logFdp(commandeChaineLue);
    logFdp("\n");

    return 0;
}

// requete invalide 0
// requete valide > 0
int LireRequete(char commandeChaineLue[TAILLE_MAX_REQUETE], char* deuxieme, char* troisieme, int* nbChamps)
{
    char premier[100];
    char fin[TAILLE_MAX_REQUETE];
    int i, j, compteChamps;

    for (i = 0; i < 100 && commandeChaineLue[i] != '\0' && commandeChaineLue[i] != ' '; i++)
    {
        premier[i] = commandeChaineLue[i];
    }

    premier[i] = '\0';

    if (i < TAILLE_MAX_REQUETE && commandeChaineLue[i] != '\0')
    {
        i++;
        j = 0;
        for (; i < TAILLE_MAX_REQUETE && commandeChaineLue[i] != '\0' && commandeChaineLue[i] != ' '; i++)
        {
            deuxieme[j] = commandeChaineLue[i];
            j++;
        }

        deuxieme[j] = '\0';

        if (i < TAILLE_MAX_REQUETE && commandeChaineLue[i] != '\0')
        {
            i++;
            j = 0;
            for (; i < TAILLE_MAX_REQUETE && commandeChaineLue[i] != '\0' && commandeChaineLue[i] != ' '; i++)
            {
                troisieme[j] = commandeChaineLue[i];
                j++;
            }

            troisieme[j] = '\0';


            if (i < TAILLE_MAX_REQUETE && commandeChaineLue[i] != '\0')
            {
                i++;
                j = 0;
                for (; i < TAILLE_MAX_REQUETE && commandeChaineLue[i] != '\0' && commandeChaineLue[i] != ' '; i++)
                {
                    fin[j] = commandeChaineLue[i];
                    j++;
                }

                fin[j] = '\0';

                if (strcmp(fin, "fdp/1.0") == 0)
                    compteChamps = 4;
                else
                    return 0; // cela veut dire que la chaine lue ne se termine fdp/1.0
            }
            else if (strcmp(troisieme, "fdp/1.0") == 0)
            {
                fin[0] = '\0';
                compteChamps = 3;
            }
            else
                return 0; // cela veut dire que la chaine lue ne se termine fdp/1.0
        }
        else if (strcmp(deuxieme, "fdp/1.0") == 0)
        {
            troisieme[0] = '\0';
            compteChamps = 2;
        }
        else
            return 0; // cela veut dire que la chaine lue ne se termine fdp/1.0
    }
    else
        return 0; // cela veut dire que la chaine lue ne se termine fdp/1.0

    if (nbChamps != NULL)
        *nbChamps = compteChamps;

    if (strcmp(premier, "prend") == 0 && compteChamps == 4)
    {
        return 1;
    }
    else if (strcmp(premier, "donne") == 0 && (compteChamps == 3 || compteChamps == 4))
    {
        return 2;
    }
    else if (strcmp(premier, "je-suis") == 0 && compteChamps == 4)
    {
        return 3;
    }
    else if (strcmp(premier, "ok") == 0 && compteChamps == 2)
    {
        return 4;
    }
    else if (strcmp(premier, "arrete") == 0 && compteChamps == 2)
    {
        return 5;
    }

    return 0;
}

void envoyer(int cnx, char* str)
{
    int tailleChaine = strlen(str);
    char* res = malloc(tailleChaine + 3);

    strcpy(res, str);
    strcat(res, "\r\n");

    write(cnx, res, tailleChaine + 2);

    logHeure();
    logIP();
    logFdp(">> ");
    logFdp(res);

    free(res);
}

int prend(etatColis* ec, char* cible, char* numColisSTR)
{
    uint32_t numColisINT;
    char* temp;

    if (strcmp(cible, "la-commande") != 0)
        return 1;

    numColisINT = strtol(numColisSTR, &temp, 0);

    if (numColisSTR == temp || *temp != '\0')
        return 1;
    
    if (ajouterColis(ec, numColisINT, 1 + 1 + rand() % 3) != 0)
        return 1;
    
    return 0;
}

int donneLaCommande(int cnx, etatColis* ec, char* numColisSTR)
{
    // cela représente la correspondance entre le nombre de jours restants et l'état de la commande
    // 1 -> vers régionale, 2 -> vers locale, 3 -> livré
    const int tabCorrespondance[NB_JOURS_MAX + 1] = {3, 2, 1, 1, 1};

    uint32_t numColisINT;
    char* temp;
    int ret;
    char charRes[32];

    numColisINT = strtol(numColisSTR, &temp, 0);

    if (numColisSTR == temp || *temp != '\0')
        return 1;
    
    ret = contientColis(ec, numColisINT);

    if (ret != -1)
    {
        sprintf(charRes, "prend la-commande %d fdp/1.0", tabCorrespondance[ret]);
        envoyer(cnx, charRes);
        return 0;
    }   
    else
    {
        return 1;
    }
}

int donneLesCommandesFinies(int cnx, etatColis* ec)
{
    char charRes[1024], unColis[16];
    listeChainee* element = ec->colisNBJoursRestants[0]->debut;

    strcpy(charRes, "prend les-commandes-finies");

    while (element != NULL)
    {
        sprintf(unColis, " %d", element->numeroColis);
        strcat(charRes, unColis);

        element = element->suivant;
    }

    strcat(charRes, " fdp/1.0");

    envoyer(cnx, charRes);

    return 2;
}

int donne(int cnx, etatColis* ec, char* cible, char* numColisSTR, int nbChamps)
{
    if (nbChamps == 3)
    {
        if (strcmp(cible, "les-commandes-finies") != 0)
            return 1;

        return donneLesCommandesFinies(cnx, ec);
    }
    else if (nbChamps == 4)
    {
        if (strcmp(cible, "la-commande") != 0)
            return 1;

        return donneLaCommande(cnx, ec, numColisSTR);
    }
    
    return 1;
}

int creerSocketAccept(int* cnx, listeArgs* la)
{
    int sock, ret, size;
    struct sockaddr_in addr;
    struct sockaddr_in conn_addr;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    int option = 1;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));
    // addr.sin_addr.s_addr = inet_addr("192.168.0.43");
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(la->port);
    ret = bind(sock, (struct sockaddr*)&addr, sizeof(addr));
    if (ret != 0)
    {
        perror("erreur bind");
        return -1;
    }
    ret = listen(sock, 1);
    if (ret != 0)
    {
        perror("erreur listen");
        return -1;
    }
    size = sizeof(conn_addr);
    *cnx = accept(sock, (struct sockaddr *)&conn_addr, (socklen_t *)&size);
    addrClient = conn_addr;
    logHeure();
    logIP();
    logFdp("--------------- Client connecté ---------------\n");
    printf("connection réussie\n");

    return sock;
}