FROM php:apache
RUN apt-get update
RUN apt-get install -y libpq-dev
RUN docker-php-ext-install pdo_pgsql
RUN docker-php-ext-install sockets

COPY . /var/www/html
COPY ./apache/alizon.meatman.fr.conf /etc/apache2/sites-available/
COPY ./apache/admin-alizon.meatman.fr.conf /etc/apache2/sites-available/
COPY ./apache/php.ini /usr/local/etc/php/

RUN chown -R www-data:www-data /var/www/html/
RUN a2ensite alizon.meatman.fr
RUN a2ensite admin-alizon.meatman.fr