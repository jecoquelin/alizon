<?php
    // se script n'est utile que pendant le developpement
    // il fait plein de chose
    // il reset la bdd
    // il enleve toutes les images de produits
    // il importe un catalogue de produit
    // il enleve le cookie de panier

    session_start();
    include("script/catalogue.php");
    include('connectParams.php');
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage();
        die();
    }
    $fileContent = file_get_contents("../admin/BDD/script.sql");
    $dbh->exec($fileContent);

    $imageDir = scandir("./images/");
    if (!in_array("produits", $imageDir)){
        mkdir("./images/produits");
    }

    setcookie("alizonCookie", 1, 1, "/");

    //supression des images déjà là
    $dirImgProd = scandir('./images/produits');
    $dirImgProd = array_diff($dirImgProd, array('.', '..'));
    foreach ($dirImgProd as $dir) {
        $imgProd = scandir('./images/produits/'.$dir);
        $imgProd = array_diff($imgProd, array('.', '..'));
        foreach ($imgProd as $img) {
            unlink('./images/produits/' . $dir . '/' . $img);
        }
        rmdir('./images/produits/'.$dir);
    }

    //supression des images déjà là
    $dirImgProfil = scandir('./images/photoProfil');
    $dirImgProfil = array_diff($dirImgProfil, array('.', '..'));
    foreach ($dirImgProfil as $dir) {
        if ($dir != "defaut"){
            $imgProfil = scandir('./images/photoProfil/'.$dir);
            $imgProfil = array_diff($imgProfil, array('.', '..'));
            foreach ($imgProfil as $img) {
                unlink('./images/photoProfil/' . $dir . '/' . $img);
            }
            rmdir('./images/photoProfil/'.$dir);
        }
    }

    //supression des images et des fichiers d'un vendeur déjà là
    $dirFicVendeur = scandir('./images/vendeur/photoProfil/');
    $dirFicVendeur = array_diff($dirFicVendeur, array('.', '..'));
    foreach ($dirFicVendeur as $dir) {
        if ($dir != "defaut"){
            $contenuDirFicVendeur = scandir('./images/vendeur/photoProfil/'.$dir);
            $contenuDirFicVendeur = array_diff($contenuDirFicVendeur, array('.', '..'));
            foreach ($contenuDirFicVendeur as $ficEtDossier) {
                if (is_dir('./images/vendeur/photoProfil/'.$dir.'/'.$ficEtDossier)) {
                    foreach (array_diff(scandir('./images/vendeur/photoProfil/'.$dir.'/'.$ficEtDossier), array('.', '..')) as $elem) {
                        unlink('./images/vendeur/photoProfil/'.$dir.'/'.$ficEtDossier.'/'.$elem);
                    }
                    rmdir('./images/vendeur/photoProfil/'.$dir.'/'.$ficEtDossier);
                } else {
                    unlink('./images/vendeur/photoProfil/' . $dir . '/' . $ficEtDossier);
                }
            }
            rmdir('./images/vendeur/photoProfil/'.$dir);
        }
    }

    // on ajoute des image par défaut au compte client et aux deux compte vendeur
    // creation dossier vendeur
    mkdir("./images/vendeur/photoProfil/1");
    mkdir("./images/vendeur/photoProfil/2");
    // creéation dossier client
    mkdir("./images/photoProfil/1");
    // copie image vendeur
    copy("./images/vendeur/photoProfil/defaut/profilDefaut.jpg", "./images/vendeur/photoProfil/1/profilDefaut.jpg");
    copy("./images/vendeur/photoProfil/defaut/profilDefaut.jpg", "./images/vendeur/photoProfil/2/profilDefaut.jpg");
    // copie image client
    copy("./images/vendeur/photoProfil/defaut/profilDefaut.jpg", "./images/photoProfil/1/profilDefaut.jpg");

    $aImporter = array("./produits/mon_super_csv.csv",
                       "./produits/mon_super_csv2.csv");

    $i = 1;

    foreach ($aImporter as $chemin) {
        copy($chemin, "./fichierImportCatalogue.csv");
        $tabProduitsCSV = fiCSVersTab("fichierImportCatalogue.csv");
        unlink("./fichierImportCatalogue.csv");

        if ($tabProduitsCSV === false){
            echo("Les données dans fichier CSV sont invalides");
        } else {
            $pbImport = false;

            foreach ($tabProduitsCSV as $tuple){
                if (ajouterUnProduitCSV($tuple, $i) === false){
                    $pbImport = true; 
                }
            }

            if ($pbImport === true){
                echo("Les données dans fichier CSV sont invalides");
            } else {
                echo("L'insertion des données du fichier CSV à fonctionné");
            }
        }
        $i += 1;
    }

    //unset la variable qui dit qu'on arrive du panier
    unset($_SESSION['connect_panier']);
    //deconnect user
    $_SESSION['connect'] = false ;
    header("Location: ./index.php" );
?>