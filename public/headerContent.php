<?php
    include("script/client.php");
?>

<header>

    <!--le haut du header-->
    <div id="topHeader" class="d-flex justify-content-beetween align-items-center">
        <div class="col-md-2" >
            
            <!-- Affiche le logo petit ou pas en fonction de la taille de l'écran -->
            <a href="./index.php"><img id="logoAlizon" class="d-none d-lg-block " src="images/logos/Logo_blanc.png" alt="logo Alizon" ></a>
            <a href="./index.php"><img id="logoAlizonPetit" class="d-lg-none" src="images/logos/logo_simple_blanc.png" alt="logo simple Alizon" ></a>
        </div>
        <!-- Affiche une barre de recherche -->
        <form class="col" action="recherche.php">
            <div class="input-group barreRech rounded">
                <input id="input" name="search" type="text" class="form-control" placeholder="Rechercher..."  >
                <button id="search-button" type="submit" class="btn btn-search">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                    </svg>
                </button>
            </div>
        </form>
        <!-- Ce bloc sert au bouton connexion si on est connecté ou pas -->
        <div id ="flexCenter" class="col-md-2">
            <?php
                if (!isset($_SESSION['connect'])){
                    $_SESSION['connect'] = false;
                }
                // si nous somme connecté et que on clique sur notre photo de profil, nous avons un menu déroulant pour aller sur son profil etc...
                if ($_SESSION["connect"] == true ){
                    $client = getClientById($_SESSION["idClient"]);
                    echo '        
                        <ul class="nav nav-pills" style = "z-index : 99999;">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false"><img class = "rounded-circle" src="images/photoProfil/'.$client['id'].'/'.getMainPicClient($client['id']).'" title="'.$client['pseudo'].'" alt="'.$client['pseudo'].'"></a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="./profil.php">Mon profil</a></li>
                                    <li><a class="dropdown-item" href="#">Wishlist</a></li>
                                    <li><a class="dropdown-item" href="./commandeClient.php">Mes commandes</a></li>
                                    <li><a class="dropdown-item" href="./script/deconnect.php"> Se Déconnecter</a></li>
                                </ul>
                            </li>
                        </ul>
                    ';
                } else {
                    echo '<a href="./connexion.php" class="btn d-none d-lg-block btn-secondary" id="boutonAnime"  role="button">Connexion</a>
                    <a href="./connexion.php"><img id = "iconPersonne" class ="d-lg-none" src="images/icones/person.png" alt=""></a>';
                }
            ?>
            <a href="./panier.php"><img id="panier" src="images/icones/cart4.png" alt="icone de panier"></a>
        </div>
    </div>

    <!--le nav-->
    <nav class="navbar navbar-expand-xl navbar-dark sticky-top">
        <!--Bouton du menu burger-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> 
            <span class="navbar-toggler-icon"></span>                           
        </button>
        
        <!--Les items du nav-->
        <div class="collapse navbar-collapse" id="collapsibleNavbar"> <!-- mx-2 permet de gérer l'espacement de gauche -->
            <ul class="navbar-nav text-center align-items-center" >
                <!-- Toutes les catégorie du site sont disponible dans le nav -->
                <li class="nav-item">
                    <a class="itemDuNav nav-link" href="./index.php">Accueil</a>
                </li>
                <li class="nav-item">
                    <a class="itemDuNav nav-link" href="./recherche.php?cat[]=Peluche&resetCat=1&search=">Peluches</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="itemDuNav nav-link" href="./recherche.php?cat[]=Technologie&resetCat=1&search=">Technologie</a> 
                </li>
                <li class="nav-item dropdown">
                    <a class="itemDuNav nav-link" href="./recherche.php?cat[]=Cuisine&resetCat=1&search=">Cuisine</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="itemDuNav nav-link" href="./recherche.php?cat[]=Bricolage&resetCat=1&search=">Bricolage</a> 
                </li>
                <li class="nav-item dropdown">
                    <a class="itemDuNav nav-link" href="./recherche.php?cat[]=Jardin&resetCat=1&search=">Jardin</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="itemDuNav nav-link" href="./recherche.php?cat[]=Livre&resetCat=1&search=">Livres</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="itemDuNav nav-link" href="./recherche.php?cat[]=Film&resetCat=1&search=">Films</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="itemDuNav nav-link" href="./recherche.php?cat[]=Histoire&resetCat=1&search=">Histoire</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="itemDuNav nav-link" href="./recherche.php?cat[]=Jeu+video&resetCat=1&search=">Jeux vidéos</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="itemDuNav nav-link" href="./recherche.php?cat[]=Jouet&resetCat=1&search=">Jouets</a>
                </li>
            </ul>
        </div>
    </nav>
</header>