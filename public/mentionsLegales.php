<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Alizon_CGU</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="cgu_cgv.css" media="screen">
    </head>
    <body>
        <?php require('headerContent.php'); ?>
        
        <div class='container'>

            <?php   //si on vient de la page inscription, un lien permet d'y retourner
                if (isset($_GET["inscription"])) {
                    echo '<a href="inscription.php">Retour à l\'inscription</a>';
                }
            ?>

            <h1>Mentions légales</h1>
            <p>
            Conformément aux dispositions des Articles 6-III et 19 de la Loi n°2004-575 du 21 juin 2004 pour la Confiance dans l’économie numérique, dite L.C.E.N., il est porté à la connaissance des utilisateurs et visiteurs, ci-après l’"Utilisateur", du site www.cobrec.bzh , ci-après le "Site", les présentes mentions légales.
            La connexion et la navigation sur le Site par l’Utilisateur implique l'acceptation intégrale et sans réserve des présentes mentions légales.
            Ces dernières sont accessibles sur le Site à la rubrique « Mentions légales ».
            </p>
            <h4>ARTICLE 1 - L'ÉDITEUR</h4>
            <p>L'édition du Site est assurée par Cobrec SCOP SAS au capital de 2020 euros, immatriculée au Registre du Commerce et des Sociétés de Lannion sous le numéro 517403572 dont le siège social est situé au 20 Rue de Placenn ar Guer, Lannion. 
            <br>Numéro de téléphone 0123456789 
            <br>Adresse e-mail : azer@gmail.com
            <br>N° de TVA intracommunautaire : FR 85 517 403 572
            <br>Le Directeur de la publication est Tristan Chardès
            </p>
            <h4>ARTICLE 2 - L'HÉBERGEUR</h4>
            <p>
            L'hébergeur du Site est la société Breizhos, dont le siège social est situé au Lannion
            numéro de téléphone : 339876543210
            adresse mail: azerty@gmail.com
            </p>
            <h4>ARTICLE 3 - ACCÈS AU SITE</h4>
            <p>Le Site est accessible en tout endroit, 7j/7, 24h/24 sauf cas de force majeure, interruption programmée ou non et pouvant découlant d’une nécessité de maintenance.
            En cas de modification, interruption ou suspension du Site, l'Éditeur ne saurait être tenu responsable.
            </p>
            <h4>ARTICLE 4 - COLLECTE DES DONNÉES</h4>
            <p>Le Site assure à l'Utilisateur une collecte et un traitement d'informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. 
            En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978, l'Utilisateur dispose d'un droit d'accès, de rectification, de suppression et d'opposition de ses données personnelles. L'Utilisateur exerce ce droit :
            <br>-par mail à l'adresse email qwerty@gmail.com
            <br>-via son espace personnel 
            <br>Toute utilisation, reproduction, diffusion, commercialisation, modification de toute ou partie du Site, sans autorisation de l'Éditeur est prohibée et pourra entraîner des actions et poursuites judiciaires telles que notamment prévues par le Code de la propriété intellectuelle et le Code civil.
            <br>Pour plus d’informations, se reporter aux CGU du site www.cobrec.bzh accessible à la rubrique "CGU" 
            <br>Pour plus d’informations, se reporter aux CGV du site www.cobrec.bzh accessible à la rubrique "CGV" 
            <br>Pour plus d'informations en matière de protection des données à caractère personnel , se reporter à la Charte en matière de protection des données à caractère personnel du site www.cobrec.bzh accessible à la rubrique "Données personnelles".
            </p>

    
        </div>

        <?php require('footerContent.html'); ?>
    </body>
</html>