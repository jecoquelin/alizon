<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Alizon_CGV</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="cgu_cgv.css" media="screen">
    </head>
    <body>
        <?php require('headerContent.php'); ?>
        
        <div class='container'>

            <?php   //si on vient de la page inscription, un lien permet d'y retourner
                if (isset($_GET["inscription"])) {
                    echo '<a href="inscription.php">Retour à l\'inscription</a>';
                }
            ?>

            <h1>Conditions Générales de Vente</h1>
            <h4>ARTICLE 1 - CHAMP D’APPLICATION</h4>
            <p>Le site internet propose les services suivants :
            Les présentes Conditions Générales de vente (dites « CGV ») s'appliquent, sans restriction ni réserve à l'ensemble des ventes conclues par le Vendeur auprès d'acheteurs non professionnels (« Les clients ou le client »), désirant acquérir les produits proposés à la vente (« Les produits ») par le vendeur sur le site alizon. Le choix et l'achat d'un produit sont de la seule responsabilité du client. Les offres de produits s'entendent dans la limite des stocks disponibles, tels que précisés lors de la passation de la commande. Les données enregistrées dans le système informatique du Vendeur constituent la preuve de l'ensemble des transactions conclues avec le client. Les coordonnées du Vendeur sont les suivantes COBREC, SCOP SA Capital social de 2020 euros Immatriculé au RCS de Lannion, sous le numéro 123 Lannion Email: azerty@gmail.com
            </p>
            <h4>ARTICLE 2 - PRIX ET DISPONIBILITÉ</h4>
            <p>
            Les produits sont fournis aux tarifs en vigueur figurant sur le site alizon, lors de l'enregistrement de la Commande par le Vendeur. La disponibilité des produits est affichée sur le site. Les prix sont exprimés en Euros, TTC sauf indication contraire. Les tarifs tiennent compte d'éventuelles réductions qui seraient consenties par le Vendeur sur le site alizon. Ces tarifs sont non révisables pendant leur période de validité mais le vendeur se réserve le droit, hors période de validité, d'en modifier les prix à tout moment. Les prix ne comprennent pas les frais de traitement, d'expédition, de transport et de livraison. Le paiement demandé au client correspond au montant total de l'achat, y compris ces frais. Une facture est établie par le Vendeur et remise au client lors de la commande des produits commandés.
            </p>
            <h4>ARTICLE 3 - COMMANDE</h4>
            <p>Le contenu du site alizon.bzh est la propriété du Vendeur et de ses partenaires et est protégé par les lois françaises et internationales relatives à la propriété intellectuelle. Toute reproduction totale ou partielle de ce contenu est strictement interdite et est susceptible de constituer un délit de contrefaçon.
            </p>
            <h4>ARTICLE 4 - CONDITIONS DE PAIEMENT</h4>
            <p>Le prix est payé par voie de paiement sécurisé, selon les modalités suivantes: paiement par carte bancaire, paypal. Le prix est payable comptant par le client, en totalité au jour de la passation de la commande.
            </p>
            <h4>ARTICLE 5 - LIVRAISONS</h4>
            <p>Les produits commandés par le client seront livrés en France métropolitaine. Les livraisons interviennent dans un délai de 30 jours à l'adresse indiquée par le client lors de sa commande sur le site. La livraison est constituée par le transfert au client de la possession physique ou du contrôle du produit. Si les produits n'ont pas été livrés dans un délai de 30 jours après la date indicative de livraison, la vente pourra être résolue à la demande écrite au client dans les conditions prévues aux articles L216-2, L216-3 et L241-4 du Code de la consommation. Les sommes versées par le client lui seront alors restituées au plus tard dans les quatorze jours qui suivent la date de dénonciation du contrat.Les produits voyagent aux risques et périls du Vendeur.
            </p>
            <h4>ARTICLE 6- DROIT DE RÉTRACTATION</h4>
            <p>Selon les modalités de l'article L221-18 du Code de la Consommation. Le droit de rétractation peut être exercé en ligne, à l'aide du formulaire de rétractation disponible sur le site, à envoyer par courrier postal adressé au vendeur aux coordonnées postales ou mail indiquées à L'ARTICLE 1 des CGV. Les retours sont à effectuer dans leur état d'origine et complets permettant leur re-commercialisation à l'état neuf, accompagnés de la facture d'achat. Les produits endommagés, salis ou incomplets ne sont pas repris. Les frais de retour restant à la charge du client. L'échange (sous réserve de disponibilité) ou le remboursement sera effectué dans un délai de 14 jours à compter de la réception, par le vendeur, des produits retournés par le client dans les conditions prévues au présent article.
            </p>
            <h4>ARTICLE 7 - RESPONSABILITE DU VENDEUR GARANTIES</h4>
            <p>Les produit fournis par le vendeur bénéficient de la garantie légale de conformité pour : 
            <br>les produits défectueux, abîmés ou endommagés ou ne correspondant pas à la commande.
            <br>la garantie contre les vices cachés provenant d’un défaut de matière, de conception, de fabrication affectant les produits livrés et les rendant impropres à l'utilisation. 
            <br>Le site répond aux exigences des articles L217-4, L217-12 du Code de la consommation ainsi que l’article 1641, 1648 alinéa 1er du Code civil.
            Afin de faire valoir ses droits, le client devra informer le vendeur dans un délai de 15 jours à compter de la livraison, de la non conformité des produits ou de l'existence des vices cachés à compter de leur découverte.
            Le Vendeur remboursera, remplacera ou fera réparer les produits ou pièces sous garantie jugés non conformes. Ces traitements seront effectués au plus tard dans les 2 semaines jours suivant la constatation par le Vendeur du défaut de conformité.
            Les photographies et graphismes présentés sur le site ne sont pas contractuels et ne sauraient engager la responsabilité du vendeur. 
            </p>
            <h4>ARTICLE 8 - DONNÉES PERSONNELLES</h4>
            <p>Le client est informé que la collecte de ses données à caractère personnel est nécessaire à la vente des produits et à leur livraison, confiées au vendeur. Ces données à caractère personnel sont récoltées uniquement pour l'exécution du contrat de vente. Les données à caractère personnel collectées sur le site alizon sont : Noms, prénoms, adresse postale, numéro de téléphone et adresse e-mail. 
            <br><strong>8.1</strong> Destinataires des données à caractère personnel : Les données à caractère personnel sont utilisées par le Vendeur et ses cocontractants pour l'exécution du contrat et pour assurer l'efficacité de la prestation de vente et de délivrance des produits. 
            <br><strong>8.2</strong> Responsable de traitement : Le responsable de traitement des données est le Vendeur, au sens de la loi Informatique et libertés et à compter du 25 mai 2018 du Règlement 2016/6/9 sur la protection des données à caractère personnel. 
            <br><strong>8.3</strong> Durée de conservation des données : Le Vendeur conservera les données recueillies pendant un délai de 5 ans, le temps de la responsabilité civile.
            <br><strong>8.4</strong> Sécurité et confidentialité : Le Vendeur met en œuvre des mesures organisationnelles, techniques, logicielles et physiques en matière de sécurité pour protéger les données personnelles contre les altérations, destructions et accès non autorisés. Toutefois le Vendeur ne peut garantir la sécurité de la transmission ou du stockage des informations sur Internet.
            </p>

    
        </div>

        <?php require('footerContent.html'); ?>
    </body>
</html>