<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <!--HEAD-->
    <head>
        <title>Alizon</title>
        <?php require('head.php'); ?>
        <?php include('./script/product.php'); ?>
        <link rel="stylesheet" type="text/css" href="supprimerCompte.css" media="screen">
    </head>

    <!--BODY-->
    <body>
        <?php
            // si l'internaute n'est pas connecté alors on le redirige vers l'accueil
            if ($_SESSION["connect"] == false){
                header("Location: index.php");
            }
        ?>
        <!--HEADER-->
        <?php require('headerContent.php'); ?>

        <!--MAIN-->
        <main class="container-fluid">
            <article class="py-3">
                <p>
                    Vous êtes sur le point de supprimer votre compte, le compte sera archivé et vous ne pourrez plus vous connecter à celui-ci. 
                    <br>Rentrez votre mot de passe et cliquez sur "Supprimer le compte" pour confirmer la suppression.
                </p>
            </article>
            <section>
                <!-- form avec les champs de supression de compte -->
                <form class="form-inline col-md-4" action="script/deleteAcount.php" methode="get">
                    <div class="input-group py-2">
                        <input name="password" class="form-control" type="password" placeholder="Mot de passe" required="required"/>
                    </div>
                    <div class="input-group py-2">
                        <input name="confirmPassword" class="form-control" type="password" placeholder="Confirmer le mot de passe" required="required"/>
                    </div>
                    <div>
                        <input type="submit" class="btn btn-secondary button" value="Supprimer le compte">
                    </div>
                </form>
            </section>
        </main>

        <!--FOOTER-->
        <?php require('footerContent.html'); ?>
    </body>
</html>