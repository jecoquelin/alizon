<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php 
        $pageConnexion = "oui" ;
        require('head.php'); 
    ?>
    <title>Inscription</title>
    <link rel="stylesheet" href="connexion.css" type="text/css">
</head>
<body>
    <main>
        <!-- le main de la page -->
        <div class="row">
            <!-- Partie gauche du site -->
            <div id="PartieGauche" class="col-md-5 mb-0 text-center justify-content-md-center">
            <a href="index.php"><img id="logo" src="./images/logos/Logo_blanc.png" alt=""></a>
                <p>Vous avez déjà un compte ? <br>Connectez-vous</p>
                <a id="bouton_creer_compte" class="btn btn-secondary button" href="./connexion.php" role="button">Se connecter</a> 
            </div>
            <!-- Partie droite du site -->
            <div id="PartieDroite" class="col-md-7 justify-content-center">
                <!-- Form permettant de s'inscrire au site -->
                <div class=" pour_form">
                    <button onclick="auto()">Auto</button>
                    <form class="form-inline col-md-5" action="script/register.php" method="post">
                        <h2 class="text-center">Créer un compte</h2>
                        <!-- Champ du prénom -->
                        <input name="name" class="form-control champ name" type="text" placeholder="Prénom" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["name"] : ''); ?>"/>
                    
                        <!-- Champ du nom de famille -->
                        <input name="surname" class="form-control champ name" type="text" placeholder="Nom de famille" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["surname"] : ''); ?>"/>
                    
                        <!-- Champ du pseudo -->
                        <input name="pseudo" class="form-control champ name" type="text" placeholder="Pseudo" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["pseudo"] : ''); ?>"/>
                    
                        <!-- Champ de l'email -->
                        <input name="email" class="form-control champ email" type="email" placeholder="Email" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["email"] : ''); ?>"/>
                    
                        <!-- Champ du mot de passe -->
                        <input id="password" name="password" onkeydown="afficher();" onkeyup="CheckMdp(this.value)" class="form-control password champ" type="password" placeholder="Mot de passe" value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["password"] : ''); ?>" />
                        
                            
                        <!-- Div permettant d'afficher les éventuels erreurs lors de la création du mot de passe -->
                        <div id = "error">
                            <span id = "msg1"></span><br>
                            <span id = "msg2"></span><br>
                            <span id = "msg3"></span><br>
                            <span id = "msg4"></span><br>
                            <span id = "msg5"></span>
                        </div>

                        <!-- Champ pour confirmer le mot de passe -->
                        <input name="secondpassword" class="form-control password champ" type="password" placeholder="Confirmer le mot de passe" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["secondpassword"] : ''); ?>"/>
                        
                        <!-- Champ des question en cas d'oubli de mot de passe -->
                        <select name=question1 class="question champ" required>
                        <option selected id="firstLabel" disabled>Sélectionner une question</option>
                        <?php
                          global $dbh;
                          $sth = $dbh->prepare('SELECT * from alizon._question');
                          $sth -> execute();
                          $questions = $sth -> fetchAll();
                          foreach($questions as $q) {
                              echo "<option>";
                              echo($q["libelle"]);
                              echo "</option>";
                          }
                        ?>
                         </select>
                        
                        <input name="rep1" class="form-control rep champ" type="text" placeholder="Réponse" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["rep1"] : ''); ?>"/>
                        
                        <select name="question2" class="question champ" required>
                            <option selected id="firstLabel" disabled>Sélectionner une question</option>
                            <?php
                                foreach($questions as $q) {
                                    echo "<option>";
                                    echo($q["libelle"]);
                                    echo "</option>";
                             }
                        ?>
                        </select>
                        
                        <input name="rep2" class="form-control rep champ" type="text" placeholder="Réponse" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["rep2"] : ''); ?>"/>
                        
                        <!-- Permet d'accepter les CGU et CGV avant de s'inscrire -->
                        <div>
                            <p id="acceptCGU">En validant votre inscription vous accepter les <a href="./cgu.php?inscription=1">CGU</a>, les <a href="./cgv.php?inscription=1">CGV</a> et les <a href="./mentionsLegales.php?inscription=1">Mentions Légales</a> de Alizon.</p>
                        </div>
                        <div class="text-center padding">

                        <!-- Bouton pour s'inscrire -->
                        <input id="inscrire" type="submit" class="btn btn-secondary button" value="S'inscrire" disabled>
                        
                        <!-- Appele au script pour vérifier le mot de passe -->
                        <script src="./script/password.js"></script>
                    
                    </div>
                        <?php
                            // Récuperation et affichages des erreur si il en a 
                            if (isset($_GET['erreur'])){
                                switch ($_GET['erreur']) {
                                    case 0:
                                        echo    "<div style='color : #EF0009'; class='text-center'>
                                                    <p>Cette adresse mail est déjà utilisé</p>
                                                </div>";
                                        break;

                                    case 1:
                                        echo    "<div  style='color : #EF0009;' class='text-center'>
                                                    <p>Le mot de passe et la confirmation de mot de passe sont différents</p>
                                                </div>";
                                        break;

                                    case 2:
                                        echo    "<div  style='color : #EF0009;' class='text-center'>
                                                    <p>Veuillez choisir deux questions différentes</p>
                                                </div>";
                                        break;

                                    case 3:
                                        echo    "<div  style='color : #EF0009;' class='text-center'>
                                                    <p>Veuillez choisir les questions secrètes</p>
                                                </div>";
                                        break;

                                    case 4:
                                        echo    "<div  style='color : #EF0009;' class='text-center'>
                                                    <p>Au moins un des champs était vide</p>
                                                </div>";
                                        break;

                                    default:
                                        echo "<p>Huuum !</p>";
                                        break;
                                }
                            } 
                        ?>
                    </form>
                    
                </div>
            </div>
        </div>
    </main>
    <script>
        function auto() {
            var champs = document.getElementsByClassName("champ");
            for (var elem of champs) {
                var name = elem.name
                switch (name) {
                    case "name":
                        elem.value = "Tristan"
                        break;
                        
                    case "surname":
                        elem.value = "Chardès"
                        break;

                    case "pseudo":
                        elem.value = "Meatman"
                        break;

                    case "email":
                        elem.value = "tristan.chardes@gmail.com"
                        break;
                        
                    case "password":
                        elem.value = "Azerty123!"
                        break;

                    case "secondpassword":
                        elem.value = "Azerty123!"
                        break;

                    case "question1":
                        elem.selectedIndex = 1
                        break;
                        
                    case "rep1":
                        elem.value = "les pates, étudiant ou rien"
                        break;

                    case "question2":
                        elem.selectedIndex = 2
                        break;

                    case "rep2":
                        elem.value = "mon ordi, info ou rien"
                        break;

                    default:
                        console.log("ça bug par la")
                        break;
                }
            }
            var btn = document.getElementById("inscrire")
            btn.disabled = false;
            btn.click();
        }
    </script>
</body>
</html>