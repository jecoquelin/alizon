<!-- c'est du html qui servira dans TOUTES les pages qui importe ce fichiers (quasi toutes) -->
<meta charset="UTF-8" >
    <meta name="description" content="Alizon">
    <meta name="keyword" content="Alizon,site de e-commerce">
    <meta name="author" content="Brezhos">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Ligne permettant de mettre bootstrap-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    <!--Link le css au html-->
<?php
session_start();
/* --/!\ A lire pour comprendre les chemin relatifs /!\-- */
/*  Les pages qui importe ce fichier (la grane majorité) et qui sont dans les dossiers
    admin et vendeur doivent connaitre les chemins relatif des scripts et des images situés dans le dossier public.
    Pour ce faire le fichier prefixe.php est importé ici. Ce fichier est présent dans les dossiers vendeur, admin et public,
    ce qui fait que peu importe la page qui importe ce fichier il y aura forcément un fichier nomé prefixe.php dans le dossier courrant.
    Ce fichier prefixe contient une variable qui contient la chaine de caractere représentant le chemin relatif pour accèder au dossier public.
    Ainsi chaque page peut ajouter la variable $prefixe au début (d'où le nom) des chemin vers les images ou les scripts contenu dans le dossier public.
    */
require('./prefixe.php');
// dans les connect params on trouve toutes les infos de connection pour la bdd
include($prefixe.'connectParams.php');
echo '<link rel="stylesheet" href="'.$prefixe.'header.css" type="text/css">';
echo '<link rel="stylesheet" href="'.$prefixe.'footer.css" type="text/css">';
// Permet de mettre l'image en haut a gauche de l'onglet
echo '<link rel="shortcut icon" href="'.$prefixe.'images/logos/logo_simple.png" type="image/gif">';
try {
    $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
    
    $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    print "Erreur : " . $e->getMessage() . "<br/>";
    die();
}

require($prefixe.'script/managePanier.php');

if(!isset($_COOKIE["alizonCookie"])){ // on set un cookie pour stoquer l'id du panier d'un internaute
    $rand=rand(0,9999999);
    $fin=0;
    while($fin!=1){
        if(checkPanier($rand)==0){
            setcookie("alizonCookie", $rand, time() + (86400 * 15), "/");
            createPanier($rand);
            $fin=1;
        }else{
            $rand=rand(0,9999999);
        }
    }
    // on renvoie l'utilisateur sur la même page (on recharge la page) pour que le cookie soit actualier
    if (!isset($_SESSION['connect_panier'])){
        header('Location: ' . $prefixe.'index.php');
    }
    
} else { // si on a un cookie alors on est déjà aller sur le site donc on reset toutes les fonctions de sessions si elle ne sont pas set
    if (!isset($_SESSION['connect'])){
        $_SESSION['connect'] = false;
    }
    
    if (!isset($_SESSION['idPanier'])){
        $_SESSION['idPanier'] = $_COOKIE["alizonCookie"];
    }
    
    if ($_SESSION['connect'] == true){
        $sth = $dbh->prepare('SELECT * from alizon._compte_client where id = ?');
    
        $sth -> execute(array($_SESSION['idClient']));
        $clients = $sth -> fetchAll();
        $_SESSION['idPanier'] = $clients[0]['id_panier'];

        // SI le compte est désactivé, et sur une page client
        // Alors on déconnecte l'utilisateur
        if ($clients[0]['active'] != 1 && $page = "client"){
            $_SESSION['connect'] = false ;
        }

    } else {
        $_SESSION['idPanier'] = $_COOKIE["alizonCookie"];
    }
}

//require('./script/product.php');   //Utile par la suite pour eviter un nb_produit>stock  ///solution:deplacer tt le code dessous dans le head des pages concernées

if (isset($_SESSION['merge_panier'])){
    $idPanierOld=$_SESSION['merge_panier'];
    unset($_SESSION['merge_panier']);
    $idPanier = $_SESSION['idPanier'];
    foreach($dbh->query("SELECT * from alizon._dans_panier where id_panier=$idPanierOld") as $produit_panier) { /*Pour chaque article de l'ancien panier*/
        $doublon=false; //sert a savoir si le produit actuel existait déjà dans le panier qu'on merge
        $idProduit=$produit_panier['id_produit']; // Id produit dans panier anonyme
        $nbProduit=$produit_panier['nombre'];

        foreach($dbh->query("SELECT * from alizon._dans_panier where id_panier=$idPanier") as $produit_panier2) { //on verifie si des produits en doublon entre les paniers
            $idProduit2=$produit_panier2['id_produit']; //id produit dans panier client
            if ($idProduit2 == $idProduit){ //si un meme idArticle dans les 2 paniers
                $doublon=true;  //previent qu'on a des doublons
                $nbProduit2=$produit_panier2['nombre']; //nb produit dans panier client
            }
        }

        if ($doublon==true){    //Si les paniers qu'on merge ont des doublons, on update la quantité
            $newNbProduit=$nbProduit+$nbProduit2;
            //equivalent de SelectProduitById, qu'on ne peut pas importer ici
            $sth = $dbh->prepare('SELECT * from alizon._produit where id = ?');
            $sth -> execute(array($idProduit));
            $produits = $sth -> fetchAll();
            $LeProduit=$produits[0];     // produit cherché dans la table _produit
            $maxStock=$LeProduit['stock'];
            if ($newNbProduit>$maxStock){   //si la nouvelle somme> stock du produit
                $newNbProduit=$maxStock;     //set la valeur au max du stock
            }
            updateQuantity($newNbProduit,$idProduit,$idPanier);
        }
        else{   // sinon, on déplace le produit panier_anonyme=> panier client
            $sth = $dbh ->query("INSERT INTO alizon._dans_panier(id_panier,id_produit,nombre) values($idPanier, $idProduit , $nbProduit)"); //inserer les articles dans le nouveau panier
        }
        deleteItemPanier($idProduit, $idPanierOld); //supprime l'article du panier anonyme
       
    }
    
}

if (!isset($page)){
  $page = "IDK" ;  
}

// Vérification d'autorisation d'accès aux pages vendeurs
if ($page == "vendeur" && !(isset($pageConnexion))){
    $idVendeur = $_SESSION['idVendeur'] ;

    // Récupération des informations lié au vendeur
    $sth = $dbh->prepare('SELECT active from alizon._compte_vendeur where id = ?');
    $sth -> execute(array($idVendeur));
    $vendeurs = $sth -> fetchAll();

    // SI le compte est désactivé, on déconnecte l'utilisateur
    if ($vendeurs[0]['active'] != 1){
        $_SESSION['connectVendeur'] = false;
    }

    // SI le vendeur n'est pas connecter 
    // ALORS on le redirectionne sur la page de connexion vendeur
    if ($_SESSION['connectVendeur'] == false){
        header('Location: '.$prefixe.'vendeur/connexionVendeur.php?erreur=3');
    }
}

?>