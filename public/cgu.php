<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Alizon_CGU</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="cgu_cgv.css" media="screen">
    </head>
    <body>
        <?php require('headerContent.php'); ?>
        
        <div class='container'>

            <?php   //si on vient de la page inscription, un lien permet d'y retourner
                if (isset($_GET["inscription"])) {
                    echo '<a href="inscription.php">Retour à l\'inscription</a>';
                }
            ?>

            <h1>Conditions Générales d’Utilisation</h1>
            <p>
            Les présentes conditions générales d'utilisation (dites « CGU ») ont pour objet l'encadrement juridique des modalités de mise à disposition du site et des services par Breizhos et de définir les conditions d’accès et d’utilisation des services par « l'Utilisateur ».
            En cas de non-acceptation des CGU stipulées dans le présent contrat, l'Utilisateur se doit de renoncer à l'accès des services proposés par le site.
            alizon.bzh se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes CGU.
            </p>
            <h4>ARTICLE 1 : ACCÈS AU SITE</h4>
            <p>Le site internet propose les services suivants :
            Vente de produits bretons en tout genre. Le site est accessible gratuitement.
            L'utilisateur non membre n'a pas accès aux services réservés. Pour cela, il doit s’inscrire en remplissant le formulaire. En acceptant de s’inscrire aux services réservés, l'utilisateur membre s’engage à fournir des informations sincères et exactes concernant son état civil et ses coordonnées, notamment son adresse email.
            Pour accéder aux services, l’Utilisateur doit ensuite s'identifier à l'aide de son identifiant et de son mot de passe qu’il aura choisis lors de son inscription.
            Tout utilisateur inscrit pourra également solliciter sa désactivation en se rendant sur la page dédiée. Celle-ci sera effective dans un délai raisonnable.
            Tout événement dû à un cas de force majeure ayant pour conséquence un dysfonctionnement du site ou serveur et sous réserve de toute interruption ou modification en cas de maintenance, n'engage pas la responsabilité de alizon. Dans ces cas, l’Utilisateur accepte ainsi ne pas tenir rigueur à l’éditeur de toute interruption ou suspension de service, même sans préavis.
            </p>
            <h4>ARTICLE 2 : COLLECTE DES DONNÉES</h4>
            <p>
            Le site assure à l'utilisateur une collecte et un traitement d'informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. Le site est déclaré à la CNIL sous le numéro 1234.
            En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978, l'Utilisateur dispose d'un droit d'accès, de rectification, de suppression et d'opposition de ses données personnelles. L'utilisateur exerce ce droit via son espace personnel.
            </p>
            <h4>ARTICLE 3 : PROPRIÉTÉ INTELLECTUELLE</h4>
            <p>Le contenu du site alizon.bzh est la propriété du Vendeur et de ses partenaires et est protégé par les lois françaises et internationales relatives à la propriété intellectuelle. Toute reproduction totale ou partielle de ce contenu est strictement interdite et est susceptible de constituer un délit de contrefaçon.
            </p>
            <h4>ARTICLE 4 : RESPONSABILITÉ</h4>
            <p>Le site ne garantit pas que les sources d’information soient exemptes de défauts, d’erreurs ou d’omissions. Malgré des mises à jour régulières, le site alizon.bzh ne peut être tenu responsable de la modification des dispositions administratives et juridiques survenant après la publication. De même, le site ne peut être tenu responsable de l’utilisation et de l’interprétation de l’information contenue dans ce site.
            L'utilisateur s'assure de garder son mot de passe secret. Il assume les risques liés à l'utilisation de son identifiant et mot de passe. Le site décline toute responsabilité.
            Le site alizon.bzh ne peut être tenu pour responsable d’éventuels virus qui pourraient infecter tout matériel informatique de l’Internaute
            La responsabilité du site ne peut être engagée en cas de force majeure.
            </p>
            <h4>ARTICLE 5 : PUBLICATION PAR L’UTILISATEUR</h4>
            <p>Le site permet aux membres de publier des commentaires. Dans ses publications, le membre s’engage à respecter les règles de la Netiquette (règles de bonne conduite de l’internet) et les règles de droit en vigueur.
            Le site peut exercer une modération sur les publications et se réserve le droit de refuser leur mise en ligne, sans avoir à s’en justifier auprès du membre.
            </p>
            <h4>ARTICLE 6 : DROIT APPLICABLE ET JURIDICTION COMPÉTENTE</h4>
            <p>La législation française s'applique au présent contrat. En cas d'absence de résolution amiable d'un litige né entre les parties, les tribunaux français seront seuls compétents pour en connaître.
            Pour toute question relative à l’application des présentes CGU, vous pouvez joindre l’éditeur aux coordonnées inscrites à l’ARTICLE 1.
            </p>
            <h4>ARTICLE 7 - LITIGES</h4>
            <p>Pour toute réclamation merci de contacter le service clientėle à l'adresse postale ou mail du Vendeur inauguré à l'ARTICLE 1 des présentes CGV. Le Client est informé qu'il peut en tout état de cause recourir à une médiation conventionnelle, auprès des instances de médiation sectorielles existantes ou à tout mode alternatif de règlement des différends (conciliation, par exemple) en cas de contestation.
            En l'espèce, le médiateur désigné est La médiation du commerce coopératif et associé Médiateur du Commerce Coopératif et Associé FCA 77 rue de Lourmel 75015 Paris https://www.mcca-mediation.fr/contact E-mail: servicemediation@mcca-mediation.fr. Le Client est également informé qu'il peut, également recourir à la plateforme de Règlement en Ligne des Litige (RLL) : https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.show tous les litiges auxquels les opérations d'achat et de vente conclues en application des présentes CGV et qui n'auraient pas fait l'objet d'un règlement amiable entre le vendeur ou par médiation, seront soumis aux tribunaux compétents dans les conditions de droit commun. 
            </p>

    
        </div>

        <?php require('footerContent.html'); ?>
    </body>
</html>