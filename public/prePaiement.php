<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
   <head>
      <?php require('head.php'); ?>
      <?php $_SESSION['connect_panier']=false;?>
      <title>Pré-Paiement</title>
      <link rel="stylesheet" type="text/css" href="prePaiement.css" media="screen">
      <?php require('script/paiement.php'); ?>
   </head>
   <?php
      $idPanier = $_SESSION['idPanier'];

      if ($_SESSION['connect']== false) {
         header("Location: ./connexion.php");
      }
   ?>

<body>
   <?php require('headerContent.php'); ?>
   <main>
   <!-- Bouton de navigation -->
   <div class="tab d-flex justify-content-center">
      <button id="btn-left" class="tablinks" onclick="changerFenetre(event, 'pPanier')">Panier</button> <!-- <div id="panier" class="tabcontent"> -->
      <button id="btn-md-left" class="tablinks" onclick="changerFenetre(event, 'aFacturation')">Adresse de facturation</button>
      <button id="btn-md-right" class="tablinks" onclick="changerFenetre(event, 'aLivraison')" disabled>Adresse de livraison</button>
      <button id="btn-right" class="tablinks" onclick="changerFenetre(event, 'paiement')" disabled>Paiement</button>
   </div>

   <div id="pPanier" class="tabcontent">
      <section id="sectionProduits" class="row col-lg-12 col-xl-10 mt-5">
         <h1>Récapitulatif Panier</h1>
         <?php
         global $dbh;
         $prixTotalTTC=0;
         $prixTotalHT=0;
         $prixTotalTVA=0;
         foreach($dbh->query("SELECT * from alizon._dans_panier where id_panier=$idPanier") as $produit_panier) { /*Pour chaque article du panier n°$idPanier*/
         $produit=getProduitById($produit_panier['id_produit']);
         $estRemise = getRemise($produit['id']);
         $remiseHT = $produit['prix_ht']*((100-getRemise($produit['id']))/100); 
         //Calcul des prix

         if ($estRemise != false) {
            $prixTTC = round(($remiseHT*getTauxTva($produit["libelle_categorie"]))*$produit_panier['nombre'],2);
            $prixHT= round(($remiseHT)*$produit_panier['nombre'],2);
         }
         else {
            $prixTTC= round(($produit['prix_ht'] * getTauxTva($produit['libelle_categorie']))*$produit_panier['nombre'],2);
            $prixHT= round($produit['prix_ht'] *$produit_panier['nombre'],2);
         }

         $prixTVA= $prixTTC-$prixHT;
         $prixTotalTTC= round(($prixTotalTTC+$prixTTC),2);
         $prixTotalHT= round(($prixTotalHT+$prixHT),2);
         $prixTotalTVA= round(($prixTotalTVA+$prixTVA),2);
         $taux_tva=(getTauxTva($produit['libelle_categorie'])-1)*100; ?>

         <hr>
         <!-- Recap Panier -->
         <div class="row justify-content-start">   
            <div class="img_article col-sm-5 col-lg-2 text-center ">
               <figure class="col-sm">
                  <?php echo '<img class="image" src="images/produits/'. $produit['id_photos'] . '/' . getMainPicProd($produit['id']) . '" alt="' . $produit['libelle'] . '" title="' . $produit['libelle'] . '"  height="150" width="140" > ' ;?>
               </figure>
            </div>
            <div class="text_article col-sm-7 col-lg-8 ">
               <?php echo '<h3>' . $produit['libelle'] . '</h3>'; ?>
               <br>    
               <div class="row">
                  <p class="col-12"> <?php echo 'Quantité: ' . $produit_panier['nombre'] ?> </p>
                  <p class="col-3"> <?php echo 'HT: ' . $prixHT. '€' ?> </p>
                  <p class="col-3"> <?php echo 'Taux TVA: ' . $taux_tva.'%' ?> </p>
                  <p class="col-3"> <?php echo 'TVA: ' . $prixTVA.'€' ?> </p>
                  <p cactivelass="col-3"> <?php echo 'TTC: ' . $prixTTC.'€' ?> </p>
               </div>
            </div>                
         </div>
         <?php }?>
      </section>
      <div class="d-flex justify-content-between">
         <a href="./panier.php" class="btn d-lg-block btn-secondary" id="boutonAction"  role="button">Retour au panier</a>  
         <button class="btn d-lg-block btn-secondary" id="boutonAction" onclick="changerFenetre(event, 'aFacturation');document.getElementById('btn-md-left').click();">Suivant</button>
      </div>
   </div>

   <form action="./script/addOrder.php" method="POST" name="prepaiement">
   <!-- Formulaire de l'adresse de facturation -->
   <div id="aFacturation" class="tabcontent">
      <div id="aFacturationContent">
         <article>
            <h3>Identité</h3>
            <div class="paddingDroite d-inline">
               <label>Nom : *</label>
               <input class="inputPrepaiement" name="nom" type="text" placeholder="Nom" value="<?php echo $client["nom"];?>" required>
            </div>
            <div class="d-inline">
               <label>Prenom : *</label>
               <input class="inputPrepaiement" name="prenom" type="text" placeholder="Prenom" value="<?php echo $client["prenom"];?>" required>
            </div>
         </article>
         <article>   
            <div>
               <label>Date de naissance :</label>
               <input class="inputPrepaiement" name="date_naissance" type="date" value="<?php echo $client['date_naissance'] ?>">
            </div>
            <br>
            <div class="div_ret">
               <input class="inputPrepaiement checkbox" name="retenirDate" type="checkbox">
               <label class="labelCheckBox">Retenir la date de naissance sur ce compte</label>
            </div>
         </article>
         <article>
            <h3>Adresse de Facturation :</h3>
            <label>Numéro de voie *</label>
            <input class="inputPrepaiement input_adr" name="numeroVoieF"   type="text"  placeholder="N°" pattern="\d{1,4}[a-zA-Z]?" value="<?php echo $client["numero_voie"];?>"    required>
            <br>
            <label>Nom de voie *</label>
            <input class="inputPrepaiement input_adr" name="nomVoieF"      type="text"    placeholder="Nom de voie"  value="<?php echo $client["nom_voie"];?>"       required>
            <br>
            <label>Ville *</label>
            <input class="inputPrepaiement input_adr" name="villeF"        type="text"    placeholder="Ville"        value="<?php echo $client["ville"];?>"          required>
            <br>
            <label>Code postal *</label>
            <input class="inputPrepaiement input_adr" name="postalF"       type="text"    placeholder="Code postal"  value="<?php echo $client["code_postal"];?>"    required>
            <br>
            <label>Batiment</label>
            <input class="inputPrepaiement input_adr" name="batimentF"     type="text"    placeholder="Batiment"     value="<?php echo $client["batiment"];?>">
            <br>
            <label>Etage</label>
            <input class="inputPrepaiement input_adr" name="etageF"        type="text"   placeholder="Etage"         value="<?php echo $client["etage"];?>">
            <br>
            <label>N° porte</label>
            <input class="inputPrepaiement input_adr" name="numeroPorteF"  type="text"  placeholder="N° porte"       value="<?php echo $client["numero_porte"];?>">
            <br>
            <div class="div_ret">
               <input class=" inputPrepaiement checkbox" name="retenirAdr" type="checkbox">
               <label class="labelCheckBox">Retenir l’adresse pour ce compte</label>
            </div>
         </article>
      </div>
         <div class="d-flex justify-content-between"> 
         <button class="btn d-lg-block btn-secondary" id="boutonAction" onclick="changerFenetre(event, 'pPanier');document.getElementById('btn-left').click();">Retour</button>
         <button class="btn d-lg-block btn-secondary" id="boutonAction" onclick="isEmptyFacturation()">Suivant</button>
         </div>
   </div>
   <!-- Formulaire de l'adresse de livraison -->
   <div id="aLivraison" class="tabcontent">
      <div id="aLivraisonContent">
      <article>
         <h3>Adresse de livraison</h3>
         <div class="div_ret">
            <input id='sameAddr' class=" inputPrepaiement checkbox" onchange="sameAddress()" name="sameAddr" type="checkbox">
            <label class="labelCheckBox">L'adresse de facturation et l'adresse de livraison sont identiques</label>
         </div>
         <div id='formAdresseLivraison'>
            <label>Numéro de voie *</label>
            <input class="inputPrepaiement input_adrL" name="numeroVoie"   type="text"  placeholder="N°" pattern="\d{1,4}[a-zA-Z]?" value="<?php echo $client["numero_voie"];?>"    required>
            <br>
            <label>Nom de voie *</label>
            <input class=" inputPrepaiement input_adrL" name="nomVoie"      type="text"    placeholder="Nom de voie"  value="<?php echo $client["nom_voie"];?>"       required>
            <br>
            <label>Ville *</label>
            <input class="inputPrepaiement input_adrL" name="ville"        type="text"    placeholder="Ville"        value="<?php echo $client["ville"];?>"          required>
            <br>
            <label>Code postal *</label>
            <input class="inputPrepaiement input_adrL" name="postal"       type="text"    placeholder="Code postal"  value="<?php echo $client["code_postal"];?>"    required>
            <br>
            <label>Batiment</label>
            <input class="inputPrepaiement input_adrL" name="batiment"     type="text"    placeholder="Batiment"     value="<?php echo $client["batiment"];?>">
            <br>
            <label>Etage</label>
            <input class="inputPrepaiement input_adrL" name="etage"        type="text"   placeholder="Etage"         value="<?php echo $client["etage"];?>">
            <br>
            <label>N° porte</label>
            <input class="inputPrepaiement input_adrL" name="numeroPorte"  type="text"  placeholder="N° porte"       value="<?php echo $client["numero_porte"];?>">
         </div>
      </article>
   </div>
   <div class="d-flex justify-content-between">
      <button class="btn d-lg-block btn-secondary" id="boutonAction" onclick="changerFenetre(event, 'aFacturation');document.getElementById('btn-md-left').click();">Retour</button>
      <button class="btn d-lg-block btn-secondary" id="boutonAction" onclick="isEmptyLivraison()">Suivant</button>
   </div>
   </div>

   <!-- Option de paiement -->
   <div id="paiement" class="tabcontent">
      <div id="paiementContent">
         <article id="methodePaiement">
            <h1>Methode de paiement</h1>
            <br>
            <div>
               <select onchange="change(this.value);" id="pet-select">
                  <option id="CB" value="cb" >Carte bancaire</option>
                  <option value="paypal">Paypal</option>
               </select>
            </div>
            <br>
         </article>      
         <div id="paiementCB">
            <h1>Paiement par Carte bancaire</h1>
            <div>
               <div class="d-activeinline">
                  <label>Nom :</label>
                  <input class="inputPrepaiement input_cb" name="nomCarte" type="text" value="" required>
               </div>
               <div class="">
                  <label>Numero de carte :</label>
                  <input class="inputPrepaiement input_cb" name="numCarte" type="text" onkeyup="CheckCreditCard(this.value)" required>
                  <span id="error"></span>
               </div> 
            </div>
            <div>
               <span class="fallbackLabel d-inline-block">Date d'expiration :</span>
               <div class="fallbackDatePicker d-inline-block ">
                  <div>
                     <span>
                        <select id="month" name="month">
                           <option selected>01</option>
                           <option>02</option>
                           <option>03</option>
                           <option>04</option>
                           <option>05</option>
                           <option>06</option>
                           <option>07</option>
                           <option>08</option>
                           <option>09</option>
                           <option>10</option>
                           <option>11</option>
                           <option>12</option>
                        </select>
                     </span>
                     <?php 
                     $Year = date("Y");
                     echo "<span>";
                     echo  "<select id='year' name='year'>";
                     echo     "<option selected>",$Year,"</option>";
                     echo     "<option>",$Year+1,"</option>";
                     echo     "<option>",$Year+2,"</option>";
                     echo     "<option>",$Year+3,"</option>";
                     echo     "<option>",$Year+4,"</option>";
                     echo     "<option>",$Year+5,"</option>";
                     echo     "<option>",$Year+6,"</option>";
                     echo  "</select>";
                     echo "</span>";
                     ?>
                  </div>
               </div>
               <div>
                  <label>Cryptogramme :</label>
                  <input class="inputPrepaiement input_cb" name="crypto" type="number" onkeyup="CheckCriptogramme(this.value)" required>
                  <span id="errorCryptgramme"></span>
               </div>
            </div>  
         </div>
         <aside id="sec_aside">
            <!--Aside revenir panier -->
            <div id="revenir_panier" class="d-flex justify-content-center">
               <a href="./panier.php" id="revenirAuPanier">Revenir au panier</a>
            </div>
            <!--Aside passer commande -->
            <div id="passe_commande" >
               <p id="alignerCentre">
                 Total
               </p>
               <?php
                  echo "<p>Total HT: ". $prixTotalHT ."€</p>";
                  echo "<p>Total TVA : ". $prixTotalTVA ."€</p>";
                  echo "<p>Total TTC : ". $prixTotalTTC ."€</p>";
               ?>
               <div class="d-flex justify-content-center">
                  <input class="inputPrepaiement" id="payer" type="submit" name="payer" value="Payer" > 
               </div>
               <script src="./script/payment.js"></script>
            </div>
         </aside>
      </div>
   </form>
   </div>
   </main>
   <?php require('footerContent.html'); ?>

<script>

   document.getElementById('btn-left').click();
   /*Changement de fenetre*/
   function changerFenetre(evt, fenetre) {
     var i, tabcontent, tablinks;
     tabcontent = document.getElementsByClassName("tabcontent");
     for (i = 0; i < tabcontent.length; i++) {
       tabcontent[i].style.display = "none";
     }
     tablinks = document.getElementsByClassName("tablinks");
     for (i = 0; i < tablinks.length; i++) {
       tablinks[i].className = tablinks[i].className.replace(" active", "");
     }
     document.getElementById(fenetre).style.display = "block";
     evt.currentTarget.className += " active";
   }

   /*Verification si les champs obligatoire de facturation ne sont pas vide*/
   function isEmptyFacturation(){
      let aSuite = true;
      for (let index = 0; index < document.getElementsByClassName('input_adr').length; index++) {
         if (document.getElementsByClassName('input_adr')[index].value === '' && document.getElementsByClassName('input_adr')[index].required) {
            aSuite = false;
         }
      }
      if (aSuite == true){
         changerFenetre(event, 'aLivraison')
         document.getElementById("btn-md-right").disabled = false;
         document.getElementById("btn-md-right").click();
      }   
   }

   /*Verification si les champs obligatoire de livraison ne sont pas vide*/
   function isEmptyLivraison(){
      let aSuite = true;
      for (let index = 0; index < document.getElementsByClassName('input_adrL').length; index++) {
         if (document.getElementsByClassName('input_adrL')[index].value === '' && document.getElementsByClassName('input_adrL')[index].required) {
            aSuite = false;
         }
      }
      if (aSuite == true){
         changerFenetre(event, 'paiement')
         document.getElementById("btn-right").disabled = false;
         document.getElementById("btn-right").click();
      } 
   }
</script>
   
</body>

