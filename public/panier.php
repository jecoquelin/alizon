<!DOCTYPE html> 
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">

<!-- HEAD et REQUIRE des scripts -->
<head>
	<title>Mon Panier</title>
    <?php require('head.php'); ?>
    <?php require('script/product.php'); ?>
    <?php $_SESSION['connect_panier']=false; ?>
    <link href="panier.css" rel="stylesheet" type="text/css">
</head>

<?php
/*--------Recupere id_panier si cookie existe-------*/
$idPanier = $_SESSION['idPanier'];

/*-------Initialise des variables-------*/
$prixTotalTTC=0;
$prixTotalHT=0;
$prixTotalTVA=0;
$nombreTotal=0;
$nombreArticle=0;
?>

<!--BODY-->
<body> 
    <?php require('headerContent.php'); ?>
    <main>
        
    <!-- Le main de la page -->
        <div class="container">
            <!-- La page presentera les informations en ligne -->
            <div class="row justify-content-between">
                <div class="col-lg-8 col-md-8 text-center">
                <h1>Mon Panier</h1>
                <hr>
                <!--Presentation article -->
                <?php
                    if(isset($_POST['vider'])){
                        videPanier($idPanier);
                    }
                    global $dbh;
                    /*Pour chaque article du panier n°$idPanier*/
                    foreach($dbh->query("SELECT * from alizon._dans_panier where id_panier=$idPanier") as $produit_panier) { 
                        $produit=getProduitById($produit_panier['id_produit']);
                        $estRemise = getRemise($produit['id']);
                        $remiseHT = $produit['prix_ht']*((100-getRemise($produit['id']))/100); 
                        $quantiteProduitDansStock = $produit['stock'];
                        ?>

                        <?php
                        $show=true;
                        if(isset($_POST['delete'.$produit_panier['id_produit']])){
                            deleteItemPanier($produit_panier['id_produit'], $idPanier);
                            $show=false;
                        }
                        else if(isset($_POST['quantity'.$produit_panier['id_produit']])){
                            if ($_POST['quantity']<="0" ){
                                deleteItemPanier($produit_panier['id_produit'], $idPanier);
                                $show=false;
                            }
                            else{ // on change la quantité dans le pannier en fonction de ce qui est demandé par le client
                                if (verifComposeDeChiffres($_POST['quantity'])) {
                                    // qte représente la quantité de l'article qui sera dans le pannier
                                    $qte = $_POST['quantity'];

                                    // si la quantité que le client essai de mettre dans son pannier est supérieure à ce qui est en stock
                                    //      on met la quantité dans le pannier à la quantité en stock
                                    if ($qte > $quantiteProduitDansStock){
                                        $qte = $quantiteProduitDansStock;
                                    }
                                    
                                    updateQuantity($qte,$produit_panier['id_produit'],$idPanier);
                                    $produit_panier['nombre']=$qte;
                                }
                            }
                        // on vérifie que la quantité de l'article dans le panier
                        // n'est pas supérieur à ce qui est en stock
                        }else{
                            if ($produit_panier['nombre'] > $quantiteProduitDansStock) {
                                // si il y en a trop en stock
                                //      on met la quantité dans le pannier à la quantité en stock
                                updateQuantity($quantiteProduitDansStock,$produit_panier['id_produit'],$idPanier);
                                $produit_panier['nombre']=$quantiteProduitDansStock;
                            }
                        }

                        if ($show==true){   
                                
                        ?>
                             
                        <div class="row">   
                            <!-- bloc reservé pour afficher la photo du produit avec le line vers le descriptif du produit -->
                            <div class="col-lg-2 justify-content-end">
                                <figure>
                                    <?php echo '<a href="./detailProduit.php?id=' . $produit['id'] . '">'; ?>
                                    <?php echo '<img class="image" src="images/produits/'. $produit['id_photos'] . '/' . getMainPicProd($produit['id']) . '" alt="' . $produit['libelle'] . '" title="Cliquer ici pour accéder au détail du produit"  height="150" width="140" > ' ;?>
                                    </a>
                                </figure>
                            </div>
                            <!-- Bloc réservé pour afficher le titre et la description du produit avec un lien pour aller au descriptif du produit-->
                            <div class="col-lg-3  text-start" title="Cliquer ici pour accéder au détail du produit">
                                <?php echo '<a class="titre_article" href="./detailProduit.php?id=' . $produit['id'] . '"><h3>' . $produit['libelle'] . '</h3></a>'; ?>
                                <br>    
                                <?php echo '<p class="text-truncate">' . $produit['_description'] . '</p>'; ?>
                            </div>

                            <!-- Bloc réservé pour afficher le nombre de quantité et valider pour mettre le prix a jour -->
                            <div class="quantity col-lg-3 d-flex ">
                                <?php 
                                if ($produit_panier['nombre']<1){
                                    echo "<p class='pasEnStock'>Cet article n'est plus en stock</p>"; 
                                }
                                else{
                                
                                    echo "<form action='panier.php' class='controle_quantite counter' method='post'>";

                                        // boutons appelant une fonction js pour enlever un produit parmi la quantité quil y avait -->';
                                        echo '<div class="d-flex justify-content-center" >';
                                        echo "<span class='down' onClick='decreaseCount(event, this)''>-</span>";
                                        //Input au niveau de la quantité -->';
                                        echo '<input type="text" name="quantity" value='. $produit_panier["nombre"].' min="0" >';
                                        //pariel que pour le bouton - mais en + -->';
                                        echo '<span class="up" onClick="increaseCount(event, this)">+</span>';
                                        echo '</div>';
                                        
                                        // Bouton permettant de valider la quantité -->';
                                        echo '<input class="valider_quantite" type="submit" name="quantity'. $produit_panier['id_produit'] .'" value="Valider" >';
                                    
                                    echo '</form>';
                                }
                                ?>
                                <!-- <div class="w-100"></div> -->
                                
                            </div>

                            <!-- Bloc réservé pour afficher les prix TTC et TVA -->
                            <div class="col-lg-2 align-self-center prix_tva">
                                     
                                <?php
                                   if ($estRemise != false) {
                                        // Calcul des prix ttc
                                        $prixTTC = round(($remiseHT*getTauxTva($produit["libelle_categorie"]))*$produit_panier['nombre'],2);
                                        $prixHT= round(($remiseHT)*$produit_panier['nombre'],2);
                                    }
                                    else {
                                        $prixTTC= round(($produit['prix_ht'] * getTauxTva($produit['libelle_categorie']))*$produit_panier['nombre'],2);
                                        $prixHT= round($produit['prix_ht'] *$produit_panier['nombre'],2);
                                    }
                                    // Calcul du prix tva et ht et ttc
                                    $prixTVA= $prixTTC-$prixHT;
                                    $prixTotalTTC= round(($prixTotalTTC+$prixTTC),2);
                                    $prixTotalHT= round(($prixTotalHT+$prixHT),2);
                                    $prixTotalTVA= round(($prixTotalTVA+$prixTVA),2);
                                    $nombreTotal=$nombreTotal+$produit_panier['nombre'];
                                    $nombreArticle=$nombreArticle+1;
                                    $taux_tva=(getTauxTva($produit['libelle_categorie'])-1)*100;
                                    // Affiche le prix TTC
                                    echo '<p> TTC: '. $prixTTC .'€</p>';
                                    $taux_tva=(getTauxTva($produit['libelle_categorie'])-1)*100;
                                    // Affiche le prix TVA
                                    echo '<p> TVA: '. $taux_tva .'%</p>';
                                ?>
                                 
                            </div>             
                                     
                            
                            <!-- Bloc reservé pour supprimer le produit avec n'importe quel quantité du panier -->
                            <div class="divSuppr col-lg-2 align-self-center">
                                
                                <!--Bouton de suppression de l'item -->
                                <form action="panier.php" method="post">
                                    <?php echo'<input class="bouton_supprimer" type="submit" name="delete'. $produit_panier['id_produit'] .'" value="Supprimer" >';?>
                                    
                                </form>
                                
                            </div>
                                     
                            
                        </div>
                        <hr> 
                        <?php
                        }
                    }
             
                        if ($nombreArticle==0) {  //Message si le panier est vide
                            echo '<p>Votre panier est vide<p>';
                        } ?>
                </div>
                <!-- Bloc réservé pour payer -->
                <div id="Partie_Droite" class="col-lg-3 col-md-3  ">
                    <!-- block reservé pour afficher les prix du panier -->
                    <aside id="passe_commande" class="row">

                        <!-- Bloc reservé pour afficher juste le texte sans le prix mais il sera à côté -->
                        <div id="les_mots" class="col-4 col-md-5  text-left">
                            <p>Produits</p>
                            <p>TVA</p>
                            <p>Livraison</p>
                        </div>
                        
                        <!-- Bloc reservé pour afficher les prix avec deux chiffre après la virgule-->
                        <div id="les_prix" class="col-8 col-md-7  text-right">
                            <?php
                            echo "<p>". sprintf("%.02f", $prixTotalHT) ."€</p>";
                            echo "<p>". sprintf("%.02f", $prixTotalTVA) ."€</p>";
                            echo "<p>gratuite</p>";
                            ?>
                        </div>
                        <hr>

                        <!-- Bloc reservé pour afficher juste total -->
                        <div id="les_mots" class="col-4 text-left">
                            <p>Total</p>
                        </div>

                        <!-- bloc reservé pour afficher le prix du total -->
                        <div id="les_prix" class="col-8 text-right">
                            <?php echo "<p>". sprintf("%.02f", $prixTotalTTC) ."€</p>"; ?>

                            
                        </div>



                    </aside>  
                    <div>
                        <?php
                            // si l'utilisateur a des produit dans son panier
                            if ($nombreTotal!=0) {
                                // SI la personne est connecter alors elle pourra payer
                            if ($_SESSION['connect'] == 1){
                                ?> <a id="bouton_passe_commande" href="./script/cartToPayment.php" class="btn d-lg-block d-flex btn-secondary text-wrap justify-content-center" role="button">Passer la commande</a> <?php
                            }
                            // Sinon le bouton payer renvoie a la page de connexion
                            else{
                                $_SESSION['connect_panier']=true; //variable servant a revenir au panier après la connexion
                                ?> <a id="bouton_passe_commande" href="connexion.php" class="btn d-lg-block btn-secondary"role="button">Passer la commande</a> <?php
                            }

                        }?>
                    </div>
                    
                    <div>
                        <?php
                            // si l'utilisateur a des produits dans son panier on affiche la poubelle permettant de vider le panier
                            if ($nombreTotal!=0) {?>
                            <aside id="vider_panier" class = "d-flex justify-content-center ">
                                <form method="post" action="panier.php">
                                    <button id="poubelle" type="submit" name="vider" value="Vider le panier">
                                        <img src="images/icones/poubelle.png" alt="icone poubelle">
                                    </button>
                                    <input type="submit" name="vider" value="Vider le panier">
                                </form>
                            </aside> 
                        <?php
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>

        
        
    </main>
    

    <?php require('footerContent.html'); ?>
</body>


</html>


<script>

    // Fonction permettant d'ajouter de un le produit
function increaseCount(a, b) {
    // Frère direct du b
  var input = b.previousElementSibling;
  var value = parseInt(input.value, 10);
  value = isNaN(value) ? 0 : value;
  
  if (value < <?php echo $produit['stock']?>){
    value++;
    input.setAttribute("value",value) // met la valeur de value dans le value de l'html

  }
  
  
}
    // Fonction permettant de diminuer de un le produit
function decreaseCount(a, b) {
    // Frère direct du b
  var input = b.nextElementSibling;
  var value = parseInt(input.value, 10);
  if (value > 0) {
    value = isNaN(value) ? 0 : value;
    value--;
    input.setAttribute("value",value) // met la valeur de value dans le value de l'html
  }
}
    
    

</script>