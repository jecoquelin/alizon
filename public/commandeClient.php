<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<!--HEAD-->
<head>
    <title>Mon profil</title> 
    <?php require('head.php'); 
     require('script/product.php');
    ?>
    <?php
        // Redirection vers l'accueil si l'internaute n'est pas connecté
        if ($_SESSION['connect'] == false){
            header("Location: ./index.php");
        }
    ?>
    <link href="commandeClient.css" rel="stylesheet">
</head>

<!--BODY-->
<body>
    <!--HEADER-->
    <?php include('headerContent.php'); ?>

    <!--Récupération de l'id client-->
    <?php $idClient = $_SESSION['idClient'] ;?>

    <!--MAIN-->
    <main class="container mt-5">
        <!--COMMANDE EN COURS-->
        <section class="row col-lg-12 col-xl-10">
            <h2>En cours</h2>
            <?php
                global $dbh ;
                $sth = $dbh->prepare('SELECT id, date_commande, etat_l from alizon._commande where id_client = ?');
                $sth -> execute(array($idClient));
                $commandes = $sth -> fetchAll();
                majEtatCommandeSimu($commandes);

                $sth = $dbh->prepare('SELECT id, date_commande, etat_l from alizon._commande where id_client = ? and etat_l != ?');
                $sth -> execute(array($idClient, 3));
                $commandes = $sth -> fetchAll();
                // Si aucune commandes n'est en cours, il affiche un message
                if (count($commandes) == 0){
                    echo '<p>Aucune commandes n\'est en cours</p>';
                }
                else {
                    // Récupère dans un premier temps les ID des commandes 
                    // Puis dans chaque commande les différents produits 
                    foreach ($commandes as $commande) {
                        echo '<article><h3>Commande '.$commande['id'].'</h3>' ;
                        echo '<details>';
                        echo '<summary>Voir les produits</summary>';
                        // On récupère les ID des produits présent de la commande
                        // $produits est une liste de tableau de produit qui contient uniquement l'id_produit
                        $sth = $dbh->prepare('SELECT * from alizon._est_commande where id_commande = ?');
                        $sth -> execute(array($commande['id']));
                        $produits = $sth -> fetchAll();
                        $prixTotalTTC = 0 ;
                        // Début de liste des produit présent dans la commande
                        // On affiche et calcul les différentes informations si manquante
                        foreach ($produits as $produit) {
                            $id = $produit['id_produit'];
                            $quantite = $produit['quantite'];
                            $prixTTC= $produit['prix_ttc'] ;
                            $libelle = getLibelle($id) ;
                            $prixTotalTTC += $prixTTC ;
                        
            ?>
                            <!--Détail d'un produit commandé-->
                            <div class="row justify-content-start produit">   
                                <div class="col-sm-5 col-lg-4">
                                    <figure class="col-sm">
                                        <?php echo '<img class="rounded" src="images/produits/'. $id . '/' . getMainPicProd($id) . '" alt="' . $libelle . '" title="' . $libelle . '"  height="150" width="140" > ' ;?>
                                    </figure>
                                </div>
                                <div class="col-sm-7 col-lg-8 ">
                                    <?php echo '<h3 class="text-left">' . $libelle . '</h3>'; ?>
                                    <br>    
                                    <div class="row">
                                        <p class="col-5"> <?php echo 'Quantité : ' . $quantite ; ?> </p>
                                        <p class="col-5 offset-auto"> <?php echo 'Prix unitaire TTC : '.round($prixTTC/$quantite,2).'€' ; ?> </p>
                                    </div>
                                    <?php echo '<p>Total article.s : '.$prixTTC.'€</p>';?>
                                </div>  
                            </div>
            <?php
                        }
                        
                        echo '</details>' ;
                        // Récapitulatif de la commande entière
                        echo '<div class="row">' ;
                        echo '<p class="col-5">Date de la commande : '.$commande['date_commande'].'</p>' ;
                        echo '<p class="text-start">Total commande TTC : '.$prixTotalTTC.' €</p>';
                        echo '<p class="col-5">Etat : '.getStringEtatCommande($commande['etat_l']).'</p>';
                        echo '</div>' ;
                        echo '<hr>' ;
                        echo '</article>' ;
                    }
                                
                } 
            ?>
        </section>

        <!--COMMANDE TERMINEE-->
        <section>
            <h2>Terminé</h2>
            <?php
            // Récupère toutes les commandes d'un client qui est terminée
                $sth = $dbh->prepare('SELECT id, date_commande, etat_l from alizon._commande where id_client = ? and etat_l = ?');
                $sth -> execute(array($idClient, 3));
                $commandes = $sth -> fetchAll();
                // Si aucune commandes n'est en cours, il affiche un message
                if (count($commandes) == 0){
                    echo '<p>Aucune commande n\'est terminé</p>';
                }
                else {
                    // Récupère dans un premier temps les ID des commandes 
                    // Puis dans chaque commande les différents produits 
                    foreach ($commandes as $commande) {
                        echo '<article><h3>Commande '.$commande['id'].'</h3>' ;
                        echo '<details>';
                        echo '<summary>Voir les produits</summary>';
                        // On récupère les ID des produits présent de la commande
                        // $produits est une liste de tableau de produit qui contient uniquement l'id_produit
                        $sth = $dbh->prepare('SELECT * from alizon._est_commande where id_commande = ?');
                        $sth -> execute(array($commande['id']));
                        $produits = $sth -> fetchAll();
                        $prixTotalTTC = 0 ;
                        // Début de liste des produit présent dans la commande
                        // On affiche et calcul les différentes informations si manquante
                        foreach ($produits as $produit) {
                            $id = $produit['id_produit'];
                            $quantite = $produit['quantite'];
                            $prix_unitaire = getPrixUnitaireTTC($id);
                            $prixTTC= $produit['prix_ttc'] ;
                            $libelle = getLibelle($id) ;
                            $prixTotalTTC += $prixTTC ;
            ?>
                            <!--Détail d'un produit commandé-->
                            <div class="row justify-content-start produit">   
                                <div class="col-sm-5 col-lg-4">
                                    <figure class="col-sm">
                                        <?php echo '<img class="rounded" src="images/produits/'. $id . '/' . getMainPicProd($id) . '" alt="' . $libelle . '" title="' . $libelle . '"  height="150" width="140" > ' ;?>
                                    </figure>
                                </div>
                                <div class="col-sm-7 col-lg-8 ">
                                    <?php echo '<h3 class="text-left">' . $libelle . '</h3>'; ?>
                                    <br>    
                                    <div class="row">
                                        <p class="col-5"> <?php echo 'Quantité : ' . $quantite ; ?> </p>
                                        <p class="col-5 offset-auto"> <?php echo 'Prix unitaire TTC : '.$prix_unitaire.'€' ; ?> </p>
                                    </div>
                                    <?php echo '<p>Total article.s : '.$prix_unitaire*$quantite.'€</p>';?>
                                </div>  
                            </div>
            <?php
                        }
                        
                        echo '</details>' ;
                        // Récapitulatif de la commande entière
                        echo '<div class="row">' ;
                            echo '<p class="col-5">Date de la commande : '.$commande['date_commande'].'</p>' ;
                            echo '<p class="text-start">Total commande TTC : '.$prixTotalTTC.' €</p>';
                            echo '<p class="col-5">Etat : '.getStringEtatCommande($commande['etat_l']).'</p>';
                        echo '</div>' ;
                        echo '<hr>' ;
                        echo '</article>' ;
                    }
                                
                }
            ?>
        </section>
    </main>

    <!--FOOTER-->
    <?php
        include('footerContent.html');
    ?>
</body>
</html>




