<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<!--HEAD-->
<head>
    <title>Mon profil</title> 
    <?php require('head.php'); ?>
    <?php
    // Redirection si l'internaute n'est pas connecté
        if ($_SESSION['connect'] == false){
            header("Location: ./connexion.php");
        }
    ?>
    <link href="profil.css" rel="stylesheet">
</head>

<!--BODY-->
<body>
    <!--HEADER-->
    <?php include('headerContent.php'); ?>
    <!--MAIN-->
    <main>
        <!--Partie principale de la page-->
        <div class="container" >
            <div class="row">
                <section class="my-4 p-3">
                    <!--Récupération de l'ID du client-->
                    <?php $idClient = $_SESSION["idClient"];?>
                    <article class="justify-content-center align-item-center">

                    <!-- script permettant de modifier ou non la photo de profil-->
                    <?php
                        if (isset($_FILES['profilePicture'])){
                            modifierPhotoDeProfil($idClient, $_FILES['profilePicture']);
                        }
                    ?>

                    <!-- script permettant de modifier les informations personnelles ou non-->
                    <?php  
                        if (isset($_POST["formInfoPerso"])){
                            modifierInfoPersonnelles($_POST['pseudo'], $_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['date_naissance'], $_POST['numero_voie'], $_POST['nom_voie'], $_POST['ville'], $_POST['code_postal'], $_POST['batiment'], $_POST['etage'], $_POST['numero_porte'], $idClient) ;
                        } 
                    ?>

                    <!-- script permettant la modification de mot de passe-->
                    <?php
                        if (isset($_POST['formMDP'])){
                            modifierMotDePasse($_POST['mdp'], $_POST['new_mdp'], $_POST['new_mdp_conf'], $idClient) ;
                        }
                    ?>

                    <!--Récupération des données liées aux clients-->
                    <?php $profil = getClientById($_SESSION["idClient"]);?>

                    
                        <!--Profil lorsque l'écran est petit-->
                        <figure class="my-2 text-center d-md-none">
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="menuImageP" data-bs-toggle="dropdown" aria-expanded="false">
                                <?php echo '<img class="photoProfil rounded-circle" src="./images/photoProfil/'.$idClient.'/'.getMainPicClient($idClient).'" alt="'.$profil['pseudo'].'" title="'.$profil['pseudo'].'">'; ?>
                                </button>
                                <!--Menu apparaissant losque l'on clique sur la photo de profil-->
                                <div class="dropdown-menu" aria-labelledby="menuImageP">
                                    <form action="profil.php" method="POST" enctype="multipart/form-data">
                                        <label>Fichier (.png, .jpeg, .jpg, .gif)</label>
                                        <input type="file" name='profilePicture' accept=".png, .jpeg, .jpg, .gif">
                                        <button type="submit">Enregistrer</button>
                                    </form> 
                                </div>
                            </div>
                            <?php echo '<p>Bonjour, '.$profil['nom'].' '.$profil['prenom'].'</p>'; ?>
                        </figure>
                        
                        <!--Profil lorsque l'écran est grand-->
                        <figure id="menuImageGrand" class="my-2 text-center d-none d-md-block">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="menuImageG" data-bs-toggle="dropdown" aria-expanded="false">
                                    <?php echo '<img class="photoProfil rounded-circle" src="./images/photoProfil/'.$idClient.'/'.getMainPicClient($idClient).'" alt="'.$profil['pseudo'].'" title="'.$profil['pseudo'].'">'; ?>
                                    </button>
                                    <!--Menu apparaissant losque l'on clique sur la photo de profil-->
                                    <div class="dropdown-menu" aria-labelledby="menuImageG">
                                        <form action="profil.php" method="POST" enctype="multipart/form-data">
                                            <label>Fichier (.png, .jpeg, .jpg, .gif)</label>
                                            <input type="file" name='profilePicture' accept=".png, .jpeg, .jpg, .gif">
                                            <button type="submit">Enregistrer</button>
                                        </form> 
                                    </div>
                                </div>
                                <?php echo '<p>Bonjour, '.$profil['nom'].' '.$profil['prenom'].'</p>'; ?>                            
                        </figure>   
                    </article>
 
            
                    <!--Informations personnelles liées au client-->
                    <article id="infoPerso">
                        <h3>Informations personnelles</h3>
                        <!--Affichage normal de lecture des informations-->
                        <div id="affichInfoPerso">
                            <ul>
                                <?php echo '<li>Pseudo : '.$profil['pseudo'].'</li>'; ?>
                                <?php echo '<li>Nom : '.$profil['nom'].'</li>'; ?>
                                <?php echo '<li>Prénom : '.$profil['prenom'].'</li>'; ?>
                                <?php echo '<li>Date de naissance : '.$profil['date_naissance'].'</li>'; ?>
                                <?php echo '<li>Adresse mail : '.$profil['email'].'</li>'; ?>
                                <?php echo '<li> <strong>Adresse de livraison </strong>
                                                <ul>
                                                     <li>Numero de voie : '.$profil['numero_voie'].'</li>
                                                     <li>Nom de voie : '.$profil['nom_voie'].'</li>
                                                     <li>Ville : '.$profil['ville'].'</li>
                                                     <li>Code postal : '.$profil['code_postal'].'</li>
                                                     <li>Batiment : '.$profil['batiment'].'</li>
                                                     <li>Etage : '.$profil['etage'].'</li>
                                                     <li>Numero_porte : '.$profil['numero_porte'].'</li>
                                                </ul>
                                            </li>';
                                ?>
                            </ul>
                            <a class="btn-secondary " role="button" onclick="modifierInfoPerso()">Modifier</a>
                        </div>
                        <!--Affichage du formulaire de modification lié au informations personnelles-->
                        <form id="formInfoPerso" action="./profil.php" method="POST">
                            <ul>
                                <?php echo '<li><label>Pseudo :             </label><input type="text"  name="pseudo"            value="'.$profil['pseudo'].'">      </li>'; ?>
                                <?php echo '<li><label>Nom :                </label><input type="text"  name="nom"               value="'.$profil['nom'].'">         </li>'; ?>
                                <?php echo '<li><label>Prénom :             </label><input type="text"  name="prenom"            value="'.$profil['prenom'].'">      </li>'; ?>
                                <?php echo '<li><label>Date de naissance :  </label><input type="date"  name="date_naissance"    value="'.$profil['date_naissance'].'"></li>'; ?>
                                <?php echo '<li><label>Adresse mail :       </label><input type="email"  name="email"            value="'.$profil['email'].'" required>       </li>'; ?>
                                <?php echo '<li> <strong>Adresse de livraison </strong> 
                                                <ul>
                                                     <li><label>Numero de voie :</label><input type="text"  name="numero_voie"   value="'.$profil['numero_voie'].'"> </li>
                                                     <li><label>Nom de voie :   </label><input type="text"  name="nom_voie"      value="'.$profil['nom_voie'].'">    </li>
                                                     <li><label>Ville :         </label><input type="text"  name="ville"         value="'.$profil['ville'].'">       </li>
                                                     <li><label>Code postal :   </label><input type="text"  name="code_postal"   value="'.$profil['code_postal'].'"> </li>
                                                     <li><label>Batiment :      </label><input type="text"  name="batiment"      value="'.$profil['batiment'].'">    </li>
                                                     <li><label>Etage :         </label><input type="text"  name="etage"         value="'.$profil['etage'].'">       </li>
                                                     <li><label>Numero_porte :  </label><input type="text"  name="numero_porte"  value="'.$profil['numero_porte'].'"></li>
                                                </ul>
                                                </li>';
                                ?>
                            </ul>
                            <a class="btn-secondary"    role="button" onclick="afficherInfoPerso()">Annuler</a>
                            <input class="btn-secondary"  type="submit"        value="Valider" name="formInfoPerso">
                        </form>

                    </article>

                    <!--Donnée lié au client-->
                    <article id="donneeEtConfidentialite">
                        <h3>Données et confidentialité</h3>
                        <ul>
                            <li>Cookies : Autorisé</li>
                            <li>Historique des achats : Afficher</li>
                        </ul>
                    </article>
            
                    <!--Sécurité / Mot de passe-->
                    <article id="securite">
                        <h3>Sécurité</h3>
                        <!--Consultation des informations de sécurité-->
                        <div id='infoMotDePasse'>
                            <ul>
                                <li>Mot de passe : *********</li>
                            </ul>
                            <a class="btn-secondary" onclick="modifierMotDePasse()" role="button">Modifier</a>
                        </div>

                        <!--Formulaire de modification de mot de passe-->
                        <div id='formMotDePasse'>
                            <form action="./profil.php" method="POST">
                                <label>Mot de passe actuel : </label>                       <input type="password" name="mdp" ><br>
                                <label>Nouveau mot de passe : </label>                      <input type="password" name="new_mdp" onkeydown="afficher();" onkeyup="CheckMdp(this.value)"><br>
                                <label>Confirmation nouveau mot de passe : </label>         <input type="password" name="new_mdp_conf"><br>
                                <a class="btn-secondary"    role="button"   onclick="afficherMotDePasse(), cache()">Annuler</a>
                                <input class="btn-secondary" type="submit"        value="Valider" name="formMDP">
                            </form>
                        </div>

                        <!--Affichage des caractères manquant du nouveaux mot de passe-->
                        <div id="error">
                            <span id = "msg1"></span><br>
                            <span id = "msg2"></span><br>
                            <span id = "msg3"></span><br>
                            <span id = "msg4"></span><br>
                            <span id = "msg5"></span>
                        </div>

                    </article>
                      
                    <!--Script JS pour les informations lié au nouveau mot de passe-->
                    <script src="./script/password.js"></script>

                    <!--Supression d'un compte-->
                    <article id="supressionCompte">
                        <h3>Suppression de compte</h3>
                        <a class="btn-danger" href="./supprimerCompte.php" role="button">Supprimer</a>
                    </article>
                </section>
            </div>
        </div>
    </main>

    <!--FOOTER-->
    <?php require('footerContent.html'); ?>

    <!--Script pour les éléments dynamique de la page (apparition des formulaires ou de la simple consultation)-->
    <script src="./script/profil.js"></script>
</body>
</html>
