<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Alizon</title>
        <!-- on importe head pour avoir l'instance de la connexction a la bdd et les autre dépendances de la page -->
        <?php require('head.php'); ?>
        <?php include('./script/product.php'); ?>
        <?php include('./script/seller.php'); ?>
        <!-- on n'a pas besoins de rediriger la conection vers le panier -->
        <?php $_SESSION['connect_panier']=false; ?>
        <link rel="stylesheet" type="text/css" href="index.css" media="screen">
        <script src="https://code.jquery.com/jquery-3.6.3.min.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <?php require('headerContent.php'); ?>
        <main class="container-fluid">
            <!-- c'est un element html qui sert juste à js pour retenir la valeur du scroll de l'utilisateur quand il ajoute un produit au panier dpuis cette page -->
            <div id="scroll" value=<?php if (isset($_GET["scroll"])) {
                echo $_GET["scroll"];
            } ?>></div>
            <?php
            // on utilise une variable GET pour savoir si un produit viens d'être ajouter au panier pour afficher une notifaction
            if (isset($_GET['add'])){
                if($_GET['add'] =='Y'){
                    echo '<div class="card"id="notif">';
                    echo '<h6>Article ajouté au panier</h6>';
                    echo '<a href="./panier.php" class="btn d-lg-block btn-secondary"  role="button">voir mon panier</a>';
                    echo '</div>';
                }
            }
            $idPanier = $_SESSION['idPanier'] ;
            ?>
            <div id="emplacement_notif"></div>
            <!-- le carousel avec les meilleures ventes -->
            <div class="row justify-content-center">
                <div class="col-10 my-3 meilleurVente">
                    <div class="col d-flex justify-content-center text-center">
                        <a href="./categorie.php?libelle=meilleuresVentes" class="lienMeilleurVente"><h1 class="py-3">Meilleures ventes</h1></a>
                    </div> 
                    <div id="carouselExampleCaptions" class="carousel carousel-dark slide" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner flex-nowrap">
                            <!-- Carousel -->
                            <?php
                                // on récupère les produit les produits les plus vendu pour les afficher dans le carousel
                                $produitsLesPlusVendus = getBestSellers(3);
                                $premiereSlide = true;
                                // cette variable sert à spécifier un id html pour chaque produit dans les slide du arousel
                                $inc=0;
                                foreach ($produitsLesPlusVendus as $produit) {
                                    // si le produit à une remise on récupère son pourcentage
                                    $estRemise = getRemise($produit['id']);
                                    $remiseHT = $produit['prix_ht']*((100-getRemise($produit['id']))/100); 
                                    $inc++;
                                    // simple condition pour empecher d'avoir plusiere premiere slide
                                    if ($premiereSlide == true){
                                        $premiereSlide = false;
                                        echo '<a class="carousel-item active" href="./detailProduit.php?id=' . $produit['id'] . '">';
                                    } else {
                                        echo '<a class="carousel-item" href="./detailProduit.php?id=' . $produit['id'] . '">';
                                    }
                                    // image du prod
                                    echo '<img class="w-25 d-block rounded my-2" src="./images/produits/' . $produit['id_photos'] . '/' . getMainPicProd($produit['id']) . '" alt="' . $produit['libelle'] . '" title="' . $produit['libelle'] .'">';
                                    // texte du produit
                                    echo '<div class="carousel-caption">';
                                    echo '<h2>' . $produit['libelle'] . '</h2>';
                                    echo '<p class="my-2 desc">' . getDesc250($produit['_description']) .'</p>';
                                    //nom vendeur
                                    echo '<p class="vendeur">vendu par : <span class="lienVendeur">'. getVendeurByProduit($produit)["nom"] .'</span></p>';
                                    //prix
                                    echo '<p id="prix',$inc,'" class="prix">TTC ' . round($produit['prix_ht'] * getTauxTva($produit["libelle_categorie"]), 2) . '€</p>';
                                    // prix remiser ou non, affiché différement
                                    if ($estRemise != false) {
                                        echo '<p class="prix prixRemise">TTC '.round(($remiseHT*getTauxTva($produit["libelle_categorie"])),2).'€ <span id="pourcentageRemise">-'.getRemise($produit['id']).'%</span></p>';
                                        echo '<style> #prix',$inc,'{text-decoration: line-through;} </style>';
                                    }
                                    echo '<p id="prix_ht',$inc,'" class="prix_ht">HT ' . round($produit['prix_ht'], 2) . '€</p>';
                                    if ($estRemise != false) {
                                        echo '<p id="prix_remise_ht" class="prixRemise">HT '.round(($remiseHT),2)."€</p>";
                                        echo '<style> #prix_ht',$inc,'{text-decoration: line-through;} </style>';
                                    }
                                    echo '<p class="tva">TVA ' . round((getTauxTva($produit["libelle_categorie"])-1)*100, 2) . '%</p>';
                                    echo '</div>';
                                    echo '</a>';
                                }
                                ?>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- les nouveautés et les promotions -->
            <div class="row flex-nowrap justify-content-center">
                <article class="col-5 text-center my-2 nouveauPorduitsEtNouveaute">
                    <div class="row my-2 justify-content-center">
                        <div class="col-md-6">
                            <a href="./categorie.php?libelle=nouveautes"><h3 class="py-2">Nouveautés</h3></a>
                        </div>
                    </div>
                    <!-- Les nouveaux produits -->
                    <div class="row">
                        <?php
                            $nouveauxProduits = getNewProduct();
                            foreach ($nouveauxProduits as $produit) {
                                $estRemise = getRemise($produit['id']);
                                $remiseHT = $produit['prix_ht']*((100-getRemise($produit['id']))/100); 
                                $inc++;
                                echo '<div class="contenuCat col-md text-center px-1">';
                                echo '<a href="./detailProduit.php?id=' . $produit['id'] . '">';
                                echo '<figure><img class="rounded mx-1" src="./images/produits/' . $produit['id_photos'] . '/' . getMainPicProd($produit['id']) . '" alt="' . $produit['libelle'] . '" title="' . $produit['libelle'] .'">';
                                echo '<figcaption>' . $produit['libelle'] . '</figcaption></figure></a>';
                                echo '<div class="row">';
                                echo '<div class="col-md">';
                                // on empeche d'ajouter au panier si le stock du produit est trop bas
                                $nbMax=maxAjoutPanier($produit, $idPanier);
                                if ($nbMax > 0) { // produit dispo
                                    echo '<p class="enStock">En stock</p>';
                                    echo '</div>';
                                    echo '</div>';
                                    //nom vendeur
                                    echo '<div class="row">';
                                    echo '<div class="col-md">';
                                    echo '<p class="vendeur">vendu par : <a class="lienVendeur" href="./detailVendeur.php?id='.$produit['id_vendeur'].'&idproduit='.$produit['id'].'">'. getVendeurByProduit($produit)["nom"] .'</a></p>'; 
                                    echo '</div>';
                                    echo '</div>';
                                    echo '<div class="row">';
                                    echo '<div class="col-md d-flex flex-column">';
                                    echo '<p id="prix',$inc,'" class="prixCat">' . round($produit['prix_ht'] * getTauxTva($produit["libelle_categorie"]), 2) . '€</p>';
                                    if ($estRemise != false) {
                                        echo '<p class="prixCat prixRemise">'.round(($remiseHT*getTauxTva($produit["libelle_categorie"])),2).'€ </p>';
                                        echo '<style> #prix',$inc,'{text-decoration: line-through;} </style>';
                                    }
                                    echo '</div>';
                                    echo '<div class="col-md">';
                                    echo '<a class="lienAjoutPanier" href="./script/addToCart.php?quantity=1&idProd=' . $produit['id'] . '">';
                                    echo '<img class="imgAjoutPanier" src="./images/icones/ajoutPanier.png" alt="ajouter au panier"/>';
                                    echo '</a>';
                                    echo '</div>';
                                    echo '</div>';
                                    echo '</div>';
                                } else { // produit pas dispo
                                    echo '<p class="pasEnStock">Non disponible</p>';
                                    echo '</div>';
                                    echo '</div>';
                                    //nom vendeur 
                                    echo '<div class="row">';
                                    echo '<div class="col-md">';
                                    echo '<p class="vendeur">vendu par : <a class="lienVendeur" href="./detailVendeur.php?id='.$produit['id_vendeur'].'&idproduit='.$produit['id'].'">'. getVendeurByProduit($produit)["nom"] .'</a></p>';
                                    echo '</div>';
                                    echo '</div>';
                                    echo '<div class="row">';
                                    echo '<div class="col-md">';
                                    echo '<p class="prixCat">' . round($produit['prix_ht'] * getTauxTva($produit["libelle_categorie"]), 2) . '€</p>';
                                    echo '</div>';
                                    echo '<div class="col-md">';
                                    echo '<a class="lienAjoutPanier" href="./script/addToCart.php?quantity=1&idProd=' . $produit['id'] . '" style="pointer-events:none" >';
                                    echo '<img class="imgAjoutPanierGris" src="./images/icones/ajoutPanier.png" alt="ajouter au panier"/>';
                                    echo '</a>';
                                    echo '</div>';
                                    echo '</div>';
                                    echo '</div>';
                                }
                                
                            }
                        ?>
                    </div>
                </article>
                <span class="col-1"></span>
                <article class="col-5 text-center my-2 nouveauPorduitsEtNouveaute">
                    <div class="row my-2 justify-content-center">
                        <div class="col-md-6">
                            <a href="./recherche.php?promo=1"><h3 class="py-2">Promotions</h3></a>
                        </div>
                    </div>
                    <!-- Les promos -->
                    <div class="row">
                        <?php
                            $produits = getPromotions();
                            foreach ($produits as $produit) {
                                $estRemise = getRemise($produit['id']);
                                $remiseHT = $produit['prix_ht']*((100-getRemise($produit['id']))/100); 
                                $inc++;
                                echo '<div class="contenuCat col-md text-center px-1">';
                                echo '<a href="./detailProduit.php?id=' . $produit['id'] . '">';
                                echo '<figure><img class="rounded mx-1" src="./images/produits/' . $produit['id_photos'] . '/' . getMainPicProd($produit['id']) . '" alt="' . $produit['libelle'] . '" title="' . $produit['libelle'] .'">';
                                echo '<figcaption>' . $produit['libelle'] . '</figcaption></figure></a>';
                                echo '<div class="row">';
                                echo '<div class="col-md">';
                                // on empeche d'ajouter au panier si le stock du produit est trop bas
                                $nbMax=maxAjoutPanier($produit, $idPanier);
                                if ($nbMax > 0) { // produit dispo
                                    echo '<p class="enStock">En stock</p>';
                                    echo '</div>';
                                    echo '</div>';
                                    //nom vendeur
                                    echo '<div class="row">';
                                    echo '<div class="col-md">';
                                    echo '<p class="vendeur">vendu par : <a class="lienVendeur" href="./detailVendeur.php?id='.$produit['id_vendeur'].'&idproduit='.$produit['id'].'">'. getVendeurByProduit($produit)["nom"] .'</a></p>';                                    
                                    echo '</div>';
                                    echo '</div>';
                                    echo '<div class="row">';
                                    echo '<div class="col-md d-flex flex-column">';
                                    echo '<p id="prix',$inc,'" class="prixCat">' . round($produit['prix_ht'] * getTauxTva($produit["libelle_categorie"]), 2) . '€</p>';
                                    if ($estRemise != false) {
                                        echo '<p class="prixCat prixRemise">'.round(($remiseHT*getTauxTva($produit["libelle_categorie"])),2).'€ </p>';
                                        echo '<style> #prix',$inc,'{text-decoration: line-through;} </style>';
                                    }
                                    echo '</div>';
                                    echo '<div class="col-md">';
                                    echo '<a class="lienAjoutPanier" href="./script/addToCart.php?quantity=1&idProd=' . $produit['id'] . '">';
                                    echo '<img class="imgAjoutPanier" src="./images/icones/ajoutPanier.png" alt="ajouter au panier"/>';
                                    echo '</a>';
                                    echo '</div>';
                                    echo '</div>';
                                    echo '</div>';
                                } else { // produit pas dispo
                                    echo '<p class="pasEnStock">Non disponible</p>';
                                    echo '</div>';
                                    echo '</div>';
                                    //nom vendeur
                                    echo '<div class="row">';
                                    echo '<div class="col-md">';
                                    echo '<p class="vendeur">vendu par : <a class="lienVendeur" href="./detailVendeur.php?id='.$produit['id_vendeur'].'&idproduit='.$produit['id'].'">'. getVendeurByProduit($produit)["nom"] .'</a></p>';                                    
                                    echo '</div>';
                                    echo '</div>';
                                    echo '<div class="row">';
                                    echo '<div class="col-md">';
                                    echo '<p class="prixCat">' . round($produit['prix_ht'] * getTauxTva($produit["libelle_categorie"]), 2) . '€</p>';
                                    echo '</div>';
                                    echo '<div class="col-md">';
                                    echo '<a class="lienAjoutPanier" href="./script/addToCart.php?quantity=1&idProd=' . $produit['id'] . '" style="pointer-events:none">';
                                    echo '<img class="imgAjoutPanierGris" src="./images/icones/ajoutPanier.png" alt="ajouter au panier"/>';
                                    echo '</a>';
                                    echo '</div>';
                                    echo '</div>';
                                    echo '</div>';
                                }
                                
                            }
                        ?>
                    </div>
                </article>
            </div>
            <!-- les différentes catégories avec des produits -->
            <?php
                // on récupère les 6 catégories avec le plus de produits
                $categories = getCatWithMaxProd(6);
                foreach ($categories as $cat) {
                    echo '<div class="row text-center justify-content-center">';
                        echo '<article class="my-2 col-10">';
                            echo '<div class="row justify-content-center my-2">';
                                // un lien vers la page de recherche pour la catégorie
                                echo '<div class="col-md-3">';
                                    echo '<a href="./recherche.php?cat[]=' . $cat . '&resetCat=1&search="><h3 class="py-2">' . $cat . '</h3></a>';
                                echo '</div>';
                            echo '</div>';
                            // Les prod d'une cat
                            echo '<div class="row justify-content-center">';
                                $nbProd = getNumberOfProdInCat($cat);
                                if ($nbProd == 0){
                                    echo '<p class="noProduit">Pas de produit '.$cat.' actuellement vendu !</p>' ;
                                }
                                else {
                                    $produits = getProduitByCatAndNb($cat, $nbProd);
                                    foreach ($produits as $produit) {
                                        $estRemise = getRemise($produit['id']);
                                        $remiseHT = $produit['prix_ht']*((100-getRemise($produit['id']))/100); 
                                        $inc++;
                                        echo '<div class="contenuCat col-md-2 text-center px-1">';
                                        echo '<a href="./detailProduit.php?id=' . $produit['id'] . '">';
                                        echo '<figure><img class="rounded mx-1" src="./images/produits/' . $produit['id_photos'] . '/' . getMainPicProd($produit['id']) . '" alt="' . $produit['libelle'] . '" title="' . $produit['libelle'] .'">';
                                        echo '<figcaption>' . $produit['libelle'] . '</figcaption></figure></a>';

                                        echo '<div class="row">';
                                        echo '<div class="col-md">';
                                        // on empeche d'ajouter au panier si le stock du produit est trop bas
                                        $nbMax=maxAjoutPanier($produit, $idPanier);
                                        if  ($nbMax > 0) { // produit dispo
                                            echo '<p class="enStock">En stock</p>';
                                            echo '</div>';
                                            echo '</div>';
                                            //nom vendeur
                                            echo '<div class="row">';
                                            echo '<div class="col-md">';
                                            echo '<p class="vendeur">vendu par : <a class="lienVendeur" href="./detailVendeur.php?id='.$produit['id_vendeur'].'&idproduit='.$produit['id'].'">'. getVendeurByProduit($produit)["nom"] .'</a></p>';                                        
                                            echo '</div>';
                                            echo '</div>';
                                            echo '<div class="row prixEtBouton">';
                                            echo '<div class="col-md d-flex flex-column">';
                                            echo '<p id="prix',$inc,'" class="prixCat">' . round($produit['prix_ht'] * getTauxTva($produit["libelle_categorie"]), 2) . '€</p>';
                                            if ($estRemise != false) {
                                                echo '<p class="prixCat prixRemise">'.round(($remiseHT*getTauxTva($produit["libelle_categorie"])),2).'€ </p>';
                                                echo '<style> #prix',$inc,'{text-decoration: line-through;} </style>';
                                            }
                                            echo '</div>';
                                            echo '<div class="col-md">';
                                            echo '<a class="lienAjoutPanier" href="./script/addToCart.php?quantity=1&idProd=' . $produit['id'] . '">';
                                            echo '<img class="imgAjoutPanier" src="./images/icones/ajoutPanier.png" alt="ajouter au panier"/>';
                                            echo '</a>';
                                            echo '</div>';
                                            echo '</div>';
                                            echo '</div>';
                                        } else { // produit pas dispo
                                            echo '<p class="pasEnStock">Non disponible</p>';
                                            echo '</div>';
                                            echo '</div>';
                                            //nom vendeur
                                            echo '<div class="row">';
                                            echo '<div class="col-md">';
                                            echo '<p class="vendeur">vendu par : <a class="lienVendeur" href="./detailVendeur.php?id='.$produit['id_vendeur'].'&idproduit='.$produit['id'].'">'. getVendeurByProduit($produit)["nom"] .'</a></p>';                                        
                                            echo '</div>';
                                            echo '</div>';
                                            echo '<div class="row prixEtBouton">';
                                            echo '<div class="col-md">';
                                            echo '<p class="prixCat">' . round($produit['prix_ht'] * getTauxTva($produit["libelle_categorie"]), 2) . '€</p>';
                                            echo '</div>';
                                            echo '<div class="col-md">';
                                            echo '<a class="lienAjoutPanier" href="./script/addToCart.php?quantity=1&idProd=' . $produit['id'] . '" style="pointer-events:none">';
                                            echo '<img class="imgAjoutPanierGris" src="./images/icones/ajoutPanier.png" alt="ajouter au panier"/>';
                                            echo '</a>';
                                            echo '</div>';
                                            echo '</div>';
                                            echo '</div>';
                                        }
                                    }
                                }
                                
                            echo '</div>';
                        echo '</article>';
                    echo '</div>';
                }
            ?>
        </main>
        <!-- footer -->
        <?php require('footerContent.html'); ?>
        <script>

            // on fait la liste des bouton qui permettent d'ajouter directement au panier (c'est des liens)
            var lienAjoutPanier = document.getElementsByClassName("lienAjoutPanier");

            // on ajoute un évènement pour gérer ce qu'il se passe quand on appuie sur le bouton
            // sans recharger la page
            let i = 0;
            for (let elem of lienAjoutPanier) {
                elem.addEventListener("click", async function(event) {
                    event.preventDefault();
                    // vérifier la réponse et afficher un message quand l'ajout se passe bien
                    const response = await fetch(elem.href + "&reload=non");
                    if (response.ok) {
                        let qteAjoutee = await response.text();
                        let div = '<div class="card"id="notif">';
                        if (qteAjoutee > 0) {
                            div += '<h6>Article ajouté au panier</h6>';
                        } else {
                            div += '<h6>Article non ajouté au panier</h6>';
                            let style = document.createAttribute("style");
                            style.value = "pointer-events:none";
                            elem.setAttributeNode(style);
                            let image = elem.firstChild;
                            image.setAttribute("class", "imgAjoutPanierGris");
                        }
                        div += '<a href="./panier.php" class="btn d-lg-block btn-secondary"  role="button">voir mon panier</a>';
                        div += '</div>';
                        $('#emplacement_notif').append(div);
                    } else {
                        let div = '<div class="card"id="notif">';
                        div += '<h6>Erreur lors de l\'ajout</h6>';
                        div += '<a href="./panier.php" class="btn d-lg-block btn-secondary"  role="button">voir mon panier</a>';
                        div += '</div>';
                        $('#emplacement_notif').append(div);
                    }
                });
                i++;
            }
        </script>
    </body>
</html>