<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
    
    <?php require('head.php'); ?>
    <?php require('script/product.php'); ?>
    <link rel="stylesheet" type="text/css" href="recapitulatifCommande.css" media="screen" />
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.10.1/html2pdf.bundle.min.js"
        integrity="sha512-GsLlZN/3F2ErC5ifS5QtgpiJtWd43JWSuIgh7mbzZ8zBps+dvLusV+eNQATqgA/HdeKFVgA5v3S/cIrLF7QnIg=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer"
    ></script>
    
    <title>Recapitucatif de commande</title>
    
</head>
<body>
    <?php
        if ($_SESSION['connect'] == false){
            header("Location: ./index.php");
        }
    ?>
    <?php require('headerContent.php'); ?>
    <main>
        <?php 
        $idClient = $_SESSION['idClient'];
        $idCommande = $_SESSION['idCommande'];
        global $dbh;
        /* Recuperation du nom et prenom du client dans la base */
        $sth = $dbh->prepare('SELECT nom,prenom from alizon._compte_client where id=?');
        $sth -> execute(array($idClient));
        $nomPrenom = $sth -> fetchAll();
        
        /* Recuperation de la commande du client dans la base */
        $sth = $dbh->prepare('SELECT * from alizon._commande where id=?');
        $sth -> execute(array($idCommande));
        $commande = $sth -> fetchAll();
        
        /* Recuperation des infos des produits dans la base */
        $sth = $dbh->prepare('select *, _produit.libelle as nom_produit from alizon._est_commande inner join alizon._Produit on alizon._est_commande.id_produit=alizon._Produit.id inner join alizon._categorie on alizon._Produit.libelle_categorie=alizon._categorie.libelle where id_commande=?');
        $sth -> execute(array($commande[0]['id']));
        $produits = $sth -> fetchAll();
                

        $sth2 = $dbh->prepare('select * from alizon._est_commande inner join alizon._Produit on alizon._est_commande.id_produit=alizon._Produit.id where id_commande=?');
        $sth2 -> execute(array($commande[0]['id']));
        $estCommande = $sth2 -> fetchAll();
        
        echo "<button class='btn btn-primary' onclick='genererPDF()'>Télécharger</button>";
        echo "<div id='PDF'>";
        echo "<h1 id='test' class=thxmessage>", $nomPrenom[0]['nom']," ",$nomPrenom[0]['prenom'],", merci pour votre achat !</h1>";
        echo "<hr>";
        echo "<div class='d-flex justify-content-around'>";
        echo "<div><p>Adresse de livraison : <br>",$commande[0]['numero_voie_l']," ",$commande[0]['nom_voie_l'],"<br> ",$commande[0]['code_postal_l']," ",$commande[0]['ville_l'],"</p></div>";
        echo "<div><p>Adresse de facturation : <br>",$commande[0]['numero_voie_f']," ",$commande[0]['nom_voie_f'],"<br> ",$commande[0]['code_postal_f']," ",$commande[0]['ville_f'],"</p></div>";
        echo "<div><p>Vendu par : <br>Alizon</p></div>";
        echo "</div>";
        echo "<hr>";
        echo "<div class='infoCommande'>";
        echo "<p>Date de commande : ",$commande[0]['date_commande'],"<br>Numéro de commande : ",$commande[0]['id'],"</p>";
        echo "</div>";
        
        $prixTTC=0;
        $prixHT=0;
        /* Verification des remises pour chaque produit et calcul des prix */
        foreach($produits as $prod) {
            $estRemise = getRemise($prod['id']);
            $remiseHT = $prod['prix_ht']*((100-getRemise($prod['id']))/100); 
             


            if ($estRemise != false) {
                $prixTTC = round(($remiseHT*getTauxTva($prod["libelle_categorie"]))*$prod["quantite"],2);
                $prixHT= round(($remiseHT)*$prod["quantite"],2);
            }
            else {
                $prixTTC= round(($prod['prix_ht'] * getTauxTva($prod['libelle_categorie']))*$prod["quantite"],2);
                $prixHT= round($prod['prix_ht'] *$prod["quantite"],2);
            }
            
            ?>
            <hr>
            <div class='list-prod d-md-flex justify-content-between'>
                <div class='d-md-flex'>
                    <?php 
                        echo "<div class='p-2 d-flex justify-content-center'><img class='fit-picture' src='images/produits/",$prod['id_photos'],"/",getMainPicProd($prod['id']),"' alt=''></div>";
                        echo "<div class='p-2'>";
                        echo "<div class='d-flex justify-content-center'><h2>",$prod["nom_produit"],"</h2></div>";
                        echo "<div ><p>Qté : ",$prod["quantite"],"</p></div>";
                        echo "</div>";
                    ?>
                    </div>
                    <?php
                    echo "<div class = 'col-md-2 d-flex justify-content-center'>
                            <p>Prix :<br> HT : ",$prixHT,"€<br>
                            Taux TVA : ",($prod["taux_tva"]-1)*100,"% <br>
                            Montant TVA : ",$prixTTC-$prixHT,"€<br>
                            TTC : ",$prixTTC,"€ </p>
                        </div>";
                    ?>
                </div>
            <?php 
        } 
        
        echo "<hr>";
        echo "<p class='finalPrice col-md-11'>Facture totale : ",$commande[0]['prix_total_ttc'],"€ TTC</p>";

        ?>
        </div>
    </main>
    <?php require('footerContent.html'); ?> 
    <script>
        
        function genererPDF() {
            // Choix du fichier PDF.
            const element = document.getElementById("PDF");
            // Parametre de conversion du fichier.
            const opt = {
                margin:2,
                filename:'commande.pdf',
                image:{type:'jpeg',quality:0.98},
                html2canvas:{scale:2},
                pagebreak: { mode: 'css' },
                jsPDF:{unit:'mm',format:'a4',orientation:'portrait'}
            };
            html2pdf().from(element).set(opt).save();
        }
        
    </script>
</body>
</html>