<?php
// Récupération de l'id du vendeur
    require('head.php');
    $info=$_GET;
    include("./script/seller.php");
    $vendeur = getVendeurById($info['id']);
?>

<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<!--HEAD-->
<head>
    <title>Profil vendeur</title> 
    <link href="detailVendeur.css" rel="stylesheet">
</head>

<body>
    <!-- HEADER -->
    <?php include('headerContent.php'); ?>
    
    <main>
        <!--Partie principale de la page-->
        <div class="container" >
            <div class="row">
                <section class="my-4 p-3">
                    
                    <figure>
                        <!-- Affiche une banière de son entreprise -->
                        <?php echo '<img src="./images/vendeur/photoProfil/'.$vendeur['id'].'/'.getMainPicVendeur($vendeur['id']).'" alt="'.$vendeur['nom'].'" title="'.$vendeur['nom'].'">' ;?>
                    </figure>
                    <article class="justify-content-center align-item-center">
                    <!-- Affiche la boutique du vendeur ainsi que sa descrition -->
                        <h3>Boutique <?php echo $vendeur['nom'];?> </h3>
                        <?php echo '<p>Texte de présentation du vendeur : '.$vendeur['texte_presentation'].'</p>'; ?>
                    </article>
            
                    <!--Informations personnelles liées au vendeur qui vend le produit -->
                    <article id="infoPerso">
                        <h3>Informations personnelles</h3>
                        <!--Affichage normal de lecture des informations-->
                        <div id="affichInfoPerso">
                            <ul>
                                <?php echo '<li>Raison sociale : '.$vendeur['nom'].'</li>'; ?>
                                <?php echo '<li>Adresse mail : '.$vendeur['email'].'</li>'; ?>
                                <?php echo '<li>Adresse du siège social : '.$vendeur['numero_voie'].' '.$vendeur['nom_voie'].', '.$vendeur['code_postal'].' '.$vendeur['ville'].'</li>';?>
                                <?php echo '<li>TVA Intracommunautaire : '.$vendeur['code_tva'].'&emsp;'.$vendeur['cle_tva'].'&emsp;'.$vendeur['siren'].''; ?>             
                                <?php echo '<li>Siren : '.$vendeur['siren'].'&emsp;'. $vendeur['nic'] .'</li>'; ?> 
                                <?php echo '<li>Note du vendeur : '.$vendeur['note_vendeur'].'</li>'; ?> 
                                
                            </ul>
                            <?php echo '<a class="btn-secondary" href="./detailProduit.php?id='.$info['idproduit'].'" role="button">Aller sur la page du produit</a>' ;?>
                        </div>
                    </article>
                </section>
            </div>
        </div>
    </main>

    <!--FOOTER-->
    <?php require('footerContent.html'); ?>

    <!--Script pour les éléments dynamique de la page (apparition des formulaires ou de la simple consultation)-->
    
</body>
</html>
</body>
