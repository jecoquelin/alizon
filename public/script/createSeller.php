<?php
    // ce script s'excute de bout en bout

    session_start();
    include('../connectParams.php');
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        // cette variable de session sert à stoquer les infos d'inscriptiond pour les restituer dans le formulaire si une erreur est commise
        $_SESSION['dataRegister'] = $_POST;

        // on vérifie que tous les champs sont bien remplie
        if (isset($_POST['nomVendeur'], $_POST['emailVendeur'], $_POST['voieVendeur'], $_POST['noVoieVendeur'], $_POST['postalVendeur'], $_POST['villeVendeur'],$_POST['password'],$_POST['secondpassword'],$_POST['descriptionVendeur'],$_POST['codeTVA'],$_POST['cleTVA'],$_POST['siren'],$_POST['nic'])) {
            // on regarde si les question secrete sont bien remplie
                    $email = $_POST['emailVendeur'];
                    $pass = $_POST['password'];
                    $passConfirm = $_POST['secondpassword'];
                    //on verifie que les champs correspondent à ce qui est attendu (mdp et confirmation, les champs numéric etc.)
                    if ($pass == $passConfirm){
                        $sql = "SELECT * FROM alizon._compte_vendeur where email = :email";
                        $result = $dbh->prepare($sql);
                        $result->bindValue(":email",$email,PDO::PARAM_STR);
                        $result->execute();
                        if ($result->rowCount() == 0) {
                            $nomVendeur = htmlspecialchars($_POST['nomVendeur']);
                            $emailVendeur = htmlspecialchars($_POST['emailVendeur']);
                            $voieVendeur = htmlspecialchars($_POST['voieVendeur']);
                            $hashedPassword = password_hash($pass, PASSWORD_DEFAULT);
                            $noVoieVendeur = htmlspecialchars($_POST['noVoieVendeur']);
                            $postalVendeur = htmlspecialchars($_POST['postalVendeur']);
                            if(!is_numeric($postalVendeur)){
                               header('Location: ./../../admin/inscriptionVendeur.php?erreur=3');
                            }else{
                                $villeVendeur = htmlspecialchars($_POST['villeVendeur']);
                                $codeTVA = htmlspecialchars($_POST['codeTVA']);
                                $cleTVA = htmlspecialchars($_POST['cleTVA']);
                                $siren = htmlspecialchars($_POST['siren']);
                                if(!is_numeric($siren)){
                                    header('Location: ./../../admin/inscriptionVendeur.php?erreur=4');
                                 }else{
                                    $description=$_POST['descriptionVendeur'];
                                    $nic = htmlspecialchars($_POST['nic']);
                                    if(!is_numeric($nic)){
                                        header('Location: ./../../admin/inscriptionVendeur.php?erreur=5');
                                     }else{
                                         echo is_numeric($nic);
                                        $note = rand(0,5);
                                        
                                        $sqlajout = "INSERT into alizon._compte_vendeur(nom, mdp, email, numero_voie, nom_voie, code_postal, ville, code_TVA, cle_TVA, siren, nic, texte_presentation, note_vendeur, active) values (:nom,:hashpassword,:email,:novoie,:voie,:postal,:ville,:code,:cle,:siren,:nic,:_description,:note, 1)";
                                        $resultajout = $dbh->prepare($sqlajout);
                                        $resultajout->bindValue(":nom",$nomVendeur,PDO::PARAM_STR);
                                        $resultajout->bindValue(":email",$emailVendeur,PDO::PARAM_STR);
                                        $resultajout->bindValue(":voie",$voieVendeur,PDO::PARAM_STR);
                                        $resultajout->bindValue(":hashpassword",$hashedPassword,PDO::PARAM_STR);
                                        $resultajout->bindValue(":novoie",$noVoieVendeur,PDO::PARAM_STR);
                                        $resultajout->bindValue(":postal",$postalVendeur,PDO::PARAM_INT);
                                        $resultajout->bindValue(":ville",$villeVendeur,PDO::PARAM_INT);                                
                                        $resultajout->bindValue(":code",$codeTVA,PDO::PARAM_INT);
                                        $resultajout->bindValue(":cle",$cleTVA,PDO::PARAM_INT);
                                        $resultajout->bindValue(":siren",$siren,PDO::PARAM_INT);
                                        $resultajout->bindValue(":nic",$nic,PDO::PARAM_INT);
                                        $resultajout->bindValue(":_description",$description,PDO::PARAM_INT);
                                        $resultajout->bindValue(":note",$note,PDO::PARAM_INT);

                                        // on ajoute à la bdd
                                        $resultajout->execute();

                                        // ajout de la photo de profil par défaut
                                        // on chope l'id du vendeur grace à son mail donc on part du principe que le mail est unique donc c'est pas bien en vrai c'est pas possible de créer deux compte avec le meme mail mais quand meme
                                        $sql = "SELECT * FROM alizon._compte_vendeur where email = :email";
                                        $result = $dbh->prepare($sql);
                                        $result->bindValue(":email",$email,PDO::PARAM_STR);
                                        $result->execute();
                                        $vendeur = $result->fetch();

                                        if ( !(file_exists('../images/vendeur/photoProfil/'.$vendeur['id'])) ){
                                            mkdir('../images/vendeur/photoProfil/'.$vendeur['id'], 0777);
                                        }
                                        copy("../images/vendeur/photoProfil/defaut/profilDefaut.jpg", '../images/vendeur/photoProfil/'.$vendeur['id'].'/profilDefaut.jpg');
                                    
                                        $sql = "SELECT * FROM alizon._compte_vendeur where email = :email";                                $result = $dbh->prepare($sql);
                                        $result->bindValue(":email",$email,PDO::PARAM_STR);
                                        $result->execute();
                                        $data = $result->fetch();
                                    
                                        // on a plus besoins des données d'inscription vu que l'utilisateur ne devra pas retaper ses infos de d'inscription
                                        if (isset($_SESSION['dataRegister'])){
                                            unset($_SESSION['dataRegister']);
                                        }
                                        header('Location: ../../admin/index.php');
                                }
                            }
                        }
                        }else { // gestion des erreur en focntion de infos de connections qui pose probleme
                            header('Location: ./../../admin/inscriptionVendeur.php?erreur=0');
                        }
                    } else {
                        header('Location: ./../../admin/inscriptionVendeur.php?erreur=1');
                    }
            } else {
            header('Location: ./../../admin/inscriptionVendeur.php?erreur=2');
        }
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
?>