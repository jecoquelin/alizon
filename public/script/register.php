<?php
    // ce script s'excute de bout en bout

    session_start();
    include('../connectParams.php');
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        // cette variable de session sert à stoquer les infos d'inscriptiond pour les restituer dans le formulaire si une erreur est commise
        $_SESSION['dataRegister'] = $_POST;

        // on vérifie que tous les champs sont bien remplie
        if (isset($_POST['name'], $_POST['surname'], $_POST['pseudo'], $_POST['email'], $_POST['password'], $_POST['secondpassword'])) {
            // on regarde si les question secrete sont bien remplie
            if($_POST['question1']!=''){
                if($_POST['question2']!=''){
                    if($_POST['question1']!=$_POST['question2']){
                        $email = $_POST['email'];
                        $pass = $_POST['password'];
                        $passConfirm = $_POST['secondpassword'];
                        if ($pass == $passConfirm){
                            $sql = "SELECT * FROM alizon._compte_client where email = :email";
                            $result = $dbh->prepare($sql);
                            $result->bindValue(":email",$email,PDO::PARAM_STR);
                            $result->execute();
                            if ($result->rowCount() == 0) {
                                $pseudo = htmlspecialchars($_POST['pseudo']);
                                $name = htmlspecialchars($_POST['name']);
                                $surname = htmlspecialchars($_POST['surname']);
                                $hashedPassword = password_hash($pass, PASSWORD_DEFAULT);
                                $email = htmlspecialchars($_POST['email']);
                                $idPannier = htmlspecialchars($_COOKIE['alizonCookie']);
                                $question1 = htmlspecialchars($_POST['question1']);
                                $reponse1 = htmlspecialchars($_POST['rep1']);
                                $question2 = htmlspecialchars($_POST['question2']);
                                $reponse2 = htmlspecialchars($_POST['rep2']);

                                $sqlajout = "INSERT into alizon._compte_client(pseudo, nom, prenom, mdp, email, id_panier, active, libelle_question1, reponse_question1, libelle_question2, reponse_question2) values (:pseudo, :name, :surname, :hashpassword, :email, :idPannier, 1 , :question1 , :answer1, :question2 , :answer2)";
                                $resultajout = $dbh->prepare($sqlajout);

                                $resultajout->bindValue(":pseudo",$pseudo,PDO::PARAM_STR);
                                $resultajout->bindValue(":name",$name,PDO::PARAM_STR);
                                $resultajout->bindValue(":surname",$surname,PDO::PARAM_STR);
                                $resultajout->bindValue(":hashpassword",$hashedPassword,PDO::PARAM_STR);
                                $resultajout->bindValue(":email",$email,PDO::PARAM_STR);
                                $resultajout->bindValue(":idPannier",$idPannier,PDO::PARAM_INT);
                                $resultajout->bindValue(":question1",$question1,PDO::PARAM_INT);
                                $resultajout->bindValue(":answer1",$reponse1,PDO::PARAM_INT);
                                $resultajout->bindValue(":question2",$question2,PDO::PARAM_INT);
                                $resultajout->bindValue(":answer2",$reponse2,PDO::PARAM_INT);
                                // on ajoute à la bdd
                                $resultajout->execute();

                                // ajout de la photo de profil par défaut
                                // on chope l'id du client grace à son mail donc on part du principe que le mail est unique donc c'est pas bien en vrai c'est pas possible de créer deux compte avec le meme mail mais quand meme
                                $sql = "SELECT * FROM alizon._compte_client where email = :email";
                                $result = $dbh->prepare($sql);
                                $result->bindValue(":email",$email,PDO::PARAM_STR);
                                $result->execute();
                                $client = $result->fetch();

                                if ( !(file_exists('../images/photoProfil/'.$client['id'])) ){
                                    mkdir('../images/photoProfil/'.$client['id'], 0777);
                                }
                                copy("../images/photoProfil/defaut/profilDefaut.jpg", '../images/photoProfil/'.$client['id'].'/profilDefaut.jpg');

                                // on reset le cookie
                                setcookie("alizonCookie", 1, 1, "/");

                                $sql = "SELECT * FROM alizon._compte_client where email = :email";
                                $result = $dbh->prepare($sql);
                                $result->bindValue(":email",$email,PDO::PARAM_STR);
                                $result->execute();
                                $data = $result->fetch();

                                // on initialise les variables de session essentiel au bon fonctionnement du site
                                $_SESSION['email'] = $email;
                                $_SESSION['connect'] = true;
                                $_SESSION['idClient'] = $data['id'];

                                // on a plus besoins des données d'inscription vu que l'utilisateur ne devra pas retaper ses infos de d'inscription
                                if (isset($_SESSION['dataRegister'])){
                                    unset($_SESSION['dataRegister']);
                                }
                                // si l'internaute vient du panier on l'y redirige
                                if ($_SESSION['connect_panier']==true){
                                    header('Location: ../panier.php');
                                }
                                else{ // si non on le redirige vers la page d'accueil
                                    header('Location: ../index.php');
                                }
                            } else { // gestion des erreur en focntion de infos de connections qui pose probleme
                                header('Location: ../inscription.php?erreur=0');
                            }
                        } else {
                            header('Location: ../inscription.php?erreur=1');
                        }
                    }else{
                        header('Location: ../inscription.php?erreur=2');
                    }
                }else{
                    header('Location: ../inscription.php?erreur=3');
                }  
            }else{
                header('Location: ../inscription.php?erreur=3');
            }
        } else {
            header('Location: ../inscription.php?erreur=4');
        }
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
?>