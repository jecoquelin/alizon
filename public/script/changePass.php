<?php
    session_start();
    include('../connectParams.php');
    $_SESSION['dataOublie'] = $_POST;

    $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
    $mail=$_POST['email'];
    $question1=$_POST['question1'];
    $reponse1=$_POST['rep1'];
    $question2=$_POST['question2'];
    $reponse2=$_POST['rep2'];
    $mdp_nouveau=$_POST['password'];
    $mdp_nouveau_confirmation=$_POST['confirm'];
    
    // Récupération du mot de passe actuel du client 
    try{
    $sth = $dbh->prepare('SELECT id,email,mdp,libelle_question1,reponse_question1,libelle_question2,reponse_question2 FROM alizon._compte_client WHERE email=? and active = 1');
    $sth->execute(array($mail));
    $res = $sth->fetchAll();
    }catch(PDOException $e){
        echo $e;
    }


    if(isset($res[0])){
        $id=$res[0]['id'];
        if($question1==$res[0]['libelle_question1']){
            echo "1";
            if($question2==$res[0]['libelle_question2']){
                echo "2";
                if($reponse1==$res[0]['reponse_question1']){
                    echo "3";
                    if($reponse2==$res[0]['reponse_question2']){
                        echo "4";
                            // Vérification que le mot de passe et sa confirmation sont identique
                            if ($mdp_nouveau == $mdp_nouveau_confirmation){
                                $mdp_nouveau = password_hash($mdp_nouveau, PASSWORD_DEFAULT);
                                $sth = $dbh->prepare('UPDATE alizon._compte_client SET mdp=? WHERE id=?');
                                $sth->execute(array($mdp_nouveau, $id));
                                unset($_SESSION['dataOublie']);
                                header('Location: ../connexion.php?erreur=3');
                            }else{
                                header('Location: ../mdpOublie.php?erreur=1');
                            }
                    }else{
                        header('Location: ../mdpOublie.php?erreur=2');
                    }
                }else{
                    header('Location: ../mdpOublie.php?erreur=2');
                }
            }else{
                header('Location: ../mdpOublie.php?erreur=2');
            }
        }else if($question1==$res[0]['libelle_question2']){
            if($question2==$res[0]['libelle_question1']){
                if($reponse1==$res[0]['reponse_question2']){
                    if($reponse2==$res[0]['reponse_question1']){
                            // Vérification que le mot de passe et sa confirmation sont identique
                            if ($mdp_nouveau == $mdp_nouveau_confirmation){
                                $mdp_nouveau = password_hash($mdp_nouveau, PASSWORD_DEFAULT);
                                $sth = $dbh->prepare('UPDATE alizon._compte_client SET mdp=? WHERE id=?');
                                $sth->execute(array($mdp_nouveau, $id));
                                unset($_SESSION['dataOublie']);
                                header('Location: ../connexion.php?erreur=3');
                            }else{
                                header('Location: ../mdpOublie.php?erreur=1');
                            }
                    }else{
                        header('Location: ../mdpOublie.php?erreur=2');
                    }
                }
            }else{
                header('Location: ../mdpOublie.php?erreur=2');
            }
        }else{
            header('Location: ../mdpOublie.php?erreur=2');
        }
    }else{
        header('Location: ../mdpOublie.php?erreur=3');
    }
?>