<?php

// Permet de récupérer les informations lié à un client via son ID
function getClientById($id){
    global $dbh ;
    
    $sth = $dbh->prepare('SELECT * from alizon._compte_client where id = ?');
    
    $sth -> execute(array($id));
    $produits = $sth -> fetchAll();
    return $produits[0];
}


// Permet de récupérer la photo de profil du client via son ID
function getMainPicClient($id){
    global $prefixe;
    $client = getClientById($id);
    $photos = scandir($prefixe.'images/photoProfil/' . $client['id'] . '/');
    $nomFicPhoto = $photos[2];
    return $nomFicPhoto; 
}


// Permet de modifier la photo de profil du lcient via son ID et la nouvelle image
function modifierPhotoDeProfil($id, $newPicture){ 
    global $prefixe;
    if ($_SESSION['connect'] == true ){

        // Liste des extentions valider par le site
        $extentionValide = array('jpg', 'jpeg', 'png', 'gif');

        // Liste les différentes erreurs possibles lié au POST / FILES | Sauf 0 qui lui est pour signaler que tout est OK
        $erreur = $_FILES['profilePicture']['error'] ;

        switch ($erreur) {
            case 1 :
                echo "<p class='feedbackERR'>Erreur : Le poid de l'image téléchargé est trop grande</p>" ;
                break ;
            case 2 :
                echo "<p class='feedbackERR'>Erreur : La taille du l'image téléchargé est trop grande</p>" ;
                break ;
            case 3 :
                echo "<p class='feedbackERR'>Erreur : Le fichier a été que partiellement télécharger</p>" ;
                break ;
            case 4 :
                echo "<p class='feedbackERR'>Erreur : Aucune image n'a été donné</p>";
                break ;
            case 6 :
            echo "<p class='feedbackERR'>Erreur : Un dossier temporaire est manquant. Veillez réessayer plus tard !</p>";
                break ;
            case 7 :
            echo "<p class='feedbackERR'>Erreur : Echec de l'écriture sur les disques du serveur. Veillez réessayer plus tard !</p>";
                break ;
            case 8 :
            echo "<p class='feedbackERR'>Erreur : Une extension PHP a arrêté l'envoie. Veillez réessayer plus tard !</p>";
                break ;
            case 0 :
                $client = getClientById($id);
                $oldPicture = getMainPicClient($id);

                // Supprime l'ancienne photo de profil
                if ( (!empty($oldPicture)) && ( (!empty($newPicture['name'])) ) ){
                    unlink($prefixe."images/photoProfil/".$client['id']."/".$oldPicture);
                }

                // Créer un fichier si le dossier de la photo de profil du client n'existe pas encore
                if ( !(file_exists($prefixe.'images/photoProfil/'.$client['id'])) ){
                    mkdir($prefixe.'images/photoProfil/'.$client['id'], 0777);
                }

                // Vérification que l'extension est accepter
                $tmp = explode('.', $newPicture['name']);
                $fileExtention = end($tmp);
                if ( in_array($fileExtention, $extentionValide) ){
                    // Upload la nouvelle photo de profil au bonne endroit
                    move_uploaded_file($newPicture['tmp_name'], $prefixe.'images/photoProfil/'.$client['id'].'/'.$newPicture['name']);  
                    echo "<p class='feedbackOK'>Votre nouvelle photo de profil a été modifié avec succès !</p>" ;
                }
                else {
                    echo "<p class='feedbackERR'>Erreur : Votre fichier contient une extention qui n'est pas valide</p>";
                }
                break ;

            default :
                echo "<p class='feedbackERR'>.Erreur : ".$erreur." plus d'info sur https://www.php.net/manual/fr/features.file-upload.errors.php</p>";
        }
    }
}

// Permet de vérifier que la date de naissance ne soit pas plus tard que celle d'aujourd'hui
function modifierDateNaissance($dateNaissance, $idClient) {
    global $dbh ;

    // Si la date n'est pas vide alros on vérifie sa validité
    if ( !($dateNaissance == "") ) {

        // Découpage de la date de naissance en annee, mois, jour
        $tabDateNaissance   = explode('-', $dateNaissance);
        $anneeNaissance  = $tabDateNaissance[0] ;
        $moisNaissance   = $tabDateNaissance[1] ;
        $jourNaissance   = $tabDateNaissance[2] ;

        // Récupération de la date du jour
        $currentYear    = date('Y') ;
        $currentMouth   = date('m') ;
        $currentDay     = date('d') ;

        // Vérification que la date ne soit pas supérieur à aujourd'hui
        if ($anneeNaissance > $currentYear){
            echo '<p class="feedbackERR">Erreur : Veillez saisir une année valide ! (Vous ne pouvez être né dans le futur...)</p>' ;
        }
        else if ($anneeNaissance == $currentYear){
            if ($moisNaissance > $currentMouth){
                echo '<p class="feedbackERR">Erreur : Veillez saisir un mois valide ! (Vous ne pouvez être né dans le futur...)</p>' ;   
            }
            else if ($moisNaissance == $currentMouth){
                if ($jourNaissance > $currentDay){
                    echo '<p class="feedbackERR">Erreur : Veillez saisir un jour valide ! (Vous ne pouvez être né dans le futur...)</p>' ;
                }
                else {
                    // On éxécute la requêtes sql si uniquement la date est antérieur à celle d'aujourd'hui
                    try {
                        $sth = $dbh->prepare('UPDATE alizon._compte_client SET date_naissance=? WHERE id=?');
                        $sth -> execute(array($dateNaissance, $idClient)) ;
                    } catch (PDOException $e){
                        echo '<p class="feedbackERR">Erreur au niveau de la date de naissance !</p>';
                    }
                }
            }
            else {
                // On éxécute la requêtes sql si uniquement la date est antérieur à celle d'aujourd'hui
                try {
                    $sth = $dbh->prepare('UPDATE alizon._compte_client SET date_naissance=? WHERE id=?');
                    $sth -> execute(array($dateNaissance, $idClient)) ;
                } catch (PDOException $e){
                    echo '<p class="feedbackERR">Erreur au niveau de la date de naissance !</p>';
                }
            }
        }
        // On éxécute la requêtes sql si uniquement la date est antérieur à celle d'aujourd'hui
        else {
            try {
                $sth = $dbh->prepare('UPDATE alizon._compte_client SET date_naissance=? WHERE id=?');
                $sth -> execute(array($dateNaissance, $idClient)) ;
            } catch (PDOException $e){
                echo '<p class="feedbackERR">Erreur au niveau de la date de naissance !</p>';
            }
        }
    }
    // Si le champs date est vide alors on éxécute la requête avec la date de naissance à NULL
    else {
        try {
            $sth = $dbh->prepare('UPDATE alizon._compte_client SET date_naissance=NULL where id=?');
            $sth -> execute(array($idClient)) ;
        } catch (PDOException $e){
            echo '<p class="feedbackERR">Erreur au niveau de la date de naissance !</p>';
        }
    }
}

// Permet de vérifier si l'email est valide ou pas et envoie un mail
function modifierMail($email, $id){
    global $dbh ;

    // Requête qui permettra de vérifié que l'email n'est pas déjà utiliser
    $sql = "SELECT * FROM alizon._compte_client where email = :email";
    $result = $dbh->prepare($sql);
    $result->bindValue(":email",$email,PDO::PARAM_STR);
    $result->execute();

    // Requête récupérant l'email du client avec son ID
    $sth = $dbh->prepare('SELECT email FROM alizon._compte_client WHERE id= ?');
    $sth->execute(array($id));
    $emailInBDD = $sth->fetchAll() ;

    // Si l'email est déjà utilisé alors if ce n'est pas celle du client alors erreur
    // sinon on exécute la requête pour mettre à jour son email
    if ($result->rowCount() != 0){
        if($email != $emailInBDD[0]['email']){
            echo '<p class="feedbackERR">Erreur : Cette adresse mail est déjà utilisé</p>' ;
        }    
    }  else {
        $sth = $dbh->prepare('UPDATE alizon._compte_client SET email = ? WHERE id = ?');
        $sth -> execute((array(htmlspecialchars($email), $id)));     
    }
}

// Permet de modifier l'adresse de livraison 
function modifierAdresseLivraison($numVoie, $nomVoie, $ville, $postal, $batiment, $etage, $numPorte, $idClient){
    global $dbh ;

    try {
        $sth = $dbh->prepare('UPDATE alizon._compte_client SET numero_voie=?, nom_voie=?, ville=?, code_postal=?, batiment=?, etage=?, numero_porte=? where id=?');
        $sth -> execute(array(htmlspecialchars($numVoie), htmlspecialchars($nomVoie), htmlspecialchars($ville), htmlspecialchars($postal), htmlspecialchars($batiment), htmlspecialchars($etage), htmlspecialchars($numPorte), htmlspecialchars($idClient))) ;
    } catch (PDOException $e){
        echo '<p class="feedbackERR">Une erreur à été rencontré avec l\'adresse de livraison !</p>' ;
    }
}

// Permet de modifier les infos personnelles du client
function modifierInfoPersonnelles($pseudo, $nom, $prenom, $email, $dateNaissance, $numVoie, $nomVoie, $ville, $postal, $batiment, $etage, $numPorte, $idClient){
    global $dbh ;

    // Vérification et modification de la date de naissance + feedback
    modifierDateNaissance($dateNaissance, $idClient) ;

    // Vérification et modification du mail + feedback
    modifierMail($email, $idClient);

    // Modicfication de l'adresse de livraison + feedback
    modifierAdresseLivraison($numVoie, $nomVoie, $ville, $postal, $batiment, $etage, $numPorte, $idClient) ;

    // Exécution de la requêtes SQL pour les champs pas encore traité + feedback
    try {
        $sth = $dbh->prepare('UPDATE alizon._compte_client SET pseudo=?, nom=?, prenom=? where id=? ');
        $sth -> execute(array(htmlspecialchars($pseudo), htmlspecialchars($nom), htmlspecialchars($prenom), htmlspecialchars($idClient))) ;
    } catch (PDOException $e){
        echo '<p class="feedbackERR">Une erreur à été rencontré sur minimum un des trois (psuedo, nom, prenom): '.$e.'</p>';
    }
    echo '<p class="feedbackOK">Les nouvelles informations personnelles ont été enregistré !</p>';
}





// Permet de modifier le mot de passe
function modifierMotDePasse($mdp_actuel, $mdp_nouveau, $mdp_nouveau_confirmation, $id){
    global $dbh ;

    // Récupération du mot de passe actuel du client 
    $sth = $dbh->prepare('SELECT mdp FROM alizon._compte_client WHERE id=?');
    $sth->execute(array($id));
    $res = $sth->fetchAll();

    // Vérification que le mot de passe correspond à celui du compte
    if ( password_verify($mdp_actuel, $res[0]['mdp']) ){
        // Vérification que le mot de passe et sa confirmation sont identique
        if ($mdp_nouveau == $mdp_nouveau_confirmation){
            $mdp_nouveau = password_hash($mdp_nouveau, PASSWORD_DEFAULT);
            try {
                $sth = $dbh->prepare('UPDATE alizon._compte_client SET mdp=? WHERE id=?');
                $sth->execute(array(htmlspecialchars($mdp_nouveau), $id));
            }catch (PDOException $e){
                echo '<p class="feedbackERR">Une erreur est survenu lors de la modification de mot de passe !</p>' ;
            }
            echo '<p class="feedbackOK">Mot de passe modifié !</p>' ;
        }else{
            echo '<p class="feedbackERR">Erreur : Le mot de passe et la confirmation sont différent !</p>' ;
        }
    }
    else {
        echo '<p class="feedbackERR">Mauvais mot de passe !</p>' ;
    }
}

// Permet de déscativé un compte à partir de l'ID (admin)
function desactiveCompte($id){
    global $dbh ;

    try {
        $sql = "UPDATE alizon._compte_client SET active=0 where id = :id";
        $result = $dbh->prepare($sql);
        $result->bindValue(":id",$id,PDO::PARAM_INT);
        $result->execute();
        
        echo '<p class="feedbackOK">Compte désactivé avec succès !</p>' ;
    }
    catch(PDOException $e){
        echo '<p class="feedbackERR">Une erreur c\'est produite ! '.$e.'</p>';
    }
    
}
?>