<?php

// Permet de récupérer les informations liées à un vendeur via son ID
function getVendeurById($id){
    global $dbh ;
    try{
        $sth = $dbh->prepare('SELECT * from alizon._compte_vendeur where id=?');
        $sth -> execute(array($id));
        $vendeur = $sth -> fetchAll();
    } catch (PDOException $e){
        echo '<p>Erreur lors du chargement du vendeur'.$e.'</p>';
    }

    
    return $vendeur[0];
}



// Permet de modifier les infos personnelles du vendeur
function modifierInfoVendeur($nom, $email, $numVoie, $nomVoie, $ville, $postal, $code_TVA, $cle_TVA, $siren, $nic, $texte_presentation, $idVendeur){
    global $dbh ;

    // Vérification et modification du mail + feedback
    modifierMailVendeur($email, $idVendeur);

    // Modicfication de l'adresse de livraison + feedback
    modifierAdresseEntreprise($numVoie, $nomVoie, $ville, $postal, $idVendeur) ;
    modifierLegaleVendeur($code_TVA, $cle_TVA, $siren, $nic, $idVendeur);

    // Exécution de la requêtes SQL pour les champs pas encore traité + feedback
    try {
        $sth = $dbh->prepare('UPDATE alizon._compte_vendeur SET nom=?, texte_presentation=? where id=? ');
        $sth -> execute(array(htmlspecialchars($nom), htmlspecialchars($texte_presentation), htmlspecialchars($idVendeur))) ;
    } catch (PDOException $e){
        echo '<p class="feedbackERR">Une erreur à été rencontré sur le nom ou la description de présentation : '.$e.'</p>';
    }
    echo '<p class="feedbackOK">Les nouvelles informations personnelles ont été enregistré !</p>';
}

// permet de modifier l'adresse de l'entreprise 
function modifierAdresseEntreprise($numVoie, $nomVoie, $ville, $postal, $idVendeur){
    global $dbh ;

    try {
        $sth = $dbh->prepare('UPDATE alizon._compte_vendeur SET numero_voie=?, nom_voie=?, ville=?, code_postal=? where id=?');
        $sth -> execute(array(htmlspecialchars($numVoie), htmlspecialchars($nomVoie), htmlspecialchars($ville), htmlspecialchars($postal), htmlspecialchars($idVendeur))) ;
    } catch (PDOException $e){
        echo '<p class="feedbackERR">Une erreur à été rencontré avec l\'adresse de livraison !</p>' ;
    }
}

// Permet de modifier les informations légale du vendeur
function modifierLegaleVendeur($code_TVA, $cle_TVA, $siren, $nic, $id){
    global $dbh;

    try{
        $sth = $dbh->prepare('UPDATE alizon._compte_vendeur SET code_TVA=?, cle_TVA=?, siren=?, nic=? where id=?');
        $sth -> execute(array(htmlspecialchars($code_TVA),htmlspecialchars($cle_TVA),htmlspecialchars($siren),htmlspecialchars($nic), htmlspecialchars($id))) ;
    } catch (PDOException $e){
        echo '<p class="feedbackERR">Une erreur à été rencontré avec l\'le code de TVA, la clé, le numéro de siren ou encore le nic !</p>' ;
    }
    
}

// Permet de modifier l'adresse mail du vendeur
function modifierMailVendeur($email, $id){
    global $dbh ;

    // Requête qui permettra de vérifié que l'email n'est pas déjà utiliser
    $sql = "SELECT * FROM alizon._compte_vendeur where email = :email";
    $result = $dbh->prepare($sql);
    $result->bindValue(":email",$email,PDO::PARAM_STR);
    $result->execute();

    // Requête récupérant l'email du ven$vendeur avec son ID
    $sth = $dbh->prepare('SELECT email FROM alizon._compte_vendeur WHERE id= ?');
    $sth->execute(array($id));
    $emailInBDD = $sth->fetchAll() ;

    // Si l'email est déjà utilisé alors if ce n'est pas celle du ven$vendeur alors erreur
    // sinon on exécute la requête pour mettre à jour son email
    if ($result->rowCount() != 0){
        if($email != $emailInBDD[0]['email']){
            echo '<p class="feedbackERR">Erreur : Cette adresse mail est déjà utilisé</p>' ;
        }    
    }  else {
        $sth = $dbh->prepare('UPDATE alizon._compte_vendeur SET email = ? WHERE id = ?');
        $sth -> execute((array(htmlspecialchars($email), $id)));     
    }
}



// Permet de récupérer la photo de profil du vendeur via son ID
function getMainPicVendeur($id){
    global $prefixe;
    $vendeur = getVendeurById($id);
    $photos = preg_grep('~\.(jpeg|jpg|png)$~', scandir($prefixe.'images/vendeur/photoProfil/' . $vendeur['id'] . '/'));
    $photos = array_values($photos);
    $nomFicPhoto = $photos[0];
    return $nomFicPhoto; 
}

function modifierMotDePasseVendeur($mdp_actuel, $mdp_nouveau, $mdp_nouveau_confirmation, $id){
    global $dbh ;

    // Récupération du mot de passe actuel du client 
    $sth = $dbh->prepare('SELECT mdp FROM alizon._compte_vendeur WHERE id=?');
    $sth->execute(array($id));
    $res = $sth->fetchAll();

    // Vérification que le mot de passe correspond à celui du compte
    if ( password_verify($mdp_actuel, $res[0]['mdp']) ){
        // Vérification que le mot de passe et sa confirmation sont identique
        if ($mdp_nouveau == $mdp_nouveau_confirmation){
            $mdp_nouveau = password_hash($mdp_nouveau, PASSWORD_DEFAULT);
            try {
                $sth = $dbh->prepare('UPDATE alizon._compte_vendeur SET mdp=? WHERE id=?');
                $sth->execute(array(htmlspecialchars($mdp_nouveau), $id));
            }catch (PDOException $e){
                echo '<p class="feedbackERR">Une erreur est survenu lors de la modification de mot de passe !</p>' ;
            }
            echo '<p class="feedbackOK">Mot de passe modifié !</p>' ;
        }else{
            echo '<p class="feedbackERR">Erreur : Le mot de passe et la confirmation sont différent !</p>' ;
        }
    }
    else {
        echo '<p class="feedbackERR">Mauvais mot de passe !</p>' ;
    }
}

// Permet de modifier la photo de profil du lcient via son ID et la nouvelle image
function modifierPhotoDeProfilVendeur($id, $newPicture){ 
    global $prefixe;
    if ($_SESSION['connect'] == true ){

        // Liste des extentions valider par le site
        $extentionValide = array('jpg', 'jpeg', 'png', 'gif');

        // Liste les différentes erreurs possibles lié au POST / FILES | Sauf 0 qui lui est pour signaler que tout est OK
        $erreur = $_FILES['profilePicture']['error'] ;

        switch ($erreur) {
            case 1 :
                echo "<p class='feedbackERR'>Erreur : Le poid de l'image téléchargé est trop grande</p>" ;
                break ;
            case 2 :
                echo "<p class='feedbackERR'>Erreur : La taille du l'image téléchargé est trop grande</p>" ;
                break ;
            case 3 :
                echo "<p class='feedbackERR'>Erreur : Le fichier a été que partiellement télécharger</p>" ;
                break ;
            case 4 :
                echo "<p class='feedbackERR'>Erreur : Aucune image n'a été donné</p>";
                break ;
            case 6 :
            echo "<p class='feedbackERR'>Erreur : Un dossier temporaire est manquant. Veillez réessayer plus tard !</p>";
                break ;
            case 7 :
            echo "<p class='feedbackERR'>Erreur : Echec de l'écriture sur les disques du serveur. Veillez réessayer plus tard !</p>";
                break ;
            case 8 :
            echo "<p class='feedbackERR'>Erreur : Une extension PHP a arrêté l'envoie. Veillez réessayer plus tard !</p>";
                break ;
            case 0 :
                $vendeur = getVendeurById($id);
                $oldPicture = getMainPicVendeur($id);

                // Supprime l'ancienne photo de profil
                if ( (!empty($oldPicture)) && ( (!empty($newPicture['name'])) ) ){
                    unlink($prefixe."images/photoProfil/".$vendeur['id']."/".$oldPicture);
                }

                // Créer un fichier si le dossier de la photo de profil du ven$vendeur n'existe pas encore
                if ( !(file_exists($prefixe.'images/photoProfil/'.$vendeur['id'])) ){
                    mkdir($prefixe.'images/photoProfil/'.$vendeur['id'], 0777);
                }

                // Vérification que l'extension est accepter
                $tmp = explode('.', $newPicture['name']);
                $fileExtention = end($tmp);
                if ( in_array($fileExtention, $extentionValide) ){
                    // Upload la nouvelle photo de profil au bonne endroit
                    move_uploaded_file($newPicture['tmp_name'], $prefixe.'images/photoProfil/'.$vendeur['id'].'/'.$newPicture['name']);  
                    echo "<p class='feedbackOK'>Votre nouvelle photo de profil a été modifié avec succès !</p>" ;
                }
                else {
                    echo "<p class='feedbackERR'>Erreur : Votre fichier contient une extention qui n'est pas valide</p>";
                }
                break ;

            default :
                echo "<p class='feedbackERR'>.Erreur : ".$erreur." plus d'info sur https://www.php.net/manual/fr/features.file-upload.errors.php</p>";
        }
    }
}


//permet de récupérer les informations liées à un vendeur via un produit
function getVendeurByProduit($prod){
    global $dbh ;
    //on sélectionne le vendeur qui correspond au produit
    $sth = $dbh->prepare('SELECT * from alizon._compte_vendeur inner join alizon._produit on alizon._produit.id_vendeur = alizon._compte_vendeur.id where alizon._produit.id = ?');
    $sth -> execute(array($prod["id"]));
    $vendeur = $sth -> fetchAll();
    //si le résultat est vide (produit n'a pas de vendeur indiqué), on renvoie un 'vendeur inconnu'
    if ($vendeur == null){
        $res=array( 'id' => -1, 'nom' => 'vendeur inconnu');
    }
    else{
        $res=$vendeur[0];
    }
    return $res; 
}



// Permet de verifier l'état d'activité du compte vendeur
function getEtatVendeur($id){
    global $dbh ;

    global $dbh ;
    try{
        $sth = $dbh->prepare('SELECT * from alizon._compte_vendeur where id=?');
        $sth -> execute(array($id));
        $vendeur = $sth -> fetchAll();
    } catch (PDOException $e){
        echo '<p>Erreur lors du chargement du vendeur'.$e.'</p>';
    }

    
    return $vendeur[0]["active"];
    
}


// Permet de déscativé un compte vendeur à partir de l'ID (admin)
function desactiveCompteVendeur($id){
    global $dbh ;

    try {
        $sql = "UPDATE alizon._compte_vendeur SET active=0 where id = :id";
        $result = $dbh->prepare($sql);
        $result->bindValue(":id",$id,PDO::PARAM_INT);
        $result->execute();
        
        echo '<p class="feedbackOK">Compte désactivé avec succès !</p>' ;
    }
    catch(PDOException $e){
        echo '<p class="feedbackERR">Une erreur c\'est produite ! '.$e.'</p>';
    }
    
}

?>