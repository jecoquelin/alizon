<?php
session_start();
// Nous regardons si le catalogue existe
if (file_exists('../images/vendeur/photoProfil/'.$_SESSION['idVendeur'].'/catalogue/catalogue.json')){
    // Supprime le fichier catalogue JSON
    unlink('../images/vendeur/photoProfil/'.$_SESSION['idVendeur'].'/catalogue/catalogue.json');
    // Réinitialisation du tableau qui contient l'id des produit séléctionné
    $_SESSION['produits_active'] = [];
    header("Location: ../vendeur/catalogue.php");
} else {
    header("Location: ../vendeur/catalogue.php");
}
?>