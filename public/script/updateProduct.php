<?php
    // ce script est utilisé pour modifier l'état d'un produit

    // Connection à la BDD
    require('./../connectParams.php');
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }

    // Importation des functions de product.php : Modification des états
    require('./product.php');

    // Gestion de la modification de l'état d'un produit
    if (isset($_GET['etat']) and  $_GET['id']){
        if ($_GET['etat']==0){
            activerProduitById($_GET['id']);
            return 1 ;
        }else if ($_GET['etat']==1){
            desactiverProduitById($_GET['id']) ;
            return 1 ;
        }else if($_GET['etat']==2){
            supprimmerProduitById($_GET['id']);
            return 1;
        }
    }


?>