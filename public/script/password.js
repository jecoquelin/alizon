// Lorsque la page se ralance on vérifie si le mot de passe est ok
CheckMdp(document.getElementById('password').value) ;

// Fonction permettant l'affichage des erreurs du mot de passe
function afficher(){
  document.getElementById("error").style.display = "block";
}

// Fonction permettant de verifier si le mot de passe respecte la bonne syntaxe
function CheckMdp(val){

    // Plusieur message qui contiennent un message différent
    var msg1 = document.getElementById("msg1");
    var msg2 = document.getElementById("msg2");
    var msg3 = document.getElementById("msg3");
    var msg4 = document.getElementById("msg4");
    var msg5 = document.getElementById("msg5");

    msg1.textContent = "Il faut une minuscule minimum."
    msg2.textContent = "Il faut une MAJUSCULE minimum."
    msg3.textContent = "Il faut un chiffre minimum."
    msg4.textContent = "Il faut un caractère spécial minimum."
    msg5.textContent = "Il faut 6 caractères minimum."

    // Au début ils sont rouges puisque nous ne savons pas encore si la syntaxe est bonne
    msg1.style.color = "red";
    msg2.style.color = "red";
    msg3.style.color = "red";
    msg4.style.color = "red";
    msg5.style.color = "red";

    // Regarde si le mot de passe contient une minuscule et le met en vert si oui
    if(val.match( /[a-z]/g)){
        msg1.style.color = "green";
    }
    // Regarde si le mot de passe contient une majuscule et le met en vert si oui
    if (val.match( /[A-Z]/g)){
        msg2.style.color = "green";
    }
    // Regarde si le mot de passe contient un chiffre et le met en vert si oui
    if (val.match( /[0-9]/g)){
        msg3.style.color = "green";
    }
    // Regarde si le mot de passe contient un caractère spécial et le met en vert si oui
    if (val.match( /[^a-zA-Z\d]/g)){
        msg4.style.color = "green";
    }
    // Regarde si le mot de passe contient 6 caractères et le met en vert si oui
    if (val.length >= 6){
        msg5.style.color = "green";
    }
    // Regarde toute les conditions précédentes sont vert et si oui, il cache les messages sinon il les montes
    if (msg1.style.color == "green" && msg2.style.color == "green" && msg3.style.color == "green" && msg4.style.color == "green" && msg5.style.color == "green"){
      cache();
      document.getElementById('inscrire').disabled = false;
    }
    else {
      document.getElementById('inscrire').disabled = true;
    }
  }
  
// Permet de cacher le bloc d'erreur
function cache () {
  document.getElementById("error").style.display = "none";
}

