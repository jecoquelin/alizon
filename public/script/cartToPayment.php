<?php
require('./product.php'); 
session_start();

try {
    include('../connectParams.php');
    $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
    
    $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (PDOException $e) {

    print "Erreur : " . $e->getMessage() . "<br/>";
    die();
}

$idPanier = $_SESSION['idPanier'];

//verifie que chaque article a du stock encore disponible pour la qte demandee, si non, recharge le panier
foreach($dbh->query("SELECT * from alizon._dans_panier where id_panier=$idPanier") as $produit_panier) { 
    $produit=getProduitById($produit_panier['id_produit']);
    $stock = $produit['stock'];
    $nbDansPanier = $produit_panier['nombre'];
    if ($nbDansPanier>$stock){
        header("Location: ./../panier.php");
    }
    else{
        header("Location: ./../prePaiement.php");
    }
}
?>
