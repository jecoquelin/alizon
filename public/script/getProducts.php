<?php
session_start();

try {
    include('../connectParams.php');
    $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
    
    $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    
    print "Erreur : " . $e->getMessage() . "<br/>";
    die();
}
include('./search.php');
include('./product.php');

// prend en entrée un tableau (de 2 de profondeur) et renvoie ce tableau sous forme de JSON
function produitsToJSON($tabProduits) {
    /*
        attributs des produit à envoyer:
            - id
            - libelle (nom)
            - description
            - prix_ht
            - taux_tva

            - id_photos

            - maxAjoutPanier

            - id_vendeur
            - nom_vendeur

            - estRemise (bool)
            - remiseHT (float)
            - promo (bool)
    */
    $res = '[';
    $premier = true;
    foreach ($tabProduits as $produit) {
        if (!$premier) {
        $res = $res . ',';
        }
        $res = $res . '{';
        $premier2 = true;
        foreach ($produit as $cle => $val) {
            if (!$premier2) {
                $res = $res . ',';
            }
            $res = $res . "\"$cle\":\"$val\"";
            $premier2 = false;
        }
        $res = $res . '}';
        $premier = false;
    }
    $res = $res . ']';
    return $res;
}

// cette fonction prend en entrée un tableau de produits et renvoie 
function produitsToDonnees($tabProduitsBrut) {
    $tabProduitsPropre = [];
    foreach ($tabProduitsBrut as $produit) {
        $produitPropre = [];

        $estRemise = getRemise($produit['id']);
        $remiseHT = $produit['prix_ht']*((100-getRemise($produit['id']))/100);
        
        $produitPropre['id'] = $produit['id'];
        $produitPropre['libelle'] = $produit['libelle'];
        $produitPropre['description'] = $produit['_description'];
        $produitPropre['prix_ht'] = $produit['prix_ht'];
        $produitPropre['prix_ht'] = $produit['prix_ht'];
        $produitPropre['taux_tva'] = $produit['taux_tva'];
        $produitPropre['id_photos'] = $produit['id_photos'];
        // $produitPropre['maxAjoutPanier'] = ...;
        $produitPropre['id_vendeur'] = $produit['id_vendeur'];
        $produitPropre['nom_vendeur'] = $produit['nom'];
        // $produitPropre['estRemise'] = ...;
        // $produitPropre['remiseHT '] = ...;
        $produitPropre['promo'] = $produit['promotion'];

        $tabProduitsPropre[] = $produitPropre;
    }
    return $tabProduitsPropre;
}

// cherche les produits selon les paramètres et renvoie le résultat sous forme de JSON
if (isset($_GET['search'])) {
    $produits = rechercher($_GET["search"], (isset($_GET["cat"])) ? $_GET["cat"] : null, (isset($_GET["prix"])) ? $_GET["prix"] : null, (isset($_GET["promo"])) ? true : false, (isset($_GET["vend"])) ? $_GET["vend"] : null);
    echo produitsToJSON($produits) . "<br>";
    echo produitsToJSON(produitsToDonnees($produits));
}

?>