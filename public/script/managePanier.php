<?php
/* fonction de test qui ne sert plus
    function getNewId(){
        include('connectParams.php');
        try {
            $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
            
            $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            
            $sth = $dbh->prepare('SELECT count(*) from alizon._panier');
            $sth -> execute();
            $produits = $sth -> fetchAll();
            $resultat = $produits[0]['count']+1;
            return $resultat;
            
        } catch (PDOException $e) {
            print "Erreur : " . $e->getMessage() . "<br/>";
            die();
        }
    }
*/

    //crée un panier dans la bdd avec l'id donné en parametre
    function createPanier($idPanier){
        global $dbh;
        $sth = $dbh->prepare("INSERT INTO alizon._panier(id_panier) values ($idPanier)");
        $sth -> execute();
    }

    //vérifie si au moins 1 panier existe dans la BDD et renvoie un boolean
    function checkPanier($nb){
        $bool=0;
        global $dbh;
        $sth = $dbh->prepare("SELECT count(*) from alizon._panier WHERE id_panier= $nb ");
        $sth -> execute();
        $produits = $sth -> fetchAll();
        $resultat = $produits[0]['count'];
        if($resultat>=1){
            $bool=1;
        }
        return $bool;
    }

    //supprime un article d'un panier, en prenant leurs id en paramètres
    function deleteItemPanier($idproduit,$idPanier){
        global $dbh;                 
        $sth = $dbh->prepare('DELETE from alizon._dans_panier where id_produit = ? and id_panier = ?');
        $sth -> execute(array($idproduit,$idPanier));  
    }

    //supprime tous les articles d'un panier
    function videPanier($idPanier){
        global $dbh;      
        $sth = $dbh->prepare('DELETE from alizon._dans_panier where id_panier = ?');
        $sth -> execute(array($idPanier));
    }

    //modifie la quantité d'un article dans un panier
    function updateQuantity($quantity, $idproduit, $idPanier){
        global $dbh;                 
        $sth = $dbh->prepare('UPDATE alizon._dans_panier SET nombre=? where id_produit = ? and id_panier = ?');
        $sth -> execute(array($quantity,$idproduit, $idPanier));  
    }

    // vérifie qu'un nombre (stocké sous forme de chaine) n'est composé que de chiffres
    function verifComposeDeChiffres($chaine) {
        $composeDeChiffres = true;

        foreach (str_split($chaine) as $caractere) {
            if (!in_array($caractere, array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'))) {
                $composeDeChiffres = false;
                break;
            }
        }

        return $composeDeChiffres;
    }
?>