<!DOCTYPE html>
<html>
<head lang="fr">
    <meta charset="utf-8">
    <title> PHP création d'un répertoire en php</title>
</head>
<body>
<p>
<?php
include('../connectParams.php');
require('prefixe.php');
try {
    $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
    
    $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    
    
} catch (PDOException $e) {
    print "Erreur : " . $e->getMessage() . "<br/>";
    die();
}

// le chemin du dossier à créer
// Verifier l'existence du dossier

// https://forums.commentcamarche.net/forum/affich-13163647-php-lister-dossier-et-contenu-arborescence
$director = "../images/produits/";

function mkmap($dir){
    $dossiers = scandir("../images/produits/");
    $max = $dossiers[2];
    foreach ($dossiers as $dossier) {
        if ($max < $dossier){
            $max = $dossier;
        }
    }
    return $max;
}




function creerDossier($inc) {
    $dir = "../images/produits/".$inc;
    if(!file_exists($dir)){
        if(mkdir($dir)){
            /*echo "Répertoire créé avec succès.";
            // echo ($_FILES['file1']."\n");
            // echo ($_POST['file2']."\n");
            // echo ($_POST['file3']."\n");
            // echo ($_POST['file4']."\n");
            // echo ($_POST['file5']."\n");
            // echo ($_POST['file6']."\n");
            echo($tmpFile = $_FILES['file1']['tmp_name']."\n");

            echo(empty($_FILES['file1']));*/
            if(!empty($_FILES['file1']))
            {
                $nameFile = $_FILES['file1']['name'];
                $typeFile = $_FILES['file1']['type'];
                $sizeFile = $_FILES['file1']['size'];
                $tmpFile = $_FILES['file1']['tmp_name'];
                $errFile = $_FILES['file1']['error'];

                $extension = explode('.', $nameFile);
                if(move_uploaded_file($tmpFile, "../images/produits/".$inc."/1".".".strtolower(end($extension) ) ) )
                    echo "This is uploaded!";
                else 
                    echo "failed";         
            }
            else 
            {
                echo("L'image 1 n'existe pas");
            }
            if(!empty($_FILES['file2']))
            {
                $nameFile = $_FILES['file2']['name'];
                $typeFile = $_FILES['file2']['type'];
                $sizeFile = $_FILES['file2']['size'];
                $tmpFile = $_FILES['file2']['tmp_name'];
                $errFile = $_FILES['file2']['error'];

                $extension = explode('.', $nameFile);
                if(move_uploaded_file($tmpFile, "../images/produits/".$inc."/2 ".$nameFile ) )
                    echo "This is uploaded!";
                else 
                    echo "failed";         
            }
            else 
            {
                echo("L'image 2 n'existe pas");
            }
            if(!empty($_FILES['file3']))
            {
                $nameFile = $_FILES['file3']['name'];
                $typeFile = $_FILES['file3']['type'];
                $sizeFile = $_FILES['file3']['size'];
                $tmpFile = $_FILES['file3']['tmp_name'];
                $errFile = $_FILES['file3']['error'];

                $extension = explode('.', $nameFile);
                if(move_uploaded_file($tmpFile, "../images/produits/".$inc."/3 ".$nameFile ) )
                    echo "This is uploaded!";
                else 
                    echo "failed";         
            }
            else 
            {
                echo("L'image 3 n'existe pas");
            }
            if(!empty($_FILES['file4']))
            {
                $nameFile = $_FILES['file4']['name'];
                $typeFile = $_FILES['file4']['type'];
                $sizeFile = $_FILES['file4']['size'];
                $tmpFile = $_FILES['file4']['tmp_name'];
                $errFile = $_FILES['file4']['error'];

                $extension = explode('.', $nameFile);
                if(move_uploaded_file($tmpFile, "../images/produits/".$inc."/4 ".$nameFile ) )
                    echo "This is uploaded!";
                else 
                    echo "failed";         
            }
            else 
            {
                echo("L'image 4 n'existe pas");
            }
            if(!empty($_FILES['file5']))
            {
                $nameFile = $_FILES['file5']['name'];
                $typeFile = $_FILES['file5']['type'];
                $sizeFile = $_FILES['file5']['size'];
                $tmpFile = $_FILES['file5']['tmp_name'];
                $errFile = $_FILES['file5']['error'];

                $extension = explode('.', $nameFile);
                if(move_uploaded_file($tmpFile, "../images/produits/".$inc."/5 ".$nameFile ) )
                    echo "This is uploaded!";
                else 
                    echo "failed";         
            }
            else 
            {
                echo("L'image 5 n'existe pas");
            }
            if(!empty($_FILES['file6']))
            {
                $nameFile = $_FILES['file6']['name'];
                $typeFile = $_FILES['file6']['type'];
                $sizeFile = $_FILES['file6']['size'];
                $tmpFile = $_FILES['file6']['tmp_name'];
                $errFile = $_FILES['file6']['error'];

                $extension = explode('.', $nameFile);
                if(move_uploaded_file($tmpFile, "../images/produits/".$inc."/6 ".$nameFile ) )
                    echo "This is uploaded!";
                else 
                    echo "failed";         
            }
            else 
            {
                echo("L'image 6 n'existe pas");
            }

        } else{
            echo "Erreur";
        }
    }
    else{
        $inc++;
        creerDossier($inc);
    }
}

function ajoutProduit(){
    /*
    $stmt = $dbh->prepare("INSERT INTO alizon._Produit (ref_fabricant, libelle, prix_ht, id_photos, _description, stock, seuil, libelle_categorie) VALUES (:ref_fabricant, :libelle, :prix_ht, :id_photos, :_description, :stock, :seuil, :libelle_categorie)");
    $stmt->bindParam(':ref_fabricant', $ref_fabricant);
    $stmt->bindParam(':libelle', $libelle);
    $stmt->bindParam(':prix_ht', $prix_ht);
    $stmt->bindParam(':id_photos', $id_photos);
    $stmt->bindParam(':_description', $_description);
    $stmt->bindParam(':stock', $stock);
    $stmt->bindParam(':seuil', $seuil);
    $stmt->bindParam(':libelle_categorie', $libelle_categorie);
    
    // insertion d'une ligne
    $ref_fabricant = $_POST['nomsociete'];
    $libelle = $_POST['nomproduit'];
    $prix_ht = $_POST['prix'];
    $id_photos = $dir;
    $_description = $_POST['description'];
    $stock = $_POST['nbStock'];
    $seuil = $_POST['seuil'];
    $libelle_categorie = $_POST['inputCat'];
    $stmt->execute();
    */
    global $director;
    $i=mkmap($director);
    
    creerDossier($i); 

    $i=mkmap($director);
    
    global $dbh;

    $sth = $dbh->prepare("INSERT INTO alizon._produit(ref_fabricant, libelle, prix_ht,id_photos,_description,stock,seuil,duree_livraison,libelle_categorie) values (?,?,?,?,?,?,?,?,?)");
    $sth -> execute(Array($_POST['nomsociete'],
                            $_POST['nomproduit'],
                            $_POST['prix'],
                            $i,
                            $_POST['description'],
                            $_POST['nbStock'],
                            $_POST['seuil'],
                            3,
                            $_POST['inputCat']));

}

//ajoutProduit();
ajoutProduit();
header('Location: ../confirmAjoutProduit.php');
//print_r($_POST);
//print_r($direct);

?>
</p>
</body>
</html>