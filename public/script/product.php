<?php

    // ce script contient toutes les fonctions qui sont en rapport avec les produits

    function getProduitByCatAndNb($libelleCat, $nbProd){
        // cette fonction récupère les produits avec leur catégorie et elle prend les nbProd dernier
        global $dbh;
        $sth = $dbh->prepare('SELECT * from alizon._produit where libelle_categorie = ? and act=0');
        $sth -> execute(array($libelleCat));
        $produits = $sth -> fetchAll();
        
        $produitsRet = [];
        // boucle qui chope les nbProd dernier produit des produit dune catégori libelleCat donnée 
        for ($i=sizeof($produits)-1; $i > (sizeof($produits)-1) - $nbProd; $i--) { 
            $produitsRet[] = $produits[$i];
        }

        return $produitsRet;
    }

    function getNumberOfProdInCat($libelleCat, $smart=true){
        // cette fonction récupère le nombre de produits qu'il y a dans une catégorie
        global $dbh;
        $sth = $dbh->prepare('SELECT * from alizon._produit where libelle_categorie = ? and act=0');
        $sth -> execute(array($libelleCat));
        $produits = $sth -> fetchAll();
        
        // si smart est à true à alors le nombre de produit retourné sera 5 pour ne pas qu'il y ai trop de produit âr catégorie sur la page d'accueil
        $nbProd = sizeof($produits);
        if ($smart == true){
            if ($nbProd > 5){
                $nbProd = 5;
            }
        }
        return $nbProd;
    }

    function getNewProduct($nbProd=3){
        // cette fonciton retournera les nbProd nouveaux produits
        global $dbh;
        $sth = $dbh->prepare('SELECT * from alizon._produit where act=0 order by id');
        $sth -> execute();
        $produits = $sth -> fetchAll();
        // si il n'y a pas assez de prod dans les nouveau prod par rapport au nbProd alors le nbProd deviens égal au nombre de nouveau produits
        if (sizeof($produits) < $nbProd){
            $nbProd = sizeof($produits);
        }

        $newProduits = [];
        // cette boucle remplie le tableau newProduits en partant de la fin et en focntion du nbProd
        for ($i=sizeof($produits)-1; $i > (sizeof($produits)-1) - $nbProd; $i--) { 
            $newProduits[] = $produits[$i];
        }
        return $newProduits;
    }

    function getBestSellers($nbProd=3){
        // cette fonction retourne les nbProd produits les plus vendu (commandés)
        global $dbh;
        $sth = $dbh->prepare('SELECT * from alizon._est_commande order by quantite');
        $sth -> execute();
        $produits = $sth -> fetchAll();

        $bestSellers = [];
        // si il y assez de nouveaux produits
        if (sizeof($produits) >= $nbProd) {
            // cette bouvle remplie un tableau de produit les plus vendu en partant de la fin du ableau de produits les plus vendus car il est rangé par ordre croissant
            for ($i=sizeof($produits)-1; $i > (sizeof($produits)-1) - $nbProd; $i--) { 
                $bestSellers[] = getProduitById($produits[$i]['id_produit']);
            }
        } else { // si non on prend les nouveaux produits
            $bestSellers = getNewProduct();
        }
        return $bestSellers;
    }

    function getCatWithMaxProd($nbCat=2){
        // cette fonction récupère les nbCat catégories qui ont le plus de produit
        global $dbh;
        $sth = $dbh->prepare('select count(*), libelle_categorie from alizon._produit group by libelle_categorie order by count desc;');
        $sth -> execute();
        $categories = $sth -> fetchAll();
        
        $catRet = [];
        // un simple parcours du tableau de catégories pour en récupére nbCat
        for ($i=0; $i < $nbCat; $i++) { 
            $catRet[$i] = $categories[$i]['libelle_categorie'];
        }

        return $catRet;
    }

    function getPromotions($nbProd=3){
        // cette fonction retourne les nbProd promotions
        global $dbh;
        $sth = $dbh->prepare('SELECT * from alizon._produit where promotion=1 and act=0');
        $sth -> execute();
        $produits = $sth -> fetchAll();

        // si il n'y a pas de promos ou pas assez alors on retourne les nouveaux produits
        if (!isset($produits[0])){
            $produitsRes = getNewProduct();
        } else if (sizeof($produits[0]) < 3) {
            $produitsRes = getNewProduct();
        } else {
            // cette boucle récupère les nbProd produits en promos après avoir mélanger la liste de tous les produits pour en avoir tous les temps des différents
            for ($i=0; $i < $nbProd; $i++) {
                shuffle($produits);
                // on prend juste les trois premier produits de la liste mélangé
                $produitsRes = array_slice($produits, 0, 3);
            }
        }
        return $produitsRes;
    }

    function getProduitById($id){
        // la fonction qui récupère simplement un produit en fonction de son id
        global $dbh;
        $sth = $dbh->prepare('SELECT * from alizon._produit where id = ?');
        $sth -> execute(array($id));
        $produits = $sth -> fetchAll();

        return $produits[0];
    }

    function getProduitsByVendeur($id){
        // La fonction récupère les produits à partir de l'id du vendeur
        global $dbh ;
        $sth = $dbh->prepare('SELECT * FROM alizon._produit where id_vendeur = ?');
        $sth -> execute(array($id));
        $produits = $sth -> fetchAll() ;

        return $produits ;
    }

    // Retourne tous les produits d'une commande
    function getProduitsCommande($id){
        global $dbh;

        $sth = $dbh->prepare('SELECT * from alizon._est_commande where id_commande = ?');
        $sth -> execute(array($id));
        $produits = $sth -> fetchAll();

        return $produits ;
    }

    // prend une chaine qui représente une catégorie en paramètre
    // renvoie le taux de tva associé à cette catégorie
    // ou -1 si la catégorie n'existe pas
    function getTauxTva($cat){
        global $dbh;

        if (!isset($_SESSION['taux_tva']))
        {
            $sth = $dbh->prepare('SELECT * from alizon._categorie');
            $sth -> execute();
            $resRequete = $sth -> fetchAll();
            $_SESSION['taux_tva'] = array();
            foreach ($resRequete as $catPlusTva)
            {
                $_SESSION['taux_tva'][$catPlusTva['libelle']] = $catPlusTva['taux_tva'];
            }
        }

        if (isset($_SESSION['taux_tva'][$cat]))
        {
            $res = $_SESSION['taux_tva'][$cat];
        }
        else
        {
            $res = -1;
        }

        return $res;
    }

    function getMainPicProd($id){
        global $prefixe;
        // cette fonction renvoie la photo (nom) principale d'un prpduit, celle à affiché sur la page d'accueil par ex
        $produit = getProduitById($id);
        // on prend toutes les images
        $photos = scandir($prefixe.'images/produits/' . $produit['id_photos'] . '/');
        foreach($photos as $photo) {
            // on prend que la photo qui commencent par 1 car c'est la photo principale
            if (substr($photo, 0, 1) == '1'){
                $nomFicPhoto = $photo;
            }
        }
        return $nomFicPhoto;
    }

    function getAllPicProd($id){
        global $prefixe;
        // retourne toutes les images (chemin) du produits
        $produit = getProduitById($id);
        // on s'embete pas avec . et ..
        $photos = array_diff(scandir($prefixe.'images/produits/' . $produit['id_photos'] . '/'),array('..','.'));
        return $photos;
    }
    
    function getDesc250($desc){
        // cette fonction prend un description de produit desc et la formatate de manière à ce que si elle dépasse 250 caractère elle soit raccourcie et qu'elle se termine par ...
        $desc250 = "";
        if (strlen($desc) > 250){
            $desc250 = substr($desc, 0, 250);
            $debutDernierMot = strrpos($desc250, ' ') + 1;
            $desc250 = substr($desc250, 0, $debutDernierMot) . " ...";
        } else {
            $desc250 = $desc;
        }
        return $desc250;
    }

    // Retourne le prix HT unitaire d'un produit
    function getPrixUnitaireTTC($id){
        // cette focntion calcul le prix ttc d'un prduit dont l'id est id
        global $dbh;
        $sth = $dbh->prepare('SELECT prix_ht, libelle_categorie from alizon._produit where id = ?');
        $sth -> execute(array($id));
        $produit = $sth -> fetchAll();

        // on chope la catégorie du prod pour avoir le taux de tva associé à cette catégorie
        $categorie = $produit[0]['libelle_categorie'] ;
        // on prend le taux de tva
        $tva = getTauxTva($categorie) ;
        
        // on prend le prix ht du produit
        $prix_unitaire_ht = $produit[0]['prix_ht'];

        // on retourne le prix ttc
        return $prix_unitaire_ht * $tva;
    }

    // Retourne le nom / libelle du produit
    function getLibelle($id){
        global $dbh ;
        $sth = $dbh->prepare('SELECT libelle from alizon._produit where id = ?');
        $sth -> execute(array($id));
        $libelle = $sth -> fetchAll();

        return $libelle[0]['libelle'] ;
    }

    // Retourne la phrase correspondant au numéro de l'état de la commande
    function getStringEtatCommande($etat){
        switch ($etat) {
            case 0:
                return 'En cours de préparation' ;
                break;
            
            case 1:
                return 'Transport vers la plateforme réginonale' ;
                break;
        
            case 2:
                return 'Transport entre la platefrome et le site local' ;
                break;
                  
            case 3:
                return 'Livrée' ;
                break;
            
            default:
                return 'Inconnu' ;
                break;
        }
    }

    // les 3 fonctions suivantes sont en rapport avec le simulateur

    function seConnecterSimu() {
        // cette fonction créer un socket et se connecte au simulateur en tant que tristan avec le mdp 1234
        // elle retourne le socket
        $identifiant = "tristan";
        $mdp = "1234";
        $synthaxe = " fdp/1.0\n";
        $message = "je-suis " . $identifiant . " " . $mdp . $synthaxe;
        //Creation de la socket
        $sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP) or die('Création de socket refusée');
        // on elenves les warinig
        error_reporting(E_ERROR | E_PARSE);
        //Connexion au serveur
        if (socket_connect($sock,"home.meatman.fr", 7777)) {
            //Écriture du paquet vers le serveur
            socket_write($sock, $message, strlen($message));
            // on lit la réponse du simulateur et si la connection à échouer on routourn null
            $rep = socket_read($sock, 150);
        }
        if (!isset($rep) || str_starts_with($rep, "pas-ok")){
            $sock = null;
        }
        // on remet les warning
        error_reporting(E_ALL);
        return $sock;
    }
    
    function majEtatCommandeSimu($commandes){
        // cette fonction met à jour l'état des commandes commandes
        $sock = seConnecterSimu();
        global $dbh;
        $i = 0;
        if ($sock != null) {
            foreach ($commandes as $commande) {
                // on demande la commande au simulateur
                $message = "donne la-commande " . $commande["id"] . " fdp/1.0\n";
                socket_write($sock, $message, strlen($message));
                // on lit la réponse du simulteur dans rep
                $rep = socket_read($sock, 150);
                // si la réponse commence par prend alors la commande à fonctionnné
                if (str_starts_with($rep, 'prend')) {
                    // on prend le 3 eme champ de la réponse car il corespond à l'etat de la commande
                    $intPourEtat = (int)explode(" ", $rep)[2];
                    $sth = $dbh->prepare('UPDATE alizon._commande set etat_l = ?  where id = ?');
                    // oon met à jour la commande avec le nouvelle état de la commande
                    $sth -> execute(array($intPourEtat, $commande["id"]));
                }
            }
            socket_close($sock);
        }
    }

    function donnerCommandeSimu($idCommande) {
        // cette fonction donne une nouvelle comamnde au simulateur
        $sock = seConnecterSimu();
        global $dbh;
        if ($sock != null) {
            // requete pour donner la comamnde
            $message = "prend la-commande " . $idCommande . " fdp/1.0\n";
            socket_write($sock, $message, strlen($message));
            // on oublie pas de lire la rep pour ne pas avoir de probleme
            socket_read($sock, 150);
            socket_close($sock);
        }
    }

    function getDateLivraison($id){

        global $dbh;
        $sth = $dbh->prepare('SELECT * from alizon._produit where id = ?');
        $sth -> execute(array($id));
        $produits = $sth -> fetchAll();

        $produit=$produits[0];

        $curJour= date('d'); //jour actuel
        $curMonth= date('m'); //mois actuel
        $curYear= date('y'); //année actuelle
        $duree=$produit['duree_livraison']; //duree de livraison du produit

        $livreJour=$curJour+$duree;
        $livreMonth=$curMonth;
        $next=true;
        while ($next==true){
        //si mois de 31 jours et jour > 31
        if ((($livreMonth==1) || ($livreMonth==3) || ($livreMonth==5) || ($livreMonth==7) || ($livreMonth==8) || ($livreMonth==10) || ($livreMonth==12)) && ($livreJour>31)){
            if ($livreMonth==12){
                $livreMonth=1;
            }
            else{
                $livreMonth=$livreMonth+1;
            }
            $livreJour=$livreJour-31;
        }
        //si mois de 30 jours et jour > 30
        else if((($livreMonth==4) || ($livreMonth==6) || ($livreMonth==9) || ($livreMonth==11))&& ($livreJour>30)){
            $livreMonth=$livreMonth+1;
            $livreJour=$livreJour-30;
        }
        //si fevrier bisextile de 29 jours et jour > 29
        else if(($livreMonth==2) && ($livreJour>29) && ($curYear%4==0)){
            $livreMonth=$livreMonth+1;
            $livreJour=$livreJour-29;
        }
        //si fevrier non bisextile de 28 jours et jour > 28
        else if(($livreMonth==2) && ($livreJour>28) && ($curYear%4!=0)){
            $livreMonth=$livreMonth+1;
            $livreJour=$livreJour-28;
        }
        else{
            $next=false;
        }
    }

    //affichage et retour
    $tabMonth=array('Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Octobre','Novembre','Décembre');
    $mois= $tabMonth[(int) $livreMonth-1];  
    return "Livraison gratuite le $livreJour $mois";

        
    }  
    
    // calcule et renvoie le nombre max d'un article que l'utilisateur peut ajouter a son panier sans depasser le stock
    function maxAjoutPanier($produit, $idPanier){
        global $dbh;
        $nb_dans_panier=0;
        $idProd=$produit["id"]; //id du produit qu'on veut ajouter
        $stock=$produit ["stock"];  //stock restant du produit dans la bdd
        //requete sql pour trouver le nombre de cet article deja dans le panier
        $sth = $dbh ->query("SELECT nombre FROM alizon._dans_panier WHERE id_panier=$idPanier AND id_produit=$idProd");
        while ($u = $sth->fetch(PDO::FETCH_ASSOC)) {
            $nb_dans_panier= $u['nombre'];     //recuperation de la valeur
        }
        //calcul de la difference et return
        $nbMax=$stock- $nb_dans_panier; 
        return $nbMax;
    }

    function getRemise($id){
        // cette fonction retourne le taux de la remise si une rmeise est appliqué en ce moment sur le produit dont l'id est id
        $tauxRemise = false;
        global $dbh ;
        $sth = $dbh->prepare('select * from alizon._remise where id_produit = ?');
        $sth -> execute(array($id));
        $remise = $sth -> fetchAll();
        
        if (isset($remise[0])) {
            $dateDebut = $remise[0]["date_heure_debut"];
            $dateFin = $remise[0]["date_heure_fin"];
            // on compare les date pour savoir si la remise est maintenant
            $currentDate=date('Y-m-d');
            if($dateDebut <= $currentDate){
                if (($currentDate <= $dateFin)) { 
                    $tauxRemise =  $remise[0]["pourcentage_remise"];
                }
            }
        }

        
        return $tauxRemise;
    }

    // Permet d'activer un produit par ID
    function activerProduitById($idProduit){
        global $dbh ;

        try {
            $sth = $dbh->prepare('UPDATE alizon._produit set act=0 where id = ?');
            $sth -> execute(array($idProduit));
        } catch (PDOException $e){
            echo '<p class="feedbackERR">Erreur : '.$e.'</p>';
        }
        echo '<p class="feedbackOK">Le produit '.$idProduit.' à bien été activé !</p>' ;
    }

    // Permet de désactiver un produit par ID
    function desactiverProduitById($idProduit){
        global $dbh ;

        try {
            $sth = $dbh->prepare('UPDATE alizon._produit set act=1 where id = ?');
            $sth -> execute(array($idProduit));
        } catch (PDOException $e){
            echo '<p class="feedbackERR">Erreur : '.$e.'</p>';
        }
        echo '<p class="feedbackOK">Le produit '.$idProduit.' à bien été désactivé !</p>' ;
    }

    // Permet de supprimer un produit par ID
    function supprimmerProduitById($idProduit){
        global $dbh ;

        try {
            $sth = $dbh->prepare('UPDATE alizon._produit set act=2 where id = ?');
            $sth -> execute(array($idProduit));
        } catch (PDOException $e){
            echo '<p class="feedbackERR">Erreur : '.$e.'</p>';
        }
        echo '<p class="feedbackOK">Le produit '.$idProduit.' à bien été supprimé !</p>' ;
    }
?>