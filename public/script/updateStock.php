<?php

    require('./../connectParams.php');

    var_dump($_POST);
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
 
    global $dbh;

    try {
        $sth = $dbh->prepare('UPDATE alizon._produit set stock= ? where id = ?');
        $sth -> execute(array($_POST['quantity'],$_POST['id']));
    } catch (PDOException $e){
        echo '<p class="feedbackERR">Erreur : '.$e.'</p>';
    }

    header("Location: ../vendeur/mesProduits.php");

?>