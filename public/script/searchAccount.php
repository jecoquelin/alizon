<?php

// pour une explication de ce code voire rechercher dans search.php
function rechercherCompte($nomCompte){
    global $dbh;
    try {
        $nomCompte = "%" . $nomCompte . "%";
        $sth = $dbh->prepare('SELECT * FROM alizon._compte_client WHERE pseudo LIKE ? OR nom LIKE ? OR prenom LIKE ? OR email LIKE ?');

        $sth->execute(array($nomCompte, $nomCompte, $nomCompte, $nomCompte));
        $compte = $sth->fetchAll(PDO::FETCH_ASSOC);

        return $compte;
        
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
}

function rechercherCompteVendeur($nomCompte){
    global $dbh;
    try {
        $nomCompte = "%" . $nomCompte . "%";
        $sth = $dbh->prepare('SELECT * FROM alizon._compte_vendeur WHERE nom LIKE ? OR email LIKE ?');

        $sth->execute(array($nomCompte, $nomCompte));
        $compte = $sth->fetchAll(PDO::FETCH_ASSOC);

        return $compte;
        
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
}

?>