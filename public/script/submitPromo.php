<?php 
require('prefixe.php');
include($prefixe.'../connectParams.php');
$dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
$dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

/*on regarde d'où l'on viens, du vendeur ou de l'admin*/
if(isset($_POST["vendeur"])){
    /*on change l'url de retour*/
    $path="../vendeur/remises.php";
}else{
    $path="../../admin/produits.php";
}

//ce if sert à savoir de quel bouton on accède à ce script
if(isset($_POST["submitRemise"])){
    //ici on ajoute les promo
    try {
        foreach($_POST["Prod"] as $key => $promo){
            $id=$_POST["num"][$key];
            if($promo=="on"){
                //code pour update à l'id retourné par key selon on/off
                $sql = "UPDATE alizon._produit SET promotion=1 WHERE id=?";
                $result = $dbh->prepare($sql);
                $result->execute(array($id));
            }else{
                //code pour update à l'id retourné par key selon on/off
                $sql = "UPDATE alizon._produit SET promotion=0 WHERE id=?";
                $result = $dbh->prepare($sql);
                $result->execute(array($id));
            }
        }
        header("Location: ".$path);
    
    } catch (PDOException $e) {
    
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
}else if(isset($_POST["create"])){
    //ici on créer une remise
    $key = array_search('Valider', $_POST["create"]);
    // on verifie qu'il n'y a pas d'erreur
    if($_POST["nomRemise$key"]=="" || $_POST["dateDebut$key"]=="" || $_POST["dateFin$key"]=="" || $_POST["tauxRemise$key"]==""){
        header("Location: ".$path."?erreur=3");
    }else{
        $nomRemise=$_POST["nomRemise$key"];
        $id_produit=$_POST["num"][$key];
        $dateDebut=$_POST["dateDebut$key"];
        $dateFin=$_POST["dateFin$key"];
        $tauxRemise=$_POST["tauxRemise$key"];
        //pas d'erreur ici non plus
        if($dateDebut > $dateFin){
            header("Location: ".$path."?erreur=2");
        }else{
            $currentDate=date('Y-m-d');
            //toujours verifier les erreurs
            if($dateDebut < $currentDate){
                header("Location: ".$path."?erreur=1");
            }else{ 
                //finalement on ajoute la remise
                $sql = "INSERT INTO alizon._remise(nom_remise,id_produit,date_heure_debut,date_heure_fin,pourcentage_remise) VALUES(?,?,?,?,?)";
                $result = $dbh->prepare($sql);
                $result->execute(array($nomRemise,$id_produit,$dateDebut,$dateFin,$tauxRemise));
                header("Location: ".$path."?erreur=5");
            }
        }
        
    }
}else if(isset($_POST["update"])){
    //ici on la modifie
    $key = array_search('Valider', $_POST["update"]);
    //toujours pour les erreurs
    if($_POST["nomRemise$key"]=="" || $_POST["dateDebut$key"]=="" || $_POST["dateFin$key"]=="" || $_POST["tauxRemise$key"]==""){
        header("Location: ".$path."?erreur=3");
    }else{
        $nomRemise=$_POST["nomRemise$key"];
        $id_produit=$_POST["num"][$key];
        $dateDebut=$_POST["dateDebut$key"];
        $dateFin=$_POST["dateFin$key"];
        $tauxRemise=$_POST["tauxRemise$key"];
        //toujours pour les erreurs
        if($dateDebut > $dateFin){
            header("Location: ".$path."?erreur=2");
        }else{
            $currentDate=date('Y-m-d');
            //toujours pour les erreurs
            if($dateDebut < $currentDate){
                header("Location: ".$path."?erreur=1");
            }else{
                //ici on update
                $sql = "UPDATE alizon._remise SET nom_remise=?,date_heure_debut=?,date_heure_fin=?,pourcentage_remise=? WHERE id_produit=?";
                $result = $dbh->prepare($sql);
                $result->execute(array($nomRemise,$dateDebut,$dateFin,$tauxRemise,$id_produit));
                header("Location: ".$path."?erreur=4");
            }
        }
    }
}else if(isset($_POST["delete"])){
    //ici on la supprime sans verification
    $key = array_search('Supprimer', $_POST["delete"]);
    $id_produit=$_POST["num"][$key];
    $currentDate=date('Y-m-d');
    $sql = "DELETE FROM alizon._remise WHERE id_produit=?";
    $result = $dbh->prepare($sql);
    $result->execute(array($id_produit));
    header("Location: ".$path."?erreur=6");
}

?>