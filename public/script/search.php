<?php
    function rechercher($mot, $cat = null, $prix = null, $promo = false, $vend = null){
        // cette fonction retourne les produits qui coresponde aux critère de recherches
        global $dbh;
        try {
            $produits = [];
            // on previent les injections js avec htmlspecialchars
            $mot = htmlspecialchars(preg_replace('/\s+/', '%', $mot));

            /* objectif à terme de la recherche:
                - séparer les différents mots, 
                - faire une requète qui cherche ces mots dans le titre (ou la description)
                - avoir les autres critères dans la recherche (prix, catégorie)
                - donner un nombre de points à chaque produit basé sur:
                    - mots se suivent ?
                    - mot dans le titre ou la description ?
                    - nbr étoiles
                - afficher les produits de manière triée en fonction du nombre de points
            */

            // https://www.php.net/manual/fr/pdostatement.bindparam.php#99698 merci
            // cela signifie que que les caractères spéciaux du LIKE sont exécutés !?
            // ça n'est pas grave !
            //  -> de toute manière " " est remplacé par %
            //     et le _ n'est pas dangereux
            // donc cette solution permet à lutilisateur d'utiliser le % et le _ mais on l'accepte
            
            // construction de la base de la requète
            // on prend le mot sans les % pour comparer le nom du vendeur exact et donc sans le like qui a besoins des pour cent
            $mot100pour100 = $mot;
            $mot = "%" . $mot . "%";
            $req = 'SELECT alizon._produit.id, id_vendeur, alizon._produit.libelle, prix_ht, id_photos, _description, stock, seuil, duree_livraison, libelle_categorie, taux_TVA, promotion, alizon._compte_vendeur.nom from alizon._produit inner join alizon._categorie on alizon._produit.libelle_categorie = alizon._categorie.libelle inner join alizon._compte_vendeur on alizon._produit.id_vendeur = alizon._compte_vendeur.id WHERE act=0 and (UPPER(alizon._produit.libelle) LIKE UPPER(?) OR UPPER(alizon._compte_vendeur.nom) = UPPER(?))';
            $tabArgsExecute = array($mot, $mot100pour100);
            
            // ajout optionel de promo
            if ($promo) {
                $req = $req . " AND promotion = 1";
            }
            
            // ajout optionnel du prix dans la requete
            if ($prix != null)
            {
                $req = $req . " AND prix_ht * alizon._categorie.taux_TVA <= ?";
                $tabArgsExecute[] = $prix;
            }

            // ajout optionnel de la categorie dans la requete
            if ($cat != null)
            {
                $req = $req . " AND (";
                $premier = true;
                foreach ($cat as $c)
                {
                    if (!$premier)
                    {
                        $req = $req . " OR ";
                    }
                    else
                    {
                        $premier = false;
                    }
                    $req = $req . "libelle_categorie = ?";
                    $tabArgsExecute[] = $c;
                }
                $req = $req . ")";
            }

            // ajout optionnel de des vendeurs dans la requete
            if ($vend != null)
            {
                $req = $req . " AND (";
                $premier = true;
                foreach ($vend as $c)
                {
                    if (!$premier)
                    {
                        $req = $req . " OR ";
                    }
                    else
                    {
                        $premier = false;
                    }
                    $req = $req . "nom = ?";
                    $tabArgsExecute[] = $c;
                }
                $req = $req . ")";
            }

            // on veut que les promo arrivent en haut de la recherche
            $req = $req . " order by promotion desc";

            $sth = $dbh->prepare($req);
            // ajout des différentes valeurs à vérifier avec un prepare pour éviter les injections SQL
            $sth->execute($tabArgsExecute);
            $produits = $sth->fetchAll(PDO::FETCH_ASSOC);
            return $produits;
            
        } catch (PDOException $e) {
            print "Erreur : " . $e->getMessage() . "<br/>";
            die();
        }
    }

    function prixMax($produits){
        // cette fonction retourne le prix maximal de tous les produits produits (attention il faut que les produits aient un taux de tva grace à une jointure sur la table catégorie et sur le champ libele catégorie)
        if (!isset($produits[0]['taux_tva'])) // mec il faut mettre taux_TVA (trop stylé mysql)
        {
            $produits[0]['taux_tva'] = 10000;
        }
        // on calcule le ttc
        $prixMax = $produits[0]['prix_ht'] * $produits[0]['taux_tva'];
        // on cherche le max
        foreach ($produits as $key => $produit) {
            if ($produit['prix_ht'] * $produit['taux_tva'] > $prixMax){
                $prixMax = $produit['prix_ht'] * $produit['taux_tva'];
            }
        }
        return $prixMax;
        // return array($prixMax, $produitMax);
    }

    function getCats($produits){
        // on retourne toutes les catégories des produits recherchés
        $categories = [];
        foreach ($produits as $key => $produit) {
            $categories[$produit['libelle_categorie']] = 1;
        }
        return $categories;
    }

    function getVendeurs($produits){
        // on retourne touts les vendeurs des produits recherchés
        $vendeurs = [];
        foreach ($produits as $key => $produit) {
            $vendeurs[$produit['nom']] = 1;
        }
        return $vendeurs;
    }
?>