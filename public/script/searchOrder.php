<?php

function rechercherCommande($nomClient){
    global $dbh;
    try {
        $nomClient = "%" . $nomClient . "%";
        $sth = $dbh->prepare('SELECT * FROM alizon._commande WHERE nomC LIKE ? OR prenomC LIKE ?');

        $sth->execute(array($nomClient, $nomClient));
        $commandes = $sth->fetchAll(PDO::FETCH_ASSOC);

        return $commandes;
        
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
}

?>