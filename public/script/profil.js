/************************************
 * Partie "Information personnelle" *
 ************************************/
// Par défaut, on affiche les informations personnelles et on cache le formulaire pour modifier
document.getElementById('formInfoPerso').style.display          = 'none'  ;
document.getElementById('affichInfoPerso').style.display        = 'block' ;

// Fait apparaître le formualire de modification des informations personnelles
function modifierInfoPerso(){
    document.getElementById('formInfoPerso').style.display      = 'block' ; //reprend le block et block permet de l'afficher
    document.getElementById('affichInfoPerso').style.display    = 'none'  ;
}

// Enlève le formulaire de modification des infoamtions personnelles et remet l'affichage classique
function afficherInfoPerso(){
    document.getElementById("affichInfoPerso").style.display    = 'block' ; //reprend le block et block permet de ne pas l'afficher
    document.getElementById('formInfoPerso').style.display      = 'none'  ;
}

/*************************
 * Partie "Sécurité" *
 *************************/
// Par défault, on affiche les informations de sécurité
document.getElementById('formMotDePasse').style.display ='none';
document.getElementById('infoMotDePasse').style.display ='block' ;

// Fait apparaître le formulaire de modification de mot de passe
function modifierMotDePasse(){
    document.getElementById('formMotDePasse').style.display      = 'block' ; 
    document.getElementById('infoMotDePasse').style.display      = 'none'  ;
}

// Fait disparaitre le formulaire de modification de mot de passe
function afficherMotDePasse(){
    document.getElementById("infoMotDePasse").style.display        = 'block' ;
    document.getElementById('formMotDePasse').style.display        = 'none' ;
}
