<?php 
session_start();
require("../vendeur/prefixe.php");
require('../connectParams.php');
require('product.php');
//ce if sert à savoir de quel bouton on accède à ce script
try {
    $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
    
    $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    print "Erreur : " . $e->getMessage() . "<br/>";
    die();
}
// Variable de session qui reçoit l'id des produit qui ont été selectionné
$_SESSION['produits_active'] = [];

if(isset($_POST["submitCatalogue"])){
    //ici on ajoute les promo
    try {
        $tab = []; // tab sera la liste des produit que le vendeur a choisi
        foreach($_POST["Prod"] as $key => $promo){
            $id=$_POST["num"][$key];
            if($promo=="on"){
                $p = getProduitById($id);
                // On push les id dedans
                array_push($_SESSION['produits_active'], $id);
                
                $tab = $tab + [ $id => [
                            'image' => 'https://alizon.meatman.fr/images/produits/'.$id.'/'.getMainPicProd($p['id']),
                            'nom' => $p['libelle'],
                            'description' => $p['_description'],
                            'prix ttc' => round(($p['prix_ht']*getTauxTva($p["libelle_categorie"])),2),
                            'prix ht' => $p['prix_ht']
                        ]
                ];
            }
        }

        

        //ajouter les produit choisi par le vendeur dans le tableau de la SESSION
        $_SESSION['listeProduits']['vendeurs'][$p['id_vendeur']]['produits'] = $tab; 

        //Met le chemin du fichier dans une variable
        $chemin = '../images/vendeur/photoProfil/'.$p['id_vendeur'].'/';

        //Regarde si le dossier est créer et sinon il le créer
        if (!is_dir($chemin.'catalogue')){
            mkdir($chemin.'catalogue');
        }
        
        //met le nom fichier dans une variable
        $file = $chemin.'catalogue/catalogue.json';

        //Tranforme le tableau en JSON 
        $donnee = json_encode($_SESSION['listeProduits']);

        //Ecrit les donnée du JSON dans le dossier spécifié
        file_put_contents($file, $donnee);

        //Efface les produits sélectionné au cas ou il reselectionnerais d'autre produit
        $_SESSION['listeProduits']['vendeurs'][$p['id_vendeur']]['produits'] = [];

        header("Location: ../vendeur/catalogue.php");
    
    } catch (PDOException $e) {
    
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
}
?>