<?php

require('product.php'); 


//$produit=getProduitById($produit_panier['id_produit']);

//fonction permettat de retrouner le prix HT total des produits
function prixHT($idPanier){
    global $dbh;
    $prixTotalHT = 0;


    foreach($dbh->query("SELECT * from alizon._dans_panier where id_panier=$idPanier") as $produit_panier) { /*Pour chaque article du panier n°$idPanier*/
        $produit=getProduitById($produit_panier['id_produit']);
        $prix= round($produit['prix_ht'] *$produit_panier['nombre'],2);  //Le prix du produitr * le nombre de ce produit dans le panier
        $prixTotalHT= round(($prixTotalHT+$prix),2);

    }
    return number_format($prixTotalHT,2); 
}

//Fonction permettant de retourner le prix totale comprenant la TVA des produits 
function prixTotal($idPanier){
    global $dbh;
    $prixTotal = 0;


    foreach($dbh->query("SELECT * from alizon._dans_panier where id_panier=$idPanier") as $produit_panier) { /*Pour chaque article du panier n°$idPanier*/
        $produit=getProduitById($produit_panier['id_produit']);
        $estRemise = getRemise($produit['id']);
        $remiseHT = $produit['prix_ht']*((100-getRemise($produit['id']))/100); 

        if ($estRemise != false) {
            $prix = round(($remiseHT*getTauxTva($produit["libelle_categorie"]))*$produit_panier['nombre'],2);
        }
        else {
            $prix = round(($produit['prix_ht'] * getTauxTva($produit['libelle_categorie']))*$produit_panier['nombre'],2);
        }


        
        $prixTotal= round(($prixTotal+$prix),2);

    }
    return number_format($prixTotal,2); 
}

/*
function verifDateCB($dateCB,){
    $tabDateCB   = explode('-', $dateCB);
    $anneeCB = $tabDateCB[0] ;
    $moisCB   = $tabDateCB[1] ;
    $jourCB   = $tabDateCB[2] ;
    // Récupération de la date du jour
    $currentYear    = date('Y') ;
    $currentMouth   = date('m') ;
    $currentDay     = date('d') ;
    // Vérification que la date ne soit pas supérieur à aujourd'hui
    if ($anneeCB > $currentYear){
        echo '<p class="feedbackERR">Erreur : Veillez saisir une année valide ! (Vous ne pouvez être né dans le futur...)</p>' ;
    }
    else if ( == $currentYear){
        if ($moisCB > $currentMouth){
            echo '<p class="feedbackERR">Erreur : Veillez saisir un mois valide ! (Vous ne pouvez être né dans le futur...)</p>' ;   
        }
        else if ($moisCB == $currentMouth){
            if ($jourCB > $currentDay){
                echo '<p class="feedbackERR">Erreur : Veillez saisir un jour valide ! (Vous ne pouvez être né dans le futur...)</p>' ;
            }
        }
    }
}
*/

?>