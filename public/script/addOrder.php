
    <?php 
    session_start() ;

    require('./prefixe.php');
    // dans les connect params on trouve toutes les infos de connection pour la bdd
    include('./../connectParams.php');

    // Connection à l abase de données
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }


    require( "./paiement.php");
    require('./client.php');
            // Réception des différentes variables utiles suite à la page de prépaiement ;
            $idPanier = $_SESSION['idPanier'] ;
            $idClient=  $_SESSION["idClient"] ;
            $prixT = prixTotal($idPanier);

            //verifie que chaque article a du stock encore disponible pour la qte demandee, si non, recharge le panier
            foreach($dbh->query("SELECT * from alizon._dans_panier where id_panier=$idPanier") as $produit_panier) { 
                $produit=getProduitById($produit_panier['id_produit']);
                $stock = $produit['stock'];
                $nbDansPanier = $produit_panier['nombre'];
                if ($nbDansPanier>$stock){
                    header("Location: ./../panier.php");
                    exit();
                }
            }

            if (isset($_POST)){
                // Addresse de facturation
                $numVoieF =  $_POST["numeroVoieF"];
                $nomVoieF =  $_POST['nomVoieF'];
                $villeF   =  $_POST["villeF"];
                $postalF  =  $_POST["postalF"];
                $batimentF=  $_POST['batimentF'];
                $etageF   =  $_POST['etageF'];
                $numPorteF=  $_POST['numeroPorteF'];

                if (isset($_POST['sameAddr'])){
                    // Addresse de livraison
                    $numVoie  = $numVoieF;
                    $nomVoie  = $nomVoieF;
                    $ville    = $villeF;
                    $postal   = $postalF;
                    $batiment = $batimentF;
                    $etage    = $etageF;
                    $numPorte = $numPorteF;
                }
                else {
                    $numVoie =  $_POST["numeroVoie"];
                    $nomVoie =  $_POST['nomVoie'];
                    $ville   =  $_POST["ville"];
                    $postal  =  $_POST["postal"];
                    $batiment=  $_POST['batiment'];
                    $etage   =  $_POST['etage'];
                    $numPorte=  $_POST['numeroPorte'];
                }
                // Identité
                $nom    =   htmlspecialchars($_POST["nom"]);
                $prenom =   htmlspecialchars($_POST["prenom"]);
                $dateNaissance = $_POST['date_naissance'] ;
                // Checkbox
                if (!isset($_POST['retenirAdr'])){ // Si la case n'est pas coché il ne connait pas retenirAdr qui est problématique pour savoir si on doit ou non modofier l'adresse
                    $checkModifierAdresse = false ;
                }else {
                    if ($_POST['retenirAdr'] == true){
                        $checkModifierAdresse = true ;
                    }
                    else {
                        $checkModifierAdresse = false ;
                    }

                }
                if (!isset($_POST['retenirDate'])){ // Si la case n'est pas coché il ne connait pas retenirAdr qui est problématique pour savoir si on doit ou non modofier l'adresse
                    $checkModifierDateNaissance = false ;
                }else {
                    if ($_POST['retenirDate'] == true){
                        $checkModifierDateNaissance = true ;
                    }
                    else {
                        $checkModifierDateNaissance = false ;
                    }
                }
            }


            // Pour modifier l'adresse postale d'un client dans la base de donnée
            if ($checkModifierAdresse == true){
                modifierAdresseLivraison($numVoie, $nomVoie, $ville, $postal, $batiment, $etage, $numPorte, $idClient) ;
            }

            // Pour modifier la date de naissance d'un client dans la base de donnée
            if ($checkModifierDateNaissance == true){
                modifierDateNaissance($dateNaissance, $idClient);
            }

            //verification qu'une commande n'existe pas
            function checkCommande($nb){
                $bool=0;
                global $dbh;
                $sth = $dbh->prepare("SELECT count(*) from alizon._commande WHERE id=$nb ");
                $sth -> execute();
                $commande = $sth -> fetchAll();
                $resultat = $commande[0]['count'];
                if($resultat>=1){
                    $bool=1;
                }
                return $bool;
            }

            // Creation commande
            $rand=rand(0,9999999);
            $fin=0;
            while($fin!=1){
                if(checkCommande($rand)==0){
                    global $dbh;
                    $sth = $dbh->prepare("INSERT INTO alizon._commande(id,prix_total_TTC, date_expedition, date_commande, etat_l, numero_voie_l, nom_voie_l, code_postal_l, ville_l, batiment_l, etage_l, numero_porte_l, numero_voie_f, nom_voie_f, code_postal_f, ville_f, batiment_f, etage_f, numero_porte_f,id_client, nomC, prenomC) values ($rand,$prixT, current_date, current_date, 0, '$numVoie', '$nomVoie', '$postal', '$ville', '$batiment', '$etage', '$numPorte', '$numVoieF', '$nomVoieF', '$postalF', '$villeF', '$batimentF', '$etageF', '$numPorteF',$idClient, '$nom', '$prenom')");
                    $sth -> execute();
                    donnerCommandeSimu($rand);
                    echo "creation commande OK ";
                    $fin=1;
                }else{
                    $rand=rand(0,9999999);
                }
            }

            //Recuperation de l ID de la commande créée
            $sth= $dbh->query("SELECT $rand from alizon._commande");
            $idCommande=$sth->fetchColumn();
            echo "id: ".$idCommande;  
            
            
            //decrementation stock, vidage panier, insertion des produits dans _est_commandé
            foreach($dbh->query("SELECT * from alizon._dans_panier where id_panier=$idPanier") as $produit_panier) { /*Pour chaque article du panier n°$idPanier*/
                $produit=getProduitById($produit_panier['id_produit']);
                $nbProduit=$produit_panier['nombre'];
                $idProduit=$produit_panier['id_produit'];
                $estRemise = getRemise($produit['id']);
                $remiseHT = $produit['prix_ht']*((100-getRemise($produit['id']))/100); 

                if ($estRemise != false) {
                    $prixTTC = round(($remiseHT*getTauxTva($produit["libelle_categorie"]))*$produit_panier['nombre'],2);
                }
                else {
                    $prixTTC= round(($produit['prix_ht'] * getTauxTva($produit['libelle_categorie']))*$produit_panier['nombre'],2);
                }
                $sth = $dbh ->query("UPDATE alizon._produit SET stock = stock - $nbProduit WHERE id= $idProduit");
                $sth = $dbh ->query("DELETE FROM alizon._dans_panier WHERE id_produit= $idProduit and id_panier= $idPanier");
                $sth = $dbh ->query("INSERT INTO alizon._est_commande(id_commande,id_produit,quantite,prix_TTC) values ($idCommande, $idProduit, $nbProduit,$prixTTC)");
            }
            echo "\nDecrementation stock, vidage Panier et table _est_commandé =>OK ";
            $_SESSION['idCommande']=$idCommande; // idcommande
            header("Location: ./../recapitulatifCommande.php");
            
        ?>

