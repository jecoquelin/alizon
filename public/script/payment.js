//test();

// Fonction permettant de rediriger vers une autre fonction.
// Elle nous permet de voir quel est la value de l'option 
// pour ensuite redirigé vers une fonction pour afficher ou non les inforations de la carte bancaire.
function change(param) {
  switch (param){
      case "cb" : changeCB(); break;
      case "paypal" : changePaypal() ; break;
  }
}

// Fonction permettant d'afficher le block pour les infos 
// de la carte bancaires quand on clique sur la carte bancaire
function changeCB(){
  let input = document.getElementsByClassName("input_cb");
  document.getElementById("paiementCB").style.display = "block"; //reprend le block et block permet de l'afficher
  document.getElementById('payer').disabled = true;
  for (i = 0; i < input.length; i++) {
    input[i].required = true;
  }
  document.getElementById("payer").disabled = false ;
  document.getElementById("payer").style.backgroundColor = "#FF8605" ;

}

// Fonction permettant de ne pas affiche le block pour les infos 
// de la CB quand on change pour paypal dans la liste déroulante 
function changePaypal(){
  let input = document.getElementsByClassName("input_cb");
  document.getElementById("paiementCB").style.display = "none"; //reprend le block et block permet de ne pas l'afficher
  document.getElementById('payer').disabled = false;

  for (i = 0; i < input.length; i++) {
    input[i].required = false;
  }

  document.getElementById("payer").disabled = true ;
  document.getElementById("payer").style.backgroundColor = "grey" ;
}



// fonction permettant de cacher un bloc si l'adresse de livraison est pareil que l'adresse de livraison
function sameAddress(){
  let input = document.getElementsByClassName("input_adrL");
  // Si la case a coché est coché, le form n'est plus visible et le champ n'as pas besoin d'être rempli sinon C'est comme un bloc normal
  if(document.getElementById("sameAddr").checked){ 
    document.getElementById("formAdresseLivraison").style.display = "none" ;
    
    for (i = 0; i < input.length; i++) {
      input[i].required = false;
    }

  }else{ 
    document.getElementById("formAdresseLivraison").style.display = "block"
    for (i = 0; i < input.length; i++) {
      input[i].required = true;
    }
  } 
}








/**
 * @breif La fonction permet de veriffier si une Carte bancaire est valide
 * @param {*} val 
 */
function CheckCreditCard(val) {

  document.getElementById('payer').disabled = true;
  

  error = document.getElementById("error"); //Cette variable vas servir a créer un message d'erreur
  error.textContent = "";                   // Elle ne contient aucun texte au début
  error.style.color = "red";                

  if(val.match(/^([0-9])/g) && !val.match(/[a-zA-Z]/g)){ // regex permettant de filtrer que les nombre et pas les autre caractères
      
    

      let myFunc = num => Number(num);  //défini une fonction permettant de transformer un string en nombre 

      var tabloNumber = Array.from(String(val), myFunc); //met dans un tableau les valeur de val qui sont transformé en int
      
      // fonction anonyme permettant de voir si le nombre doit se multiplier et s'il est plus grand que 9 quand il est multiplié
      // Les nombre sont ainsi contenu dans le tableau "tabAfterModif"
      // Après cela, on met dans somme les valeurs qui ont été multiplié plus ceux qui ne l'ont pas été et on fait la somme de tout cela 
      let tabAfterModif = tabloNumber.map((element, i) => {
          if (((tabloNumber.length-i)%2) == 0){
              element = element*2;
              if (element > 9) {
                  element = element - 9;
              }
          } 
          
          return element;
      })
      let somme = tabAfterModif.reduce((accumulator, currentValue) => { return (accumulator + currentValue) }, 0)

      //Verifie si la longueur correspond au norme ainsi que la somme du chiffre
      if (((somme % 10) != 0) || (val.length < 13) || (val.length>16)) {
        
        error.textContent = "Carte non valide";
        
      }else {
        document.getElementById('payer').disabled = false;
      }
    
  } else if (val) { //Vérifie si le champ val n'est pas un string vide
      error.textContent = "Le champ ne peut acceuillir des lettres";
  }
}

// Regarde su le criptogramme est valide
function CheckCriptogramme (val){
  // Prépare l'erreur en créant un lien avec le block html et le mettant en rouge
  errorCryptgramme = document.getElementById("errorCryptgramme");
  errorCryptgramme.textContent = "";
  errorCryptgramme.style.color = "red";
  document.getElementById('payer').disabled = true;

  // Regarde si le criptogramme contient que des chiffres et a une longueur de 3
  // si oui, l'erreur n'est pas afficher sinon elle est affiché
  if (val.match(/([0-9]{3})/g) && val.length == 3){
    errorCryptgramme.textContent = "";
    document.getElementById('payer').disabled = false;
  } else {
    errorCryptgramme.textContent = "Le cryptogramme doit contenir 3 chiffres seulement.";
    document.getElementById('payer').disabled = true;
  }
}

var yearSelect = document.querySelector('#year');


function test(){

  const nativePicker = document.querySelector('.nativeDatePicker');
  const fallbackPicker = document.querySelector('.fallbackDatePicker');
  const fallbackLabel = document.querySelector('.fallbackLabel');


  // affiche les constante
  fallbackPicker.style.display = 'block';
  fallbackLabel.style.display = 'block';

  // Testez si une nouvelle date saisie revient à une saisie de texte ou non.
  const test = document.createElement('input');
  console.log("coucou");
  try {
    test.type = 'month';
  } catch (e) {
    console.log(e.description);
  }

  // Si c'est le cas, exécutez le code dans le bloc if () {}.
  if (test.type === 'text') {
    // Masquer le sélecteur natif et afficher le sélecteur de remplacement
    nativePicker.style.display = 'none';
    fallbackPicker.style.display = 'block';
    fallbackLabel.style.display = 'block';

    // Remplir les années dynamiquement
    // (les mois sont toujours les mêmes, donc codés en dur)
    populateYears();
  } else {
    nativePicker.style.display = 'block';
    fallbackPicker.style.display = 'none';
    fallbackLabel.style.display = 'none';
  }
}


function populateYears() {
  // Obtenir l'année en cours sous la forme d'un nombre
  const date = new Date();
  const year = date.getFullYear();
  const yearSelect = document.querySelector('#year');
  // Rendre cette année, et les 5 années suivantes, disponibles dans l'année <select>.
  for (let i = 0; i <= 5; i++) {
    const option = document.createElement('option');
    option.textContent = year + i;
    yearSelect.appendChild(option);
  }
}
