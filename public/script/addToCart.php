<?php
session_start();

try {
    include('../connectParams.php');
    $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
    
    $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (PDOException $e) {

    print "Erreur : " . $e->getMessage() . "<br/>";
    die();
}

// si $_GET["quantity"], $_GET["idProd"] et $_SESSION["idPanier"]) sont set
//      on fait l'update dans la base de donnée dans la quantitée dans le panier
// sinon
//      on passe cette étape et on fait directement la redirection
$qteReelementAjoutee = 0;
if (isset($_GET["quantity"]) && isset($_GET["idProd"]) && isset($_SESSION["idPanier"]))
{
    $qte=$_GET["quantity"]; //quantité a ajouter au panier
    $idProduit=$_GET["idProd"]; //id du produit a ajouter au panier
    $idPanier = $_SESSION['idPanier'];  //id du panier dans lequel ajouter et rediriger
    
    //cherche dans la bdd la quantité de l'article dans le panier
    $sth = $dbh->prepare('SELECT nombre from alizon._dans_panier where id_panier = ? and id_produit = ?');
    $sth -> execute(array($idPanier,$idProduit));
    $dansPanier = $sth -> fetchAll();

    // cherche dans la bdd la quantité de l'article en stock
    $sth = $dbh->prepare('SELECT stock from alizon._produit where id = ?');
    $sth->execute(array($idProduit));
    $quantiteProduitDansStock = $sth->fetchall();

    // si le produit existe
    //      on essaie l'ajout dans le pannier
    // sinon
    //      on passe cette étape et on fait directement la redirection
    if (isset($quantiteProduitDansStock[0])){
        //si le produit existe deja dans le panier, on update sa quantité
        $produitDansPanier = false ;
        if (isset($dansPanier[0])){
            if($dansPanier[0]['nombre']>0){
                $produitDansPanier = true ;
            }
        }
        if($produitDansPanier){
            if ($quantiteProduitDansStock[0]['stock'] >= $dansPanier[0]['nombre'] + $qte){
                $sth = $dbh->prepare("UPDATE alizon._dans_panier SET nombre = nombre+? where id_produit = ?");
                $sth -> execute(array($qte, $idProduit));
                $qteReelementAjoutee = $qte;
            }else{
                $sth = $dbh->prepare("UPDATE alizon._dans_panier SET nombre = ? where id_produit = ?");
                $sth -> execute(array($quantiteProduitDansStock[0]['stock'], $idProduit));
                $qteReelementAjoutee = $quantiteProduitDansStock[0]['stock'] - $dansPanier[0]['nombre'];
            }
        }else{  //sinon on l'ajoute dans le panier
            if ($quantiteProduitDansStock[0]['stock'] >= $qte){
                $sth = $dbh->prepare("INSERT INTO alizon._dans_panier(id_produit,id_panier,nombre) values (?,?,?)");
                $sth -> execute(array($idProduit,$idPanier,$qte));
                $qteReelementAjoutee = $qte;
            }else{
                $sth = $dbh->prepare("INSERT INTO alizon._dans_panier(id_produit,id_panier,nombre) values (?,?,?)");
                $sth -> execute(array($idProduit,$idPanier,$quantiteProduitDansStock[0]['stock']));
                $qteReelementAjoutee = $quantiteProduitDansStock[0]['stock'];
            }
        }
    }
}

// on vérifie si on doit recharger la page
$recharger = true;
if (isset($_GET["reload"])) {
    if ($_GET["reload"] == "non") {
        echo $qteReelementAjoutee;
        $recharger = false;
    }
}

// si la page doit être rechargée
//    on la recharge
// sinon
//    on ne fait rien
if ($recharger){
    // on utilise une variable GET pour connaitre la provenance de l'utilisateur quand il est redirigé vers ce script de manière à le rredirigé ensuite vers la page d'ou il venait
    if (isset($_GET["provenance"])){
        if (isset($_GET["scroll"])){
            $scroll = $_GET["scroll"];
        }
        else {
            $scroll = 0;
        }
        
        if ($_GET["provenance"] == "recherche.php"){
            // dans le cas d'une recherche on a besoins de connaitre plusieurs champs
            $contenuLien = "&search=" . $_SESSION['infosRecherches']['search'];
            if ($_SESSION['infosRecherches']['prix'] != null) {
                $contenuLien = $contenuLien . "&prix=" . $_SESSION['infosRecherches']['prix'];
            }
            foreach ($_SESSION['infosRecherches']["cat"] as $cat) {
                $contenuLien = $contenuLien . "&cat[]=" . $cat;
            }
            // on redirig vers la page de recherche
            header("Location: ../".$_GET['provenance']."?add=Y&scroll=" . $scroll . $contenuLien);
        } else {
            // on renvoie l'utilisateur d'ou il vient
            header("Location: ../".$_GET['provenance']."?add=Y&scroll=" . $scroll);
        }
    } else {
        // on renvoie l'utilisateur sur la page détail produit
        header("Location: ../detailProduit.php?id=$idProduit&add=Y&scroll=" . $scroll);
    }
}
?>
