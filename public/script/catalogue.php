<?php
    // Cette fonction sert à lire le fichier CSV dont le nom est passé en entrée
    // Si la lecture des données ne comporte pas d'erreurs une tableau contenant les données est renvoyé
    // Sinon, si des erreurs sont détéctées, false est renvoyé
    function fiCSVersTab($nomFic)
    {
        $fic = fopen($nomFic, "r");
        $res = Array();
        $champs = fgetcsv($fic, null, ",");

        if ($champs === false)
        {
            return false;
        }

        try
        {
            while (!feof($fic))
            {
                $pos = ftell($fic);
                $ligne = fgetcsv($fic, null, ",");
                if ($ligne === false and $pos != ftell($fic)) // on vérifie que la fin du fichier n'est composé que de lignes vides
                {
                    return false;
                }
                if ($ligne != Array())
                {
                    $temp = Array();
                    foreach ($ligne as $nbr => $val)
                    {
                        if ($val === "")
                        {
                            return false;
                        }
                        if (!array_key_exists($nbr, $champs))
                        {
                            return false;
                        }
                        $temp[$champs[$nbr]] = $val;
                    }
                    if (count($temp) == count($champs))
                    {
                        $res[] = $temp;
                    }
                    else if (count($temp) != 1 or $temp[$champs[0]] != "")
                    {
                        return false;
                    }
                }
            }
        }
        catch (Exception $e)
        {
            return false;
        }

        if (count($res) == 0)
        {
            return false;
        }
        
        fclose($fic);

        return $res;
    }

    // Cette fonction renvoie le nom du dossier dans lequel les images du prochain produit seront ajoutées
    function rechercherNumDossierSuivant(){
        global $prefixe;
        $dossiers = scandir($prefixe."images/produits/");
        $max = 0;
        foreach ($dossiers as $dossier) {
            if ($max < $dossier){
                $max = $dossier;
            }
        }
        return $max + 1;
    }

    // Cette fonction prend en entrée un produit et l'ajoute au site
    // Cela comprend les ajouter à la base de donnée, et ajouter les images dans un nouveau dossier dans /html/images/produits/<numéro du dossier>
    // Cette fonction renvoie true si l'import du produit s'est bien passé
    // Sinon, si il y a eu un problème lors de l'import du produit cette fonction renvoie false
    // ATTENTION, si cette fonction échoue, il peut n'y avoir que la partie de création du dossier qui ait fonctionnée sans que l'import dans la base
    // de donnée ait fonctionné
    function ajouterUnProduitCSV($tab,$idVendeur)
    {
        global $dbh;
        global $prefixe;

        if (isset($tab['dossier_photos']) and isset($tab['libelle']) and isset($tab['prix_ht']) and isset($tab['_description']) and isset($tab['stock']) and isset($tab['seuil']) and isset($tab['duree_livraison']) and isset($tab['libelle_categorie']))
        {
            // on importe seulement les images avec le format .png, .jpeg, .jpg, .gif
            $images = preg_grep('/.*.(png|jpeg|jpg|gif)/', scandir($prefixe.'produits/' . $tab['dossier_photos']));
    
            // création dossier
            $numDossier = rechercherNumDossierSuivant();
            $cheminDest = $prefixe."images/produits/" . $numDossier . '/';
            mkdir($cheminDest);
    
            $inc = 1;
    
            // import des images
            foreach ($images as $img)
            {
                $cheminSource = $prefixe.'produits/' . $tab['dossier_photos'] . '/' . $img;
                $extension = explode('.', $img);
                copy($cheminSource, $cheminDest . $inc . '.' . end($extension));
                $inc++;
            }
            try {
                // import du produit dans la base de donnée
                $sth = $dbh->prepare("INSERT INTO alizon._Produit(id_vendeur, libelle, prix_ht,id_photos,_description,stock,seuil,duree_livraison,libelle_categorie, act) values (?,?,?,?,?,?,?,?,?,?)");
                $sth -> execute(Array($idVendeur,
                $tab['libelle'],
                $tab['prix_ht'],
                $numDossier,
                $tab['_description'],
                $tab['stock'],
                $tab['seuil'],
                $tab['duree_livraison'],
                $tab['libelle_categorie'],
                0));

                $res = true;
            } catch (\Throwable $th) {
                $res = false;
            }
        }
        else
        {
            $res = false;
        }

        return $res;
    }
?>