<?php

// pour une explication de ce code voire rechercher dans search.php
function rechercherProduits($nomProduit){
    global $dbh;
    try {
        $produits = [];
        $nomProduit = htmlspecialchars(preg_replace('/\s+/', '%', $nomProduit));

        /* objectif à terme de la recherche:
            - séparer les différents mots, 
            - faire une requète qui cherche ces mots dans le titre (ou la description)
            - avoir les autres critères dans la recherche (prix, catégorie)
            - donner un nombre de points à chaque produit basé sur:
                - mots se suivent ?
                - mot dans le titre ou la description ?
                - nbr étoiles
            - afficher les produits de manière triée en fonction du nombre de points
        */

        // https://www.php.net/manual/fr/pdostatement.bindparam.php#99698 merci
        // cela signifie que que les caractères spéciaux du LIKE sont exécutés !?
        // ça n'est pas grave !
        //  -> de toute manière " " est remplacé par %
        //     et le _ n'est pas dangereux
        // donc cette solution permet à lutilisateur d'utiliser le % et le _ mais on l'accepte
        
        // construction de la base de la requète
        $nomProduit = "%" . $nomProduit . "%";
        $req = 'SELECT * from alizon._produit WHERE act=0 and UPPER(alizon._produit.libelle) LIKE UPPER(?) ORDER BY id';
        $tabArgsExecute = array($nomProduit);

        $sth = $dbh->prepare($req);
        // ajout des différentes valeurs à vérifier avec un prepare pour éviter les injections SQL
        $sth->execute($tabArgsExecute);
        $produits = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $produits;
        
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
}

// Retournes tous les produits d'un vendeur et correspondant à la recherche
function rechercherProduitsByVendeur($nomProduit, $idVendeur){
    global $dbh;
    try {
        $produits = [];
        $nomProduit = htmlspecialchars(preg_replace('/\s+/', '%', $nomProduit));
        
        // Construction de la requête
        $nomProduit = "%".$nomProduit."%";
        $req = 'SELECT * from alizon._produit WHERE act!=2 and UPPER(alizon._produit.libelle) LIKE UPPER(?) and id_vendeur=? ORDER BY id';
        $tabArgsExecute = array($nomProduit, $idVendeur);

        $sth = $dbh->prepare($req);
        // Execution de la requête avec les arguments
        $sth->execute($tabArgsExecute);
        $produits = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $produits;
        
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
}

function rechercherProduitsEnVenteByVendeur($nomProduit, $idVendeur){
    global $dbh;
    try {
        $produits = [];
        $nomProduit = htmlspecialchars(preg_replace('/\s+/', '%', $nomProduit));
        
        // Construction de la requête
        $nomProduit = "%".$nomProduit."%";
        $req = 'SELECT * from alizon._produit WHERE act=0 and UPPER(alizon._produit.libelle) LIKE UPPER(?) and id_vendeur=? and act<>? ORDER BY id';
        $tabArgsExecute = array($nomProduit, $idVendeur, 2);

        $sth = $dbh->prepare($req);
        // Execution de la requête avec les arguments
        $sth->execute($tabArgsExecute);
        $produits = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $produits;
        
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
}



?>