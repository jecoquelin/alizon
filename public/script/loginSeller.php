<?php
    // ce script s'excute de bout en bout

    session_start();
    include('../connectParams.php');
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        // cette variable de session sert à stocker les infos de connections pour les restituer dans le formulaire si une erreur est commise
        $_SESSION['dataConnection'] = $_POST;
        // on vérifie que tous les champs sont bien remplie
        if (isset($_POST['emailVendeur']) && isset($_POST['password'])) {
            $email = $_POST['emailVendeur'];
            $pass = $_POST['password'];
            $sql = "SELECT * FROM alizon._compte_vendeur where email = :email";
            $result = $dbh->prepare($sql);
            $result->bindValue(":email",$email,PDO::PARAM_STR);
            $result->execute();
            $_SESSION["connect"] = false;
            // si l'email est dans la bdd donc que le compte existe
            if ($result->rowCount() > 0) {
                $data = $result->fetch();
                if ($data['active'] == 1){
                    // on utilise password verify pour vérifier que le hash du mdp de la bdd coresponf au mdp donnée
                    if (password_verify($pass, $data['mdp'])) {
                        $_SESSION['email'] = $email;
                        // l'authentification à réussi on utilise la variable de session connect pour spécrifer que l'utilisateur est connecté
                        $_SESSION['connectVendeur'] = true;
                        $_SESSION['idVendeur'] = $data['id'];
                        // on a plus besoins des données de connection vu que l'utilisateur ne devra pas retaper ses infos de connection
                        if (isset($_SESSION['dataConnection'])){
                            unset($_SESSION['dataConnection']);
                        }
                        header('Location: ../vendeur/index.php');
                    } else { // gestion des erreur en focntion de infos de connections qui pose probleme
                        header('Location: ../vendeur/connexionVendeur.php?erreur=0');
                    }
                } else {
                    header('Location: ../vendeur/connexionVendeur.php?erreur=3');
                }
            } else {
                header('Location: ../vendeur/connexionVendeur.php?erreur=0');
            }
        } else {
            header('Location: ../vendeur/connexionVendeur.php?erreur=2');
        }
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
?>
