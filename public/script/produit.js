// Affiche une pop-up pour avoir la confirmation de suppression d'un produit
function confirmationSuppression(){
    
    // On récupère toutes les classes poubelles
    let lienSuppressionProduits = document.getElementsByClassName("poubelle");
    
    // On affecte au classe poubelle un écouteur click sur eux
    for (let elem of lienSuppressionProduits) {
        elem.addEventListener("click", async function(event) {
            let href = elem.getAttribute("href");
            var confirmation = confirm("Êtes-vous sûre de vouloir supprimer définitevement ce produit ?");

            // Si l'utilisateur est sur de supprimer, on envoie la requete de suppression + ajoute la notif + rechargement de la page
            if (confirmation){
                let response = await fetch(href);
                notif(response, "Produits supprimé", "Erreur lors de la suppresion du produit");
                location.reload();
            }
        })
    }
}


// Permet de modifier l'état d'un produit sans recharger la page
function modifierEtatProduit(){

    // On récupère toutes les classes switch
    let lienActiveProduits = document.getElementsByClassName("switch");

    // On affecte au classe switch un écouteur click sur eux
    for (let elem of lienActiveProduits) {
        changerCouleur(elem);

        elem.addEventListener("click", async function(event) {
            
            // Récupère la requête présent dans le href de l'élément
            let href = elem.getAttribute("href");
            // Execute la requête
            const response = await fetch(href);
            // Ajoute la notif
            notif(response, "Etat correctement modifié !", "Erreur lors de la modification de l\'état du produit");
            
            changerCouleur(elem);

            // changerEtat(elem);   
        
        });
    }
}

// Permet d'ajouter une notif avec un message d'erreur ou un message de succès
function notif(response, messageOK, messageERR){
    // Si requete OK alors ajouter messageOK
    // Sinon ajouter messageERR
    if (response.ok){
        console.log(response.text());
        let div = '<div class="card"id="notif">';
        div += '<h6>'+ messageOK +'</h6>';
        div += '</div>';
        $('#emplacement_notif').append(div);
    }
    else {
        let div = '<div class="card"id="notif">';
        div += '<h6>'+ messageERR +'</h6>';
        div += '</div>';
        $('#emplacement_notif').append(div);
    }
}

// Permet de modifier la couleur (grisé ou non grisé) en fonction de l'état du produit
// grisé si désactivé | non grisé si activé
function changerCouleur(elem){
    // Récupération du parent
    article = elem.parentElement.parentElement.parentElement ;
    // Récupération de l'image au sein de l'article
    img = article.getElementsByTagName('a')[0].getElementsByTagName('img')[0] ;
    // Récupération des paragraphes au sein de l'article
    listeP = article.getElementsByTagName('a')[0].getElementsByTagName('p') ;
    // Récupération des span ;
    listeS = article.getElementsByTagName('a')[0].getElementsByTagName('span')


    // Modification des couleur en fonction de s'il est actif ou non
    if (elem.checked){
        img.style.filter = "grayscale(0)" ;
        for (const p of listeP) { p.style.color = "black" ; }
        for (const s of listeS) { s.style.color = "black" ; }
    } else {
        img.style.filter = "grayscale(1)" ;
        for (const p of listeP) { p.style.color = "gray" ; }
        for (const s of listeS) { s.style.color = "gray" ; }
    }
}

// NON FONCTIONNEL
// GENERE BUGS
// Permet de changer la requête en à envoyer : d'activé ou désactivé un produit
function changerEtat(elem){
    if (elem.checked == true){
        elem.checked = false ;
        elem.href="../../script/updateProduct.php?id='.$produit['id'].'&etat=0\'";
    } else {
        elem.checked = true ;
        elem.href="../../script/updateProduct.php?id='.$produit['id'].'&etat=1\'";
    } 
}
