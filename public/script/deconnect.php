<?php
// ce script est excétuer de bout en bout et il sert à se déconnecté du site
    session_start();
    $_SESSION['connect'] = false ;
    $_SESSION['connectVendeur'] = false ;
    // on remet l'id du panier à la valeur du cookie pour que l'internaute n'ai aps accès au panier du client
    $_SESSION['idPannier'] = $_COOKIE["alizonCookie"];
    // on revient sur l'index
    header("Location: ./../index.php" );
?>