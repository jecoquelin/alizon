<?php
    // ce script s'execute de bout en bout

    session_start();
    require('prefixe.php');
    // ne connection à la bdd
    include($prefixe.'../connectParams.php');
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
    // on a besoins d'une fonction pour les client
    include($prefixe.'client.php');
    // on récupère le client avec son id
    $client = getClientById($_SESSION['idClient']);

    // on vérifie que tous les champs sont remplie
    if (isset($_GET['password']) && isset($_GET['confirmPassword']) && ($_GET['password'] == $_GET['confirmPassword'])) {
        $email = $client['email'];
        $pass = $_GET['password'];
        $sql = "SELECT * FROM alizon._compte_client where email = :email";
        $result = $dbh->prepare($sql);
        $result->bindValue(":email",$email,PDO::PARAM_STR);
        $result->execute();
        // si le compte existe
        if ($result->rowCount() > 0) {
            $data = $result->fetch();
            // si le mdp du client est le bon
            if (password_verify($pass, $data['mdp'])) {
                
                // on "supprime le compte enfin juste on le désactive quoi"
                $sql = "UPDATE alizon._compte_client SET active=0 where email = :email";
                $result = $dbh->prepare($sql);
                $result->bindValue(":email",$email,PDO::PARAM_STR);
                $result->execute();

                // on deconnecte l'utilisateur
                $_SESSION['connect'] = false ;
                $_SESSION['idPannier'] = $_COOKIE["alizonCookie"];

                // on redirige vers le feedback de supression
                header('Location: '.$prefixe.'../supprimerSuccess.php');
            } else { // gestion des erreur
                header('Location: '.$prefixe.'../supprimerCompte.php?erreur=0');
            }
        } else {
            header('Location: '.$prefixe.'../supprimerCompte.php?erreur=1');
        }
    } else {
        header('Location: '.$prefixe.'../supprimerCompte.php?erreur=2');
    }
?>