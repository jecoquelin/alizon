<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php 
        $pageConnexion = "oui" ;
        require('head.php'); ?>
    <title>Connexion</title>
    <link rel="stylesheet" href="connexion.css" type="text/css">
</head>
<body>

    
    <main >
        
        <!-- le main de la page -->
        <div class="row">
            <!-- Partie gauche du site -->
            <div id="PartieGauche" class="col-md-5 align-items-md-center text-center justify-content-md-center">
                <a href="index.php"><img id="logo" src="./images/logos/Logo_blanc.png" alt=""></a>
                <p>Vous n’avez pas de compte ?<br>Cliquez ici pour en créer un </p>
                <a id="bouton_creer_compte" class="btn btn-secondary button" href="./inscription.php" role="button">S'inscrire</a> 

            </div>
            <!-- Partie droite du site -->
            <div id="PartieDroite" class="col-md-7 justify-content-center">
                <!-- Form permettant de se connecter au site -->
                <div class="pour_form">
                    <form class="form-inline col-md-5" action="script/login.php" method="post">
                        <h2 class="text-center">Se connecter</h2>

                        <!-- Champ de l'email -->
                        <input name="email" class="form-control champ email" type="email" placeholder="Email" required value="<?php echo (isset($_SESSION['dataConnection']) ? $_SESSION['dataConnection']["email"] : ''); ?>"/>
                    
                        <!-- Champ du mot de passe -->
                        <input name="password" class="form-control champ password" type="password" placeholder="Mot de passe" required value="<?php echo (isset($_SESSION['dataConnection']) ? $_SESSION['dataConnection']["password"] : ''); ?>"/>
                        
                        <!-- lien au cas où que l'utilisateur a perdu son mdp -->
                        <a href="mdpOublie.php"><p class="text-center side">mot de passe oublié ?</p></a>

                        <!-- Bouton permettant de se connecter -->
                        <div class="text-center padding">
                            <input type="submit" class="btn btn-secondary button" value="Se connecter">
                        </div>
                    </form>

                    <?php
                        // Récuperation et affichages des erreur si il en a 
                        if (isset($_GET['erreur'])){
                            switch ($_GET['erreur']) {
                                case 0:
                                    echo    "<div style='color : #EF0009'; class='text-center'>
                                                <p>Mot de passe et/ou E-mail incorrect</p>
                                            </div>";
                                    break;
                                
                                case 1:
                                    echo    "<div style='color : #EF0009'; class='text-center'>
                                                <p>Mot de passe et/ou E-mail incorrect</p>
                                            </div>";
                                    break;
                                
                                case 2:
                                    echo    "<div style='color : #EF0009'; class='text-center'>
                                                <p>Veillez saisir les informations dans les champs</p>
                                            </div>";
                                    break;
                                case 3:
                                    echo    "<div style='color : #FF8605'; class='text-center'>
                                                <p>Mot de passe modifié !</p>
                                            </div>";
                                    break;

                                case 4 :
                                    echo    "<div style='color : #EF0009'; class='text-center'>
                                                <p>Votre compte est désactivée !</p>
                                            </div>";
                                default:
                                    break;
                            }
                        }
                    ?>
                    
                </div>
                
            </div>
                
        </div>
        
        
    </main>
    
</body>

</html>