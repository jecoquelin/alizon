<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Compte suprimé</title>
        <?php require('head.php'); ?>
        <?php include('./script/product.php'); ?>
        <link rel="stylesheet" type="text/css" href="supprimerSuccess.css" media="screen">
    </head>
    <body>
        <?php require('headerContent.php'); ?>
        <!-- simple feedback disant que lc ompte a bien été suprimé -->
        <main class="container-fluid">
            <h3 class="confirmMessage py-3">Votre compte a bien été supprimé.</h3>
            <p>Vous ne pouvez désormais plus accéder à ce compte.</p>
            <div class="blocBouton py-3">
                <a class="btn btn-primary btn-lg" href="index.php" role="button">Retourner à l'accueil</a>
            </div>
        </main>
        <?php require('footerContent.html'); ?>
    </body>
</html>