<?php
$id=$_GET['id'];
if (isset($_GET['add'])){
   $add=$_GET['add'];
}
else{
   $add=null;
}
?>

<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
   <head>
      <?php require('head.php'); ?>
      <?php require('./script/product.php'); ?>
      <?php require('./script/seller.php'); ?>
      <?php $produit = getProduitById($id); ?>
      <title>Détail du produit : <?php echo $produit['libelle']; ?></title>
      <link rel="stylesheet" type="text/css" href="detailProduit.css" media="screen">
   </head>
   <body>
   <?php require('headerContent.php'); ?>
      <main>
         
         <?php
         // notification pour l'ajout au panier
            if($add=='Y'){
               echo '         <div class="card"id="notif">
                                 <h6>Article ajouté au panier</h6>
                                 <a href="./panier.php" class="btn d-lg-block btn-secondary"  role="button">voir mon panier</a>
                              </div>';
            }
         ?> 
         <!-- Section produit -->
         <section id="produit" class="container col-lg-12">
            <div class="row justify-content-center">
               <div class="col-lg-3 ">
                  <div id="carouselExampleInterval" class="carousel slide" data-bs-ride="carousel">
                     <div class="carousel-inner text-center">
                        <div class="carousel-item active" data-bs-interval="20000">
                        <!-- caroussel d'images produit -->
                        <?php
                           echo '<img class="carouss" src="./images/produits/' . $produit['id_photos'] . '/' . getMainPicProd($produit['id']) . '" height="300" width="300"   alt="' . $produit['libelle'] . '" title="' . $produit['libelle'] .'">';
                           ?>    
                        </div>
                        <?php
                        $photos=getAllPicProd($produit['id']);
                        // parcours de toutes les images disponibles pour un produit
                        foreach($photos as $p){
                           if($p!=getMainPicProd($produit['id'])){
                              echo '<div class="carousel-item" data-bs-interval="20000">';
                              echo '<img class="carouss" src="./images/produits/' . $produit['id_photos'] . '/' . $p . '" height="300" width="300"  alt="' . $produit['libelle'] . '" title="' . $produit['libelle'] .'">';
                              echo'</div>';
                           }
                        }

                        ?>
                     </div>
                     <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="visually-hidden">Previous</span>
                     </button>
                     <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="visually-hidden">Next</span>
                     </button>
                  </div>
               </div>
               <div class="container col-lg-5">
               <?php
                  //recupère le nom du produit
                  echo "<h3>".$produit['libelle']."</h3>";
                  ?>
                  <hr>
                  <div class="rating  ">
                     
                        <a href="#5" >★</a>
                        <a href="#4" >★</a>
                        <a href="#3" >★</a>
                        <a href="#2" >★</a>
                        <a href="#1" >★</a>

                  </div>
                  <!--affichage du nom du vendeur-->
                  <?php $vendeur = getVendeurByProduit($produit); ?> 
                  <p id="vendeur">Vendu par: <?php echo '<a class="lienVendeur" href="./detailVendeur.php?id='.$vendeur['id_vendeur'].'&idproduit='.$vendeur['id'].'">'.$vendeur["nom"].'</a>'; ?> </p> 
                  <div class="col-12">
                     <h5>À propos de cet article</h5>
                     <?php
                        //recupère la description d'un produit
                        echo "<p style=\"white-space: pre-wrap;\">".$produit['_description']."</p>";
                     ?>
                  </div>
               </div>
               <div class="col-lg-3">
                  <!-- l'affichage du prix -->
                  <article id="prix" class="text-left">
                     <?php
                        //verification des remises d'un produit
                        $estRemise = getRemise($produit['id']);
                        $remiseHT = $produit['prix_ht']*((100-getRemise($produit['id']))/100); 
                        echo '<h2 id="prixTTC">TTC '.round(($produit['prix_ht']*getTauxTva($produit["libelle_categorie"])),2).'€</h2>';
                        if ($estRemise != false) {
                           //prix remisé TTc
                           echo '<h2 id="prixRemiseTTC">TTC '.round(($remiseHT*getTauxTva($produit["libelle_categorie"])),2).'€ <span id="pourcentageRemise">-'.getRemise($produit['id']).'%</span></h2>';
                           echo '<style> #prixTTC{text-decoration: line-through;} </style>';
                        }
                        echo '<p id="prix_ht">HT '.round(($produit['prix_ht']),2)."€</p>";
                        if ($estRemise != false) {
                           //prix remisé HT
                           echo '<p id="prix_remise_ht">HT '.round(($remiseHT),2)."€</p>";
                           echo '<style> #prix_ht{text-decoration: line-through;} </style>';
                        }
                        echo '<p id="tva">TVA '.round((getTauxTva($produit["libelle_categorie"])-1)*100, 2)." %</p>";
                     ?>
                     <!-- recuperation de la date de livraison -->
                     <p><?php echo getDateLivraison($id); ?></p>

                     <?php //Récupération du max quantité ajoutable en prenant les articles dans le panier en compte+permet affichage rupture de stock
                        $idPanier = $_SESSION['idPanier'] ;
                        $nbMax=maxAjoutPanier($produit, $idPanier);
                        if ($nbMax>0){ /*si article encore en stock: form normal*/
                           ?>
                           <p id="enstock"><?php echo $nbMax;?> article(s) en stock</p>
                           <form action="./script/addToCart.php" method="GET">
                              <div id="qte">
                                 <Label for="quantity">Qté:    </Label>
                                 <input type="number" id="quantity" name="quantity" min="1" max=<?php echo $nbMax; ?> value="1">
                                 <input type="text" name="idProd" value=<?php echo '"'.$id.'"'; ?> style="display:none">
                              </div>
                              <input id="btn" type="submit" name="Valider" value="Ajouter au panier">
                           </form>
                           <?php
                        }
                        else{ /*si plus d'articles disponibles: form readonly*/
                           ?>
                           <p id="pasEnStock"><?php echo $nbMax;?> article(s) en stock</p>
                           <form action="./script/addToCart.php" method="GET">
                              <div id="qte">
                                 <Label for="quantity">Qté:    </Label>
                                 <input readonly="readonly" type="number" id="quantity" name="quantity" min="0" max=0 value="0">
                                 <input type="text" name="idProd" value=<?php echo '"'.$id.'"'; ?> style="display:none">
                              </div>
                              <input id="btn" type="submit" name="Valider" value="Ajouter au panier"disabled style="background-color:grey">
                           </form>
                           <?php
                        }
                     ?> 
                  </article>
               </div>
            </div>
         </section>
      </main>
      <?php require('footerContent.html'); ?>
   </body>
