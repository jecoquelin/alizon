<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Alizon</title>
        <!-- on importe head ppur avoir l'instance de la connexction a la bdd et les autre dépendances de la page -->
        <?php require('head.php'); ?>
        <?php include('script/product.php'); ?>
        <?php include('script/search.php'); ?>
        <?php include('script/seller.php'); ?>
        <link rel="stylesheet" type="text/css" href="recherche.css" media="screen"/>
    </head>
    <body>
        <?php require('headerContent.php'); ?>
        <main class="container-fluid">
            <?php
                // on utilise une variable GET pour savoir si un produit viens d'être ajouter au panier pour afficher une notifaction
                if (isset($_GET['add'])){
                    if($_GET['add'] =='Y'){
                        echo '<div class="card"id="notif">';
                        echo '<h6>Article ajouté au panier</h6>';
                        echo '<a href="./panier.php" class="btn d-lg-block btn-secondary" role="button">voir mon panier</a>';
                        echo '</div>';
                    }
                }
                $idPanier = $_SESSION['idPanier'] ;
            ?>
            <!-- c'est un element html qui sert juste à js pour retenir la valeur du scroll de l'utilisateur quand il ajoute un produit au panier dpuis cette page -->
            <div id="scroll" value=<?php if (isset($_GET["scroll"])) {
                echo $_GET["scroll"];
            } ?>></div>
            <div class="row">
            <?php
                // si on un terme à rechercher on l'affiche et sinon on fait une recherche vide
                if (isset($_GET["search"])) {
                    $_GET["search"] = htmlspecialchars($_GET["search"]);
                    echo '<h3>Résultat de la recherche pour : "' . $_GET["search"] . '"</h3>';
                } else {
                    echo '<h3>Résultat de la recherche pour : ""';
                    $_GET["search"] = '';
                }
            ?>
            </div>
            <!-- tous les filtres sont générés dinamiquement -->
            <div class="row">
                <div class="col-md-2 personalise">
                <div class="row stick scrollable">
                    <?php
                        // on met à jour toutes les variables qui contiennent les filtres (catégories, prix, promos)
                        if (!isset($_GET['cat']) || isset($_GET['resetCat'])){
                            unset($_SESSION['catRecherche']);
                        }
                        if (!isset($_GET['vend'])){
                            unset($_SESSION['vendRecherche']);
                        }
                        $prix = null;
                        $cat = [];
                        $promo = false;
                        $vendeurs = [];
                        if (isset($_GET["prix"])) {
                            $prix = $_GET["prix"];
                        }
                        if (isset($_GET["cat"])) {
                            $cat = $_GET["cat"];
                        }
                        if (isset($_GET["promo"])){
                            $promo = true;
                        }
                        if (isset($_GET["vend"])){
                            $vendeurs = $_GET["vend"];
                        }
                        $_SESSION['infosRecherches'] = array("search" => $_GET["search"], "cat" => $cat, "prix" => $prix, "vend" => $vendeurs);
                        // on fait une recherche avec ces variables
                        $produits = rechercher($_GET["search"], (isset($_GET["cat"])) ? $_GET["cat"] : null, (isset($_GET["prix"])) ? $_GET["prix"] : null, $promo, (isset($_GET["vend"])) ? $_GET["vend"] : null);
                    ?>

                    <!-- les filtres par catégories -->
                    <h4 class="recherchePerso">Filtrer par catégorie</h4>
                    <form class="recherchePerso" action="recherche.php" method="get">
                        <!-- on utilise un input pour retneir cette variable dans le get -->
                        <input type="hidden" class="inputScroll" name="scroll" value="0">
                        <?php
                            if (isset($produits[0])){
                                $categories = getCats($produits);
                                if (!isset($_SESSION['catRecherche'])){
                                    $_SESSION['catRecherche'] = $categories;
                                }
                                // on affiches des filtres pour les catégories des produits retournés par la recherche
                                foreach ($_SESSION['catRecherche'] as $key => $value) {
                                    $checked = "";
                                    if (isset($_GET['cat'])){
                                        foreach ($_GET['cat'] as $cat => $value) {
                                            // on check les check box si la catégories a été sélectionée par l'utilisateur
                                            if ($value == $key){
                                                $checked = "checked";
                                            }
                                        }
                                    }
                                    echo '<div class="form-check">';
                                    echo '<input class="form-check-input" type="checkbox" name="cat[]" value="' . $key . '"' . $checked . '>';
                                    echo '<label class="form-check-label">' . $key . '</label>';
                                    echo '</div>';
                                }
                            }
                        ?>
                        <!-- on utilise un input pour retneir cette variable dans le get -->
                        <input type="text" name="search" style="display: none" value="<?php echo $_GET["search"]; ?>">
                        <?php
                            // si il y a cette variable alors on utilise un input pour retneir cette variable dans le get
                            if (isset($_GET["prix"])){
                                echo '<input type="text" name="prix" style="display: none" value="' . $_GET["prix"] . '">';
                            }
                            // si il y a cette variable alors on utilise un input pour retneir cette variable dans le get
                            if ($promo){
                                echo '<input type="hidden" name="promo" value="1">';
                            }
                            // si il y a cette variable alors on utilise un input pour retenir les vendeurs dans le get
                            if (isset($_GET["vend"])){
                                foreach ($_GET["vend"] as $vend) {
                                echo '<input type="text" name="vend[]" style="display: none" value=' . $vend . '>';
                                }
                            }
                        ?>
                        <button type="submit" class="btn-secondary btnCatPrix">Valider</button>
                    </form>
                    
                    <!-- version mobile des filtres par catégories -->
                    <!-- le fonctionnement est exactement le même, la seul différence c'est l'affichage qui ici ce fait dans un details pour prendre moins de place sur téléphone -->
                    <details class="detailsMobile">
                    <summary>Filtrer par catégorie</summary>
                    <form action="recherche.php" method="get">
                        <input type="hidden" class="inputScroll" name="scroll" value="0">
                        <?php
                            if (isset($_SESSION['catRecherche'])){
                                $categories = getCats($produits);
                                if (!isset($_SESSION['catRecherche'])){
                                    $_SESSION['catRecherche'] = $categories;
                                }
                                foreach ($_SESSION['catRecherche'] as $key => $value) {
                                    $checked = "";
                                    if (isset($_GET['cat'])){
                                        foreach ($_GET['cat'] as $cat => $value) {
                                            if ($value == $key){
                                                $checked = "checked";
                                            }
                                        }
                                    }
                                    echo '<div class="form-check">';
                                    echo '<input class="form-check-input checkBox" type="checkbox" name="cat[]" value="' . $key . '"' . $checked . '>';
                                    echo '<label class="form-check-label">' . $key . '</label>';
                                    echo '</div>';
                                }
                            }
                        ?>
                        <input type="text" name="search" style="display: none" value="<?php echo $_GET["search"]; ?>">
                        <?php
                            if (isset($_GET["prix"])){
                                echo '<input type="text" name="prix" style="display: none" value="' . $_GET["prix"] . '">';
                            }
                            if ($promo){
                                echo '<input type="hidden" name="promo" value="1">';
                            }
                            // si il y a cette variable alors on utilise un input pour retenir les vendeurs dans le get
                            if (isset($_GET["vend"])){
                                foreach ($_GET["vend"] as $vend) {
                                echo '<input type="text" name="vend[]" style="display: none" value=' . $vend . '>';
                                }
                            }
                        ?>
                        <button type="submit" class="btn-secondary btnCatPrix">Valider</button>
                    </form>
                    </details>

                    <!-- le filtre par prix -->
                    <h4 class="recherchePerso">Filtrer par prix</h4>
                    <form class="recherchePerso" action="recherche.php" method="get">
                        <input type="hidden" class="inputScroll" name="scroll" value="0">
                        <!-- slider pour séléctioner le prix -->
                        <div class="slidecontainer">
                            <?php
                                // on recherche les produits avec exactements les mêmes filtres que l'utilisateur mais sans le prix
                                $produitsSansPrix = rechercher($_GET["search"], (isset($_GET["cat"])) ? $_GET["cat"] : null, null, (isset($_GET["promo"])) ? $_GET["promo"] : null, (isset($_GET["vend"])) ? $_GET["vend"] : null);
                                if (isset($produitsSansPrix[0])){
                                    // le prix maximum est ensuite déterminé pour connaitre la borne maximum du slider
                                    $prixMax = prixMax($produitsSansPrix) + 1;
                                } else {
                                    // si aucun produit n'est trouvé le prix max du slider sera 1
                                    $prixMax = 1;
                                }
                                (isset($_GET["prix"])) ? $prix = $_GET["prix"] : $prix = $prixMax;
                            ?>
                            <!-- elem slider -->
                            <input type="range" min="1" max="<?php echo $prixMax; ?>" value="<?php echo $prix; ?>" class="slider" id="slider" name="prix">
                        </div>
                        <p>Prix max : <span id="prix"></span></p>
                        <!-- si il y a cette variable alors on utilise un input pour retneir cette variable dans le get -->
                        <input type="text" name="search" style="display: none" value="<?php echo $_GET["search"]; ?>">
                        <?php
                            // si il y a cette variable alors on utilise un input pour retneir les catégories dans le get
                            if (isset($_GET["cat"])){
                                foreach ($_GET["cat"] as $cat) {
                                echo '<input type="text" name="cat[]" style="display: none" value=' . $cat . '>';
                                }
                            }
                             // si il y a cette variable alors on utilise un input pour retneir cette variable dans le get
                            if (isset($_GET["promo"])){
                                echo '<input type="hidden" name="promo" value="1">';
                            }
                            // si il y a cette variable alors on utilise un input pour retenir les vendeurs dans le get
                            if (isset($_GET["vend"])){
                                foreach ($_GET["vend"] as $vend) {
                                echo '<input type="text" name="vend[]" style="display: none" value=' . $vend . '>';
                                }
                            }
                        ?>
                        <button type="submit" class="btn-secondary btnCatPrix">Valider</button>
                    </form>
                    
                    <!-- version mobile du filtre de prix -->
                    <!-- le fonctionnement est exactement le même, la seul différence c'est l'affichage qui ici ce fait dans un details pour prendre moins de place sur téléphone -->
                    <details class="detailsMobile">
                    <summary>Filtrer par prix</summary>
                    <form action="recherche.php" method="get">
                        <input type="hidden" class="inputScroll" name="scroll" value="0">
                        <div class="slidecontainer">
                            <?php
                                $produitsSansPrix = rechercher($_GET["search"], (isset($_GET["cat"])) ? $_GET["cat"] : null, null, (isset($_GET["promo"])) ? $_GET["promo"] : null, (isset($_GET["vend"])) ? $_GET["vend"] : null);
                                if (isset($produitsSansPrix[0])){
                                    $prixMax = prixMax($produitsSansPrix) + 1;
                                } else {
                                    $prixMax = 1;
                                }
                                (isset($_GET["prix"])) ? $prix = $_GET["prix"] : $prix = $prixMax;
                            ?>
                            <input type="range" min="1" max="<?php echo $prixMax; ?>" value="<?php echo $prix; ?>" class="slider" id="sliderMobile" name="prix">
                        </div>
                        <p>Prix max : <span id="prixMobile"></span></p>
                        <input type="text" name="search" style="display: none" value="<?php echo $_GET["search"]; ?>">
                        <?php
                            if (isset($_GET["cat"])){
                                foreach ($_GET["cat"] as $cat) {
                                echo '<input type="text" name="cat[]" style="display: none" value=' . $cat . '>';
                                }
                            }
                            if (isset($_GET["promo"])){
                                echo '<input type="hidden" name="promo" value="1">';
                            }
                            // si il y a cette variable alors on utilise un input pour retenir les vendeurs dans le get
                            if (isset($_GET["vend"])){
                                foreach ($_GET["vend"] as $vend) {
                                echo '<input type="text" name="vend[]" style="display: none" value=' . $vend . '>';
                                }
                            }

                        ?>
                        <button type="submit" class="btn-secondary btnCatPrix">Valider</button>
                    </form>
                    </details>
                    
                    <!-- filtre pour les promotions seulement -->
                    <h4 class="recherchePerso">Promotions seulement</h4>
                    <form class="recherchePerso" action="recherche.php" method="get">
                        <!-- on utilise un input pour retneir cette variable dans le get -->
                        <input type="hidden" class="inputScroll" name="scroll" value="0">
                        <!-- on utilise un input pour retneir cette variable dans le get -->
                        <input type="text" name="search" style="display: none" value="<?php echo $_GET["search"]; ?>">
                        <?php
                            $check = '';
                            // si on vient de faire une recherche avec promo on coche la caase sinon non
                            if ($promo) {
                                $check = "checked";
                            }
                            // affichage de la case à coché
                            echo '<div class="form-check">';
                            echo '<input class="form-check-input" type="checkbox" name="promo" value="1"' . $check . '>';
                            echo '<label class="form-check-label">Promotions</label>';
                            echo '</div>';
                            
                             // si il y a cette variable alors on utilise un input pour retneir cette variable dans le get
                            if (isset($_GET["prix"])){
                                echo '<input type="text" name="prix" style="display: none" value="' . $_GET["prix"] . '">';
                            }
                            // si il y a cette variable alors on utilise un input pour retneir les catégories dans le get
                            if (isset($_GET["cat"])){
                                foreach ($_GET["cat"] as $cat) {
                                echo '<input type="text" name="cat[]" style="display: none" value=' . $cat . '>';
                                }
                            }
                            // si il y a cette variable alors on utilise un input pour retenir les vendeurs dans le get
                            if (isset($_GET["vend"])){
                                foreach ($_GET["vend"] as $vend) {
                                echo '<input type="text" name="vend[]" style="display: none" value=' . $vend . '>';
                                }
                            }
                        ?>
                        <button type="submit" class="btn-secondary btnCatPrix">Valider</button>
                    </form>

                    <!-- version mobile des filtres par catégories -->
                    <!-- le fonctionnement est exactement le même, la seul différence c'est l'affichage qui ici ce fait dans un details pour prendre moins de place sur téléphone -->
                    <details class="detailsMobile">
                    <summary>Promotions seulement</summary>
                    <form class="recherchePersoDesktopEtMobile" action="recherche.php" method="get">
                        <!-- on utilise un input pour retneir cette variable dans le get -->
                        <input type="hidden" class="inputScroll" name="scroll" value="0">
                        <!-- on utilise un input pour retneir cette variable dans le get -->
                        <input type="text" name="search" style="display: none" value="<?php echo $_GET["search"]; ?>">
                        <?php
                            $check = '';
                            // si on vient de faire une recherche avec promo on coche la caase sinon non
                            if ($promo) {
                                $check = "checked";
                            }
                            // affichage de la case à coché
                            echo '<div class="form-check">';
                            echo '<input class="form-check-input" type="checkbox" name="promo" value="1"' . $check . '>';
                            echo '<label class="form-check-label">Promotions</label>';
                            echo '</div>';
                            
                             // si il y a cette variable alors on utilise un input pour retneir cette variable dans le get
                            if (isset($_GET["prix"])){
                                echo '<input type="text" name="prix" style="display: none" value="' . $_GET["prix"] . '">';
                            }
                            // si il y a cette variable alors on utilise un input pour retneir les catégories dans le get
                            if (isset($_GET["cat"])){
                                foreach ($_GET["cat"] as $cat) {
                                echo '<input type="text" name="cat[]" style="display: none" value=' . $cat . '>';
                                }
                            }
                            // si il y a cette variable alors on utilise un input pour retenir les vendeurs dans le get
                            if (isset($_GET["vend"])){
                                foreach ($_GET["vend"] as $vend) {
                                echo '<input type="text" name="vend[]" style="display: none" value=' . $vend . '>';
                                }
                            }
                        ?>
                        <button type="submit" class="btn-secondary btnCatPrix">Valider</button>
                    </form>
                    </details>

                    <!-- filtre par vendeur -->
                    <h4 class="recherchePerso">Filtrer par vendeur</h4>
                    <form class="recherchePerso" action="recherche.php" method="get">
                        <!-- on utilise un input pour retenir cette variable dans le get -->
                        <input type="hidden" class="inputScroll" name="scroll" value="0">
                        <?php
                            if (isset($produits[0])){
                                $vendeurs = getVendeurs($produits);
                                if (!isset($_SESSION['vendRecherche'])) {
                                    $_SESSION['vendRecherche'] = $vendeurs;
                                }
                                // on affiches des filtres pour les vendeurs des produits retournés par la recherche
                                foreach ($_SESSION['vendRecherche'] as $key => $value) {
                                    $checked = "";
                                    if (isset($_GET['vend'])){
                                        foreach ($_GET['vend'] as $vend => $value) {
                                            if ($value == $key){
                                                $checked = "checked";
                                            }
                                        }
                                    }
                                    echo '<div class="form-check">';
                                    echo '<input class="form-check-input" type="checkbox" name="vend[]" value="' . $key . '"' . $checked . '>';
                                    echo '<label class="form-check-label">' . $key . '</label>';
                                    echo '</div>';
                                }
                            }
                        ?>
                        <!-- on utilise un input pour retneir cette variable dans le get -->
                        <input type="text" name="search" style="display: none" value="<?php echo $_GET["search"]; ?>">
                        <?php
                            // si il y a cette variable alors on utilise un input pour retneir les catégories dans le get
                            if (isset($_GET["cat"])){
                                foreach ($_GET["cat"] as $cat) {
                                echo '<input type="text" name="cat[]" style="display: none" value=' . $cat . '>';
                                }
                            }
                            // si il y a cette variable alors on utilise un input pour retneir cette variable dans le get
                            if (isset($_GET["prix"])){
                                echo '<input type="text" name="prix" style="display: none" value="' . $_GET["prix"] . '">';
                            }
                            // si il y a cette variable alors on utilise un input pour retneir cette variable dans le get
                            if ($promo){
                                echo '<input type="hidden" name="promo" value="1">';
                            }
                        ?>
                        <button type="submit" class="btn-secondary btnCatPrix">Valider</button>
                    </form>

                    <!-- version mobile des filtres par catégories -->
                    <!-- le fonctionnement est exactement le même, la seul différence c'est l'affichage qui ici ce fait dans un details pour prendre moins de place sur téléphone -->
                    <details class="detailsMobile">
                    <summary>Filtrer par vendeur</summary>
                    <form action="recherche.php" method="get">
                        <input type="hidden" class="inputScroll" name="scroll" value="0">
                        <?php
                            if (isset($_SESSION['vendRecherche'])){
                                $categories = getCats($produits);
                                if (!isset($_SESSION['vendRecherche'])){
                                    $_SESSION['vendRecherche'] = $categories;
                                }
                                foreach ($_SESSION['vendRecherche'] as $key => $value) {
                                    $checked = "";
                                    if (isset($_GET['vend'])){
                                        foreach ($_GET['vend'] as $cat => $value) {
                                            if ($value == $key){
                                                $checked = "checked";
                                            }
                                        }
                                    }
                                    echo '<div class="form-check">';
                                    echo '<input class="form-check-input checkBox" type="checkbox" name="vend[]" value="' . $key . '"' . $checked . '>';
                                    echo '<label class="form-check-label">' . $key . '</label>';
                                    echo '</div>';
                                }
                            }
                        ?>
                        <input type="text" name="search" style="display: none" value="<?php echo $_GET["search"]; ?>">
                        <?php
                            // si il y a cette variable alors on utilise un input pour retneir les catégories dans le get
                            if (isset($_GET["cat"])){
                                foreach ($_GET["cat"] as $cat) {
                                echo '<input type="text" name="cat[]" style="display: none" value=' . $cat . '>';
                                }
                            }
                            if (isset($_GET["prix"])){
                                echo '<input type="text" name="prix" style="display: none" value="' . $_GET["prix"] . '">';
                            }
                            if ($promo){
                                echo '<input type="hidden" name="promo" value="1">';
                            }
                        ?>
                        <button type="submit" class="btn-secondary btnCatPrix">Valider</button>
                    </form>
                    </details>
                </div>
                </div>
                <!-- le contenu de la recherche avec tous les produits -->
                <div class="col-md-10 rechercheContent">
                    <?php
                        $inc=0;
                        if (!isset($produits[0])){
                            // pas de produit trouvés
                            echo '<div class="text-center"><h3>Aucun résultat pour cette recherche, essayez un autre terme ou d\'autres filtres.</h3></div>';
                        }
                        // on affiche tous les produits
                        foreach ($produits as $key=>$produit) {
                            $estRemise = getRemise($produit['id']);
                            $remiseHT = $produit['prix_ht']*((100-getRemise($produit['id']))/100); 
                            $inc++;
                            if (($produit["promotion"] == 1 && $promo == true) || $promo == false){
                                echo '<div class="row produit">';
                                    if ($produit["promotion"] == 1){
                                        // on affiche un banière de promo dans le coin du prod
                                        echo '<div class="promo-banner">Promotion</div>';
                                    }
                                    // lien vers le détail du produit
                                    echo '<a href="./detailProduit.php?id=' . $produit['id'] . '" class="row d-flex">';
                                        echo '<div class="col-md produit_bloc_titre_prix_etoiles">';
                                            // nom du prod
                                            echo '<p class="produit_titre"> ' . $produit['libelle'] . '</p>';
                                            echo '<div class=" produit_prix_etoiles">';
                                                // le prix
                                                echo '<p id="prix',$inc,'" class="my-1 produit_prix"> ' . round($produit['prix_ht'] * getTauxTva($produit["libelle_categorie"]), 2) . '€</p>';
                                                if ($estRemise != false) {
                                                    echo '<p id="prixRemiseTTC" class="my-1 produit_prix">'.round(($remiseHT*getTauxTva($produit["libelle_categorie"])),2).'€</p>';
                                                    echo '<style> #prix',$inc,'{text-decoration: line-through;} </style>';
                                                }
                                                echo '<p class="produit_etoiles"> ★★★★★ </p>';
                                            echo '</div>';
                                        echo '</div>';
                                        echo '<div class="col-md desc">';
                                            echo '<p class="my-2 desc">' . $produit['_description'] .'</p>';
                                        echo '</div>';
                                        // l'image
                                        echo '<div class="col-md">';
                                            echo '<img src="images/produits/' . $produit['id_photos'] . '/' . getMainPicProd($produit['id']) . '" title="' . $produit['libelle'] . '" alt="' . $produit['libelle'] . '" class="rounded produit_image">';
                                        echo '</div>';
                                    echo '</a>';
                                    // nom vendeur et bouton (lien) ajouter pannier
                                    echo '<div class="row">';
                                        echo '<div class="col-md colDuLien">';
                                        echo '<p class="vendeur">vendu par : <a class="lienVendeur" href="./detailVendeur.php?id='.$produit['id_vendeur'].'&idproduit='.$produit['id'].'">'. getVendeurByProduit($produit)["nom"] .'</a></p>';                                            // on regarde si on peut l'ajouter ou pas en fonction du stock
                                            $nbMax=maxAjoutPanier($produit, $idPanier);
                                            if ($nbMax > 0) {
                                                echo '<p class="enStock">En stock</p>';
                                                echo '<a class="lienAjoutPanier" href="./script/addToCart.php?provenance=recherche.php&quantity=1&idProd=' . $produit['id'] . '">Ajouter au panier</a>';
                                            }else{
                                                echo '<p class="pasEnStock">Non disponible</p>';
                                                echo '<a class="lienAjoutPanierGris" href="./script/addToCart.php?provenance=recherche.php&quantity=1&idProd=' . $produit['id'] . '" style="pointer-events:none">Ajouter au panier</a>';
                                            }
                                        echo '</div>';
                                    echo '</div>';
                                echo '</div>';
                            }
                        }
                    ?>
                </div>
            </div>
            </div>
        </main>
        <?php require('footerContent.html'); ?>
        <script>
            // script qui gére le slider
            var slider = document.getElementById("slider");
            var output = document.getElementById("prix");
            output.innerHTML = slider.value;
            // si on change le slider alors laa valeur de prix en html change
            slider.oninput = function() {
                sliderMobile.value = slider.value
                output.innerHTML = slider.value;
                outputMobile.innerHTML = slider.value; 
            }
            //la même chose en mobile
            var sliderMoblie = document.getElementById("sliderMobile");
            var outputMobile = document.getElementById("prixMobile");
            outputMobile.innerHTML = sliderMobile.value;
            
            sliderMobile.oninput = function() {
                slider.value = sliderMobile.value
                outputMobile.innerHTML = sliderMobile.value;
                output.innerHTML = slider.value; 
            }

            // script qui gère le scroll de l'utilisateur pour mettre à jour les variable GET scroll des liens
            var lienAjoutPanier = document.getElementsByClassName("lienAjoutPanier");
            var premiereFois = true;

            window.addEventListener('scroll',(event) => {
                for (let elem of lienAjoutPanier) {
                    // on ajouter la variable GET scroll dans le href des lien pour ajouter au panier
                    if (premiereFois == false) {
                        // si il y a déjà la variable scroll on l'enlève pour ne pas ajouter des scroll à l'infini
                        elem.href = elem.href.split("&", elem.href.split("&").length-1).join("&");
                    }
                    elem.href = elem.href + '&scroll=' + scrollY;
                    elem.value = scrollY;
                }
                premiereFois = false;
            });
            // on récupère la valeur du scroll avant que l'utilisateur change de page puis on scroll avec scrollTo
            var scroll= document.getElementById("scroll");
            scrollTo(0, Number(scroll.getAttribute("value")));

        </script>
    </body>
</html>