<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php require('head.php'); ?>
    <title>Mot de passe oublié</title>
    <link rel="stylesheet" href="connexion.css" type="text/css">
</head>
<body>

    
    <main >
        <!-- partie gauche en bleu -->
        <div class="row">
            <div id="PartieGauche" class="col-md-5 align-items-md-center text-center justify-content-md-center">
                <a href="index.php"><img id="logo" src="./images/logos/Logo_blanc.png" alt=""></a>
                <p>Vous n’avez pas de compte ?<br><br>Cliquer ici pour en créer un </p>
                <a id="bouton_passe_commande" class="btn btn-secondary button" href="./inscription.php" role="button">S'inscrire</a> 

            </div>
            <!-- partie droite -->
            <div id="PartieDroite" class="col-md-7 text-center justify-content-center">
                <div class="pour_form">
                    <form class="form-inline col-md-5" action="script/changePass.php" method="post">
                        <p class="text-center">Répondez à ces questions afin de pouvoir changer de mot de passe</p>
                        
                            <input name="email" class="form-control email champ" type="email" placeholder="Email" required value="<?php echo (isset($_SESSION['dataOublie']) ? $_SESSION['dataOublie']["email"] : ''); ?>"/>
                        
                        <!-- recuperation des questions dans la base de données -->
                        <select name=question1 class="question champ" required>
                        <option selected id="firstLabel" disabled>Sélectionner une question</option>
                            <?php
                              global $dbh;
                              $sth = $dbh->prepare('SELECT * from alizon._question');
                              $sth -> execute();
                              $questions = $sth -> fetchAll();
                              foreach($questions as $q) {
                                  echo "<option>";
                                  echo($q["libelle"]);
                                  echo "</option>";
                              }
                            ?>
                         </select>
                        
                            <input name="rep1" class="form-control rep champ" type="text" placeholder="Réponse" required value="<?php echo (isset($_SESSION['dataOublie']) ? $_SESSION['dataOublie']["rep1"] : ''); ?>"/>
                        <!--même chose pour la deuxieme-->
                          <select name="question2" class="question champ" required>
                            <option selected id="firstLabel" disabled>Sélectionner une question</option>
                            <?php
                                foreach($questions as $q) {
                                    echo "<option>";
                                    echo($q["libelle"]);
                                    echo "</option>";
                             }
                        ?>
                        </select>
                        
                        <input name="rep2" class="form-control rep champ" type="text" placeholder="Réponse" required value="<?php echo (isset($_SESSION['dataOublie']) ? $_SESSION['dataOublie']["rep2"] : ''); ?>"/>
                    
                        <!-- nouveau mot de passe -->
                        <input name="password" onkeydown="afficher();" onkeyup="CheckMdp(this.value)" class="form-control password champ" type="password" placeholder="Nouveau mot de passe" required value="<?php echo (isset($_SESSION['dataOublie']) ? $_SESSION['dataOublie']["password"] : ''); ?>"/>
                        <script src="./script/password.js"></script>
                        
                        <div id = "error" class="text-left">
                            <span id = "msg1"></span><br>
                            <span id = "msg2"></span><br>
                            <span id = "msg3"></span><br>
                            <span id = "msg4"></span><br>
                            <span id = "msg5"></span>
                        </div>
                        <!-- confirmation nouveau mot de passe -->
                        <input name="confirm" class="form-control password champ" type="password" placeholder="Confirmer le mot de passe" required/>
                        <!-- bouton qui renvoie à la connexion -->
                        <a href="connexion.php"><p class="text-center side">retour à la connexion</p></a>
                        <div class="text-center padding">
                            <!-- bouton qui renvoie au script de changement de mot de passe -->
                            <input type="submit" class="btn btn-secondary button" value="Valider">
                        </div>
                    </form>
                    <?php
                    // gestion des erreurs
                        if (isset($_GET['erreur'])){
                            switch ($_GET['erreur']) {                    
                                case 1:
                                    echo    "<div style='color : #EF0009'; class='text-center'>
                                                <p>Le mot de passe et la confirmation sont différent !</p>
                                            </div>";
                                    break;
                                
                                case 2:
                                    echo    "<div style='color : #EF0009'; class='text-center'>
                                                <p>Réponses ou Questions incorrectes</p>
                                            </div>";
                                    break;
                                case 3:
                                    echo    "<div style='color : #EF0009'; class='text-center'>
                                                <p>Adresse mail incorrecte</p>
                                            </div>";
                                    break;
                                default:
                                    break;
                            }
                        }
                    ?>
                    
                </div>
                
            </div>
                
        </div>
        
        
    </main>
    
</body>

</html>