<div class="row mx-5">
    <h2> Liste des produits </h2>
</div>
<?php
require('prefixe.php');
// Cette partie du code affiche les produits qui vont être importés
$div = true;
foreach ($_SESSION['tabProduitsCSV'] as $produit) {
    if ($div == true){
        echo '<div class="row ligne py-3">';
    }
    echo '<div class="produit d-flex justify-content-between col-md mx-2">';
    echo '<div class="produit_bloc_titre_prix_etoiles">';
    echo '<p class="produit_titre"> ' . $produit['libelle'] . '</p>';
    echo '<div class="produit_prix_etoiles d-flex">';
    echo '<p class="produit_prix my-1"> ' . round($produit['prix_ht'] * getTauxTva($produit['libelle_categorie']), 2) . '€</p>';
    echo '</div>';
    echo '</div>';
    echo '<div class="col d-flex justify-content-end">';
    $images = preg_grep('/.*.(png|jpeg|jpg|gif)/', scandir($prefixe.'produits/' . $produit['dossier_photos']));
    $cheminSource = $prefixe.'produits/' . rawurlencode($produit['dossier_photos']) . '/' . rawurlencode($images[min(array_keys($images))]);
    echo '<img src="' . $cheminSource . '" title="' . $produit['libelle'] . '" alt="' . $produit['libelle'] . '" class="rounded images_produits">';
    echo '</div></div>';
    if ($div == true){
        $div = false;
    } else {
        $div = true;
        echo '</div>';
    }
}
if ($div == false)
{
    echo '<div id="produit_bloc_titre_prix_etoiles" class="d-flex col-6-md"></div>';
    echo '</div>';
}
?>
<!-- Un bouton qui permet de valider l'import est placé en bas de la page -->
<!-- Le bouton Ajouter le catalogue se transforme en Valider la publication -->
<!-- De plus, cliquer sur l'un des deux boutons Valider la publication renvoie vers confirmAjoutCatalogue.php -->
<form action="confirmAjoutCatalogue.php" class="row col my-4" method="post">
    <div class="row col-2 offset-9 color-99CC5B">
        <button class="btn-secondary btn-a-modif"> Valider la publication </button>
    </div>
</form>
<script>
    let tab = document.getElementsByClassName("btn-a-modif");

    for (var btn of tab)
    {
        btn.innerHTML = "Valider la publication";
    }

    document.getElementById('formHautPage').action = "confirmAjoutCatalogue.php";
    document.getElementById('champFichier').removeAttribute("required");
</script>