<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php require('../head.php'); ?>
    <?php require($prefixe.'script/catalogue.php'); ?>
    <link rel="stylesheet" type="text/css" href="ajoutProduit.css" media="screen" />
    <title>Confirmation de l'ajout d'un catalogue</title>
</head>
<!-- Ce script importe les produits -->
<body>
    <?php
        $idVendeur=$_SESSION['idVendeur'];
        // cette partie du script est la partie qui import les produits un par un
        if (isset($_SESSION['tabProduitsCSV']))
        {
            foreach ($_SESSION['tabProduitsCSV'] as $tuple)
            {
                ajouterUnProduitCSV($tuple,$idVendeur);
            }

            unset($_SESSION['tabProduitsCSV']);
        }
    ?>
    <!-- Affichage du message d'ajout du catalogue et des boutons pour ajouter un nouveau catalogue ou retourner à l'accueil -->
    <br>
    <h1 class="confirmMessage">Votre catalogue à bien été ajouté !</h1>
    <div class="blocBouton d-flex justify-content-around">
        <a class="btn btn-primary button-accueil btn-lg" href="index.php" role="button">Retourner à l'accueil</a>
        <a class="btn btn-primary button-ajoutProduit btn-lg" href="importCatalogue.php" role="button">Ajouter un autre catalogue</a>
    </div>
</body>

</html>