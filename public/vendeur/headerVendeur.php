<header>
    <!--Bloc supérieur avec le logo de Alizon et Le menu présent dans la photo de profil du vendeur-->
    <div class="BlocSuperieur">
        <!--Logo Alizon-->
        <figure>
            <a href="./index.php"><img id="logoAlizon" src="<?php echo $prefixe ?>images/logos/Logo_blanc.png" alt="logo Alizon" ></a>
        </figure>
        <!--Bloc profil avec la photo de profil-->
        <div id="BlocProfil">
            <?php
            if ($_SESSION["connectVendeur"] == true && getEtatVendeur($_SESSION['idVendeur'])!=0){

                $vendeur = getVendeurById($_SESSION['idVendeur']) ;
                echo '        
                    <ul class="nav nav-pills" style="z-index : 10;">
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false"><img class="rounded-circle" src="./../images/vendeur/photoProfil/'.$vendeur['id'].'/'.getMainPicVendeur($vendeur['id']).'" title="'.$vendeur['nom'].'" alt="'.$vendeur['nom'].'"></a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="./profil.php">Mon profil</a></li>
                                <li><a class="dropdown-item" href="./mesProduits.php">Mes Produits</a></li>
                                <li><a class="dropdown-item" href="./../script/deconnect.php"> Se Déconnecter</a></li>
                            </ul>
                        </li>
                    </ul>
                ';
            }else{
                header("Location: ./connexionVendeur.php" );
            }
            ?>
        </div>
    </div>

    <!--Bloc inférieur avec une flèche permettant le retour à la page -->
    <div id="revenirListe" class="justify-content-center" >
        <a id="lienListe" href="./index.php">
            <img id="flecheComeback" src="<?php echo $prefixe ?>images/icones/undo-outline.png" alt="Revenir à la page d'accueil"><p>Menu vendeur</p>
        </a>
    </div>
</header>
