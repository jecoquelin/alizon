<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <!--HEAD-->
    <head>
        <?php require('../head.php'); ?>
        <?php 
            require('../script/searchProduct.php'); 
            require('../script/product.php');
            require('../script/seller.php')
        ?>
        <title>Mes produits</title>
        <link rel="stylesheet" type="text/css" href="./mesProduits.css" media="screen">
        <link rel="stylesheet" type="text/css" href="./header.css" media="screen">
        <script src="https://code.jquery.com/jquery-3.6.3.min.js" crossorigin="anonymous"></script>
    </head>
    <!--BODY-->
    <body>

        <!--HEADER-->
        <?php require('./headerVendeur.php')?>

        <!--MAIN-->
        <main>
            <!--Notification-->
            <div id="emplacement_notif"></div>

            <!--Barre de recherche d'un produit-->
            <div id="blocBarreRecherche">
                <form class="search" action="mesProduits.php">
                    <div id="inputGroup" class="input-group ">
                        <input id="input" name="search" type="text" class="form-control" placeholder="Rechercher par nom">
                    </div>
                    <input type="submit" style="display: none">
                </form>
            </div>
            <!--Liste des produits du vendeur-->
            <section>
                <h1>Mes produits</h1>
                <p id="actionProduits">Actif | Suppression</p>

                <?php
                // SI une recherche à été effectué on récupère le terme
                // SINON on recherhce tous les produits
                if (isset($_GET['search'])){
                    $termeRecherche = $_GET['search'];
                }
                else{
                    $termeRecherche = "";
                }
                // recuperation de tout les produits avec le terme de la recherche
                $produits = rechercherProduitsByVendeur($termeRecherche, $idVendeur);
                // Si elle est vide on invite le vendeur à en ajouter
                // Sinon on affiche les produits qu'il possède
                if (empty($produits)){
                    echo '<p>Vous ne possédez aucun produit !</p><br>' ;
                    echo '<p>Pour en rajouter, <a href="./ajoutProduit.php">cliquez-ici</a></p>' ;
                }
                else {
                    foreach($produits as $produit) {
                        echo '<article>' ;
                            echo '<a href="../detailProduit.php?id=' . $produit['id'] . '">
                                <img src="../images/produits/' . $produit['id_photos'] . '/' . getMainPicProd($produit['id']) . '" title="' . $produit['libelle'] . '" alt="' . $produit['libelle'] . '" class="rounded produit_image">
                                <p>Id : '.$produit['id'].'</p>
                                <span>|</span>
                                <p>Libelle : '.$produit['libelle']. '</p>
                                <span>|</span>
                                <p>HT : '.$produit['prix_ht'].'€</p>
                                <span>|</span>
                                <p>TTC : '.$produit['prix_ht']*getTauxTva($produit["libelle_categorie"]).'€</p>';
                            echo '</a>';

                                //Bloc réservé pour afficher le nombre de quantité en stock
                                echo "<form action='../script/updateStock.php' class='controle_quantite counter' method='post'>";
                                // boutons appelant une fonction js pour enlever un produit parmi la quantité quil y avait -->';
                                echo "<span class='down' onClick='decreaseCount(event, this)''>-</span>";
                                   //Input au niveau de la quantité -->';
                                echo '<input type="text" name="quantity" value='.$produit['stock'].' min="0" >';
                                //pariel que pour le bouton - mais en + -->';
                                echo '<span class="up" onClick="increaseCount(event, this)">+</span>';
                                
                                echo '<input type="hidden" name="id" value='.$produit['id'].' min="0" >';
                                
                                // Bouton permettant de valider la quantité -->';
                                echo'<input class="valider_quantite" type="submit" name"p'.$produit['id'].'" value="Valider" >';
                                
                                echo '</form>';
                                

                            echo '<div class="actionProduits">
                                <div class="toggle-switch">';
                                if ($produit['act'] == 0){
                                    echo '<input class="switch" type="checkbox" name="active" href=\''.$prefixe.'script/updateProduct.php?id='.$produit['id'].'&etat=1\'" checked>' ;
                                }
                                else {
                                    echo '<input class="switch" type="checkbox" name="active" href=\''.$prefixe.'script/updateProduct.php?id='.$produit['id'].'&etat=0\'">' ;
                                }
                                echo '<div class="knob"></div>
                                    <div class="layer"></div>
                                </div>
                                <button class="poubelle" onclick="confirmationSuppression()" type="submit" name="supprimer" value="supprimer produit" href=\''.$prefixe.'script/updateProduct.php?id='.$produit['id'].'&etat=2\'>
                                    <img src="./'.$prefixe.'images/icones/poubelle.png" alt="icone poubelle">
                                </button>
                            </div>
                        </article>' ;
                        echo '<hr>' ;
                    }
                }
                ?>
            </section>

        </main>

        <!--Script permettant la désactivation/activation et suppresion des produits-->
        <script src="./../script/produit.js"></script>
        <script>
            modifierEtatProduit();
            confirmationSuppression();

        // Fonction permettant d'ajouter de un le produit
        function increaseCount(a, b) {
            // Frère direct du b
            var input = b.previousElementSibling;
            var value = parseInt(input.value, 10);
            value = isNaN(value) ? 0 : value;
        
            if (value < <?php echo $produit['stock']?>){
                value++;
                input.setAttribute("value",value) // met la valeur de value dans le value de l'html
        
            }


            }

        // Fonction permettant de diminuer de un le produit
        function decreaseCount(a, b) {
            // Frère direct du b
            var input = b.nextElementSibling;
            var value = parseInt(input.value, 10);
            if (value > 0) {
                value = isNaN(value) ? 0 : value;
                value--;
                input.setAttribute("value",value) // met la valeur de value dans le value de l'html
            }
        }



        </script>

        <!--FOOTER-->
        <?php require('../footerContent.html');?>
    </body>
</html>