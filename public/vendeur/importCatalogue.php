<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
    <title>Import d'un catalogue de produits</title>
    <?php require('../head.php'); 
    require('../script/seller.php')?>
    <?php require($prefixe.'script/catalogue.php'); ?>
    <?php require($prefixe.'script/product.php'); ?>
    <link rel="stylesheet" type="text/css" href="header.css" media="screen">
    <link rel="stylesheet" type="text/css" href="importCatalogue.css" media="screen" />
</head>
<body>
    <!-- le header des pages de type admin -->
    <?php require('headerVendeur.php');?>
    <main>
        <br>
        <!-- Ces boutons servent à naviguer entre les pages importCatalogue.php et ajoutProduit.php -->
        <div class="d-flex justify-content-center">
            <a class="btn btn-primary button-left btn-lg" href="ajoutProduit.php" role="button">Ajouter un produit</a>
            <a class="btn btn-primary button-right btn-lg" href="importCatalogue.php" role="button">Ajouter un catalogue</a>
        </div>
        <div class="container-fluid">
            <!-- Ce forme ne sert qu'à faire en sorte que l'essai de l'import du catalogue ne se fasse que quand le bouton est cliqué -->
            <form action='importCatalogue.php?import=true' method="post" enctype="multipart/form-data" id="formHautPage">
                <div id="bouton_ajouter" class="row col-2 offset-9 color-99CC5B my-4">
                    <button class="btn-secondary btn-a-modif"> Ajouter le catalogue </button>
                </div>
            </form>
            <?php
                // Au premier chargement de cette page, il n'y a que le bouton pour ajouter un catalogue
                // Quand on clique dessus la page essai de charger le catalogue (cad on rentre dans ce if)
                if (isset($_GET['import']))
                {
                    $ficCsv = preg_grep('/.*\.csv/', scandir($prefixe.'produits/'));
                    if ($ficCsv === false or $ficCsv === array())
                    {
                        echo("<p> Fichier CSV introuvable </p>");
                    }
                    else
                    {
                        // lecture des données du fichier csv
                        $tabProduitsCSV = fiCSVersTab($prefixe.'produits/' . $ficCsv[min(array_keys($ficCsv))]);
                        
                        if ($tabProduitsCSV === false)
                        {
                            echo("<p> Les données dans fichier CSV sont invalides </p>");
                        }
                        else
                        {
                            // Sans le cas où le fichier CSV est trouvé et que les données qu'il contient sont valides,
                            // on sauvegarde ces données dans la session pour pouvoir réutiliser les données plus facilement
                            $_SESSION['tabProduitsCSV'] = $tabProduitsCSV;
                            // dans importCatalogueBasPage.php il y a une préview de ce qui va être importé
                            require('importCatalogueBasPage.php');
                        }
                    }
                }
            ?>
        </div>
    </main>
</body>
</html>