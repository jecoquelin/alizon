<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
    
    <?php require('../head.php'); ?>
    <link rel="stylesheet" type="text/css" href="ajoutProduit.css" media="screen" />
    <title>Confirmation de l'ajout du produit</title>

</head>
<body>
    <br>
    <h1 class="confirmMessage">Votre produit à bien été ajouté !</h1>
    <div class="blocBouton d-flex justify-content-around">
        <a class="btn btn-primary button-accueil btn-lg" href="index.php" role="button">Retourner à l'accueil</a>
        <a class="btn btn-primary button-ajoutProduit btn-lg" href="ajoutProduit.php" role="button">Ajouter un autre produit</a>
    </div>
</body>

</html>
