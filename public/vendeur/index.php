<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Alizon</title>
        <!-- on importe head pour avoir l'instance de la connexction a la bdd et les autre dépendances de la page -->

        <?php require('../head.php'); 
              require('../script/seller.php')?>
        <link rel="stylesheet" type="text/css" href="index.css" media="screen">
        <link rel="stylesheet" type="text/css" href="header.css" media="screen">
    </head>
    <body>
                <!-- on importe header du vendeur-->

        <?php require('headerVendeur.php'); ?>
        <main class="container-fluid">
            <section>
                <!-- on met tout les carte séparée et avec un paassage à la lign en cas de dépassement-->
            <div class="col d-flex flex-wrap justify-content-around">
                <!-- ceci est une carte de naviguation-->
                <a href="./ajoutProduit.php">
                    <div class="card">
                    <h6 class="card-title">Ajouter un Produit</h6>
                        <div class="card-body d-flex justify-content-center">
                        <img class="card-img-top" src="../images/icones/ajouter-un-produit.png" alt="Card image cap">
                        </div>
                    </div>
                </a>                
                <a href="./importCatalogue.php">
                    <div class="card">
                    <h6 class="card-title">Ajouter un catalogue</h6>
                        <div class="card-body d-flex justify-content-center">
                        <img class="card-img-top" src="../images/icones/ajouter-des-documents.png" alt="Card image cap">
                        </div>
                    </div>
                </a>
                </a>
                <a href="./mesProduits.php">
                    <div class="card">
                    <h6 class="card-title">Liste des produits</h6>
                        <div class="card-body d-flex justify-content-center">
                        <img class="card-img-top" src="../images/icones/liste-de-choses-a-faire.png" alt="Card image cap">
                        </div>
                    </div>
                </a>
            
                <a href="./catalogue.php">
                    <div class="card">
                    <h6 class="card-title">Gérer le catalogue</h6>
                        <div class="card-body d-flex justify-content-center">
                        <img class="card-img-top" src="../images/icones/catalogue.png" alt="Card image cap">
                        </div>
                    </div>
                </a>
                <a href="./remises.php">
                    <div class="card">
                    <h6 class="card-title">Gérer les remises</h6>
                        <div class="card-body d-flex justify-content-center">
                        <img class="card-img-top" src="../images/icones/remise.png" alt="Card image cap">
                        </div>
                    </div>
                </a>
                <a href="./profil.php">
                    <div class="card">
                    <h6 class="card-title">Informations du compte</h6>
                        <div class="card-body d-flex justify-content-center">
                        <img class="card-img-top" src="../images/icones/icone-dinformation.png" alt="Card image cap">
                        </div>
                    </div>
            </div>
            </section>
            
        </main>
        <!-- footer -->
        <?php require('../footerContent.html'); ?>
    </body>
</html>