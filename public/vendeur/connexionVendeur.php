<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php 
        $pageConnexion = "oui" ;    
        require('../head.php'); 
    ?>
    <title>Connexion</title>
    <link rel="stylesheet" href="connexion.css" type="text/css">
</head>
<body>
    <main>
        <!-- le main de la page -->
        <div class="row">
            <!-- Partie gauche du site -->
            <div id="PartieGauche" class="col-md-5 mb-0 text-center justify-content-md-center">
            <a href="index.php"><img id="logo" src="../images/logos/Logo_blanc.png" alt=""></a>
                <p>
                    Ceci est  la page de connection des vendeurs partenaires de la Cobrec, Veuillez contacter la Cobrec si vous voulez devenir partenaire.<br>
                </p>  
                <p>
                    Remplissez les champs pour vous connecter en tant que vendeur.
                </p>
            </div>
            <!-- Partie droite du site -->
            <div id="PartieDroite" class="col-md-7 justify-content-center">
                <!-- Form permettant de s'inscrire au site -->
                <div class=" pour_form">
                    <form class="form-inline col-md-5" action="../script/loginSeller.php" method="post">
                        <h2 class="text-center">Connexion en tant que vendeur</h2>
                        
                        <!-- Champ de l'email -->
                        <input name="emailVendeur" class="form-control champ email" type="email" placeholder="Email de l'entreprise" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["emailVendeur"] : ''); ?>"/>

                            <!-- Champ du mot de passe -->
                            <input id="password" name="password" onkeydown="afficher();" onkeyup="CheckMdp(this.value)" class="form-control password champ" type="password" placeholder="Mot de passe" value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["password"] : ''); ?>" />
                        </div>
                        <div class="text-center padding">

                        <!-- Bouton pour se connecter -->
                        <input id="inscrire" type="submit" class="btn btn-secondary button" value="se connecter">
                    </div>
                        <?php
                            // Récuperation et affichages des erreur si il en a 
                            if (isset($_GET['erreur'])){
                                switch ($_GET['erreur']) {
                                    case 0:
                                        echo    "<div style='color : #EF0009'; class='text-center'>
                                                    <p>Adresse mail ou mot de passe incorrect</p>
                                                </div>";
                                        break;
                                    case 2:
                                        echo    "<div style='color : #EF0009'; class='text-center'>
                                                    <p>Veuillez remplir les champs</p>
                                                </div>";
                                        break;
                                    case 3:
                                        echo    "<div style='color : #EF0009'; class='text-center'>
                                                    <p>Ce compte a été désactivé</p>
                                                 </div>"; 
                                        break;

                                    
                                    default:
                                        echo "<p>Huuum !</p>";
                                        break;
                                }
                            } 
                        ?>
                    </form>
                    
                </div>
            </div>
        </div>
    </main>
</body>
</html>