<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<!--HEAD-->
<head>
    <title>Mon profil</title> 
    <?php require('../head.php'); ?>
    <?php require('../script/seller.php'); ?>
    <link href="profil.css" rel="stylesheet">
    <link href="header.css" rel="stylesheet">
</head>

<!--BODY-->
<body>
    <!--HEADER-->
    <?php require('./headerVendeur.php'); ?>
    <!--MAIN-->
    <main>
        <!--Partie principale de la page-->
        <div class="container" >
            <div class="row">
                <section class="my-4 p-3">
                    <!--Récupération de l'ID du client-->
                    <?php $idVendeur=$_SESSION['idVendeur']; ?>
                    <article class="justify-content-center align-item-center">

                    <!-- script permettant de modifier ou non la photo de profil-->
                    <?php
                        if (isset($_FILES['profilePicture'])){
                            modifierPhotoDeProfilVendeur($idVendeur, $_FILES['profilePicture']);
                        }
                    ?>

                    <!-- script permettant de modifier les informations personnelles ou non-->
                    <?php  
                        if (isset($_POST["formInfoPerso"])){
                            modifierInfoVendeur($_POST['nom'], $_POST['email'], $_POST['numero_voie'], $_POST['nom_voie'], $_POST['ville'], $_POST['code_postal'], $_POST['code_TVA'],$_POST['cle_TVA'],$_POST['siren'],$_POST['nic'],$_POST['texte_presentation'], $idVendeur ) ;
                        } 
                    ?>

                    <!-- script permettant la modification de mot de passe-->
                    <?php
                        if (isset($_POST['formMDP'])){
                            modifierMotDePasseVendeur($_POST['mdp'], $_POST['new_mdp'], $_POST['new_mdp_conf'], $idVendeur) ;
                        }
                    ?>

                    <!--Récupération des données liées aux clients-->
                    <?php $profil = getVendeurById($idVendeur);?>

                    
                        <!--Profil lorsque l'écran est petit-->
                        <figure class="my-2 text-center d-md-none">
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="menuImageP" data-bs-toggle="dropdown" aria-expanded="false">
                                <?php echo '<img class="photoProfil rounded-circle" src="../images/vendeur/photoProfil/'.$idVendeur.'/'.getMainPicVendeur($idVendeur).'" alt="'.$profil['nom'].'" title="'.$profil['nom'].'">'; ?>
                                </button>
                                <!--Menu apparaissant losque l'on clique sur la photo de profil-->
                                <div class="dropdown-menu" aria-labelledby="menuImageP">
                                    <form action="profil.php" method="POST" enctype="multipart/form-data">
                                        <label>Fichier (.png, .jpeg, .jpg, .gif)</label>
                                        <input type="file" name='profilePicture' accept=".png, .jpeg, .jpg, .gif">
                                        <button type="submit">Enregistrer</button>
                                    </form> 
                                </div>
                            </div>
                            <?php echo '<p>Bonjour, '.$profil['nom'].'</p>'; ?>
                        </figure>
                        
                        <!--Profil lorsque l'écran est grand-->
                        <figure id="menuImageGrand" class="my-2 text-center d-none d-md-block">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="menuImageG" data-bs-toggle="dropdown" aria-expanded="false">
                                    <?php echo '<img class="photoProfil rounded-circle" src="../images/vendeur/photoProfil/'.$idVendeur.'/'.getMainPicVendeur($idVendeur).'" alt="'.$profil['nom'].'" title="'.$profil['nom'].'">'; ?>
                                    </button>
                                    <!--Menu apparaissant losque l'on clique sur la photo de profil-->
                                    <div class="dropdown-menu" aria-labelledby="menuImageG">
                                        <form action="profil.php" method="POST" enctype="multipart/form-data">
                                            <label>Fichier (.png, .jpeg, .jpg, .gif)</label>
                                            <input type="file" name='profilePicture' accept=".png, .jpeg, .jpg, .gif">
                                            <button type="submit">Enregistrer</button>
                                        </form> 
                                    </div>
                                </div>
                                <?php echo '<p>Bonjour, '.$profil['nom'].'</p>'; ?>                            
                        </figure>   
                    </article>

            
                    <!--Informations personnelles liées au client-->
                    <article id="infoPerso">
                        <h3>Informations personnelles</h3>
                        <!--Affichage normal de lecture des informations-->
                        <div id="affichInfoPerso">
                            <ul>
                                <?php 
                                    echo '<li>Raison sociale : '.$profil['nom'].'</li>'; 
                                    echo '<li>Adresse mail : '.$profil['email'].'</li>'; 
                                    echo '<li><br/> <strong>Adresse de livraison </strong>
                                            <ul>
                                                <li>Numero de voie : '.$profil['numero_voie'].'</li>
                                                <li>Nom de voie : '.$profil['nom_voie'].'</li>
                                                <li>Ville : '.$profil['ville'].'</li>
                                                <li>Code postal : '.$profil['code_postal'].'</li>
                                            </ul>
                                        </li><br/>';
                                
                                    echo '<li>TVA intracommunautaire : '.$profil['code_tva'].'&emsp;'.$profil['cle_tva'].'&emsp;'.$profil['siren'].''; 
                                    echo '<li>Siren : '.$profil['siren'].'&emsp;'. $profil['nic'] .'</li>'; 
                                    echo '<li>Description vendeur : '.$profil['texte_presentation'].'</li>'; 
                                    echo '<li>Note : '.$profil['note_vendeur'].'</li>'; ?>
                            </ul>
                            <a class="btn-secondary " role="button" onclick="modifierInfoPerso()">Modifier</a>
                        </div>
                        
                        <!--Affichage du formulaire de modification lié au informations personnelles-->
                        <form id="formInfoPerso" action="./profil.php?id=<?php echo $idVendeur ?>" method="POST">
                            <ul>
                                <?php echo '<li><label>Nom :                </label><input type="text"  name="nom"               value="'.$profil['nom'].'">         </li>'; 
                                echo '<li><label>Adresse mail :       </label><input type="email"  name="email"            value="'.$profil['email'].'" required>       </li>'; ?>
                                    <?php echo '
                                        <li><br/><strong>Adresse de livraison </strong>
                                            <ul>
                                                <li><label>Numero de voie :</label><input type="text"  name="numero_voie"   value="'.$profil['numero_voie'].'" required> </li>
                                                <li><label>Nom de voie :   </label><input type="text"  name="nom_voie"      value="'.$profil['nom_voie'].'"required>    </li>
                                                <li><label>Ville :         </label><input type="text"  name="ville"         value="'.$profil['ville'].'" required>       </li>
                                                <li><label>Code postal :   </label><input type="text"  name="code_postal"   value="'.$profil['code_postal'].'" required> </li>
                                            </ul>
                                        </li><br/>';
                                    ?>
                                <?php echo '<li><label>TVA intracommunautaire :  </label><input type="text"    name="code_TVA"    value="'.$profil['code_tva'].'" placeholder="Code" maxlength="3"  required> 
                                                                                         <input type="number"  name="cle_TVA"    value="'.$profil['cle_tva'].'"  placeholder="Clé" min="0" max="99" required> 
                                                                                         <input type="number"  name="siren"  value="'.$profil['siren'].'"    placeholder="siret" min="0" max="999999999" required >       
                                                                                         <input type="number"  name="nic"    value="'.$profil['nic'].'"      placeholder="Nic" min="0" max="99999" required >       '; 
                               
                                echo '<li><label>Description vendeur :            </label><input type="text"  name="texte_presentation"       value="'.$profil['texte_presentation'].'"       required> '; ?>
                            </ul>
                            <a class="btn-secondary"    role="button" onclick="afficherInfoPerso()">Annuler</a>
                            <input class="btn-secondary"  type="submit"        value="Valider" name="formInfoPerso">
                        </form>
                        
                    </article>

                    <!--Donnée lié au client-->
                    <article id="donneeEtConfidentialite">
                        <h3>Données et confidentialité</h3>
                        <ul>
                            <li>Cookies : Autorisés</li>
                            <li>Historique des achats : Afficher</li>
                        </ul>
                    </article>
            
                    <!--Sécurité / Mot de passe-->
                    <article id="securite">
                        <h3>Sécurité</h3>
                        <!--Consultation des informations de sécurité-->
                        <div id='infoMotDePasse'>
                            <ul>
                                <li>Mot de passe : *********</li>
                            </ul>
                            <a class="btn-secondary" onclick="modifierMotDePasse()" role="button">Modifier</a>
                        </div>

                        <!--Formulaire de modification de mot de passe-->
                        <div id='formMotDePasse'>
                            <form action="./profil.php" method="POST">
                                <label>Mot de passe actuel : </label>                       <input type="password" name="mdp" required><br>
                                <label>Nouveau mot de passe : </label>                      <input id="password" type="password" name="new_mdp" onkeydown="afficher();" onkeyup="CheckMdp(this.value)" required><br>
                                <label>Confirmation nouveau mot de passe : </label>         <input type="password" name="new_mdp_conf" required><br>
                                <a class="btn-secondary"    role="button"   onclick="afficherMotDePasse(), cache()">Annuler</a>
                                <input class="btn-secondary" type="submit"        value="Valider" name="formMDP">
                            </form>
                        </div>

                        <!--Affichage des caractères manquant du nouveaux mot de passe-->
                        <div id="error">
                            <span id = "msg1"></span><br>
                            <span id = "msg2"></span><br>
                            <span id = "msg3"></span><br>
                            <span id = "msg4"></span><br>
                            <span id = "msg5"></span>
                        </div>

                        <div id="inscrire"></div>
                        <script src="./../script/profil.js"></script>
                    </article>

                    <!--Script JS pour les informations lié au nouveau mot de passe-->
                    

                    <!--Supression d'un compte-->
                    <article id="supressionCompte">
                        <h3>Suppression de compte</h3>
                        <a class="btn-danger" href="./supprimerCompte.php" role="button">Supprimer</a>
                    </article>
                </section>
            </div>
        </div>
    </main>
    <script src="./../script/password.js"></script>
    <!--FOOTER-->
    <?php require('../footerContent.html'); ?>

    <!--Script pour les éléments dynamique de la page (apparition des formulaires ou de la simple consultation)-->
    
</body>
</html>
