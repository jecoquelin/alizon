<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php require('../head.php'); 
	require('../script/seller.php')?>
	<link rel="stylesheet" type="text/css" href="header.css" media="screen">
    <link rel="stylesheet" type="text/css" href="ajoutProduit.css" media="screen" />
    <title>Ajouter un Produit</title>
</head>
<body>
	<?php require('headerVendeur.php');?>
	<br>
	<!-- Bouton de navigation en haut de page-->
	<div class="d-flex justify-content-center">
		<a class="btn btn-primary button-left btn-lg" href="ajoutProduit.php" role="button">Ajouter un produit</a>
		<a class="btn btn-primary button-right btn-lg" href="importCatalogue.php" role="button">Ajouter un catalogue</a>
	</div>
	<!-- Fromulaire pour ajouter un produit -->
	<form action="<?php echo $prefixe ?>script/addProduct.php" class="col-lg-10 offset-lg-1" method="POST" enctype="multipart/form-data">
		<div class="row justify-content-center form-group">
			<label for="nomproduit">Nom produit <span style="color: #FF8605">*</span></label><br>	
			<input class="form-control" type="text" id="nomproduit" name="nomproduit" value="" row="3" required="required"><br>
			<label for="nomsociete">Nom de votre société <span style="color: #FF8605">*</span></label><br>
			<input class="form-control" type="text" id="nomsociete" name="nomsociete" value="" required="required"><br>
			<labe for="inputCat">Categorie <span style="color: #FF8605">*</span></
			<select id="inputCat" class="form-control" name="inputCat" required="required">
				<option selected id="firstLabel" disabled>Sélectionner une catégorie</option>
				<!-- Pouvoir afficher toutes les categorie disponible -->
				<?php
					global $dbh;
					$sth = $dbh->prepare('SELECT * from alizon._categorie');
					$sth -> execute();
					$categories = $sth -> fetchAll();
					foreach($categories as $cat) {
						echo "<option>";
						echo($cat["libelle"]);
						echo "</option>";
					}
				?>
			</select>
			<label for="prix">Prix HT <span style="color: #FF8605">*</span></label><br>
			<input class="form-control" type="number" id="prix" name="prix" min="0" step="0.01" required="required"><br> <!-- oninput="this.value=this.value.replace(/^([0-9]+\.?[0-9]{0,2})(.*)$/g,'');" -->

			<label for="description">Description <span style="color: #FF8605">*</span></label><br>
			<textarea class="form-control textarea" id="description" name="description" rows="10" required="required"></textarea>
		
				<!-- L'ensemble des photos -->
			<label for="image">Photos <span style="color: #FF8605">*</span></label>
			<div class="form">
				<div class="grid">
					<div class="form-element">
						<input type="file" id="file-1" name="file1" accept="image/*">
						<label for="file-1" id="file-1-preview">
							<img width="200" height="200">
							<div>
								<span>+</span>
							</div>
						</label>
					</div>
				<div class="form-element">
					<input type="file" id="file-2" name="file2" accept="image/*">
					<label for="file-2" id="file-2-preview">
						<img width="200" height="200">
						<div>
							<span>+</span>
						</div>
					</label>
				</div>
				<div class="form-element">
					<input type="file" id="file-3" name="file3" accept="image/*">
					<label for="file-3" id="file-3-preview">
						<img width="200" height="200">
						<div>
							<span>+</span>
						</div>
					</label>
				</div>
				<div class="form-element">
					<input type="file" id="file-4" name="file4" accept="image/*">
					<label for="file-4" id="file-4-preview">
						<img width="200" height="200">
						<div>
							<span>+</span>
						</div>
					</label>
				</div>
				<div class="form-element">
					<input type="file" id="file-5" name="file5" accept="image/*">
					<label   for="file-5" id="file-5-preview">
						<img width="200" height="200">
						<div>
							<span>+</span>
						</div>
					</label>
				</div>
				<div class="form-element">
					<input type="file" id="file-6" name="file6" accept="image/*">
					<label for="file-6" id="file-6-preview">
						<img width="200" height="200">
						<div>
							<span>+</span>
						</div>
					</label>
				</div>
			</div>
			<br>
		</div>

		<label for="nbStock">Quantité en stock <span style="color: #FF8605">*</span></label><br>
		<input class="form-control" type="number" id="nbStock" name="nbStock" min="0" required="required" oninput="this.value=this.value.replace(/[^0-9]/g,'');"><br>	
		<label for="seuil">Seuil</label><br>
		<input class="form-control" type="number" id="seuil" name="seuil" value="0" min="0" oninput="this.value=this.value.replace(/[^0-9]/g,'');"><br>
		<div><br></div>
		<div class="d-flex justify-content-between">
        <p><span style="color: #FF8605">*</span> Champs obligatoires</p>
        <input class="btn-primary" id="boutonAjouterProduit" type="submit" value="Ajouter le produit">
		</div>
		<div>
		<br>
		</div>
		<p id="msgOublie"></p>
		</div>
	</form> 
	<script>
    var btnEnvoyer= document.getElementById("boutonAjouterProduit");
    var listeCat= document.getElementById("inputCat");
    var firstCat= document.getElementById("firstLabel");
    var styles = document.createElement("style");
    var texte = document.createTextNode("#msgOublie{color:red;background-color: #F4F4F4;border-radius: 5%;border: 1px solid black;border-radius: 5px;padding : 0.5em;text-align: center;}");
    
    /*Suppression Message d'erreur oublie Selection catégorie*/
    listeCat.addEventListener("change", function(event){
		if (listeCat.value != "Sélectionner une catégorie"){
		document.getElementById("msgOublie").innerHTML = "";
		styles.removeChild(texte);
		document.head.removeChild(styles);
		}
    });

    /*Suppression Oublie Selection Image*/ 
    document.querySelector("#file-1").addEventListener("change", function(event){
		if (document.querySelector("#file-1").value != ""){
		document.getElementById("msgOublie").innerHTML = "";
		styles.removeChild(texte);
		document.head.removeChild(styles);
		}
    });

    /* Verification avant l'envoie des champs "Selectionner une categorie" et "Selectionner une image"*/
    btnEnvoyer.addEventListener("click", function(event) {
	if (listeCat.value == "Sélectionner une catégorie") {
        event.preventDefault();
        firstCat.style.color = "red";
        document.getElementById("msgOublie").innerHTML = "Vous avez oublié de sélectionner une catégorie.";
        styles.appendChild(texte);
        document.head.appendChild(styles);
	}
	else if (document.querySelector("#file-1").value == "") {
          event.preventDefault();
          document.getElementById("msgOublie").innerHTML = "Vous avez oublié d'ajouter la première image.";
          styles.appendChild(texte);
          document.head.appendChild(styles);
        }
    });

    /* Affichage de l'image une fois la selection terminé*/
    function previewBeforeUpload(id){
      document.querySelector("#"+id).addEventListener("change",function(e){
        if(e.target.files.length == 0){
          return;
        }
        let file = e.target.files[0];
        let url = URL.createObjectURL(file);
        document.querySelector("#"+id+"-preview div").innerText = null;
        document.querySelector("#"+id+"-preview img").src = url;
      });
    }

    previewBeforeUpload("file-1");
    previewBeforeUpload("file-2");
    previewBeforeUpload("file-3");
    previewBeforeUpload("file-4");
    previewBeforeUpload("file-5");
    previewBeforeUpload("file-6");

  </script>
  <?php require($prefixe.'footerContent.html'); ?>
</body>

</html>


<!--
        Pour les images ajoutées '1' pour la premiere image et ensuite
        ajouter 'n nomImage."extension"' .

        dossier incrémenter

-->