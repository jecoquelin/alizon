# Alizon

Ce dépôt contient une application web de e-commerce prête à être déployé

## Contexte
Lors du BUT informatique, nous avons eu des projets à réaliser. Celui-ci est l'un deux.

## Description 
Cette SAE nous a permis de travailler sur un projet de grande ampleur pendant 6 mois en équipe sur un site de e-commerce. Ce projet à été organisé en méthode Agile. Ce projet nous as appris d'autre façon de travailler.

## Apprentissage critique

AC31.01 | Choisir et implémenter les architectures adaptées <br>
AC31.02 | Faire évoluer une application existante<br>
AC31.03 | Intégrer des solutions dans un environnement de production<br>
AC36.04 | Accompagner le management de projet informatique

## Langage et Framework 
  * Postgresql
  * PHP
  * JavaScript
  * HTML
  * CSS

## Lien du site
Le site est hébergé sur un serveur web dont voici le lien : [alizon](http://alizon.meatman.fr/html/).
## Petit tuto pour mettre en place notre en site en local
Faites un git clone du projet.
### Selon votre SGBD suivez le tuto adapté
- ### Postregsql (branche main)
    Je pars du principe que vous avez un serveur postrgesql qui tourne sur votre pc, ensuite créez le fichier `html/connectParams.php` et remplissez le avec ces lignes :
    ```
    <?php
        $server = 'localhost';
        $driver = 'psql';
        $dbname = ''; // à complèter (postgres par défaut)
        $user   = ''; // à complèter (postgres par défaut)
        $pass	= ''; // à complèter
    ?>
    ```

- ### Mysql (branche mysql)
    Commencez par faire un `git checkout mysql` par faire 
    Je pars du principe que vous avez un serveur mysql qui tourne sur votre pc avec une base de données nommée `sae301_a11`, ensuite créez le fichier `html/connectParams.php` et remplissez le avec ces lignes :
    ```
    <?php
        $server = 'localhost';
        $driver = 'mysql';
        $dbname = 'sae301_a11';
        $user   = ''; // à complèter
        $pass	= ''; // à complèter
    ?>
    ```
### Accèder au site Alizon
- Lancez un serveur php dans le dossier `html/` avec la commande : `php -S localhost:8080`.
- Entrez dans votre navigateur préféré l'url pour réinitialiser la base de données : [http://localhost:8080/reset.php](http://localhost:8080/reset.php).
### Fin
Le site est maitenant fonctionnel, vous pouvez naviguer dessus.