<?php
session_start();
// Nous regardons si le catalogue existe
if (file_exists('../catalogue/catalogue.json')){
    // Supprime le fichier catalogue JSON
    unlink("../catalogue/catalogue.json");
    // Réinitialisation du tableau qui contient l'id des produit séléctionné
    $_SESSION['produits_active'] = [];
    header("Location: ../catalogue.php");
} else {
    header("Location: ../catalogue.php");
}
?>