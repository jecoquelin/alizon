<?php 
session_start();
require("prefixe.php");
require('../../public/connectParams.php');
require('../../public/script/product.php');
include('../../public/script/seller.php');
//ce if sert à savoir de quel bouton on accède à ce script
try {
    $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
    
    $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    print "Erreur : " . $e->getMessage() . "<br/>";
    die();
}
// Variable de session qui reçoit l'id des produits qui ont été selectionné
$_SESSION['produits_active'] = [];

$_SESSION['listeProduits'] = ['vendeurs'=>[]];

if (empty($_SESSION['listeProduits']['vendeurs']) == 1){
}

if(isset($_POST["submitCatalogue"])){
    //ici on ajoute les promo
    try {
        $tab = []; // tab sera la liste des produit que le vendeur a choisi
        foreach($_POST["Prod"] as $key => $promo){
            $id=$_POST["num"][$key];
            if($promo=="on"){
                $p = getProduitById($id);
                array_push($_SESSION['produits_active'], $id);
                $vendeur = getVendeurById($p['id_vendeur']);
                
                $tab = [$id => [
                            'image' => 'https://alizon.meatman.fr/images/produits/'.$p['id_photos'].'/'.getMainPicProd($p['id_photos']),
                            'nom' => $p['libelle'],
                            'description' => $p['_description'],
                            'prix ttc' => round(($p['prix_ht']*getTauxTva($p["libelle_categorie"])),2),
                            'prix ht' => $p['prix_ht']
                        ]
                ];

                

                if (empty($_SESSION['listeProduits']['vendeurs'][$vendeur['id']]) == 1){
                    $_SESSION['listeProduits']['vendeurs'] += [
                                                                        $vendeur['id'] => [
                                                                            'image' => 'https://alizon.meatman.fr/images/vendeur/photoProfil/'.$vendeur['id'].'/'.getMainPicVendeur($vendeur['id']).'',
                                                                            'nom' => $vendeur['nom'],
                                                                            'raison social' => $vendeur['nom'],
                                                                            'nic' => $vendeur['nic'],
                                                                            'code tva' => $vendeur['code_tva'],
                                                                            'cle tva' => $vendeur['cle_tva'],
                                                                            'siren' => $vendeur['siren'],
                                                                            'numero voie' => $vendeur['numero_voie'],
                                                                            'nom voie' => $vendeur['nom_voie'],
                                                                            'code postal' => $vendeur['code_postal'],
                                                                            'ville' => $vendeur['ville'],
                                                                            'contact' => $vendeur['email'],
                                                                            'presentation' => $vendeur['texte_presentation'],
                                                                            'note' => $vendeur['note_vendeur'],
                                                                            'produits' => []
                                                                        ]
                                                                        ];
                }
                $_SESSION['listeProduits']['vendeurs'][$vendeur['id']]['produits'] += $tab;
            }
        }

        //Regarde si le dossier est créer et sinon il le créer
        if (!is_dir('../catalogue')){
            mkdir('../catalogue');
        }
        
        //met le nom fichier dans une variable
        $file = '../catalogue/catalogue.json';

        //Tranforme le tableau en JSON 
        $donnee = json_encode($_SESSION['listeProduits']);

        //Ecrit les donnée du JSON dans le dossier spécifié
        file_put_contents($file, $donnee);

        //Efface les produits sélectionné au cas ou il reselectionnerais d'autre produit
        $_SESSION['listeProduits']['vendeurs'][$p['id_vendeur']]['produits'] = [];

        header("Location: ../catalogue.php");
        
    
    } catch (PDOException $e) {
    
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
}
