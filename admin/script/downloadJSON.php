<?php
//Force le Téléchargement du JSON 
if (file_exists('../catalogue/catalogue.json')){
    header("Content-disposition: attachment; filename=catalogue.json");
    header("Content-Type: x/y\n"); 
    readfile('../catalogue/catalogue.json');
    //Supprime le Fichier JSON lorsque la personne l'a téléchargé
    // unlink("../catalogue/catalogue.json");
    die();
} else {
    header("Location: ../catalogue.php");
}

?>