<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <?php require('../public/head.php'); ?>
        <link rel="stylesheet" href="./catalogue.css" type="text/css">
        <?php 
            require('../public/script/searchProduct.php'); 
            require('../public/script/product.php');
            require('../public/script/seller.php');
        ?>
        <title>catalogueur</title>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    </head>

    <body>

        <!--HEADER-->
        <header>
            <div class="d-flex justify-content-beetween align-items-center" style="background-color: #144FC1 ;">
                <div class="col-md-2" >
                    <a href="./index.php"><img id="logoAlizon" class="d-none d-lg-block " src="<?php echo $prefixe ?>images/logos/Logo_blanc.png" alt="logo Alizon" ></a>
                </div>       
            </div>
            <div id="revenirAcceuil" class="justify-content-center" >
                <a id="lienAcceuil" href="./index.php">
                    <img id="flecheComeback" src="<?php echo $prefixe ?>images/icones/undo-outline.png" alt="revenir à l'acceuil"><p>Revenir à l'accueil</p>
                </a>
            </div>
        </header>

        <!--Corp de la page-->
        <main class="col-md-10 offset-1">

            <div>
                <div>
                    <?php 
                        // regarde les fichiers et dossier qu'il y a dans le dossier ou est/sera stocke le catalogue
                        $scandir = false;
                        if (is_dir('./catalogue')){
                            $scandir = scandir('./catalogue');
                        }


                        //Regarde si en 2ème position, il y a un fichier qui s'appelle catalogue.php
                        // Si oui, alors on informe le vendeur qu'il y a un catalogue et qu'il peut le télécharger
                        // Si non, alors on informe le vendeur qu'il n'as pas de catalogue et qu'il peut choisir ses produit pour en avoir un  
                        if ($scandir !=false && isset($scandir[2])){
                            if ($scandir[2] == 'catalogue.json'){
                                // Renvoie au Script permettant de télécharger le fichier json 
                                echo "<p class='d-inline-block'>Vous avez un catalogue, appuyez sur ce bouton pour le télécharger.</p>";
                                echo '<form action="./script/downloadJSON.php" class="formButton">';
                                echo '<input type="submit" name="submitCatalogue" class="btn btn-secondary button " value="Télécharger">';
                                echo '</form>';
                                echo '<form method="get" action="./script/removeJSON.php" class="formButton">';
                                echo '<input type="submit" name="submitCatalogue" class="btn btn-danger" value="Réinitailiser">';
                                echo '</form>';
                            }
                        } else {
                            echo "<p class='d-inline-block'>Vous n'avez aucun catalogue. Veuillez choisir des produits pour en générer un.</p>";
                            echo '<input type="submit" name="submitCatalogue" class="btn btn-secondary button " disabled value="Télécharger">';
                        }
                        
                    
                    ?>
                </div>
                
            <!--Form de recherche d'un produit-->
                <div id="divForm">
                    <form class="search" action="produits.php">
                        <div id="inputGroup" class="input-group ">
                            <input id="input" name="search" type="text" class="form-control" placeholder="Rechercher par nom"  >
                        </div>
                        <input type="submit" style="display: none">
                    </form>
                </div>
                <!--Liste des produits-->
                <div>
                    <h2>Mes produits</h2>
                    <br>
                    <hr>
                    <!-- form pour ajouter une promo ou une remise -->
                    <form action="script/submitProduitCatalogue.php" method="post">
                        <?php
                        //gestion de la recherche
                        if (isset($_GET['search']))
                        {
                            $termeRecherche = $_GET['search'];
                        }
                        else
                        {
                            $termeRecherche = "";
                        }
                        $cataloguer=1;
                        // recuperation de tout les produits avec le terme de la recherche
                        
                        // Si elle est vide on invite le vendeur à en ajouter
                        // Sinon on affiche les produits qu'il possède
                        
                        foreach(rechercherProduits($termeRecherche) as $produit) { 
                        ?>
                        <div class = "profil">
                            <?php echo '<a href="'.$prefixe.'detailProduit.php?id=' . $produit['id'] . '">'; ?>
                                <?php echo '<img src="'.$prefixe.'images/produits/' . $produit['id_photos'] . '/' . getMainPicProd($produit['id']) . '" title="' . $produit['libelle'] . '" alt="' . $produit['libelle'] . '" class="rounded produit_image">';?>
                                <?php echo '<p>Id : '.$produit['id'].'</p>'; ?>
                                <?php echo '<p>Libelle : '.$produit['libelle']. '</p>'; ?>
                            </a>
                            
                            <input name="num[<?php echo $cataloguer;?>]" type="hidden" value="<?php echo$produit['id']; ?>">
                                <label class="switch">
                                    <?php 
                                        // Nous verifions si la variable de session existe
                                        if (isset($_SESSION['produits_active'])){
                                            // Si l'id du produit n'est pas dans le tableau, les produit sont désactivé.
                                            // Sinon ils sont activé
                                            if (in_array($produit['id'], $_SESSION['produits_active'])){
                                                echo '<input name="Prod['.$cataloguer.']" type="hidden" value="off">';
                                                echo '<input name="Prod['.$cataloguer.']" type="checkbox" checked>';
                                            }else{
                                                echo '<input name="Prod['.$cataloguer.']" type="hidden" value="off">';
                                                echo '<input name="Prod['.$cataloguer.']" type="checkbox">';
                                            }
                                        }
                                        else{
                                            echo '<input name="Prod['.$cataloguer.']" type="hidden" value="off">';
                                            echo '<input name="Prod['.$cataloguer.']" type="checkbox">';
                                    
                                        }
                                    ?>
                                    <span class="slider round"></span>
                                </label>
                        </div>
                        
                        <hr>
                        <?php
                            $cataloguer=$cataloguer+1;
                        }
                        
                        ?>
                        <!-- input pour la promo -->
                        <input type="submit" name="submitCatalogue" class="btn btn-secondary button fix" value="Valider">
                    </form>
                </div>   
            <div>
        </main>
    </body> 
</html>

<script>
    const btn = document.querySelector('.btn-danger');
    function warning(){
        event.preventDefault();
        bool = confirm("Vous êtes sur de vouloir réinitialiser le catalogue ?");
        if (bool){
            this.form.submit();
        }
    }

    btn.addEventListener("click", warning);


//     window.onload = function(){

//         $.get("catalogue/catalogue.json").done(function(data){
//             console.log('file is ready !');
//             console.log(data);
//             console.log(typeof data);
//             console.log(data.vendeurs);
//             var monTableau = Object.keys(data.vendeurs).map(function(cle) {
//                 return [Number(cle), data.vendeurs[cle]];
//                 });
//             console.log(monTableau);
            
//             monObjet = monTableau[0][1]['produits'];
//             console.log("Mon tableau de produit", monObjet)
//             idProd = Object.keys(monObjet)
            

//         }).fail(function() { 
//             console.log('Oups ! file not found ...');
//         })

// }
</script>