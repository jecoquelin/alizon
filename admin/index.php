<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Alizon</title>
        <!-- on importe head pour avoir l'instance de la connexction a la bdd et les autre dépendances de la page -->

        <?php require('../public/head.php');?>
        <link rel="stylesheet" type="text/css" href="admin.css" media="screen">
        <link rel="stylesheet" type="text/css" href="index.css" media="screen">
    </head>
    <body>
             <!--HEADER-->
    <header>
        <div class="d-flex justify-content-beetween align-items-center" style="background-color: #144FC1 ;">
            <figure class="col-2" >
                <a href="<?php echo $prefixe ?>index.php"><img id="logoAlizon" src="<?php echo $prefixe ?>images/logos/Logo_blanc.png" alt="logo Alizon" ></a>
            </figure>
        </div>
        <div id="revenirListe" class="justify-content-center" >
            <a id="lienListe" href="<?php echo $prefixe ?>index.php">
                <img id="flecheComeback" src="<?php echo $prefixe ?>images/icones/undo-outline.png" alt="Revenir à la liste des commandes"><p>Accueil public</p>
            </a>
        </div>
    </header>

        <main class="container-fluid">
            <section>
                <!-- on met tout les carte séparée et avec un paassage à la lign en cas de dépassement-->
            <div class="col d-flex flex-wrap justify-content-around">
                <!-- ceci est une carte de naviguation-->
                <a href="./clients.php">
                    <div class="card">
                    <h6 class="card-title">Moderation clients</h6>
                        <div class="card-body d-flex justify-content-center">
                        <img class="card-img-top" src="../public/images/icones/client.png" alt="Card image cap">
                        </div>
                    </div>
                </a>                
                <a href="./vendeurs.php">
                    <div class="card">
                    <h6 class="card-title">Moderation vendeurs</h6>
                        <div class="card-body d-flex justify-content-center">
                        <img class="card-img-top" src="../public/images/icones/vendeur.png" alt="Card image cap">
                        </div>
                    </div>
                </a>
                </a>
                <a href="./commandes.php">
                    <div class="card">
                    <h6 class="card-title">Liste des commandes</h6>
                        <div class="card-body d-flex justify-content-center">
                        <img class="card-img-top" src="../public/images/icones/bon-de-commande.png" alt="Card image cap">
                        </div>
                    </div>
                </a>
            
                <a href="./produits.php">
                    <div class="card">
                    <h6 class="card-title">Liste des produits</h6>
                        <div class="card-body d-flex justify-content-center">
                        <img class="card-img-top" src="../public/images/icones/liste-de-choses-a-faire.png" alt="Card image cap">
                        </div>
                    </div>
                </a>
                <a href="./inscriptionVendeur.php">
                    <div class="card">
                    <h6 class="card-title">Inscrire un vendeur</h6>
                        <div class="card-body d-flex justify-content-center">
                        <img class="card-img-top" src="../public/images/icones/formulaire-dinscription.png" alt="Card image cap">
                        </div>
                    </div>
                </a>
                <a href="./catalogue.php">
                    <div class="card">
                    <h6 class="card-title">Gérer le catalogue</h6>
                        <div class="card-body d-flex justify-content-center">
                        <img class="card-img-top" src="../public/images/icones/catalogue.png" alt="Card image cap">
                        </div>
                    </div>
            </div>
            </section>
            
        </main>
    </body>
</html>