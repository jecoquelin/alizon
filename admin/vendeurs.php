<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <?php require('../public/head.php'); ?>
        <link rel="stylesheet" href="vendeurs.css" type="text/css">
        <title>Liste de compte vendeur</title>
        <?php require($prefixe.'script/seller.php'); ?>
        <?php require($prefixe.'script/searchAccount.php'); ?>
    </head>

    <body>

        <!--Header / Haut de page-->
        <header>
            <div class="d-flex justify-content-beetween align-items-center" style="background-color: #144FC1 ;">

                <div class="col-md-2" >
                    <a href="./index.php"><img id="logoAlizon" class="d-none d-lg-block " src="<?php echo $prefixe ?>images/logos/Logo_blanc.png" alt="logo Alizon" ></a>
                </div>

            </div>

            <div id="revenirAcceuil" class="justify-content-center" >
                <a id="lienAcceuil" href="./index.php">
                    <img id="flecheComeback" src="<?php echo $prefixe ?>images/icones/undo-outline.png" alt="revenir à l'acceuil"><p>Revenir à l'accueil</p>
                </a>
            </div>
        </header>

        <!--Corp de la page-->
        <main>
            <!--Feedback suppresion/désactivation comtpe-->
            <?php 
            // Si err=1 signifie que l'admin a cherché un vendeur avec un id inconnu
                if (isset($_GET['id'])){
                    if ($_GET['err'] == 1){
                        echo '<p class="feedbackERR">Erreur : Le vendeur id:'.$_GET['id'].' n\'existe pas.</p>';
                    }
                    // Si disable == true alors l'admin demande de désactivé un compte
                    else if ($_GET['disable'] == 1){
                        // Si il rentre un id vendeur inconnu alors la désactivation est impossible
                        // Sinon il désactive
                        if (is_null(getVendeurById($_GET['id']))){
                            echo '<p class="feedbackERR">Erreur : Impossible de désactivé ce compte, id vendeur inconnu : '.$_GET['id'].'</p>';
                        } else {
                            desactiveCompteVendeur($_GET['id']);
                        }
                    }
                }            
            ?>

            <!--Form de recherche d'un utilisateur-->
            <div id="divForm">
                <form action="vendeurs.php">
                    <div id="inputGroup" class="input-group ">
                        <input id="input" name="search" type="text" class="form-control" placeholder="Rechercher par nom"  >
                    </div>
                    <input type="submit" style="display: none">
                </form>
            </div>

            <!--Liste des profils / vendeur-->
            <div>
                <h2>Les profils vendeurs</h2>
                <br>
                <hr>
                <?php

                    if (isset($_GET['search']))
                    {
                        $termeRecherche = $_GET['search'];
                    }
                    else
                    {
                        $termeRecherche = "";
                    }
                    // Fait une boucle pour afficher tout les vendeurs 
                    foreach(rechercherCompteVendeur($termeRecherche) as $vendeur) { 


                        ?>
                        <!-- Affiche toutes les infos du compte utilisateur -->
                        <div class = "profil">
                            <?php echo '<a href="./detailCompteVendeur.php?id=' . $vendeur['id'] . '">'; ?>
                                <?php echo '<img class = "rounded-circle" src="'.$prefixe.'images/vendeur/photoProfil/'.$vendeur['id'].'/'.getMainPicVendeur($vendeur['id']).'" title="'.$vendeur['nom'].'" alt="'.$vendeur['nom'].' style = width">'; ?>
                                <?php echo '<p>Id : '.$vendeur['id'].'</p>'; ?>
                                <span>|</span>
                                <?php echo '<p>Nom : '.$vendeur['nom']. '</p>'; ?>
                                <span>|</span>
                                <?php echo '<p>Email : '.$vendeur['email'].'</p>'; ?>
                                <span>|</span>
                                <?php echo '<p>Siren : '.$vendeur['siren'].'</p>'; ?>
                                <span>|</span>
                                <?php
                                // Verifie si le compte est actif ou pas et affiche en conséquence
                                if ($vendeur['active'] == 0){
                                    echo '<p>Statut d\'activité : Inactif</p>'; 
                                } else {
                                    echo '<p>Statut d\'activité : Actif</p>'; 
                                }
                                ?>
                            </a>
                        </div>
                        <hr>

                        <?php
                    }
                ?>

            </div>   
                
        </main>
    </body>
</html>