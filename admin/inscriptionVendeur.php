<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php require('../public/head.php'); ?>
    <title>Inscription</title>
    <link rel="stylesheet" href="../public/vendeur/connexion.css" type="text/css">
</head>
<body>
    <main>
        <!-- le main de la page -->
        <div class="row">
            <!-- Partie gauche du site -->
            <div id="PartieGauche" class="col-md-5 mb-0 text-center justify-content-md-center">
            <a href="index.php"><img id="logo" src="../public/images/logos/Logo_blanc.png" alt=""></a>
                <p>
                    Ceci est  la page d’inscription des vendeurs, elle ne doit pas être accessible depuis l'exterieur de l'enceinte de la Cobrec.<br>
                </p>  
                <p>
                    Remplissez les champs pour finaliser l'inscription d'un vendeur en tant que partenaire de la Cobrec.
                </p>
            </div>
            <!-- Partie droite du site -->
            <div id="PartieDroite" class="col-md-7 justify-content-center">
            <button onclick="auto()">Auto</button>
                <!-- Form permettant de s'inscrire au site -->
                <div class=" pour_form">
                    <form class="form-inline col-md-5" action="../public/script/createSeller.php" method="post">
                        <h2 class="text-center">Inscription en tant que vendeur</h2>
                        <!-- Champ du prénom -->
                        <input name="nomVendeur" class="form-control champ name auto" type="text" placeholder="Nom de vendeur" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["nomVendeur"] : ''); ?>"/>
                    
                        <!-- Champ de l'email -->
                        <input name="emailVendeur" class="form-control champ email auto" type="email" placeholder="Email de l'entreprise" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["emailVendeur"] : ''); ?>"/>

                        <!-- Champ de description vendeur -->
                        <textarea class="form-control champ auto" name="descriptionVendeur" maxlength="255" rows="5" placeholder="Description" required="required" value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["descriptionVendeur"] : ''); ?>"><?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["descriptionVendeur"] : ''); ?></textarea>

                        <!-- Champ de voie -->
                        <input name="voieVendeur" class="form-control champ street auto" type="text" placeholder="Nom de voie" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["voieVendeur"] : ''); ?>"/>
                        
                        <div class="row d-flex justify-content-around petit">
                            <!-- Champ numero de voie -->
                            <input name="noVoieVendeur" class="form-control col num auto" type="number" placeholder="N° de voie" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["noVoieVendeur"] : ''); ?>"/>
                        

                            <!-- Champ code postal -->
                            <input name="postalVendeur" class="form-control col postal auto" type="text" placeholder="Code postal" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["postalVendeur"] : ''); ?>"/>
                        </div>

                            <!-- Champ de ville -->
                            <input name="villeVendeur" class="form-control champ street auto" type="text" placeholder="Ville" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["villeVendeur"] : ''); ?>"/>
                            

                            <div class="row d-flex justify-content-around petit">

                            <!-- Champ de code pour Tva-intracommunautaire -->
                            <input name="codeTVA" class="form-control  TVA auto" maxlength="3" type="text" placeholder="Code TVA" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["codeTVA"] : ''); ?>"/>

                            <!-- Champ de clé pour Tva-intracommunautaire -->
                            <input name="cleTVA" class="form-control  TVA auto"  max="99" type="number" placeholder="Clé" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["cleTVA"] : ''); ?>"/>

                            <!-- Champ de n° Siren -->
                            <input name="siren" class="form-control  siret auto" maxlength="9" type="text" placeholder="Siren" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["siren"] : ''); ?>"/>

                            <!-- Champ de NIC pour siret -->
                            <input name="nic" class="form-control  NIC auto" maxlength="5" type="text" placeholder="NIC" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["nic"] : ''); ?>"/>


                            <!-- Champ du mot de passe -->
                            <input id="password" name="password" onkeydown="afficher();" onkeyup="CheckMdp(this.value)" class="form-control password champ auto" type="password" placeholder="Mot de passe" value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["password"] : ''); ?>" />
                        </div>
                            
                        <!-- Div permettant d'afficher les éventuels erreurs lors de la création du mot de passe -->
                        <div id = "error">
                            <span id = "msg1"></span><br>
                            <span id = "msg2"></span><br>
                            <span id = "msg3"></span><br>
                            <span id = "msg4"></span><br>
                            <span id = "msg5"></span>
                        </div>

                        <!-- Champ pour confirmer le mot de passe -->
                        <input name="secondpassword" class="form-control password champ auto" type="password" placeholder="Confirmer le mot de passe" required value="<?php echo (isset($_SESSION['dataRegister']) ? $_SESSION['dataRegister']["secondpassword"] : ''); ?>"/>
                        
                        <div class="text-center padding">

                        <!-- Bouton pour s'inscrire -->
                        <input id="inscrire" type="submit" class="btn btn-secondary button" value="Inscrire le vendeur" disabled>
                        
                        <!-- Appele au script pour vérifier le mot de passe -->
                        <script src="../public/script/password.js"></script>
                    
                    </div>
                        <?php
                            // Récuperation et affichages des erreur si il en a 
                            if (isset($_GET['erreur'])){
                                switch ($_GET['erreur']) {
                                    case 0:
                                        echo    "<div style='color : #EF0009'; class='text-center'>
                                                    <p>Cette adresse mail est déjà utilisé</p>
                                                </div>";
                                        break;

                                    case 1:
                                        echo    "<div  style='color : #EF0009;' class='text-center'>
                                                    <p>Le mot de passe et la confirmation de mot de passe sont différents</p>
                                                </div>";
                                        break;

                                    case 2:
                                        echo    "<div  style='color : #EF0009;' class='text-center'>
                                                    <p>Au moins un des champs est vide</p>
                                                </div>";
                                    break;
                                    case 3:
                                        echo    "<div  style='color : #EF0009;' class='text-center'>
                                                    <p>Code postal incorrect</p>
                                                </div>";
                                        break;
                                    case 4:
                                        echo    "<div  style='color : #EF0009;' class='text-center'>
                                                    <p>Numero de siren invalide</p>
                                                </div>";
                                        break;
                                    case 5:
                                        echo    "<div  style='color : #EF0009;' class='text-center'>
                                                    <p>Numero NIC invalide</p>
                                                </div>";
                                        break;
                                    default:
                                        echo "<p>Huuum !</p>";
                                        break;
                                }
                            } 
                        ?>
                    </form>
                    
                </div>
            </div>
        </div>
    </main>
    <script>
        function auto() {
            var champs = document.getElementsByClassName("auto");
            for (var elem of champs) {
                var name = elem.name
                switch (name) {
                    case "nomVendeur":
                        elem.value = "Mathéo"
                        break;
                        
                    case "emailVendeur":
                        elem.value = "mat@gmail.com"
                        break;

                    case "descriptionVendeur":
                        elem.value = "Ce vendeur d'exellence vend d'exellents produits, bretons et de très bonne qualité. Kenavo !"
                        break;

                    case "voieVendeur":
                        elem.value = "rue édouard branly"
                        break;
                        
                    case "noVoieVendeur":
                        elem.value = 1
                        break;

                    case "postalVendeur":
                        elem.value = 22300
                        break;

                    case "villeVendeur":
                        elem.value = "Lannion"
                        break;
                        
                    case "codeTVA":
                        elem.value = "FR"
                        break;

                    case "cleTVA":
                        elem.value = 12
                        break;

                    case "siren":
                        elem.value = 545382994
                        break;

                    case "nic":
                        elem.value = 42069
                        break;

                    case "password":
                        elem.value = "Azerty123!"
                        break;
                    
                    case "secondpassword":
                        elem.value = "Azerty123!"
                        break;


                    default:
                        console.log("ça bug par la")
                        break;
                }
            }
            var btn = document.getElementById("inscrire")
            btn.disabled = false;
            btn.click();
        }
    </script>
</body>
</html>