<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php require('../public/head.php'); ?>
        <link rel="stylesheet" href="clients.css" type="text/css">
        <title>Liste de compte</title>
        <?php require($prefixe.'script/client.php'); ?>
        <?php require($prefixe.'script/searchAccount.php'); ?>
    </head>

    <body>

        <!--Header / Haut de page-->
        <header>
            <div class="d-flex justify-content-beetween align-items-center" style="background-color: #144FC1 ;">

                <div class="col-md-2" >
                    <a href="./index.php"><img id="logoAlizon" class="d-none d-lg-block " src="<?php echo $prefixe ?>images/logos/Logo_blanc.png" alt="logo Alizon" ></a>
                </div>

            </div>

            <div id="revenirAcceuil" class="justify-content-center" >
                <a id="lienAcceuil" href="./index.php">
                    <img id="flecheComeback" src="<?php echo $prefixe ?>images/icones/undo-outline.png" alt="revenir à l'acceuil"><p>Revenir à l'accueil</p>
                </a>
            </div>
        </header>

        <!--Corp de la page-->
        <main>
            <!--Feedback suppresion/désactivation comtpe-->
            <?php 
                if (isset($_GET['id'])){
                    desactiveCompte($_GET['id']);
                }            
            ?>

            <!--Form de recherche d'un utilisateur-->
            <div id="divForm">
                <form action="clients.php">
                    <div id="inputGroup" class="input-group ">
                        <input id="input" name="search" type="text" class="form-control" placeholder="Rechercher par nom"  >
                    </div>
                    <input type="submit" style="display: none">
                </form>
            </div>

            <!--Liste des profils / client-->
            <div>
                <h2>Les profils</h2>
                <br>
                <hr>
                <?php

                    if (isset($_GET['search']))
                    {
                        $termeRecherche = $_GET['search'];
                    }
                    else
                    {
                        $termeRecherche = "";
                    }
                    // Fait une boucle pour afficher tout les clients 
                    foreach(rechercherCompte($termeRecherche) as $client) { 


                        ?>
                        <!-- Affiche toutes les infos du compte utilisateur -->
                        <div class = "profil">
                            <?php echo '<a href="./detailCompteClient.php?id=' . $client['id'] . '">'; ?>
                                <?php echo '<img class = "rounded-circle" src="'.$prefixe.'images/photoProfil/'.$client['id'].'/'.getMainPicClient($client['id']).'" title="'.$client['pseudo'].'" alt="'.$client['pseudo'].' style = width">'; ?>
                                <?php echo '<p>Id : '.$client['id'].'</p>'; ?>
                                <span>|</span>
                                <?php echo '<p>Nom : '.$client['nom']. '</p>'; ?>
                                <span>|</span>
                                <?php echo '<p>Prénom : '.$client['prenom'].'</p>'; ?>
                                <span>|</span>
                                <?php echo '<p>Email : '.$client['email'].'</p>'; ?>
                                <span>|</span>
                                <?php
                                // Verifie si le compte est actif ou pas et affiche en conséquence
                                if ($client['active'] == 0){
                                    echo '<p>Statut d\'activité : Inactif</p>'; 
                                } else {
                                    echo '<p>Statut d\'activité : Actif</p>'; 
                                }
                                ?>
                            </a>
                        </div>
                        <hr>

                        <?php
                    }
                ?>

            </div>   
                
        </main>
    </body>
</html>