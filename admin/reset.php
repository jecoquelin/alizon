<?php
    // se script n'est utile que pendant le developpement
    // il fait plein de chose
    // il reset la bdd
    // il enleve toutes les images de produits
    // il importe un catalogue de produit
    // il enleve le cookie de panier

    session_start();
    include("prefixe.php");
    include($prefixe."script/catalogue.php");
    include($prefixe.'connectParams.php');
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage();
        die();
    }
    $fileContent = file_get_contents("BDD/script.sql");
    $dbh->exec($fileContent);

    $imageDir = scandir($prefixe."images/");
    if (!in_array("produits", $imageDir)){
        mkdir($prefixe."images/produits");
    }

    setcookie("alizonCookie", 1, 1, "/");

    //supression des images déjà là
    $dirImgProd = scandir($prefixe.'images/produits');
    $dirImgProd = array_diff($dirImgProd, array('.', '..'));
    foreach ($dirImgProd as $dir) {
        $imgProd = scandir($prefixe.'images/produits/'.$dir);
        $imgProd = array_diff($imgProd, array('.', '..'));
        foreach ($imgProd as $img) {
            unlink($prefixe.'images/produits/' . $dir . '/' . $img);
        }
        rmdir($prefixe.'images/produits/'.$dir);
    }

    copy($prefixe."produits/mon_super_csv.csv", $prefixe."fichierImportCatalogue.csv");
    $tabProduitsCSV = fiCSVersTab($prefixe."fichierImportCatalogue.csv");
    unlink($prefixe."fichierImportCatalogue.csv");

    if ($tabProduitsCSV === false){
        echo("Les données dans fichier CSV sont invalides");
    } else {
        $pbImport = false;

        foreach ($tabProduitsCSV as $tuple){
            if (ajouterUnProduitCSV($tuple,1) === false){
                $pbImport = true; 
            }
        }

        if ($pbImport === true){
            echo("Les données dans fichier CSV sont invalides");
        } else {
            echo("L'insertion des données du fichier CSV à fonctionné");
        }
    }
    
    $_SESSION['connect'] = false ;
    header("Location: ./index.php" );
?>