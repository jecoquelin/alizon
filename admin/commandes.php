<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<!--HEAD-->
<head>
    <?php require('../public/head.php'); ?>
    <link rel="stylesheet" href="clients.css" type="text/css">
    <title>Liste de commande</title>
    <?php 
        require($prefixe.'script/client.php'); 
        require($prefixe.'script/product.php');
    ?>
</head>

<!--BODY-->
<body>
    <!--HEADER-->
    <header>
        <!--barre + logo-->
        <div class="d-flex justify-content-beetween align-items-center" style="background-color: #144FC1 ;">
            <div class="col-md-2" >
                <a href="./index.php"><img id="logoAlizon" class="d-none d-lg-block " src="<?php echo $prefixe ?>images/logos/Logo_blanc.png" alt="logo Alizon" ></a>
            </div>
        </div>
        <!--barre avec flèche de retour au menu admin-->
        <div id="revenirAcceuil" class="justify-content-center" >
            <a id="lienAcceuil" href="./index.php">
                <img id="flecheComeback" src="<?php echo $prefixe ?>images/icones/undo-outline.png" alt="revenir à l'acceuil"><p>Revenir à l'accueil</p>
            </a>
        </div>
    </header>

    <!--MAIN-->
    <main>
        <?php
            // Lorsque l'admin / vendeur a essayer d'accèder à une commande inexistante
            if (isset($_GET['erreur']) and ($_GET['erreur'] == 1)){
                echo '<p class="feedbackERR">Erreur : Vous avez essayer d\'accéder à une commande qui n\'existe pas</p>' ;
            }
            // Script permettant de faire les recherches
            include($prefixe.'script/searchOrder.php');
        ?>
        <!--Barre de recherche-->
        <div id="divForm">
            <form action="commandes.php" method="GET">
                <div id="inputGroup" class="input-group ">
                    <input id="input" name="search" type="text" class="form-control" placeholder="Rechercher par nom de client..."  >
                </div>
                <input type="submit" style="display: none">
            </form>
        </div>
        
        <!--Liste des commandes-->
        <div>
            <h2>Les Commandes</h2>
            <br>
            <hr>
            <?php
                // Regarde si une recherche à été faite ou non et affiche les commandes spécifique à la recherche ou toutes.
                if(isset($_GET['search'])){
                    $commandes=rechercherCommande($_GET['search']);
                }else{
                    $commandes=rechercherCommande("");
                }
                majEtatCommandeSimu($commandes);
                // Affichage des commandes une à une
                foreach($commandes as $commande) {
                    ?>
                    <div class = "profil">
                            <?php echo '<a href="./commandeVendeur.php?id=' . $commande['id'] . '">'; ?>
                            <?php echo '<p>Id de la commande: '.$commande['id'].'</p>'; ?>
                            <span>|</span>
                            <?php echo '<p>Date de la commande : '.$commande['date_commande']. '</p>'; ?>
                            <span>|</span>
                            <?php echo '<p>Id du client : '.$commande['id_client'].'</p>'; ?>
                            <span>|</span>
                            <?php echo '<p>Nom du client : '.$commande['nomc'].'</p>'; ?>
                            <span>|</span>
                            <?php echo '<p>Prenom du client : '.$commande['prenomc'].'</p>'; ?>
                            <span>|</span>
                            <?php echo '<p>Etat de la commande : '.getStringEtatCommande($commande['etat_l']).'</p>'; ?>
                        </a>
                    </div>
                    <hr>
                    <?php
                }
            ?>          
        </div>

    </main>
</body>
</html>