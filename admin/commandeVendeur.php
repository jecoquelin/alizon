<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Mon profil</title> 
    <?php   
        require('../public/head.php'); 
        include($prefixe.'script/product.php');

        // Si l'id de la commande n'est pas entré ont retourne à la page précédente
        // Sinon on récupère l'id de la commande
        if (!isset($_GET['id'])){
            //header('Location: ./listeCommande.php?erreur=1');
        }
        else {
            $id = $_GET['id'] ;
        }
        
    ?>
    <link href="vendeurs.css" rel="stylesheet">
</head>
<body>
    <!--HEADER-->
    <header>
        <div class="d-flex justify-content-beetween align-items-center" style="background-color: #144FC1 ;">
            <div class="col-md-2" >
                <a href="./index.php"><img id="logoAlizon" class="d-none d-lg-block " src="<?php echo $prefixe ?>images/logos/Logo_blanc.png" alt="logo Alizon" ></a>
            </div>
        </div>
        <div id="revenirAcceuil" class="justify-content-center" >
            <a id="lienAcceuil" href="./index.php">
                <img id="flecheComeback" src="<?php echo $prefixe ?>images/icones/undo-outline.png" alt="revenir à l'acceuil"><p>Revenir à l'accueil</p>
            </a>
        </div>
    </header>

    <!--MAIN-->
    <main class="container mt-5">
        <section class="row col-lg-12 col-xl-10">
            <?php
                global $dbh ;

                // Récupère les informations lié à la commande
                $sth = $dbh->prepare('SELECT * from alizon._commande where id = ?');
                $sth -> execute(array($id));
                $commandes = $sth -> fetchAll();
                // Si aucune commandes n'est en cours, il affiche un message
                // Sinon on commance à affiché les informations lié à la commande
                if (count($commandes) == 0){
                    echo '<p class="feedbackERR">Aucune commande avec cette id n\'a été trouvé !</p>';
                }
                else {
                    // On garde uniquement le premier résultat, il est censé y avoir q'une seul commande par id
                    $commande = $commandes[0];
                    echo '<h3>Commande '.$commande['id'].'</h3>' ;
                    echo 
                    '<div>
                        <div class="row">
                            <p class="col-4">Date de commande : '.$commande['date_commande'].'</p>
                            <p class="col-4">Etat : '.getStringEtatCommande($commande['etat_l']).'</p>
                        </div>
                        <div class="row">
                            <p class="col-4">Id client : '.$commande['id_client'].'</p>
                            <p class="col-4">Nom : '.$commande['nomc'].'</p>
                            <p class="col-4">Prenom : '.$commande['prenomc'].'</p>
                        </div> 
                    </div>
                    <h5>Liste des articles</h5>';
                    // On récupère tous les produits de la commande
                    $produits = getProduitsCommande($id);
                    // Puis on affiche les produits un par un
                    foreach ($produits as $produit) {
                        $id = $produit['id_produit'];
                        $quantite = $produit['quantite'];
                        $prix_unitaire = getPrixUnitaireTTC($id);
                        $libelle = getLibelle($id) ;
            ?>
                    <article>
                        <div class="row justify-content-start produit">   
                            <div class="col-sm-5 col-lg-4">
                                <figure class="col-sm">
                                    <?php echo '<img class="rounded" src="'.$prefixe.'images/produits/'. $id . '/' . getMainPicProd($id) . '" alt="' . $libelle . '" title="' . $libelle . '"  height="150" width="140" > ' ;?>
                                </figure>
                            </div>
                            <div class="col-sm-7 col-lg-8 ">
                                <!--Mettre à jour cette partie, alignement responsve-->
                                <?php echo '<h3 class="text-left">' . $libelle . '</h3>'; ?>
                                <br>    
                                <div class="row">
                                    <p class="col-5"> <?php echo 'Quantité : ' . $quantite ; ?> </p>
                                    <p class="col-5 offset-auto"> <?php echo 'Prix unitaire TTC : '.$prix_unitaire.'€' ; ?> </p>
                                </div>
                                <?php echo '<p>Total article.s : '.$prix_unitaire*$quantite.'€</p>';?>
                            </div>  
                        </div>
                    </article>
            <?php
                    }
                    echo '<p id="prixTotalTTC" >Total commande TTC : '.$commande['prix_total_ttc'].'€</p>' ;        
                } 
            ?>
        </section>
    </main>
</body>
</html>




