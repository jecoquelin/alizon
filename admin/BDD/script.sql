drop schema if exists alizon cascade;
create schema alizon;
set schema 'alizon';


CREATE TABLE _categorie (
    libelle VARCHAR(20),
    taux_TVA FLOAT,
    CONSTRAINT PK_categorie PRIMARY KEY (libelle)
);

CREATE TABLE _compte_vendeur(
	id          SERIAL,
	nom         VARCHAR(20),
	mdp         VARCHAR(100),
	email       VARCHAR(255),
	numero_voie VARCHAR(200),
	nom_voie    VARCHAR(200),
	code_postal VARCHAR(20),
	ville       VARCHAR(200),
	active INT,
	code_TVA VARCHAR(3),
	cle_TVA  VARCHAR(2),
	siren  VARCHAR(9),
	nic VARCHAR(5),
	texte_presentation VARCHAR(255),
	note_vendeur  NUMERIC,
	CONSTRAINT PK_compte_vendeur PRIMARY KEY (id)
);

insert into _compte_vendeur(nom, mdp, email, nom_voie, code_postal, ville, active, code_TVA, cle_TVA, siren, nic, texte_presentation, note_vendeur ) 
	values ('HAMARD', '&aZ123', 'hamard@gmail.com', 'rue Édouard Branly', '22300', 'Lannion', 1, 'FR', '00', '123456789', '00013', 'Bonjour, je suis un vendeur sympatique et je veux que tout nos clients soit satisfaits des produits que nous vendons.', 4.5),
         ('Manu&Co', 'Azer132;', 'Manu&Co@gmail.com', '55 Rue du Faubourg Saint-Honoré', '75008', 'Paris', 1, 'FR', '01', '111111111', '80420', 'Mes Chères compatriotes, vous voulez du Français, de la French Tech, alors marchons !', 5.0);

CREATE TABLE _produit (
    id              SERIAL,
    id_vendeur      INT,
    libelle         VARCHAR(100),
    prix_ht         FLOAT,
    id_photos       INT,
    _description    VARCHAR(1000),
    stock           INT,
    seuil           INT,
    act             INT,
    duree_livraison INT,
    libelle_categorie VARCHAR(20),
    promotion INT DEFAULT 0,
    CONSTRAINT PK_produit PRIMARY KEY (id),
    CONSTRAINT produit_FK_categorie FOREIGN KEY (libelle_categorie) references _categorie(libelle),
    CONSTRAINT produit_FK_vendeur FOREIGN KEY (id_vendeur) references _compte_vendeur(id)
);



CREATE TABLE _remise(
  id serial,
  nom_remise VARCHAR(100),
  id_produit int,
  date_heure_debut DATE,
  date_heure_fin DATE,
  pourcentage_remise float,
  CONSTRAINT PK_promotion PRIMARY KEY (id),
  CONSTRAINT remise_FK_produit FOREIGN KEY (id_produit) REFERENCES _produit(id)
);

CREATE TABLE _panier(
  id_panier SERIAL,
  CONSTRAINT PK_panier PRIMARY KEY (id_panier)
);

CREATE TABLE _question(
  libelle     VARCHAR(50),
  CONSTRAINT PK_question PRIMARY KEY (libelle)
);

insert into _question(libelle)values
('Quel est votre plat préféré ?'),
('Quel est le nom de votre meilleur ami ?'),
('Quel est votre film préféré ?');

CREATE TABLE _compte_client(
  id          SERIAL,
  pseudo      VARCHAR(20),
  nom         VARCHAR(20),
  prenom      VARCHAR(20),
  mdp         VARCHAR(100),
  email       VARCHAR(255),
  numero_voie VARCHAR(200),
  nom_voie    VARCHAR(200),
  code_postal VARCHAR(20),
  ville       VARCHAR(200),
  batiment    VARCHAR(200),
  etage       VARCHAR(200),
  numero_porte VARCHAR(200),
  date_naissance DATE,
  id_panier INT,
  active INT,
  libelle_question1 VARCHAR(50),
  reponse_question1 VARCHAR(100),
  libelle_question2 VARCHAR(50),
  reponse_question2 VARCHAR(100),
  CONSTRAINT PK_compte_client PRIMARY KEY (id),
  CONSTRAINT client_FK_panier FOREIGN KEY (id_panier) REFERENCES _panier(id_panier),
  CONSTRAINT client_FK_question1 FOREIGN KEY (libelle_question1) REFERENCES _question(libelle),
  CONSTRAINT client_FK_question2 FOREIGN KEY (libelle_question2) REFERENCES _question(libelle)
);

CREATE TABLE _dans_panier(
  id_panier INT,
  id_produit INT,
  nombre INT,
  CONSTRAINT  PK_dans_panier PRIMARY KEY (id_panier,id_produit),
  CONSTRAINT dans_panier_FK_panier FOREIGN KEY (id_panier) REFERENCES _panier(id_panier),
  CONSTRAINT dans_panier_FK_produit FOREIGN KEY (id_produit) REFERENCES _produit(id)
);

/*
CREATE TABLE _wishlist(
  id_wishlist int,
  CONSTRAINT PK_wishlist PRIMARY KEY id_wishlist,
  CONSTRAINT wishlist_FK_client FOREIGN KEY id_wishlist REFERENCES _compte_client(id)
);

CREATE TABLE _dans_wishlist(
  id_wishlist INT,
  id_produit INT,
  nombre INT,
  CONSTRAINT  PK_dans_wishlist PRIMARY KEY (id_wishlist,id_produit),
  CONSTRAINT dans_wishlist_FK_panier FOREIGN KEY id_wishlist REFERENCES _wishlist(id),
  CONSTRAINT dans_wishlist_FK_produit FOREIGN KEY id_produit REFERENCES _produit(id)
);*/


CREATE TABLE _commande(
  id INT,
  prix_total_TTC FLOAT,
  date_expedition DATE,
  date_commande DATE,
  etat_l INT, /*0- En préparation, 1- Expédié, 2- En cours de livraison, 3- Livré*/
  numero_voie_l VARCHAR(200),
  nom_voie_l    VARCHAR(200),
  code_postal_l VARCHAR(20),
  ville_l       VARCHAR(200),
  batiment_l    VARCHAR(200),
  etage_l       VARCHAR(200),
  numero_porte_l VARCHAR(200),
  numero_voie_f VARCHAR(200),
  nom_voie_f    VARCHAR(200),
  code_postal_f VARCHAR(20),
  ville_f       VARCHAR(200),
  batiment_f    VARCHAR(200),
  etage_f       VARCHAR(200),
  numero_porte_f VARCHAR(200),
  id_client INT,
  nomC VARCHAR(20),
  prenomC VARCHAR(20),
  CONSTRAINT PK_commande PRIMARY KEY (id),
  CONSTRAINT commande_FK_client FOREIGN KEY (id_client) REFERENCES _compte_client(id)
);


/*
insert into _commande(date_commande,etat_l,id_client,nomc,prenomc)values(current_date,'Livré',1,'Tristan','Chardes');
*/

CREATE TABLE _est_commande(
  id_commande INT,
  id_produit INT,
  quantite INT,
  prix_TTC float,
  CONSTRAINT PK_est_commande PRIMARY KEY (id_commande,id_produit),
  CONSTRAINT est_commande_FK_produit FOREIGN KEY (id_produit) REFERENCES _produit(id),
  CONSTRAINT est_commande_FK_commande FOREIGN KEY (id_commande) REFERENCES _commande(id)
);

/*FONCTIONS*/

create or replace procedure panier_to_commande(idp INT,num_vo_l VARCHAR,nom_vo_l VARCHAR,postal_l VARCHAR,vil_l VARCHAR,bat_l VARCHAR,eta_l VARCHAR,nump_l VARCHAR,num_vo_f VARCHAR,nom_vo_f VARCHAR,postal_f VARCHAR,vil_f VARCHAR,bat_f VARCHAR,eta_f VARCHAR,nump_f VARCHAR,idcli INT,nom VARCHAR,prenom VARCHAR) as $$
declare 
r _dans_panier;
prix INT;
n_commande INT;
begin
  insert into _commande(date_expedition,date_commande,numero_voie_l,nom_voie_l,code_postal_l,ville_l,batiment_l,etage_l,numero_porte_l,numero_voie_f,nom_voie_f,code_postal_f,ville_f,batiment_f,etage_f,numero_porte_f,id_client,nomC,prenomC) values (current_date,current_date,num_vo_l,nom_vo_l,postal_l,vil_l,bat_l,eta_l,nump_l,num_vo_f,nom_vo_f,postal_f,vil_f,bat_f,eta_f,nump_f,idcli,nom,prenom);
  select count(*) from _commande into n_commande;
  FOR r in select * from _dans_panier where id_panier=idp
  LOOP
      select prix_ht*taux_tva from _produit where id=r.id_produit into prix;
      insert into _est_commande(id_commande,id_produit,quantite,prix_TTC)
      values(n_commande,r.id_produit,r.nombre,prix);
      delete from _dans_panier where id_panier=idp;
      update _produit set stock = stock - r.nombre where id = r.id_produit ;
  END LOOP;
  select sum(prix_TTC) from _est_commande into prix;
  update _commande set prix_total_ttc=prix where id=n_commande;
end;
$$ language plpgsql;


/* INSERTION */

insert into _categorie(libelle, taux_tva)values
  ('Jouet', 1.2),
  ('Peluche', 1.2),
  ('Jeu video', 1.2),
  ('Histoire', 1.2),
  ('Film', 1.2),
  ('Livre', 1.2),
  ('Bricolage', 1.2),
  ('Jardin', 1.2),
  ('Cuisine', 1.2),
  ('Technologie', 1.2),
  ('Textile', 1.2),
  ('Décoration', 1.2);


-- insert into _Produit(ref_fabricant, libelle, prix_ht,id_photos,_description,taux_tva,stock,seuil,duree_livraison,libelle_categorie) values
--   ('Eglise','Peluche de Jesus 32cm','24.99','1','Une peluche de Jesus parfaite pour tout vos enfant en voie d apprentissage de la verite vraie', '1.2','150','50','2','Peluche'),
--   ('Eglise','Cyber-Jesus la revanche','69.99','2','Un jeu d action parfait pour tout vos adolescents en voie d apprentissage de la verite vraie', '1.2','200','100','2','Jeu video'),
--   ('Eglise','Cyber-Jesus la revanche le livre','12.99','3','Une adaptation de jeu video pour tout vos adolescents en voie d apprentissage de la verite vraie', '1.2','300','50','2','Livre');
  


/*APPELS PROCEDURE*/
-- call panier_to_commande(7399156,'5'::VARCHAR,'test'::VARCHAR,'22300'::VARCHAR,'Lannion'::VARCHAR,'3'::VARCHAR,'1'::VARCHAR,'21'::VARCHAR,'rue du test'::VARCHAR,'22300'::VARCHAR,'Lannion'::VARCHAR,'3'::VARCHAR,'1'::VARCHAR,'21'::VARCHAR,2,'a'::VARCHAR,'a'::VARCHAR);
-- call panier_to_commande(2,'2 rue des peupliers','LANNION','22300','Michele','Vabien',2);


insert into _compte_client(pseudo, nom, prenom,mdp,email, numero_voie, nom_voie,code_postal,ville, batiment, etage, numero_porte, date_naissance,active) values
  ('nyan_con','desbonnets','nicolas','1234', 'nicolas@gmail.de','1','rue branly','22300','Lannion','c','3','429',current_date,1);
