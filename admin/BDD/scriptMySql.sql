drop schema if exists alizon cascade;
create schema alizon;
set schema 'alizon';


CREATE TABLE _categorie (
    libelle VARCHAR(20),
    CONSTRAINT PK_categorie PRIMARY KEY (libelle)
);

CREATE TABLE  _arborescence_cat(
  libelle_categorie VARCHAR(20),
  libelle_sous_categorie VARCHAR(20),
  CONSTRAINT PK_arborescence_cat PRIMARY KEY (libelle_categorie,libelle_sous_categorie),
  CONSTRAINT arborescence_FK_categorie FOREIGN KEY (libelle_categorie) references _categorie(libelle),
  CONSTRAINT arborescence_FK2_categorie FOREIGN KEY (libelle_sous_categorie) references _categorie(libelle)
);

CREATE TABLE _produit (
    id              MEDIUMINT AUTO_INCREMENT,
    ref_fabricant   VARCHAR(15),
    libelle         VARCHAR(100),
    prix_ht         FLOAT,
    id_photos       VARCHAR(20),
    _description    VARCHAR(1000),
    taux_TVA        FLOAT,
    stock           INT,
    seuil           INT,
    duree_livraison INT,
    libelle_categorie VARCHAR(20),
    CONSTRAINT PK_produit PRIMARY KEY (id),
    CONSTRAINT produit_FK_categorie FOREIGN KEY (libelle_categorie) references _categorie(libelle)
);


/*
CREATE TABLE _promotion(
  id serial,
  date_heure_debut DATE,
  date_heure_fin DATE,
  pourcentage_remise float,
  id_image VARCHAR(20),
  _description VARCHAR(1000),
  CONSTRAINT PK_promotion PRIMARY KEY (id)
);

CREATE TABLE _en_promo(
  id_produit INT,
  id_promo INT,
  CONSTRAINT PK_en_promo PRIMARY KEY (id_produit),
  CONSTRAINT en_promo_FK_promotion FOREIGN KEY (id_promo) REFERENCES _promotion(id),
  CONSTRAINT en_promo_FK_produit FOREIGN KEY (id_produit) REFERENCES _produit(id)
);*/

CREATE TABLE _panier(
  id_panier AUTO_INCREMENT,
  CONSTRAINT PK_panier PRIMARY KEY (id_panier)
);

CREATE TABLE _compte_client(
  id MEDIUMINT AUTO_INCREMENT,
  pseudo          VARCHAR(20),
  nom             VARCHAR(20),
  prenom          VARCHAR(20),
  mdp             VARCHAR(100),
  email           VARCHAR(255),
  numero_voie     VARCHAR(200),
  nom_voie        VARCHAR(200),
  code_postal     VARCHAR(20),
  ville           VARCHAR(200),
  batiment        VARCHAR(200),
  etage           VARCHAR(200),
  numero_porte    VARCHAR(200),
  date_naissance  DATE,
  id_panier       INT,
  CONSTRAINT PK_compte_client PRIMARY KEY (id),
  CONSTRAINT client_FK_panier FOREIGN KEY (id_panier) REFERENCES _panier(id_panier)
);



CREATE TABLE _dans_panier(
  id_panier INT,
  id_produit INT,
  nombre INT,
  /*lastmodif DATE,    var pour suppression des paniers apres 1 jour*/
  CONSTRAINT  PK_dans_panier PRIMARY KEY (id_panier,id_produit),
  CONSTRAINT dans_panier_FK_panier FOREIGN KEY (id_panier) REFERENCES _panier(id_panier),
  CONSTRAINT dans_panier_FK_produit FOREIGN KEY (id_produit) REFERENCES _produit(id)
);


/*
CREATE TABLE _wishlist(
  id_wishlist int,
  CONSTRAINT PK_wishlist PRIMARY KEY id_wishlist,
  CONSTRAINT wishlist_FK_client FOREIGN KEY id_wishlist REFERENCES _compte_client(id)
);

CREATE TABLE _dans_wishlist(
  id_wishlist INT,
  id_produit INT,
  nombre INT,
  CONSTRAINT  PK_dans_wishlist PRIMARY KEY (id_wishlist,id_produit),
  CONSTRAINT dans_wishlist_FK_panier FOREIGN KEY id_wishlist REFERENCES _wishlist(id),
  CONSTRAINT dans_wishlist_FK_produit FOREIGN KEY id_produit REFERENCES _produit(id)
);*/


CREATE TABLE _commande(
  id MEDIUMINT AUTO_INCREMENT,
  prix_total_TTC FLOAT,
  date_expedition DATE,
  date_commande DATE,
  adresseC VARCHAR(200),
  villeC VARCHAR(20),
  code_postal VARCHAR(20),
  id_client INT,
  nomC VARCHAR(20),
  prenomC VARCHAR(20),
  CONSTRAINT PK_commande PRIMARY KEY (id),
  CONSTRAINT commande_FK_client FOREIGN KEY (id_client) REFERENCES _compte_client(id)
);



CREATE TABLE _est_commande(
  id_commande INT,
  id_produit INT,
  quantite INT,
  prix_TTC float,
  CONSTRAINT PK_est_commande PRIMARY KEY (id_commande,id_produit),
  CONSTRAINT est_commande_FK_produit FOREIGN KEY (id_produit) REFERENCES _produit(id),
  CONSTRAINT est_commande_FK_commande FOREIGN KEY (id_commande) REFERENCES _commande(id)
);

/*FONCTIONS*/

create or replace procedure panier_to_commande(idp INT,adr VARCHAR,ville VARCHAR,poste VARCHAR,nom VARCHAR,prenom VARCHAR,idcli INT) as $$
declare 
r _dans_panier;
prix INT;
n_commande INT;
begin
  insert into _commande(date_expedition,date_commande,adresseC,villeC,code_postal,id_client,nomC,prenomC) values (current_date,current_date,adr,ville,poste,idcli,nom,prenom);
  select count(*) from _commande into n_commande;
  FOR r in select * from _dans_panier where id_panier=idp
  LOOP
      select prix_ht*taux_tva from _produit where id=r.id_produit into prix;
      insert into _est_commande(id_commande,id_produit,quantite,prix_TTC)
      values(n_commande,r.id_produit,r.nombre,prix);
      delete from _dans_panier where id_panier=idp;
      update _produit set stock = stock - r.nombre where id = r.id_produit ;
  END LOOP;
  select sum(prix_TTC) from _est_commande into prix;
  update _commande set prix_total_ttc=prix where id=n_commande;
end;
$$ language plpgsql;


/* INSERTION */

insert into _categorie(libelle)values
  ('Jouet'),
  ('Peluche'),
  ('Jeu video'),
  ('Histoire'),
  ('Film'),
  ('Livre'),
  ('Bricolage'),
  ('Jardin'),
  ('Cuisine'),
  ('Technologie');

insert into _arborescence_cat(libelle_categorie,libelle_sous_categorie)values
  ('Jouet','Peluche'),
  ('Jouet','Jeu video'),
  ('Histoire','Film'),
  ('Histoire','Livre');

-- insert into _Produit(ref_fabricant, libelle, prix_ht,id_photos,_description,taux_tva,stock,seuil,duree_livraison,libelle_categorie) values
--   ('Eglise','Peluche de Jesus 32cm','24.99','1','Une peluche de Jesus parfaite pour tout vos enfant en voie d apprentissage de la verite vraie', '1.2','150','50','2','Peluche'),
--   ('Eglise','Cyber-Jesus la revanche','69.99','2','Un jeu d action parfait pour tout vos adolescents en voie d apprentissage de la verite vraie', '1.2','200','100','2','Jeu video'),
--   ('Eglise','Cyber-Jesus la revanche le livre','12.99','3','Une adaptation de jeu video pour tout vos adolescents en voie d apprentissage de la verite vraie', '1.2','300','50','2','Livre');
  


/*APPELS PROCEDURE*/
-- call panier_to_commande(1,'2 rue des gens','LANNION','22300','Felix','Mercier',1);
-- call panier_to_commande(2,'2 rue des peupliers','LANNION','22300','Michele','Vabien',2);


--EVENT POUR SUPPRIMER LES PANIERS QUI DATENT DE +1JOUR
/*
DROP EVENT IF EXISTS e_supprimPanier;
CREATE EVENT e_supprimPanier
ON SCHEDULE EVERY '1' HOUR
-- STARTS '2023-01-6 00:00:00' -- tous les minuits
DO
    delete from _dans_panier WHERE DATE_SUB(CURDATE(),INTERVAL 1 DAY) <= last_modif;
END

SHOW EVENTS;

*/