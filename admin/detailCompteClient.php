<?php
    // Récupération de l'id du client
    $id=$_GET['id'];
?>

<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <!--HEAD-->
<head>
    <?php require('../public/head.php'); ?>
    <?php require($prefixe.'script/client.php'); ?>
    <link rel="stylesheet" href="detailCompteClient.css" type="text/css">
    <title>Liste de compte</title> 
</head>

<body>
    <!--HEADER-->
    <header>
        <div class="d-flex justify-content-beetween align-items-center" style="background-color: #144FC1 ;">
            <div class="col-md-2" >
                <a href="./index.php"><img id="logoAlizon" class="d-none d-lg-block " src="<?php echo $prefixe ?>images/logos/Logo_blanc.png" alt="logo Alizon" ></a>
            </div>       
        </div>
        <div id="revenirAcceuil" class="justify-content-center" >
            <a id="lienAcceuil" href="./index.php">
                <img id="flecheComeback" src="<?php echo $prefixe ?>images/icones/undo-outline.png" alt="revenir à l'acceuil"><p>Revenir à la liste des clients</p>
            </a>
        </div>
    </header>

    <!--MAIN-->
    <main>
    <?php 
                    if (isset($_POST["formInfoPerso"])){
                        modifierInfoPersonnelles($_POST['pseudo'], $_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['date_naissance'], $_POST['numero_voie'], $_POST['nom_voie'], $_POST['ville'], $_POST['code_postal'], $_POST['batiment'], $_POST['etage'], $_POST['numero_porte'], $id) ;
                    } 
    ?>


    <?php $client = getClientById($id); ?>

        <section>

            <!-- Bloc reservé pour afficher la photo de profil -->
            <div class = "profil">
            <?php echo '<img class = "rounded-circle" src="'.$prefixe.'images/photoProfil/'.$client['id'].'/'.getMainPicClient($client['id']).'" title="'.$client['pseudo'].'" alt="'.$client['pseudo'].' ">'; ?>
            <?php echo '<p>Utilisateur,'.$client['prenom'].' '.$client['nom']. '</p>'; ?>
            </div>

            <!-- Bloc reservé pour afficher toutes les infos du profils -->
            <article id="affichInfoPerso">
                <ul>
                    <?php echo '<li>Pseudo : '.$client['pseudo'].'</li>'; ?>
                    <?php echo '<li>Nom : '.$client['nom'].'</li>'; ?>
                    <?php echo '<li>Prénom : '.$client['prenom'].'</li>'; ?>
                    <?php echo '<li>Date de naissance : '.$client['date_naissance'].'</li>'; ?>
                    <?php echo '<li>Adresse mail : '.$client['email'].'</li>'; ?>
                    <?php echo '<li>Adresse de livraison : 
                                    <ul>
                                         <li>Numero de voie : '.$client['numero_voie'].'</li>
                                         <li>Nom de voie : '.$client['nom_voie'].'</li>
                                         <li>Ville : '.$client['ville'].'</li>
                                         <li>Code postal : '.$client['code_postal'].'</li>
                                         <li>Batiment : '.$client['batiment'].'</li>
                                         <li>Etage : '.$client['etage'].'</li>
                                         <li>Numero_porte : '.$client['numero_porte'].'</li>
                                    </ul>'; 
                    ?>
                </ul>
                <a class=" bouton btn-secondary" role="button" onclick="modifierInfoPerso()">Modifier</a>
            </article>

            <!-- Form pour modifier les informations du profil -->
            <form id="formInfoPerso" action="./detailCompteClient.php?id=<?php echo $id ?>" method="POST">
                            <ul>
                                <?php echo '<li><label>Pseudo :             </label><input class="info_prerempli " type="text"  name="pseudo"           value="'.$client['pseudo'].'">      </li>'; ?>
                                <?php echo '<li><label>Nom :                </label><input class="info_prerempli " type="text"  name="nom"              value="'.$client['nom'].'">         </li>'; ?>
                                <?php echo '<li><label>Prénom :             </label><input class="info_prerempli " type="text"  name="prenom"           value="'.$client['prenom'].'">      </li>'; ?>
                                <?php echo '<li><label>Date de naissance :  </label><input class="info_prerempli " type="date"  name="date_naissance"   value="'.$client['date_naissance'].'"></li>'; ?>
                                <?php echo '<li><label>Adresse mail :       </label><input class="info_prerempli " type="email"  name="email"            value="'.$client['email'].'" required>       </li>'; ?>
                                <?php echo '<li>Adresse de livraison : 
                                                <ul>
                                                     <li><label>Numero de voie :</label><input class="info_prerempli " type="text"  name="numero_voie"  value="'.$client['numero_voie'].'"> </li>
                                                     <li><label>Nom de voie :   </label><input class="info_prerempli " type="text"  name="nom_voie"     value="'.$client['nom_voie'].'">    </li>
                                                     <li><label>Ville :         </label><input class="info_prerempli " type="text"  name="ville"        value="'.$client['ville'].'">       </li>
                                                     <li><label>Code postal :   </label><input class="info_prerempli " type="text"  name="code_postal"  value="'.$client['code_postal'].'"> </li>
                                                     <li><label>Batiment :      </label><input class="info_prerempli " type="text"  name="batiment"     value="'.$client['batiment'].'">    </li>
                                                     <li><label>Etage :         </label><input class="info_prerempli " type="text"  name="etage"        value="'.$client['etage'].'">       </li>
                                                     <li><label>Numero_porte :  </label><input class="info_prerempli " type="text"  name="numero_porte" value="'.$client['numero_porte'].'"></li>
                                                </ul>'; 
                                ?>
                            </ul>
                            <a class="bouton btn-secondary" role="button" onclick="afficherInfoPerso()">Annuler</a>
                            <input class = "bouton" type="submit" value="Valider" name="formInfoPerso">
                        </form>
            <script src="<?php echo $prefixe ?>script/profil.js"></script>

            <!--Permet de supprimer un compte : On retourne un id sur la page liste compte-->
            <article id="supressionCompte">
                <h3>Suppression de compte</h3>
                <?php echo '<a class="btn-danger" href="./clients.php?id='.$id.'" role="button">Supprimer</a>';?>

            </article>
            <br>
        </section>

        
    </main>
</body>
</html>