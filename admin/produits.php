<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <?php require('../public/head.php'); ?>
        <link rel="stylesheet" href="produits.css" type="text/css">
        <title>Liste des Produits</title>
        <?php include($prefixe.'script/product.php'); ?>
        <?php require($prefixe.'script/client.php'); ?>
        <?php require($prefixe.'script/searchProduct.php'); ?>
    </head>

    <body>

        <!--Header / Haut de page-->
        <header>
            <div class="d-flex justify-content-beetween align-items-center" style="background-color: #144FC1 ;">

                <div class="col-md-2" >
                    <a href="./index.php"><img id="logoAlizon" class="d-none d-lg-block " src="<?php echo $prefixe ?>images/logos/Logo_blanc.png" alt="logo Alizon" ></a>
                </div>

            </div>

            <div id="revenirAcceuil" class="justify-content-center" >
                <a id="lienAcceuil" href="./index.php">
                    <img id="flecheComeback" src="<?php echo $prefixe ?>images/icones/undo-outline.png" alt="revenir à l'acceuil"><p>Revenir à l'accueil</p>
                </a>
            </div>
        </header>

        <!--Corp de la page-->
        <main class="col-md-10 offset-1">
        <div >
            <!--Form de recherche d'un produit-->
            <div id="divForm">
                <form class="search" action="produits.php">
                    <div id="inputGroup" class="input-group ">
                        <input id="input" name="search" type="text" class="form-control" placeholder="Rechercher par nom"  >
                    </div>
                    <input type="submit" style="display: none">
                </form>
            </div>

            <!--Liste des produits-->
            <div>
                <h2>Les produits</h2>
                <br>
                <?php
                //gestiondes erreurs
                        if (isset($_GET['erreur'])){
                            switch ($_GET['erreur']) {                    
                                case 1:
                                    echo    "<div style='color : #EF0009'; class='text-center'>
                                                <p>La date de début de la remise n'est pas valide</p>
                                            </div>";
                                    break;
                                
                                case 2:
                                    echo    "<div style='color : #EF0009'; class='text-center'>
                                                <p>La date de fin de la remise n'est pas valide</p>
                                            </div>";
                                    break;
                                case 3:
                                    echo    "<div style='color : #EF0009'; class='text-center'>
                                                <p>veuillez remplir les champs obligatoires*</p>
                                            </div>";
                                    break;
                                case 4:
                                    echo    "<div style='color : #FF8605'; class='text-center'>
                                                <p>La modification de la remise à été effectué</p>
                                            </div>";
                                    break;
                                case 5:
                                    echo    "<div style='color : #FF8605'; class='text-center'>
                                                <p>L'ajout de la remise à été effectué</p>
                                            </div>";
                                    break;
                                case 6:
                                    echo    "<div style='color : #FF8605'; class='text-center'>
                                                <p>La suppression de la remise à été effectué</p>
                                            </div>";
                                    break;
                                default:
                                    break;
                            }
                        }
                    ?>
                <p class="textCheck">En promotion</p>
                <hr>
                <!-- form pour ajouter une promo ou une remise -->
                <form action="<?php echo $prefixe ?>script/submitPromo.php" method="post">
                    <?php
                    //gestion de la recherche
                    if (isset($_GET['search']))
                    {
                        $termeRecherche = $_GET['search'];
                    }
                    else
                    {
                        $termeRecherche = "";
                    }
                    $enPromo=1;
                    // recuperation de tout les produits
                    foreach(rechercherProduits($termeRecherche) as $produit) { 
                        ?>
                        <div class = "profil">
                            <?php echo '<a href="'.$prefixe.'detailProduit.php?id=' . $produit['id'] . '">'; ?>
                                <?php echo '<img src="'.$prefixe.'images/produits/' . $produit['id_photos'] . '/' . getMainPicProd($produit['id']) . '" title="' . $produit['libelle'] . '" alt="' . $produit['libelle'] . '" class="rounded produit_image">';?>
                                <?php echo '<p>Id : '.$produit['id'].'</p>'; ?>
                                <?php echo '<p>Libelle : '.$produit['libelle']. '</p>'; ?>
                            </a>
                            <input name="num[<?php echo $enPromo;?>]" type="hidden" value="<?php echo$produit['id']; ?>">
                            <?php if($produit['promotion']!=1){ ?>
                                <label class="switch">
                                    <input name="Prod[<?php echo $enPromo?>]" type="hidden" value="off">
                                    <input name="Prod[<?php echo $enPromo?>]" type="checkbox">
                                    <span class="slider round"></span>
                                </label>
                            <?php } else{ ?>
                                <label class="switch">
                                    <input name="Prod[<?php echo $enPromo?>]" type="hidden" value="off">
                                    <input name="Prod[<?php echo $enPromo?>]"type="checkbox" checked>
                                    <span class="slider round" ></span>
                                </label>
                            <?php } ?>
                        </div>
                        <?php
                            $id_produit=$produit['id'];
                            $sql = "SELECT * FROM alizon._remise WHERE id_produit=?";
                            $result = $dbh->prepare($sql);
                            $result->execute(array($id_produit,));
                            $laRemise=$result->fetchAll();
                            
                            if (isset($laRemise[0])) {
                                $nomRemise=$laRemise[0]["nom_remise"];
                                $dateDebut=$laRemise[0]["date_heure_debut"];
                                $dateFin=$laRemise[0]["date_heure_fin"];
                                $tauxRemise=$laRemise[0]["pourcentage_remise"];
                            }
                            // si aucune remise n'est créé pour un produit
                            if($result->rowCount()==0){
                                //on affiche le form de création de remise
                                echo "<details>
                                <summary>Ajouter une remise</summary>
                                <div class=\"remise\">
                                    <div class=\"sousRemise\">
                                        <label>Nom de la remise* :</label>
                                        <input class=\"inputRemise\" name=\"nomRemise$enPromo\"   type=\"text\"  placeholder=\"Ex: Remise de Noël\">
                                    </div>
                                    <div class=\"sousRemise\">
                                        <label>Date de début* :</label>
                                        <input class=\"inputRemise\" name=\"dateDebut$enPromo\" type=\"date\">
                                    </div>
                                    <div class=\"sousRemise\">
                                        <label>Date de fin* :</label>
                                        <input class=\"inputRemise\" name=\"dateFin$enPromo\" type=\"date\">
                                    </div>
                                    <div class=\"sousRemise\">
                                        <label>Taux de la remise (%)* :</label>
                                        <input class=\"inputRemise\" name=\"tauxRemise$enPromo\"   type=\"number\"  placeholder=\"taux en %\" min=\"1\">
                                    </div>
                                </div>
                                <input type=\"submit\" name=\"create[$enPromo]\" class=\"btn btn-secondary button\" value=\"Valider\">
                            </details>";
                            }else{
                                //sinon celui de modification
                                echo "<details>
                                <summary>Modifier une remise</summary>
                                <div class=\"remise\">
                                    <div class=\"sousRemise\">
                                        <label>Nom de la remise* :</label>
                                        <input class=\"inputRemise\" name=\"nomRemise$enPromo\"   type=\"text\"  placeholder=\"Ex: Remise de Noël\" value=\"$nomRemise\">
                                    </div>
                                    <div class=\"sousRemise\">
                                        <label>Date de début* :</label>
                                        <input class=\"inputRemise\" name=\"dateDebut$enPromo\" type=\"date\" value=\"$dateDebut\">
                                    </div>
                                    <div class=\"sousRemise\">
                                        <label>Date de fin* :</label>
                                        <input class=\"inputRemise\" name=\"dateFin$enPromo\" type=\"date\" value=\"$dateFin\">
                                    </div>
                                    <div class=\"sousRemise\">
                                        <label>Taux de la remise (%)* :</label>
                                        <input class=\"inputRemise\" name=\"tauxRemise$enPromo\"   type=\"number\"  placeholder=\"taux en %\" min=\"1\" value=\"$tauxRemise\">
                                    </div>
                                </div>
                                <input type=\"submit\" name=\"update[$enPromo]\" class=\"btn btn-secondary button\" value=\"Valider\">
                                <input type=\"submit\" name=\"delete[$enPromo]\" class=\"btn btn-secondary button\" value=\"Supprimer\">
                            </details>";
                            }
                        ?>
                        <hr>
                    <?php
                    //compteur de produits (sert à se reperer dans le script)
                        $enPromo=$enPromo+1;
                        }
                    ?>
                    <!-- input pour la promo -->
                    <input type="submit" name="submitRemise" class="btn btn-secondary button fix" value="Valider">
             </form>
            </div>   
            <div>
        </main>
    </body>
</html>
