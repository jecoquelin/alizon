<?php
    // Récupération de l'id du client
    $idVendeur=$_GET['id'];
?>

<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<!--HEAD-->
<head>
    <?php require('./../public/head.php'); ?>
    <?php require($prefixe.'/script/seller.php'); ?>
    <link rel="stylesheet" href="detailCompteVendeur.css" type="text/css">
    <title>Liste de compte</title> 
</head>

<body>
    
    <!--HEADER-->
    <header>
        <div class="d-flex justify-content-beetween align-items-center" style="background-color: #144FC1 ;">
            <div class="col-md-2" >
                <a href="./index.php"><img id="logoAlizon" class="d-none d-lg-block " src="<?php echo $prefixe ?>images/logos/Logo_blanc.png" alt="logo Alizon" ></a>
            </div>       
        </div>
        <div id="revenirAcceuil" class="justify-content-center" >
            <a id="lienAcceuil" href="./index.php">
                <img id="flecheComeback" src="<?php echo $prefixe ?>images/icones/undo-outline.png" alt="revenir à l'acceuil"><p>Revenir à la liste des vendeurs</p>
            </a>
        </div>
    </header>

    <!--MAIN-->
    <main>
    <?php
        // Modification des informations du compte vendeur si le formulaire a été envoyé
        if (isset($_POST["formInfoPerso"])){
            modifierInfoVendeur($_POST['nom'], $_POST['email'], $_POST['numero_voie'], $_POST['nom_voie'], $_POST['ville'], $_POST['code_postal'], $_POST['code_TVA'],$_POST['cle_TVA'],$_POST['siren'],$_POST['nic'],$_POST['texte_presentation'], $idVendeur ) ;
        }
    ?>

    <?php 
        // Récupéartion des informations lié à l'id mis dans l'URL
        // Si aucune information y est lié, on redirige vers la liste des vendeurs avec une erreur 1 et son id.
        $profil = getVendeurById($idVendeur);
        if ($profil == null){
            header("Location: ./vendeurs.php?id=$idVendeur&err=1");
        }
    ?>

        <section>

            <!-- Bloc reservé pour afficher la photo de profil -->
            <div class = "profil">
            <?php echo '<img class = "rounded-circle" src="'.$prefixe.'images/vendeur/photoProfil/'.$profil['id'].'/'.getMainPicVendeur($profil['id']).'" title="'.$profil['nom'].'" alt="'.$profil['nom'].' ">'; ?>
            <?php echo '<p>'.$profil['nom'].'</p>'; ?>
            </div>

            <!-- Bloc reservé pour afficher toutes les infos du profils -->
            <article id="affichInfoPerso">
                <ul>
            <?php   echo '<li>Raison sociale : '.$profil['nom'].'</li>';
                    echo '<li>Adresse mail : '.$profil['email'].'</li>';
                    echo '<li><strong>Adresse de livraison </strong>
                                    <ul>
                                         <li>Numero de voie : '.$profil['numero_voie'].'</li>
                                         <li>Nom de voie : '.$profil['nom_voie'].'</li>
                                         <li>Ville : '.$profil['ville'].'</li>
                                         <li>Code postal : '.$profil['code_postal'].'</li>
                                    </ul>
                                </li><br/>';
                    echo '<li>TVA intracommunautaire : '.$profil['code_tva'].' '.$profil['cle_tva'].' '.$profil['siren'].' '.$profil['nic'].'</li>';
                    echo '<li>Description vendeur : '.$profil['texte_presentation'].'</li>';
                    echo '<li>Note : '.$profil['note_vendeur'].'</li>'; ?>
                </ul>
                <a class=" bouton btn-secondary" role="button" onclick="modifierInfoPerso()">Modifier</a>
            </article>

            <!-- Form pour modifier les informations du profil -->
            <form id="formInfoPerso" action="./detailCompteVendeur.php?id=<?php echo $idVendeur ?>" method="POST">
                <ul>
                    <?php echo '<li><label>Nom :                </label><input type="text"  name="nom"               value="'.$profil['nom'].'">         </li>'; ?>
                    <?php echo '<li><label>Adresse mail :       </label><input type="email"  name="email"            value="'.$profil['email'].'" required>       </li>'; ?>
                        <?php echo '
                            <li><br/><strong>Adresse de livraison </strong>
                                <ul>
                                    <li><label>Numero de voie :</label><input type="text"  name="numero_voie"   value="'.$profil['numero_voie'].'"> </li>
                                    <li><label>Nom de voie :   </label><input type="text"  name="nom_voie"      value="'.$profil['nom_voie'].'">    </li>
                                    <li><label>Ville :         </label><input type="text"  name="ville"         value="'.$profil['ville'].'">       </li>
                                    <li><label>Code postal :   </label><input type="text"  name="code_postal"   value="'.$profil['code_postal'].'"> </li>
                                </ul>
                            </li><br/>';
                        ?>

                    <?php echo '<li><label>TVA intracommunautaire :  </label><input type="text"    name="code_TVA"    value="'.$profil['code_tva'].'" placeholder="Code" maxlength="3"  required> 
                                                                            <input type="number"  name="cle_TVA"    value="'.$profil['cle_tva'].'"  placeholder="Clé" min="0" max="99" required> 
                                                                            <input type="number"  name="siren"  value="'.$profil['siren'].'"    placeholder="siret" min="0" max="999999999" required >       
                                                                            <input type="number"  name="nic"    value="'.$profil['nic'].'"      placeholder="Nic" min="0" max="99999" required >       ';  ?> 
                   
                    <?php echo '<li><label>Description vendeur :            </label><input type="text"  name="texte_presentation"              value="'.$profil['texte_presentation'].'"       required>       '; ?>
                </ul>
                <a class="btn-secondary"    role="button" onclick="afficherInfoPerso()">Annuler</a>
                <input class="btn-secondary"  type="submit"        value="Valider" name="formInfoPerso">
            </form>
            <script src="<?php echo $prefixe ?>script/profil.js"></script>

            <!--Permet de supprimer un compte : On retourne un id sur la page liste compte ainsi qu'un disable à true-->
            <article id="supressionCompte">
                <h3>Suppression de compte</h3>
                <?php echo '<a class="btn-danger" href="./vendeurs.php?id='.$idVendeur.'&disable=1" role="button">Supprimer</a>';?>

            </article>
            <br>
        </section>

        
    </main>
</body>
</html>