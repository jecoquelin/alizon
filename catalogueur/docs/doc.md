# Catalogueur de produits
## Prérequis
- Tout le dossier catalogueur de ce dépot git (`git clone`).
- Un compte vendeur ou un accès administrateur au site [Alizon](http://alizon.meatman.fr).
## Génération du catalogue au format Json
### Accès à la page de gestion du catalogue :
- ### Administrateur
    La page de sélection des produits à cataloguer est disponible sur la page d'accueil de l'administrateur (Gérer le catalogue). Un administrateur à accès à tous les produits présents sur le site.
- ### Vendeur
    La page de sélection des produits à cataloguer est disponible sur la page d'accueil des vendeurs (Gérer le catalogue). Il faut possèder un compte vendeur pour y accèder. Un vendeur n'a accès qu'à ses propre produits.
### Selection des produits
Pour générer le catalogue au format Json il faut sélectionner des produits sur la page de gestion du catalogue à l'aide des toggle switch puis cliquer sur valider.
Un bouton télécharger apparait ensuite en haut de cette page pour télécharger le catalogue au format Json.

## Génération du catalogue au format PDF
*Pour générer le catalogue au format PDF il faut d'abord avoir le catalogue au format Json.*

Pour commencer il faut ouvrir un terminal et se rendre dans le dossier `catalogueur/script/`. Ensuite il faut taper la commande `./go.sh` (`./goIUT.sh <login>` si vous êtes sur un ordinateur de l'IUT (il faut mettre votre login en paramètre)).
Le script met en place les fichiers nécessaires à lagénration du catalogue puis il se met en attente du fichier Json. Pour continuer la génération il faut déplacer le catalogue au format Json (`catalogue.json`) le dossier `./work/` (pour les ordinateurs de l'IUT : `/Docker/<login>/work/`). Une fois le script terminé le catalogue est disponnible au format PDF dans le dossier `./work/` (pour les ordinateurs de l'IUT : `/Docker/<login>/work/`)