#!/usr/bin/bash

alias docker='__docker'
if [ -z "$1" ]
then
    read -p "Donner votre login : " login
else
login=$1
fi

# il faut créer un dossier work si il existe pas
mkdir -p /Docker/$login/work/

echo "Copie des fichier builder.php et css dans le dossier work"
# on copie builder.css et .php dans le dossier work pour qu'il soit dans le volume, c'est pas ouf il faudrai faire un autre volume entre script et le work du conteneur php mais jsp si c'est bien donc flemme
cp builder.php /Docker/$login/work/
cp builder.css /Docker/$login/work/

echo "Le script builder se lance dans son conteneur"
# on lance le conteneur php avec le script builder.php
docker container run --rm --name sae4-php -v /Docker/$login/work/:/work/ sae4-php php -f builder.php

echo "Attente de la génération du html"
# il faut attendre le html dans le dossier work
while ! test -f "/Docker/$login/work/builded.html"; do
    sleep 0.1
done

echo "La génération du html est finie, lancement de la génératione en PDF"
# on lance le conteneur html2pdf sur le html généré
docker container run --rm --name sae4-html2pdf-with-proxy -v /Docker/$login/work/:/work/ sae4-html2pdf-with-proxy "html2pdf builded.html catalogue.pdf"

echo  "Suppression des fichiers de génération"
rm -f /Docker/$login/work/builder.php /Docker/$login/work/builder.css /Docker/$login/work/builded.html

echo "Le catalogue de produits a été généré"