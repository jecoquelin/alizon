#!/usr/bin/bash

# il faut créer un dossier work si il existe pas
mkdir -p ../work/

echo "Copie des fichier builder.php et css dans le dossier work"
# on copie builder.css et .php dans le dossier work pour qu'il soit dans le volume, c'est pas ouf il faudrai faire un autre volume entre script et work mais jsp si c'est bien donc flemme
cp builder.php ../work/
cp builder.css ../work/

echo "Le script builder se lance dans son conteneur"
# on lance le conteneur php avec le script builder.php
sudo docker container run --rm --name sae4-php -v /home/tristan/tristan/alizon/catalogueur/work/:/work/ bigpapoo/sae4-php php -f builder.php

echo "Attente de la génération du html"
# il faut attendre le html dans le dossier work
while ! test -f "../work/builded.html"; do
    sleep 0.1
done

echo "La génération du html est finie, lancement de la génératione en PDF"
# on lance le conteneur html2pdf sur le html généré
sudo docker container run --rm --name sae4-html2pdf -v /home/tristan/tristan/alizon/catalogueur/work/:/work/ bigpapoo/sae4-html2pdf "html2pdf builded.html catalogue.pdf"

echo  "Suppression des fichiers de génération"
rm -f ../work/builder.php ../work/builder.css ../work/builded.html

echo "Le catalogue de produits a été généré"