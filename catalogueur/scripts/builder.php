<?php
function getDesc250($desc){
    // cette fonction prend un description de produit desc et la formatate de manière à ce que si elle dépasse 250 caractère elle soit raccourcie et qu'elle se termine par ...
    $desc250 = "";
    if (strlen($desc) > 250){
        $desc250 = substr($desc, 0, 250);
        $debutDernierMot = strrpos($desc250, ' ') + 1;
        $desc250 = substr($desc250, 0, $debutDernierMot) . " ...";
    } else {
        $desc250 = $desc;
    }
    return $desc250;
}


// il faut choper les options en paramtre

// il faut se mettre en attente d'un fichier json dans le dossier work
$jsonPasLa = true;
while ($jsonPasLa) {
    if (is_file("/work/catalogue.json")) {
        $jsonPasLa = false;
    }
}

ob_start();

echo '<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Catalogue de Produits</title>
    <link rel="stylesheet" href="builder.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</head>
<body class="container-fluid">';
    // récupération du json et transformation en tableau
    $catalogueJson = file_get_contents("/work/catalogue.json");
    $catalogue = json_decode($catalogueJson, true);
    
    echo '<section>';
    
    echo '<div class="row mt-3 justify-content-center">
    <h1>Catalogue de Produits Alizon</h1>
    </div>
    <br>';
    // Vendeurs
    // on parcours les vendeurs du catalogue
    $premierePage = true;
    foreach ($catalogue["vendeurs"] as $vendeur) {
        // un vendeur
        if ($premierePage == false){
            echo '<section>';
        } else {
            $premierePage = false;
        }
        echo '<div class="col-12 py-3">';
            // nom vendeur
            echo '<div class="row mt-3 justify-content-center">';
                echo '<div class="col-auto nomVendeur">';
                    echo '<h2>' . $vendeur["nom"] . '</h2>';
                echo '</div>';
            echo '</div>';
            // img vendeur
            echo '<div class="row banner">';
                echo '<figure>';
                    echo '<img class= "imgBanner" src="' . $vendeur["image"] . '" alt="' . $vendeur["nom"] . '">';
                echo '</figure>';
            echo '</div>';
            // description vendeur
            echo '<div class="row description">';
                echo '<p>' . $vendeur["presentation"] . '</p>';
            echo '</div>';
            // infos vendeur
            echo '<div class="row infos">';
                echo '<div class="col">';
                    echo '<ul>';
                        echo '<li>Raison sociale : ' . $vendeur["raison social"] . '</li>';
                        echo '<li>Adresse mail : ' . $vendeur["contact"] . '</li>';
                        echo '<li>Adresse siège social : ' . $vendeur["numero voie"] . ' ' . $vendeur["nom voie"] . ' ' . $vendeur["code postal"] . ' ' . $vendeur["ville"] . '</li>';
                        echo '<li>TVA Intracommunautaire : ' . $vendeur["code tva"] . ' ' . $vendeur["cle tva"] . ' ' . $vendeur["siren"] . '</li>';
                        echo '<li>Siret : ' . $vendeur["siren"] . ' ' . $vendeur["nic"] . '</li>';
                        echo '<li>Note : ' . $vendeur["note"] . '/5</li>';
                    echo '</ul>';
                echo '</div>';
            echo '</div>';
            // Sous-Titre Produits du vendeur
            echo '<div class="row justify-content-center">';
                echo '<div class="col-auto">';
                    echo '<h3>Produits du vendeur</h3>';
                echo '</div>';
            echo '</div>';

            echo '<div class="row justify-content-center justify-content-around">';
            // on parcours les produits des vendeurs
            $changementDePage = false;
            $nbProduct = 0;
            $produitsCollesVendeur = true;
            foreach ($vendeur["produits"] as $produit) {
                if ($changementDePage == true){
                    echo '<section>';
                    // alignement/centrage des produits
                    echo '<div class="row deuxProduits justify-content-center justify-content-around">';
                }
                // Un produit
                echo '<div class="produit col-5">';
                    // Nom Produit
                    echo '<div class="row justify-content-center">';
                        echo '<div class="col-auto">';
                            echo '<h4>' . $produit["nom"] . '</h4>';
                        echo '</div>';
                    echo '</div>';
                    // Img Produit
                    echo '<div class="row justify-content-center">';
                        echo '<div class="col-auto">';
                            echo '<figure>';
                                echo '<img src="' . $produit["image"] . '" alt="' . $produit["nom"] . '">';
                            echo '</figure>';
                        echo '</div>';
                    echo '</div>';
                    // Description Produit
                    echo '<div class="row justify-content-center">';
                        echo '<div class="col-auto descProd">';
                            echo '<p>' . getDesc250($produit["description"]) . '</p>';
                            echo '<p>Prix HT : ' . $produit["prix ht"] . '€</p>';
                            echo '<p>Prix TTC : ' . $produit["prix ttc"] . '€</p>';
                        echo '</div>';
                    echo '</div>';
                echo '</div>';

                $nbProduct = $nbProduct + 1;

                if ($produitsCollesVendeur == true) {
                    if ($nbProduct == 2 && sizeof($vendeur["produits"]) >= 2){
                        $changementDePage = true;
                        $produitsCollesVendeur = false;
                    } else if ($nbProduct == 1 && sizeof($vendeur["produits"]) == 1) {
                        $changementDePage = true;
                    } else {
                        $changementDePage = false;
                    }
                } else {
                    if ($nbProduct == 4) {
                        $changementDePage = true;
                    } else {
                        $changementDePage = false;
                    }
                }

                if ($changementDePage == true){
                    $nbProduct = 0;
                    echo '</div>'; // un truc jsp c'est quoi
                    echo '</div>'; // on ferme la div de ligne de produit sur une page
                    echo '</section>'; // on saute la fin de la page
                }
                
            }
        // si on a pas sauté de page avant ça veut dire qu'il n'y a pas assez de produit pour atteindre la fin de la page donc on saute une page
        if ($changementDePage == false){
            echo '</div>'; // fermeture de la div de la ligne de produits sur une page
            echo '</section>'; // on saute la fin de la page
        }
    }
echo '</body>
</html>';

file_put_contents('/work/builded.html', ob_get_clean());
?>